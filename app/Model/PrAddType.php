<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PrAddType extends Model
{
    protected $table = 'pr_add_types';
}
