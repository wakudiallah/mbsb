<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LoanAmmount  extends Model
{
   
   protected $table = 'loanammount';  


  public function Tenure() {
		return $this->belongsTo('App\Model\Tenure','id_tenure','id');	
	}
    public function term()
    {
        return $this->hasOne('App\Model\Term','id_praapplication');	
    }

   public function Package()
    {
        return $this->hasOne('App\Model\Package','package_id','id');
    }

}
