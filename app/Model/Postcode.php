<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Postcode extends Model
{
   
   protected $table = 'param_postcode';


	public function state() {
		return $this->belongsTo('App\Model\State','state_code','state_code');	
		//Postcode dipunyai city
	}
}
