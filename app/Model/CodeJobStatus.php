<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CodeJobStatus  extends Model
{
   protected $table = 'codejobstatus';  
}
