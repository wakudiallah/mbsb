<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AntiAttrition extends Model

{
    protected $table = 'anti_attritions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'mo_id', 'assign_by', 'id_clrt', 'TaskID','ACID',
            'CustIDNo',
            'Tenor',
            'Year',
            'SalIncrementPercent',
            'NewSalary',
            'NewAllowDeduct',
            'IntRate',
            'VarianceRate',
            'NewLoanOffer',
            'PayoutPercent',
            'NetProceed',
            'MonthlyInst',
            'NewDSR',
            'ProfitToEarn',
            'CalDt',
            'CustWrkSec',
            'CustName',
            'Assign',
            'CustDob',
            'State',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
  

    public function Settlement()
    {
        return $this->hasOne('App\Model\SettlementInfo','ACID','ACID');   
    }

    public function CustInfo()
    {
        return $this->hasOne('App\Model\CustomerInfo','Acid','ACID');   
    }

    public function User()
    {
        return $this->hasOne('App\User','id','assign_by');   
    }

     public function MO()
    {
        return $this->hasOne('App\MO','id_mo','mo_id');   
    }


    public function SettInfo()
    {
        return $this->hasOne('App\Model\SettlementInfo','ACID2','ACID');   
    }

    public function Address()
    {
        return $this->hasOne('App\Model\Address','Acid','ACID');   
    }

    public function Phone()
    {
        return $this->hasOne('App\Model\Address','Acid','ACID')->wherein('AddType',['2','1']);   
    }

     public function States()
    {
        return $this->hasOne('App\Model\State2','clrt_name','State');   
    }

     public function Basic()
    {
        return $this->hasOne('App\Model\Basic','id_praapplication','id_praapplication');   
    }

}
