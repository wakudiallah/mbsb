<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CustomerInfo extends Model
{
     protected $table = 'customer_infos';  
}
