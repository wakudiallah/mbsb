<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{
	protected $table = 'employments';  

 protected $hidden = ['created_at', 'updated_at','create_by'];
	 public function employer()
    {
        return $this->hasMany('App\Model\Employer');	
    }

    public function Package()
    {
        return $this->hasOne('App\Model\Package','id','package_id'); 
    }

    
}
