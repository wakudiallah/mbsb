<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PraApplication extends Model
{
     protected $table = 'praapplication';

 public $incrementing = false; 

     public function package() {
		return $this->belongsTo('App\Model\Package','id_package','id');	
	}

	public function employer() {
		return $this->belongsTo('App\Model\Employer','id_employer');	
	}
	
	 public function term()
    {
        return $this->hasOne('App\Model\Term','id_praapplication');	
    }

     public function empinfo()
    {
        return $this->hasOne('App\Model\Empinfo','id_praapplication');	
    }


    public function user()
    {
        return $this->hasOne('App\Model\User','email', 'email');  
    }


     public function loanAmmount()
    {
        return $this->hasOne('App\Model\LoanAmmount', 'id_praapplication');  
    }

     public function Basic() {
        return $this->belongsTo('App\Model\Basic','id','id_praapplication'); 
    }

     




}
