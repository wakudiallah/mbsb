<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
     
	 protected $table = 'loan';
       protected $hidden = ['created_at', 'updated_at','create_by'];

public function type() {
		return $this->belongsTo('App\Model\Package','id_type');	
	}
}
