<?php

namespace App\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $table = 'users';
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function Branch() {
		return $this->belongsTo('App\Model\Branch','id_branch','branch_code');	
	}

     public function MarOfficer() {
        return $this->belongsTo('App\MarketingOfficer','id','id');   
    }

     public function Manager() {
        return $this->belongsTo('App\Manager','id','id');   
    }
}
