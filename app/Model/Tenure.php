<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tenure extends Model
{
     
	 protected $table = 'tenure';
       protected $hidden = ['created_at', 'updated_at','create_by'];



public function loan() {
		return $this->belongsTo('App\Model\Loan','id_loan');	
	}
}
