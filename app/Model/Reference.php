<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Reference  extends Model
{
   
   protected $table = 'reference';  

   public function Relationship1()
    {
        return $this->hasOne('App\Model\Relationship','relationship_code','relationship1');	
    }

    public function Relationship2()
    {
        return $this->hasOne('App\Model\Relationship','relationship_code','relationship2');	
    }



}
