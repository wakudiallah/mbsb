<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MO extends Model
{
     protected $table = 'mo'; 
     

   
    
    public function User() {
        return $this->hasOne('App\Model\User','id');	
	}

	public function Manager() {
        return $this->hasOne('App\Manager','id','id_manager');	
	}

	 public function Branch()
    {
        return $this->hasOne('App\Model\Branch','branch_code','branch');  
    }
	
}
