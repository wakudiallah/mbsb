<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Model\Employer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Model\Employment;

class EmployerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       

          $user = Auth::user();
          $employment = Employment::get();
                if ($user->role<>1) {
                    return redirect('admin');   
                }
                $employer = Employer::with('employment')->where('id','!=','5')->get();
                return view('admin.employer', compact('employer','user','employment'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
                $employer = new Employer ;
                $name  = $request->input('Name');
                $type  = $request->input('type');

               
                  $id = Auth::user()->id;
             
              
                      $employer->name   =   $request->input('name');
                      $employer->employment_id   =  $request->input('type');
                      $employer->create_by   =  $id;
                   
                      $employer->save();
                
                    return redirect('employer')->with('message', 'insert data success');
                

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Employer = Employer::where('employment_id',$id)->orderBy('name', 'asc')->with('employment')->get();
       return $Employer;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function update2(Request $request)
    {
         $id = $request->input('id_employer');
        $employer = Employer::find($id);
        $employer->name = $request->input('name');
        $employer->employment_id =  $request->input('type');
       
        $employer->save();
        
        return redirect('employer')->with('message', 'update success');
    }
    
    public function delete(Request $request) {
      $id = $request->input('id_employer');
      $Employer = Employer::where('id',$id)->delete();
      //echo $id;
     return redirect('employer')->with('message', 'Delete success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
