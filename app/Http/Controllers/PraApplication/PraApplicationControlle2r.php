<?php



namespace App\Http\Controllers\PraApplication;



use Illuminate\Http\Request;

use Illuminate\Http\Response;

use App\Http\Requests;

use App\Model\Employment;

use App\Model\PraApplication;

use App\Model\Loan;

use App\Model\Package;

use App\Model\Basic;

use App\Model\Contact;

use App\Model\Tenure;

use App\Model\Empinfo;

use App\Model\Document;

use App\Model\LoanAmmount;

use App\Model\Spouse;

use App\Model\Reference;

use App\Model\Financial;

use App\Model\Term;

use App\Model\Blocked_ic;

use App\Http\Controllers\Controller;

use Input;

use DateTime;

use Ramsey\Uuid\Uuid;

use App\Model\User;

use Auth;

use  Session;

use App\Dsr_a;

use App\Dsr_b;

use App\Dsr_c;

use App\Dsr_d;

use App\Dsr_e;

use App\RiskRating;

use Artisaninweb\SoapWrapper\SoapWrapper;

use App\Soap\Request\MOMBsc;

use App\Soap\Response\MOMBscResponse;

use App\Soap\Request\MOMRef;

use App\Soap\Response\MOMRefResponse;

use App\Soap\Response\MOMEmpInfResponse;

use App\Soap\Response\MOMSpResponse;

use App\Soap\Response\MOMCommitResponse;

use App\Soap\Response\MOMCallBscResponse;

use App\Soap\Response\MOMDocResponse;

use App\Soap\Response\MOMCTransResponse;

use App\Soap\Response\MOMFinResponse;

use App\Soap\Response\MOMDSRAResponse;

use App\Soap\Response\MOMDSRBResponse;

use App\Soap\Response\MOMDSRCResponse;

use App\Soap\Response\MOMDSREResponse;



use App\Soap\Response\MOMLnResponse;

use App\Soap\Response\MOMRskResponse;

use App\Soap\Response\MOMTrmResponse;

use App\Soap\Request\GetConversionAmount;

use App\Soap\Response\GetConversionAmountResponse;

use SOAPHeader;

use SoapClient;

use App\Model\Waps;
use App\Model\CodeIDType;
use App\Model\CodeJobStatus;
use App\Model\CodeWorkSec;
use App\Model\Background;
use App\Model\Foreigner;
use App\Model\Subscription;
use Alert;
class PraApplicationController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

     protected $soapWrapper;



  /**

   * SoapController constructor.

   *

   * @param SoapWrapper $soapWrapper

   */

  public function __construct(SoapWrapper $soapWrapper)

  {

    $this->soapWrapper = $soapWrapper;

  }



    public function index()

    {
        $employment = Employment::get();
        $package    = Package::all();
        $idtype    = CodeIDType::get();
        $background = Background::all();
        return view('praapplication.index',compact('employment','idtype','package','background'));
      
    }

  

  public function applynow()
    {
        $user = Auth::user();
        $employment = Employment::get();
        $package = Package::all();
        $idtype    = CodeIDType::all();
        return view('praapplication.applynow',compact('employment','idtype','package','background'));
      
    }





    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    { 

        $praapplication = new PraApplication;
        $basic         = new Basic;
        $contact       = new Contact;
        $empinfo       = new Empinfo;
        $loanammount   = new LoanAmmount;
        $spouse        = new Spouse;
        $document      = new Document;
        $reference     = new Reference;
        $financial     = new Financial;
        $term          = new Term;
        $dsr_a         = new Dsr_a;
        $dsr_b         = new Dsr_b;
        $dsr_c         = new Dsr_c;
        $dsr_d         = new Dsr_d;
        $dsr_e         = new Dsr_e;
        $riskrating    = new RiskRating;
        $waps          = new Waps;
         $foreigner = new Foreigner;
        $subscription = new Subscription;

        $type = $request->input('type');

        if($type=='IN')
            {
                $icnumber = $request->input('ICNumber');
                 $new_ic = $request->input('ICNumber');
            }
        else{
            $icnumber = $request->input('dob');
            $other = $request->input('other');
            $new_ic = $request->input('other');
        }

        //$icnumber = $request->input('ICNumber');

        $employment  = $request->input('Employment');
        $employment2 = $request->input('Employment2');
        $basicsalary = $request->input('BasicSalary');
        $fullname    = $request->input('FullName');
        $phone       = $request->input('PhoneNumber');
        $employer    = $request->input('Employer');
        $employer2   = $request->input('Employer2');
        $allowance   = $request->input('Allowance');
        $deduction   = $request->input('Deduction');  
        $majikan     = $request->input('majikan');
        $loanAmount  = $request->input('LoanAmount');
        //$basicsalary = number_format_drop_zero_decimals($basicsalary);

        $batas      =  $allowance  +  $basicsalary;

        if($employment==1) {
            $package=1;
            $package_name ="Mumtaz-i";
        }
        else if($employment==2) {
            $package=2; 
            $package_name = "Afdhal-i";
        }
        else if($employment==3) {
            $package=2; 
            $package_name = "Afdhal-i";
        }
        else if($employment==4) {
            $package=3; 
            $package_name = "Private Sector PF-i";
        }
        else if($employment==5) {
            $package=2; 
            $package_name = "Afdhal-i";
        }

        $blocked_ic = Blocked_ic::latest('created_at')->where('ic',  $icnumber )->count();
        if($type=='IN')
            {
                $tanggal = substr($icnumber,4, 2);
                $bulan =  substr($icnumber,2, 2);
                $tahun = substr($icnumber,0, 2); 
                $jantina = substr($icnumber,10, 2); 
            }
        else{
                $genders = $request->input('gender');
                $tanggal = substr($icnumber,0, 2);
                $bulan =  substr($icnumber,3, 2);
                $tahun = substr($icnumber,8, 2); 
                $jantina = $genders; 
        }

        if($tahun > 30) {
            $tahun2 = "19".$tahun;
        }
        else {
            $tahun2 = "20".$tahun;
        }
         if($bulan >= 13)
        {
            if($type=='IN'){
                Session::flash('fullname', $fullname); 
                Session::flash('icnumber', $new_ic);
                Session::flash('phone', $phone); 
                Session::flash('majikan', $majikan); 
                Session::flash('basicsalary', $basicsalary);  
                Session::flash('allowance', $allowance);
                Session::flash('deduction', $deduction);
                Session::flash('loanAmount', $loanAmount); 
                //Session::flash('contact_via', $contact_via);
                Alert::error('Please Enter a valid New IC No.');
                return redirect('/applynow');
            }
            else{
                Session::flash('fullname', $fullname); 
                Session::flash('icnumber', $new_ic);
                Session::flash('phone', $phone); 
                Session::flash('majikan', $majikan); 
                Session::flash('basicsalary', $basicsalary);  
                Session::flash('allowance', $allowance);
                Session::flash('deduction', $deduction);
                Session::flash('loanAmount', $loanAmount); 
                //Session::flash('contact_via', $contact_via);
                 Alert::error('Please Enter a valid Date of Birth (dd/mm/yyyy) ');
                return redirect('/applynow');
            }
        }

        elseif($tanggal >= 32) 
        {
            if($type=='IN'){
                Session::flash('fullname', $fullname);
                //Session::flash('icnumber', $icnumber);
                Session::flash('phone', $phone); 
                Session::flash('majikan', $majikan); 
                Session::flash('basicsalary', $basicsalary);  
                Session::flash('allowance', $allowance);
                Session::flash('deduction', $deduction);
                Session::flash('loanAmount', $loanAmount); 
                //Session::flash('contact_via', $contact_via);

                 Alert::error('Please Enter a valid New IC No.');
                return redirect('/applynow');
            }

             else{
                Session::flash('fullname', $fullname); 
                Session::flash('icnumber', $new_ic);
                Session::flash('phone', $phone); 
                Session::flash('majikan', $majikan); 
                Session::flash('basicsalary', $basicsalary);  
                Session::flash('allowance', $allowance);
                Session::flash('deduction', $deduction);
                Session::flash('loanAmount', $loanAmount); 
                //Session::flash('contact_via', $contact_via);
                 Alert::error('Please Enter a valid Date of Birth (dd/mm/yyyy) ');
                return redirect('/applynow');
            }
        }

         if($jantina % 2 ==0 )
            {
                $gender =  "F" ;
            }
            else
            {
                $gender =  "M" ;

            }

       

        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month')); 

        $oDateNow   = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);
        $umur =  $oDateIntervall->y;

        $dobirth = $tahun2.'-'.$bulan.'-'.$tanggal;
        $today = date("Y-m-d");
        $diff = date_diff(date_create($dobirth), date_create($today));
        $usia = $diff->format('%y');

        if($usia >= 60) {
            Session::flash('fullname', $fullname); 
            Session::flash('icnumber', $new_ic);
            Session::flash('phone', $phone); 
            Session::flash('basicsalary', $basicsalary); 
            Session::flash('allowance', $allowance);
            Session::flash('deduction', $deduction);
            Session::flash('loanAmount', $loanAmount);
            Session::flash('employer2', $employer2);
            Session::flash('employment', $employment);
            Session::flash('employment2', $employment2);
            Session::flash('majikan', $majikan);
            Session::flash('package_name', $package_name);
            Session::flash('icnumber_error', $icnumber); 
            Alert::error('Sorry, exceed the age limit.');
            return redirect('/applynow');
        }
        else {

            // Batas Gaji

            if($employment == 1  )  { //kerajaan permanent
                $had =  3000;
            }
            else if($employment == 2  )  { //kerajaan kontrak
                $had =  3000;  
            }
            else if ($employment == 3  )  { //swasta with sallary deduction
               $had =  3500;  
            } 
            else if ($employment == 4  )  { //swasta without sallary deduction
               $had =  3500;  
            } 
             else if ($employment == 5  )  { //premium 15K
               $had =  15001;  
            } 

            if(  $batas >= $had ) {
                $totalsalary = $basicsalary + $allowance ;
                $id = Uuid::uuid4()->getHex(); 
                $praapplication->id       = $id;
                $praapplication->fullname = $fullname;
                $praapplication->icnumber = $new_ic;
                $praapplication->phone    = $phone;

                if(!empty($employer)) { 
                    $praapplication->id_employer = $employer;
                    if($employment == 1  )  {     //kerajaan permanent
                        $sector = "Government";
                    }
                    else if ($employment == 2  )  {  //kerajaan kontrak   
                        $sector = "Government";  
                    }
                    else if ($employment == 3  )  {    //swasta with sallary deduction
                        $sector = "Private";
                    }
                    else if ($employment == 4  )  {    //swasta without sallary deduction
                        $sector = "Private";  
                    }
                    else if ($employment == 5  )  {    //premium 15k
                        $sector = "Private";   
                    }
                }
                else {
                    if($employment == 1  )  {     //kerajaan permanent
                        $praapplication->id_employer = 10;   
                        $sector = "Government";
                    }
                    else if ($employment == 2  )  {  //kerajaan kontrak   
                        $praapplication->id_employer = 12; 
                        $sector = "Government";  
                    }
                    else if ($employment == 3  )  {    //swasta with sallary deduction
                        $praapplication->id_employer = 5;   
                        $sector = "Private";
                    }

                    else if ($employment == 4  )  {    //swasta without sallary deduction
                        $praapplication->id_employer = 14; 
                        $sector = "Private";  
                    }
                    else if ($employment == 5  )  {    //premium 15k
                        $praapplication->id_employer = 15;
                        $sector = "Private";   
                    }
                }



                $praapplication->basicsalary = $basicsalary;
                $praapplication->allowance = $allowance;
                $praapplication->deduction = $deduction;
                $praapplication->id_package = $package;
                $praapplication->loanAmount = $loanAmount;
                $praapplication->majikan = $majikan;
                $praapplication->save();
                
                if($type=='IN')
                {
                    $icnumbers= $request->input('ICNumber');
                }
                else{
                     $icnumbers= $request->input('dob');
                }


                if($type=='IN')
                {
                    $tanggal = substr($icnumbers,4, 2);
                    $bulan =  substr($icnumbers,2, 2);
                    $tahun = substr($icnumbers,0, 2); 
                    $jantina = substr($icnumbers,10, 2); 
                }
                else{
                    $genders = $request->input('gender');
                    $tanggal = substr($icnumbers,0, 2);
                    $bulan =  substr($icnumbers,3, 2);
                    $tahun = substr($icnumbers,8, 2); 
                    $jantina = $genders; 
                }  
                if($tahun > 30) {
                    $tahun2 = "19".$tahun;
                }
                else {
                     $tahun2 = "20".$tahun;
                }
                $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;     

                $basic->id_praapplication   =  $id;
                $basic->new_ic = $new_ic;
                $basic->name =  $fullname ;
                $basic->dob =  $lahir ; 
                $basic->gender =  $gender ; 
                $basic->corres_mobilephone = $phone;
                $basic->gender =  $gender ; 
                $basic->age =  $usia ;
                $basic->id_type =  $request->input('type') ; 
                $basic->save();

                $contact->id_praapplication   =  $id;
                $contact->handphone   =  $phone;
                $contact->save();

                $empinfo->id_praapplication   =  $id;
                if(!empty($employer)) {
                    $empinfo->employer_id = $employer; 
                }
                else {
                    $empinfo->employer_id = 5;
                }
                if($employment  != '7' ) {
                    $empinfo->empname = $majikan;
                }

                $empinfo->employment_id = $employment;
                $empinfo->salary = $basicsalary;
                $empinfo->allowance = $allowance;
                $empinfo->deduction = $deduction;
                $empinfo->save();

                $loanammount->id_praapplication   =  $id;
                $loanammount->loanammount   =  $loanAmount;
                $loanammount->save();

                $spouse->id_praapplication   =  $id;
                $spouse->save();

                $financial->id_praapplication   =  $id;
                $financial->save();

                $reference->id_praapplication   =  $id;
                $reference->save();

                $term->id_praapplication        =  $id;
                $term->status                   =  '0';
                $term->verification_status      =  '0';
                $term->verification_result      =  '0';
                $term->mo_stage                 =  '0';
                $term->verification_result_by_bank  =  '0';
                $term->id_branch                =  '0';
                $term->status_waps                =  '0';
                $term->save();

                //DSR Calculation

                $dsr_a->id_praapplication   =  $id;
                $dsr_a->sector   =  $sector;
                $dsr_a->age   =  $usia;
                $dsr_a->save();

                $dsr_b->id_praapplication   =  $id;
                $dsr_b->save();

                $dsr_c->id_praapplication   =  $id;
                $dsr_c->save();

                $dsr_d->id_praapplication   =  $id;
                $dsr_d->save();

                $dsr_e->id_praapplication   =  $id;
                $dsr_e->save();

                $riskrating->id_praapplication   =  $id;
                $riskrating->save();

                $foreigner->id_praapplication   =  $id;
                $foreigner->save();

                $subscription->id_praapplication   =  $id;
                $subscription->save();

                /*$waps->id_praapplication   =  $id;
                $waps->ic = $icnumber;
                $waps->name =  $fullname ;
                $waps->save();*/
                // Proceed

                $pra = PraApplication::where('id',$id)->limit('1')->first();

                $id_type= $pra->employer->employment->id;
                $total_salary = $pra->basicsalary + $pra->allowance ;
                $zbasicsalary = $pra->basicsalary + $pra->allowance;
                $zdeduction =  $pra->deduction ;

                $loan = Loan::where('id_type',$id_type)
                        ->where('min_salary','<=',$total_salary)
                        ->where('max_salary','>=',$total_salary)->limit('1')->get();

                // return $total_salary;

                $id_loan= $loan->first()->id; 
                $salary_dsr = ($zbasicsalary * ($loan->first()->dsr / 100)) - $zdeduction;


            if($type=='IN')
            {
                $icnumberx= $pra->first()->icnumber;
            }
            else{
                 $icnumberx= $pra->first()->dob;
            }

            if($type=='IN')
            {
                $tanggal = substr($icnumberx,4, 2);
                $bulan =  substr($icnumberx,2, 2);
                $tahun = substr($icnumberx,0, 2); 
                $jantina = substr($icnumberx,10, 2); 
            }
            else
            {
                $genders = $request->input('gender');
                $tanggal = substr($icnumber,0, 2);
                $bulan =  substr($icnumber,3, 2);
                $tahun = substr($icnumber,8, 2); 
                $jantina = $genders; 
            } 
                if($tahun > 30) {
                    $tahun2 = "19".$tahun;
                }
                else {
                    $tahun2 = "20".$tahun;
                }


                $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 
                $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
                $oDateNow = new DateTime();
                $oDateBirth = new DateTime($lahir);
                $oDateIntervall = $oDateNow->diff($oDateBirth);

                $umur = 61 - $oDateIntervall->y;
                $durasix = 60 - $oDateIntervall->y;
                if( $durasix  > 10) { 
                    $durasi = 10 ;
                } 
                else { 
                    $durasi = $durasix ;
                }

                function pembulatan($uang) {
                    $puluhan = substr($uang, -3);
                        if($puluhan<500) {
                            $akhir = $uang - $puluhan; 
                        } 
                        else {
                            $akhir = $uang - $puluhan;
                        }
                    return $akhir;
                }

                foreach($loan as $loan) {
                    $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
                    $ndi = ($zbasicsalary - $zdeduction) -  1300;
                    $max  =  $salary_dsr * 12 * 10 ;

                        if(!empty($loan->max_byammount))  {
                            $ansuran = intval($salary_dsr)-1;
                                if($pra->package->id=="1") {
                                //$a= $pra->package->effective_rate;
                                //$bunga = ((($a/12)+0.025) *10)/100;
                                    $bunga = 3.7/100;
                                   //$bunga = $pra->package->effective_rate / 100;
                                }
                                elseif($pra->package->id=="2") {
                                //$a= $pra->package->effective_rate;
                                //$bungas = ((($a/12)+0.025) *10)/100;
                                    $bunga = 4.45/100;
                                     //$bunga = $pra->package->effective_rate / 100;
                                }
                                else {
                                //$a= $pra->package->effective_rate;
                                //$bungas = ((($a/12)+0.025) *10)/100;
                                    $bunga = 7.45/100;
                                     //$bunga = $pra->package->effective_rate / 100;
                                }
                            $pinjaman = 0;

                            for ($i = 0; $i <= $loan->max_byammount; $i++) {
                                $bungapinjaman = $i  * $bunga * $durasi ;
                                $totalpinjaman = $i + $bungapinjaman ;
                                $durasitahun = $durasi * 12;
                                $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;

                                if ($ansuran2 < $ndi){
                                    $pinjaman = $i;
                                }
                            } 

                            if($pinjaman > 1) {
                                $bulat = pembulatan($pinjaman);
                                $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                                $loanz = $bulat;
                            }
                            else {
                                $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
                                $loanz = $loan->max_byammount;
                            }
                        }
                        else 
                        { 
                            $bulat = pembulatan($loan->max_bysalary * $total_salary);
                            $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                            $loanz = $bulat;
                            if ($loanz > 199000) {
                                $loanz  = 250000;
                                $loanx =  number_format($loanz, 0 , ',' , ',' ) ; 
                            }
                        }

                }

                $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get();  

                if( $pra->first()->loanamount <= $loanz ) {
                    $ndi_limit=$loan->ndi_limit;
                    foreach($tenure as $tenure) {
                        $bunga2 =  $pra->first()->loanamount * $tenure->rate /100   ;
                        $bunga = $bunga2 * $tenure->years;
                        $total = $pra->first()->loanamount + $bunga ;
                        $bulan = $tenure->years * 12 ;
                        $installment =  $total / $bulan ;
                        $ndi_state = ($total_salary - $zdeduction) - $installment; 
                        $count_installment=0;

                        if($installment  <= $salary_dsr && $ndi_state>=$ndi_limit) {
                            $count_installment++;
                        }
                    }
                } 
                else {
                    $count_installment=0;
                }



                if($count_installment>0) {
                    return redirect('praapplication/'.$id)->with('message', 'Congratulations, maximum loan eligibility up to RM '.$loanx);

                } 

                else {

                    $had2 = number_format($had, 0 , ',' , ',' ).'.00' ; 

                    Session::flash('fullname', $fullname); 
                    Session::flash('icnumber', $new_ic);
                    Session::flash('phone', $phone); 
                    Session::flash('basicsalary', $basicsalary); 
                    Session::flash('allowance', $allowance);
                    Session::flash('deduction', $deduction);
                    Session::flash('loanAmount', $loanAmount);
                    Session::flash('employer2', $employer2);
                    Session::flash('employment', $employment);
                    Session::flash('employment2', $employment2);
                    Session::flash('package_name', $package_name);
                    Session::flash('majikan', $majikan);
                    Session::flash('hadpotongan', $basicsalary); 
                    Alert::error('Sorry, you are not qualified. Exceed deduction or financing limit!');
                    return redirect('/applynow');
                }
            }
            else {
                $had2 = number_format($had, 0 , ',' , ',' ).'.00' ; 

                Session::flash('fullname', $fullname); 
                Session::flash('icnumber', $new_ic);
                Session::flash('phone', $phone); 
                Session::flash('basicsalary', $basicsalary); 
                Session::flash('allowance', $allowance);
                Session::flash('deduction', $deduction);
                Session::flash('loanAmount', $loanAmount);
                Session::flash('employer2', $employer2);
                Session::flash('employment', $employment);
                Session::flash('employment2', $employment2);
                Session::flash('package_name', $package_name);
                Session::flash('majikan', $majikan);
                 return redirect('/applynow')->with('message', "Minimum monthly income of RM  $had2 ");
            }
        }
    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        $pra = PraApplication::where('id',$id)->limit('1')->get();

        $id_type= $pra->first()->employer->employment->id;

        $total_salary = $pra->first()->basicsalary + $pra->first()->allowance ;

        $zbasicsalary = $pra->first()->basicsalary + $pra->first()->allowance;

        $zdeduction =  $pra->first()->deduction ;



        $loan = Loan::where('id_type',$id_type)

                ->where('min_salary','<=',$total_salary)

                ->where('max_salary','>=',$total_salary)->limit('1')->get();



        $id_loan= $loan->first()->id;  



        $icnumber= $pra->first()->icnumber;

        $tanggal = substr($icnumber,4, 2);

        $bulan =  substr($icnumber,2, 2);

        $tahun = substr($icnumber,0, 2); 



        if($tahun > 30) {



            $tahun2 = "19".$tahun;

        }

        else {

             $tahun2 = "20".$tahun;



        }

       

        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 

        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));

        $oDateNow = new DateTime();

        $oDateBirth = new DateTime($lahir);

        $oDateIntervall = $oDateNow->diff($oDateBirth);



        $umur = 61 - $oDateIntervall->y;



         $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->where('years','!=','1')->get();   

         //  return $tenure;

    

         return view('praapplication.result', compact('pra','loan','total_salary','tenure','id','zbasicsalary' ,'zdeduction','amount'));

       

    }

        



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $pra = PraApplication::find($id);

        $loanammount = $request->input('LoanAmount2');

        $pra->loanammount = $loanammount;

        $pra->save();

        $today = date('Y-m-d H:i:s');

        $LoanAmmount = LoanAmmount::where('id_praapplication', $id)->update(['loanammount' => $loanammount]);

         $pra = PraApplication::where('id_praapplication', $id)->update(['date_registration' => $today]);

   

        return redirect('praapplication/'.$id)->with('message', 'Sila Pilih Tempoh Pembiayaan Anda');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        //

    }

    

    public function update2(Request $request, $id)

    {

        $pra = PraApplication::find($id);

        $loanammount = $request->input('LoanAmount2');

        $pra->loanamount = $loanammount;

        $pra->save();



        $LoanAmmount = LoanAmmount::where('id_praapplication', $id)->update(['loanammount' => $loanammount]);



        return redirect('praapplication/'.$id)->with('message', 'Sila Pilih Tempoh Pembiayaan Anda');

    }



    public function updatemo(Request $request, $id)

    {

        $pra = PraApplication::find($id);

        $loanammount = $request->input('LoanAmount2');

        $pra->loanamount = $loanammount;

        $pra->save();



        $LoanAmmount = LoanAmmount::where('id_praapplication', $id)->update(['loanammount' => $loanammount]);



        return redirect('moapplication/'.$id)->with('message', 'Sila Pilih Tempoh Pembiayaan Anda');

    }

}

