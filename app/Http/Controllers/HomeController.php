<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Model\User;
use App\Model\Basic;
use App\Model\Empinfo;
use App\Model\LoanAmmount;
use App\Model\Contact;
use App\Model\Loan;
use App\Model\Tenure;
use App\Model\Spouse;
use App\Model\Reference;
use App\Model\Financial;
use App\Model\Postcode2;
use App\Model\Postcode;
use App\Model\PraApplication;
use App\Model\Employment;
use App\Model\Document;
use App\Model\Term;
use App\Model\Settlement;
use App\Model\Credit;
use App\Model\Pep;
use PDF;
use App\Model\Country;
use App\Model\Occupations;
use App\Model\Position;
use App\Model\Relationship;
use App\Model\Title;
use DateTime;
use DB;
use App\Dsr_b;
use App\Model\Log_download;
use App\Model\CodeIDType;
use App\Model\CodeJobStatus;
use App\Model\CodeWorkSec;
use App\Model\State2;
use App\Model\Foreigner;
use App\Model\Subscription;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	 $this->middleware('auth');	
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return Response
	 */
	public function index()
	{
	    $user = Auth::user();
		if($user->role==0 || $user->role==7) {
            $email =  $user->email;

		    if($user->role==0)   {
                $pra = PraApplication::latest('created_at')->where('email',$email)->limit('1')->first();
            } 
            else {
                $pra = PraApplication::latest('created_at')->where('acus_email',$email)->limit('1')->first();  
            }   
        

            $id_pra     = $pra->id;
            $status     = Term::where('id_praapplication', $id_pra)->first()->status;

            $basic      = Basic::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first();	
            $employment = Employment::get();
      	     $reference = Reference::latest('created_at')->where('id_praapplication', $id_pra)->first(); 
        	$empinfo   = Empinfo::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();

            $id_type   = $empinfo->first()->employment_id;

            $total_salary = $empinfo->first()->salary + $empinfo->first()->allowance  ;

            $loan      = Loan::where('id_type',$id_type)
                ->where('min_salary','<=',$total_salary)
                ->where('max_salary','>=',$total_salary)->limit('1')->get();

            $id_loan= $loan->first()->id;  

            $icnumber   = $pra->icnumber;
            $tanggal    = substr($icnumber,4, 2);
            $bulan      = substr($icnumber,2, 2);
            $tahun      = substr($icnumber,0, 2); 

            if($tahun > 30) {
                $tahun2 = "19".$tahun;
            }
            else {
                $tahun2 = "20".$tahun;
            }
       
            $lahir      = $tahun2.'-'.$bulan.'-'.$tanggal; 
            $lahir      =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
            $oDateNow   = new DateTime();
            $oDateBirth = new DateTime($lahir);
            $oDateIntervall = $oDateNow->diff($oDateBirth);

            $umur       = 61 - $oDateIntervall->y;

            $state      = State2::orderby('id','ASC')->get();
            $tenure     = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->get();    

        	$contact    = Contact::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->get();
        	$data       = Basic::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        	$loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

            if($loanammount->package=="Mumtaz-i") {
                $package_name = "Mumtaz-<i><font size='2'>i</font></i>";
            }
            else if($loanammount->package=="Afdhal-i") {
                $package_name = "Afdhal-<i><font size='2'>i</font></i>";
            }

            $spouse = Spouse::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        	$financial = Financial::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        	$document1 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
            $document2 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '2' )->first();
            $document3 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '3' )->first();
            $document4 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '4' )->first();
            $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
            $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
            $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
            $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
            $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
            $document10 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '10' )->first();
            $document11 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '11' )->first();
        	$term      = Term::where('id_praapplication',  $id_pra )->first();
            $dsr_b     = Dsr_b::where('id_praapplication',  $id_pra )->first();

            $today      = date("d/m/Y");
            $credit     = Credit::where('id_praapplication', $id_pra)->first(); 
            $pep        = Pep::where('id_praapplication', $id_pra)->first(); 

            $country        = Country::get(); //term
            $nationality    = Country::get(); //term
            $occupation     = Occupations::get();
            $position       = Position::get();
            $relationship   = Relationship::get();
            $relationship2  = Relationship::get();
            $title          = Title::get();
            $empstatus      = CodeJobStatus::get();
            $country2        = Country::get();
            $foreigner      = Foreigner::where('id_praapplication',$id_pra)->first();
            $subs           = Subscription::where('id_praapplication',$id_pra)->first();

            if($term->status==0 AND $term->verification_result == 0) {
                return redirect('upload');
            }

            if($status < 1 OR $status=='77') {
                return view('home.index', compact('settlement','user','data','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary','loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','document10','document11','zbasicsalary','zdeduction','id_type','today','basic','credit','pep','term','package_name','country','occupation','position','relationship','relationship2','title','dsr_b','nationality','empstatus','state','foreigner','subs','country2'));    
            }
            elseif(($status == 0) AND ($verification_result == 4))  {
                return view('home.index', compact('settlement','user','data','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary','loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','document10','document11','zbasicsalary','zdeduction','id_type','today','basic','credit','pep','term','package_name','country','occupation','position','relationship','relationship2','title','dsr_b','nationality','empstatus','state','foreigner','subs','country2'));  
            }
            else{
                // Application Status
                return view('home.index2', compact('settlement','user','data','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary','loanammount', 'reference','financial','document1','document2','document3','document4','document5','document6', 'document7','document8','document9','document10','document11','term','country','occupation','position','relationship','relationship2','title','dsr_b','nationality','empstatus','state','foreigner','subs','country2')); 
            }
        }
        else {
            return redirect('admin');
        }
	}

   public function activities_user($id_pra) {
         $user = Auth::user();
         $terma = Term::where('id_praapplication',$id_pra)->wherein('status', ['0','1','99','77','88'])->wherein('verification_result', ['0','1','2','3'])->orderBy('file_created','DESC')->with('praapplication.user')->get();

         return view('admin2.activities_user', compact('user','terma'));  
    }


	public function postcodes($id)
	{
	   
	      $data = Postcode2::where('postcode',$id)->with('state')->limit(1)->get();
         
         return $data;
	}

public function postcode($id)
    {
        $data = Postcode::where('postcode',$id)->with('state')->limit(1)->get();
        return $data;
    }

    public function review($id)
    {
       
          
          $user = Auth::user();
         $email =  $user->email;
       
        $pra = PraApplication::latest('created_at')->where('id',$id)->limit('1')->first();
        $id_pra = $pra->id;

        $status = Term::where('id_praapplication', $id_pra)->first()->status;

     

        $basic = Basic::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first(); 
        $employment = Employment::get();
     
         $term = Term::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first(); 
     
              $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();

        $id_type= $empinfo->first()->employment_id;
        $total_salary = $empinfo->first()->salary + $empinfo->first()->allowance  ;

        $loan = Loan::where('id_type',$id_type)
                ->where('min_salary','<=',$total_salary)
                ->where('max_salary','>=',$total_salary)->limit('1')->get();

         $id_loan= $loan->first()->id;  

         $icnumber= $pra->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;

   
          $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->get();   

              $contact = Contact::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->get();
            $data = Basic::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
             $loanammount = loanammount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

            $spouse = Spouse::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
            $financial = Financial::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
            $document1 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
            $document2 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '2' )->first();
            $document3 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '3' )->first();
            $document4 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '4' )->first();
            $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
            $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
            $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
            $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
            $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();

             $document10 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '10' )->first();
             $document11 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '11' )->first();
            

          
             return view('home.review', compact('user','data','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary','loanammount','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term', 'document10','document11'));     
        
       



        

            
    }



    public function upload()
    {
       
          
          $user = Auth::user();
          if ($user->role <> '0'){
            return redirect('admin');
          }
          else {


           
           
              $email =  $user->email;
                  
        $pra = PraApplication::latest('created_at')->where('email',$email)->limit('1')->first();
        $id_pra = $pra->id;

        $status = Term::where('id_praapplication', $id_pra)->first()->status;
        $loanamount = loanammount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
   
             $document1 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
            $document2 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '2' )->first();
            $document3 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '3' )->first();
            $document4 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '4' )->first();
            $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
            $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
            $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
            $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
            $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
              $document10 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '10' )->first();
             $document11 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '11' )->first();
             //front
             $document12 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '12' )->first();
             //back
             $document13 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '13' )->first();
             //selfie
             $document14 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '14' )->first();

            $term = Term::where('id_praapplication',  $id_pra )->first();
          $financial = Financial::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
          $term = Term::where('id_praapplication',  $id_pra )->first();

           $settlement = Settlement::latest('created_at')->where('id_praapplication',  $id_pra )->get();
  

               if ($status==0 && $term->verification_result==0) {
                  return view('home.upload', compact('term','financial','settlement','user','pra','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','document10','document11','loanamount','document12','document13','document14'));  

                }
                else {

                    return redirect('home');
                }
             
        
        
          }
            
    }


 public function downloaddocpdf($id, $file) {

    $file = 'storage/uploads/file/'.$id.'/'.$file;
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    exit;
  }


  public function agreement($id_pra) {
    $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
    $term = Term::where('id_praapplication',  $id_pra )->first();
    $data = Basic::where('id_praapplication',  $id_pra )->first();
     $basic = Basic::where('id_praapplication',  $id_pra )->first();
    $loanamount = loanammount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

    $time_name = date('Ymd');
    $today = date("d/m/Y");

    $credit = Credit::where('id_praapplication', $id_pra)->first(); 
    $pep = Pep::where('id_praapplication', $id_pra)->first(); 

     return view('pdf.agreement', compact('data','basic','loanamount','term','today','credit','pep')); 
  }


  public function terma($id_pra) {
    $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
  
    $today = date("d/m/Y");
    $time_name = date('Ymd');
     $data = Basic::where('id_praapplication',  $id_pra )->first();


     return view('pdf.terma', compact('pra','today','data')); 
      // $pdf = PDF::loadView('pdf.terma', compact('pra','today','data'))->setPaper('a4')->setOrientation('potrait');;
     //   return $pdf->download('TERMA_SYARAT'.$time_name.'-'.$pra->icnumber.'.pdf');
  }

   public function downloadzips($id_pra) {
        $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
        $basic = Basic::latest('created_at')->where('id_praapplication',$id_pra)->limit('1')->first();
          $dcmt = DB::table('document')->select(DB::raw(" max(id) as id"))->where('id_praapplication',$id_pra)->groupBy('type')->pluck('id');
        $files = Document::whereIn('id', $dcmt)->get(); 
        $filesd = Document::where('id_praapplication',$id_pra)->get(); 
        $prafullname =  str_replace(" ","%20",$pra->fullname);  
            

        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
        $url = url('')."/storage/uploads/file/".$id_pra."/"; 
       //$url = "https://www.mbsb.insko.my/public/storage/uploads/file/".$id_pra."/"; 
        if((!empty($document5->upload)) OR (!empty($document6->upload)) OR (!empty($document7->upload)) OR (!empty($document8->upload)) OR (!empty($document9->upload))) {
            $zip = new \ZipArchive();

 
            // store the public path
     $publicDir = public_path();
   $tmp_file = tempnam('.','');
     // Define the file name. Give it a unique name to avoid overriding.
     $zipFileName = $id_pra.'.zip';
              //if ($zip->open($url.'/'.$zipFileName, \ZipArchive::CREATE) === true) {
                if ($zip->open($publicDir . '/storage/uploads/file/'.$id_pra.'/' . $zipFileName, \ZipArchive::CREATE) === true) {
         // Loop through each file
                foreach($files as $file){
                     $url2 = $url.$file->upload;
                     $url2 = str_replace(' ', '%20', $url2);

                        if (!function_exists('curl_init')){ 
                            die('CURL is not installed!');
                        }

                     $ch = curl_init();
                     curl_setopt($ch, CURLOPT_URL, $url2);
                     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                     $output = curl_exec($ch);
                     curl_close($ch);
                     $download_file = $output;

                     $type = substr($url2, -5, 5); 
                     #add it to the zip
                     $zip->addFromString(basename($url.$file->upload.'.'.$type),$download_file);
                }
                // close zip
                $filename = $zip->filename;
                $zip->close();
            }

             // Download the file using the Filesystem API
             $filePath = $publicDir . '/storage/uploads/file/' . $zipFileName;

             if (file_exists($filePath)) {
                 return Storage::download($filePath);
             }
        
                // Set Time Download
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $time_name = date('Ymd');
                $time_download = date('Y-m-d H:i:s');
                $log_download = new Log_download;
                
                $user = Auth::user();
                $id_user =  $user->id;
                $log_download->id_praapplication   =  $id_pra;
                $log_download->id_user   =  $id_user;
                $log_download->Activity   =  'Download ZIP File';
                $log_download->type   =  'Zip Archive';
                $log_download->downloaded_at   =  $time_download;
                $log_download->save();
                
                # send the file to the browser as a download
               ob_start();
               $strFile = file_get_contents($tmp_file);         

                header('Content-disposition: attachment; filename=DOC-'.$id_pra.'-'.$url.'.zip');
                header('Content-type: application/zip');
                  echo $tmp_file;
                  while (ob_get_level()) {
                    ob_end_clean();
                  }
                  readfile($tmp_file);  

                //$filetopath=$public_dir.'/'.$zipFileName;
               //$filetopath = public_path().'/storage/uploads/file/'. $id_pra.'/'.$zipFileName;
                    // Create Download Response
                   // if(file_exists($filetopath)){
                       // return response()->download($filetopath,$zipFileName,$headers);
                    //} 
                exit;
                
            }
        else {
                
                 return redirect('admin')->with('error', 'Cannot to ZIP caused document are empty !');
                //Return redirect('admin')->with('error',$url);
        }
        
    }

    public function downloadzip($id_pra) {
        $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
        $basic = Basic::latest('created_at')->where('id_praapplication',$id_pra)->limit('1')->first();
        $dcmt = DB::table('document')->select(DB::raw(" max(id) as id"))->where('id_praapplication',$id_pra)->groupBy('type')->pluck('id');
        $files = Document::whereIn('id', $dcmt)->get(); 
        $filesd = Document::where('id_praapplication',$id_pra)->get(); 
        $prafullname =  str_replace(" ","%20",$pra->fullname);  
            
        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
        $url = url('')."/storage/uploads/file/".$id_pra."/"; 
       //$url = "https://www.mbsb.insko.my/public/storage/uploads/file/".$id_pra."/"; 
        
        if((!empty($document5->upload)) OR (!empty($document6->upload)) OR (!empty($document7->upload)) OR (!empty($document8->upload)) OR (!empty($document9->upload))) {
            $zip = new \ZipArchive();

 
        $tmp_file = tempnam('.','');
        $zip->open($tmp_file, \ZipArchive::CREATE);
             
         // Loop through each file
                foreach($files as $file){
                     $url2 = $url.$file->upload;
                     $url2 = str_replace(' ', '%20', $url2);

                        if (!function_exists('curl_init')){ 
                            die('CURL is not installed!');
                        }

                     $ch = curl_init();
                     curl_setopt($ch, CURLOPT_URL, $url2);
                     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                     $output = curl_exec($ch);
                     curl_close($ch);
                     $download_file = $output;

                     $type = substr($url2, -5, 5); 
                     #add it to the zip
                     $zip->addFromString(basename($url.$file->upload.'.'.$type),$download_file);
                }
                // close zip
                $zip->close();
            

        
                // Set Time Download
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $time_name = date('Ymd');
                $time_download = date('Y-m-d H:i:s');
                $log_download = new Log_download;
                
                $user = Auth::user();
                $id_user =  $user->id;
                $log_download->id_praapplication   =  $id_pra;
                $log_download->id_user   =  $id_user;
                $log_download->Activity   =  'Download ZIP File';
                $log_download->type   =  'Zip Archive';
                $log_download->downloaded_at   =  $time_download;
                $log_download->save();
                
                # send the file to the browser as a download
               ob_start();
               $strFile = file_get_contents($tmp_file);         

                header('Content-disposition: attachment; filename=DOC-'.$id_pra.'-'.$url.'.zip');
                header('Content-type: application/zip');
                  echo $tmp_file;
                  while (ob_get_level()) {
                    ob_end_clean();
                  }
                  readfile($tmp_file);  

                //$filetopath=$public_dir.'/'.$zipFileName;
               //$filetopath = public_path().'/storage/uploads/file/'. $id_pra.'/'.$zipFileName;
                    // Create Download Response
                   // if(file_exists($filetopath)){
                       // return response()->download($filetopath,$zipFileName,$headers);
                    //} 
                exit;
                
            }
        else {
                
                 return redirect('admin')->with('error', 'Cannot to ZIP caused document are empty !');
                //Return redirect('admin')->with('error',$url);
        }
        
    }


     public function downloadzipx($id_pra) {
        $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
        $basic = Basic::latest('created_at')->where('id_praapplication',$id_pra)->limit('1')->first();
          $dcmt = DB::table('document')->select(DB::raw(" max(id) as id"))->where('id_praapplication',$id_pra)->groupBy('type')->pluck('id');
        $filess = Document::whereIn('id', $dcmt)->get(); 
        $files = Document::where('id_praapplication',$id_pra)->get(); 
        $prafullname =  str_replace(" ","%20",$pra->fullname);  
            

        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
        $url = url('')."/storage/uploads/file/".$id_pra."/"; 
        if((!empty($document5->upload)) OR (!empty($document6->upload)) OR (!empty($document7->upload)) OR (!empty($document8->upload)) OR (!empty($document9->upload))) {
                # create new zip opbject
                $zip = new \ZipArchive();
                
                # create a temp file & open it
                $tmp_file = tempnam('.','');
                $zip->open($tmp_file, \ZipArchive::CREATE);
                
                # loop through each file
                foreach($files as $file){
                        $url2 = $url.$file->upload;
                        $url2 = str_replace(' ', '%20', $url2);
                        
                    
                           if (!function_exists('curl_init')){ 
                                die('CURL is not installed!');
                            }
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url2);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $output = curl_exec($ch);
                            curl_close($ch);
                        $download_file = $output;
                                        
                        $type = substr($url2, -5, 5); 
                        #add it to the zip
                        $zip->addFromString(basename($url.$file->upload),$download_file);
                }
                
                # close zip
                $zip->close();
        
        
                // Set Time Download
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $time_name = date('Ymd');
                $time_download = date('Y-m-d H:i:s');
                $log_download = new Log_download;
                
                $user = Auth::user();
                $id_user =  $user->id;
                $log_download->id_praapplication   =  $id_pra;
                $log_download->id_user   =  $id_user;
                $log_download->Activity   =  'Download ZIP File';
                $log_download->type   =  'Zip Archive';
                $log_download->downloaded_at   =  $time_download;
                $log_download->save();
                
                # send the file to the browser as a download
                header('Content-disposition: attachment; filename=DOC-'.$time_name.'-'.$basic->new_ic.'.zip');
                header('Content-type: application/zip');
                readfile($tmp_file);
                
        }
        else {
                
                 return redirect('admin')->with('error', 'Cannot to ZIP caused document are empty !');
                //Return redirect('admin')->with('error',$url);
        }
        
    }
     public function get_document($id_pra,$name) {
         $user = Auth::user();
         $terma = Term::where('id_praapplication',$id_pra)->wherein('status', ['0','1','99','77','88'])->wherein('verification_result', ['0','1','2','3'])->orderBy('file_created','DESC')->with('praapplication.user')->get();

         $doc = Document::where('id_praapplication',$id_pra)->where('upload',$name)->limit('1')->first();

         return view('admin2.get_document', compact('user','terma','doc','name'));  
    }
    


	
}
