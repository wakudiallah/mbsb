<?php

namespace App\Http\Controllers\WebServices;

use Illuminate\Http\Request;
//use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Brand;
use App\CarModel;
use App\ModelDetail;
use App\Year;
use DB;
use App\MO;
use Response;
class ListMOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function agent($id)
    {

        

        $data = MO::where('active_user',$id)->orderby('id','ASC')->get();
        
       // return $data;   
        //$data = MO::where('active_user',$id)->orderby('id','ASC')->get(['id', 'id_mo', 'desc_mo'])->toJson();;
return Response::json($data);
/*
return response()->json([
    'data' => $data->only(['id', 'id_mo', 'desc_mo', 'email']),

]);*/

        /* $lotto= MO::where('active_user', $id)
            ->get()
            ->map(function($deal,$mo) {
                return [
                     //'amount'    => $users->pluck('amount')->first(),
                'id_mo'    => $mo->id_mo
                ];
            })->values();
            return $lotto;*/
    }

       

    public function brand_to_model($id)
    {
        // $data = CarModel::where('id_brand',$id)->get();
        $data = CarModel::join('model_details', 'model_details.id_model', '=', 'models.id')
        ->select(DB::raw('DISTINCT model_name,id_model as id'))
        ->where('models.id_brand',$id)->get(); 
        return $data;
    }

    public function model_to_modeldetail($id) {

        $data = ModelDetail::where('id_model',$id)->groupby('year', 'variant', 'style',  'transmission')->orderby('year','ASC')->get();
        
        return $data;   
    }

    public function model_to_year($id)
    {
        $carname = ModelDetail::where('id',$id)->first()->transmission;
        $data = ModelDetail::where('transmission',$carname)->get();

        
        return $data;    
    }
    public function max_financing($id_model_detail,$id_year) {
        $carname = ModelDetail::where('id',$id_model_detail)->first()->transmission;

        $data = ModelDetail::where('transmission',$carname);
        $car_value = $data;

       /* $car = ModelName::join('Model_Det', 'Model_Det.id_model_name', '=', 'Model_Name.id')
        ->select(DB::raw('avg(value) as avg_value'))
        ->where('Model_Name.id_model',$id)->where('Model_Det.year',$year)
        ->get(); */

         $max_financing = $car_value;
             return response()->json([
              'max_financing' => $this->roundown((int)$max_financing),
              'car_value' => (int) $car_value,
            ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
