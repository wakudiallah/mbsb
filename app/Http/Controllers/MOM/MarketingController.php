<?php

namespace App\Http\Controllers\MOM;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Term;
use App\Mail_System;
use App\MarketingOfficer;
use App\Manager;
use App\MO;
use Auth;
use Ramsey\Uuid\Uuid;
use Hash;
use Mail;
use Alert;
use DB;
class MarketingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if(empty($user)) {
            return redirect('/');
        }
        if (($user->role==4))  {
                   
            // $marketing_user = User::Where('role','5')->orderBy('created_at','DESC')->get();
            $marketing_user = MO::orderBy('created_at','DESC')->get();
            $manager  = Manager::get();
            $manager1 = Manager::orderBy('created_at','ASC')->get();
            return view('admin_mbsb.marketing', compact('marketing_user','user','manager','manager1'));
        }
        elseif (($user->role==10)) {
                   
            // $marketing_user = User::Where('role','5')->orderBy('created_at','DESC')->get();
            $marketing_user = MO::Where('id_manager',$user->id)->orderBy('created_at','DESC')->get();
            $manager  = Manager::get();
            $manager1 = Manager::get();
            return view('admin_mbsb.marketing', compact('marketing_user','user','manager','manager1'));
        }
        else{
             return redirect('admin')->with("message", "User ".$user->email." has no access authorization");   
        }

         
    }

     public function mo_stats() {
         $user = Auth::user();
         $terma = Term::where("referral_id","!=","0")->wherein('status', ['1','99','77','88'])->wherein('verification_result', ['0','1','2','3'])->orderBy('file_created','DESC')->get();

         return view('admin_mbsb.stats', compact('user','terma'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email      = $request->input('email');
        $checkUser  = DB::table('users')->where('email', $email)->get();
        $countUser  = $checkUser->count();

        $checkID = DB::table('mo')->where('id_mo', $request->input('employer_id'))->get();
        $countID = $checkID->count();

        if($countUser==0) {
            if($request->set_password=="1") {
                $password1 = $request->password;
                $password2 = $request->password_confirmation;

                if($password1!=$password2) {
                   return redirect('marketing')->with('error', 'Password & Confirmation Password not match'); 
                }
            }
            else {
                $password2 = $this->generateStrongPassword('8',false,'ld');   
            }
            
            $email          = $request->input('email');
            $fullname       = $request->name;
            $mo_id          = $request->input('employer_id');
            $phone          = $request->input('phoneno');
            $active_code    = str_random(100);
            $marketing_id   = Uuid::uuid4()->getHex();

            $user                   = new User;
            $user->password         = Hash::make($password2); 
            $user->id               = $marketing_id;// toString();     
            $user->email            = $email;
            $user->activation_code  = $active_code;
            $user->name             = $request->name;
            $user->mo_id            = $request->employer_id;
            $user->active           = "1";
            $user->role             = "5";
            $user->save();

            $marketing              = new MO;
            $marketing->id          = $marketing_id;
            $marketing->phone       = $request->phone;
            $marketing->id_mo       = $request->employer_id;
            $marketing->email       = $request->email;
            $marketing->active_user = '1';
            $marketing->type        = '1';
            $marketing->id_manager  = $request->manager_id;
            $marketing->desc_mo     = $request->name;
            $marketing->save();

            $noreply_sender = Mail_System::where('id',2)->first();
            $noreply_sender_name = $noreply_sender->name;
            $noreply_sender_email = $noreply_sender->email;


            // Send to NO Email
              
            Mail::send('mail.mo.mo_register', compact('email','fullname','password2'), function ($message) use ($noreply_sender_name, $noreply_sender_email,$email ,$fullname ) {
            $message->from($noreply_sender_email, $noreply_sender_name);
            $message->subject('Your MBSB Marketing Officer Account Successfully Created');
            $message->to($email, $fullname);
            });

            Alert::success('Data added successfuly');
            return redirect('marketing'); 
        }
         else if ($countUser!=0) {
            Alert::error('Email Already Used. Please use another email address');
            return redirect('marketing'); 
        }

         else if ($countID!=0) {
            Alert::error('ID Employer Already Used.');
            return redirect('marketing'); 
        }

        
    }

    public function activate_agent(Request $request)
    {
        $checkUser = User::Where("email",$request->input('email'))->count();
        $checkMo = User::Where("mo_id",$request->input('employer_id'))->count();

        if(($checkUser>0) AND ($checkMo>0)) {
            return redirect('marketing')->with('error', 'Email Already Used. Please use another email address'); 
        }

        if($request->set_password=="1") {
            $password1 = $request->password;
            $password2 = $request->password_confirmation;

            if($password1!=$password2) {
                return redirect('marketing')->with('error', 'Password & Confirmation Password not match'); 
            }
        }
        else {
            $password2 = $this->generateStrongPassword('8',false,'ld');   
        }
        
        $email          = $request->input('email');
        $fullname       = $request->input('fullname');
        $mo_id          = $request->input('employer_id');
        $phone          = $request->input('phoneno');
        $manager_id     = $request->input('manager_id');
        $active_code    = str_random(100);
        $marketing_id   = Uuid::uuid4()->getHex();
       
        $user                   = new User;
        $user->password         = Hash::make($password2); 
        $user->id               = $marketing_id;// toString();     
        $user->email            =  $email;
        $user->activation_code  = $active_code;
        $user->name             =  $fullname;
        $user->mo_id            =  $mo_id;
        $user->active           =  "1";
        $user->role             =  "5";
        $user->save();

        /*$marketing = new MarketingOfficer;
        $marketing->id   = $marketing_id;
        $marketing->phoneno = $request->phone;
        $marketing->employer_id = $request->employer_id;
        $marketing->save();*/
        $mo = MO::where('id_mo', $mo_id)->update(['email' => $email,'phone' => $phone,'active_user' => '1','name'=>$fullname,'id_manager' => $manager_id]);

        $noreply_sender         = Mail_System::where('id',2)->first();
        $noreply_sender_name    = $noreply_sender->name;
        $noreply_sender_email   = $noreply_sender->email;

        // Send to NO Email
          
        /*Mail::send('mail.mo.mo_register', compact('email','fullname','password2'), function ($message) use ($noreply_sender_name, $noreply_sender_email,$email ,$fullname ) {
        $message->from($noreply_sender_email, $noreply_sender_name);
        $message->subject('Your MBSB Marketing Officer Account Successfully Created');
        $message->to($email, $fullname);
        });*/
        Alert::success('New Marketing Officer Successfully Activated');
        return redirect('marketing')->with('success'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
