<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\User;
use App\MO;
use App\Http\Requests;
use App\Transformers\UserTransformer;
use App\Transformers\MomTransformer;
use Auth;

class MomController extends Controller
{
    public function users(User $user)
    {
        $users  = $user->all();

        return fractal()
            ->collection($users)
            ->transformWith(new UserTransformer)
            ->toArray();
    }

     public function marketing_officer(MO $mo)
     {
        $mo = $mo->all();
        //return response()->json($users);
        return fractal()
            ->collection($mo)
            ->transformWith(new MomTransformer)
            ->toArray();
    }


    public function profile(User $user)
    {
        $user = $user->find(Auth::user()->id);

        return fractal()
            ->item($user)
            ->transformWith(new UserTransformer)
            ->includePosts()
            ->toArray();
    }

    public function profileById(User $user, $id)
    {
        $user = $user->find($id);

        return fractal()
            ->item($user)
            ->transformWith(new UserTransformer)
            ->includePosts()
            ->toArray();
    }
}
