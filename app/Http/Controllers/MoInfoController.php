<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Loglogin;
use Hash;
use Auth;
use Input;
use Mail;
use App\Http\Requests\UpdateUserRequest;
 
 
use Rhumsaa\Uuid\Uuid;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;

use App\Model\AntiAttrition;
class MoInfoController extends Controller
{

         public function mo()
    {
        
        $client = new Client();
        
        $body = $client->request('GET', 'https://mbsb.uat.ezlestari.com.my/public/api/marketing_officer', 
            [
                'headers' => 
                    [
                        'Accept'     => 'application/json',
                        'Authorization' => 'Bearer u1su6TK8w6bJy6G9wyNIOKMAYltXin4WemTx1z5SYJrHb844ddrV3s0r8uSb',
                    ]
            ])->getBody()->getContents();

            $contents = (string) $body;
            $result = json_decode($contents, true);

            dd ($result);


    }

      public function send_to_mom(Request $request)
    {
        $client = new Client();
        $CustIDNo = '910611115951';
        $antiattrition = AntiAttrition::where('CustIDNo',$CustIDNo)->limit('1')->first();

        $res = $client->request('POST', 'https://www.uat.insko.my/public/api/antiattrition', [
            'headers' => [
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer u1su6TK8w6bJy6G9wyNIOKMAYltXin4WemTx1z5SYJrHb844ddrV3s0r8uSb'
            ],
            'form_params' => [
                'mo_id' => $antiattrition->mo_id,
                'id_clrt' => $antiattrition->id,
                'TaskID' => $antiattrition->TaskID,
                'ACID' => $antiattrition->ACID,
                'CustIDNo' => $antiattrition->CustIDNo,
                'Tenor' => $antiattrition->Tenor,
                'Year' => $antiattrition->Year,
                'SalIncrementPercent'=>$antiattrition->SalIncrementPercent,
                'NewSalary' => $antiattrition->NewSalary,
                'NewAllowDeduct' => $antiattrition->NewAllowDeduct,
                'IntRate' => $antiattrition->IntRate,
                'VarianceRate' => $antiattrition->VarianceRate,
                'NewLoanOffer' => $antiattrition->NewLoanOffer,
                'PayoutPercent' => $antiattrition->PayoutPercent,
                'NetProceed' => $antiattrition->NetProceed,
                'MonthlyInst'=>$antiattrition->MonthlyInst,
                'NewDSR' => $antiattrition->NewDSR,
                'ProfitToEarn' => $antiattrition->ProfitToEarn,
                'CalDt' => $antiattrition->CalDt,
                'CustWrkSec' => $antiattrition->CustWrkSec,
                'CustName' => $antiattrition->CustName,
                'Assign' => $antiattrition->Assign,
                'CustDob' => $antiattrition->CustDob,
                'State'=>$antiattrition->State,
                'assign_by'=>$antiattrition->assign_by,
                'ACID_SI' => $antiattrition->ACID,
                'CustIDNo_SI' => $antiattrition->CustIDNo,
                'MaxAge'=>$antiattrition->Settlement->MaxAge,
                'DueDate'=> $antiattrition->Settlement->DueDate,
                'BalOutStanding'=>$antiattrition->Settlement->BalOutStanding,
                'FullSettlement'=>$antiattrition->Settlement->FullSettlement,
                'ProfitEarned'=>$antiattrition->Settlement->ProfitEarned,
                'Rebate'=> $antiattrition->Settlement->Rebate,
                'PaidInstallment'=>$antiattrition->Settlement->PaidInstallment,
                'RemInstallment'=>$antiattrition->Settlement->RemInstallment,
                'DSRUtilise'=>$antiattrition->Settlement->DSRUtilise,
                'DSRUnutilise'=> $antiattrition->Settlement->DSRUnutilise,
                'CalculateDate'=>$antiattrition->Settlement->CalculateDate,
                'TaskIDNo'=> $antiattrition->Settlement->TaskIDNo
            ]
        ]);

        $array = json_decode($res->getBody()->getContents(), true);
        dd($array);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        
         $user = Auth::user();
     


         return view('mo.moinfo', compact('user')); 
      }

       public function loglogin()
    {
        
           $user = Auth::User();
        $loglogin = Loglogin::latest('created_at')->where('email',$user->email)->get();

        return view('mo.loglogin', compact('user','loglogin')); 
      }

      public function loglogin_map($id, $lat, $lng) {

         $user = Auth::User();
        $loglogin = Loglogin::where('id',$id)->first();

        return view('mo.loglogin_map', compact('loglogin','user'));
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
   public function create()
    {
        
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function store(StoreUserRequest $request)
    {
        $user = new User;
        $email = $request->input('Email');
        $fullname = $request->input('FullName2');
             $active_code = str_random(100);
        $password2 = $request->input('password');
        $user->password = Hash::make($request->password);
        $user->id   = Uuid::uuid4()->getHex(); // toString();     
        $user->email =  $email;
        $user->activation_code = $active_code;
        $user->name =  $fullname;
        $user->active =  "1";
        $user->save();

          Mail::send('mail.register', compact('active_code','password2'), function ($message) use ($email, $fullname) {
          $message->from('mbmsb@ezlestari.com.my', 'Personal loan Co-Opbank Persatuan');
          $message->subject('Email Confirmation');
          $message->to($email, $fullname);
            });
          
     // Notif to Admin
     $email_admin = "mbmsb@ezlestari.com.my";
     $fullname_admin = "mbmsb@ezlestari.com.my";
     // Send to Admin Email
     
     $k = Mail::send('mail.register_notif', compact('email','fullname'), function ($message) use ($email_admin, $fullname_admin) {
     $message->from('no-reply@ezlestari.com.my', 'no-reply@ezlestari.com.my');
     $message->subject('New User Registration');
     $message->to($email_admin, $fullname_admin);
       });
    

       $credentials = array(
    'email' => $email,
    'password' => $password2 );

if (Auth::attempt($credentials)) {
   return redirect()->intended('home');
}



         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
  public function update()
    {


       
    }

   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {       
  
    }

      public function update_password(UpdateUserRequest $request)
            {

                $userx = Auth::user();
                $id =  $userx->id;
                 $user = User::find($id);
                 $password =  $request->input('password'); 
      
                      if (Hash::check($request->input('old_password'), $userx->password)) {
                             $user->password =  Hash::make($password); 
                             $user->save();  
                              return redirect('user')->with('message', 'update succsess');                   
                    }
                    else {

                         return redirect('user')->withErrors(['old password invalid ']);
                    }

            }
}
