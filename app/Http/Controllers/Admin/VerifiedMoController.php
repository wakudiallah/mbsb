<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Model\Basic;
use App\Model\Basic_v;
use App\Model\Empinfo;
use App\Model\Empinfo_v;
use App\Model\Spouse;
use App\Model\LoanAmmount;
use App\Model\LoanAmmount_v;
use App\Model\Reference;
use App\Model\Reference_v;
use App\Model\Financial;
use App\Model\Financial_v;
use App\Model\I_financing;
use App\Model\I_financing_v;
use App\Model\Document;
use App\Model\Document_v;
use App\Model\Term;
use App\Model\User;
use App\Model\Branch;
use App\Model\Log_download;
use App\Model\Commitments;
use App\Model\Credit;
use App\Model\Pep;
use Input;
use Mail;
use App\Model\CodeIDType;
use App\Model\CodeJobStatus;
use App\Model\CodeWorkSec;
use App\Model\Foreigner;
use App\Model\Subscription;

class VerifiedMoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
	 $this->middleware('auth');	
      $this->middleware('admin');
	}
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            
       
        if($id == '1') {
            // Update Field
              $id_praapplication = $request->input('id_praapplication');

              $title = $request->input('title');
                $title_others = $request->input('title_others');
              $name = $request->input('name');
              $new_ic = $request->input('new_ic');
              $old_ic = $request->input('old_ic');
              $dob2 = $request->input('dob');
              $dob =  $this->dateDB($dob2);
              $police_number = $request->input('police_number');
              $gender = $request->input('gender');
              $country = $request->input('country');
               $country_others = $request->input('country_others');
                $country_origin = $request->input('country_origin');

              $address = $request->input('address');
              $address2 = $request->input('address2');
              $address3 = $request->input('address3');

              $postcode = $request->input('postcode');
              $state = $request->input('state');
              $ownership = $request->input('ownership');
              $corres_address1 = $request->input('corres_address1');
               $corres_address2 = $request->input('corres_address2');
                $corres_address3 = $request->input('corres_address3');
              $corres_state = $request->input('corres_state');
              $corres_postcode = $request->input('corres_postcode');
              $corres_homephone = $request->input('corres_homephone');
              $corres_mobilephone = $request->input('corres_mobilephone');
             // $corres_email = $request->input('corres_email');
              $citizen = $request->input('citizen');
              $race = $request->input('race');
               $race_others = $request->input('race_others');

              $bumiputera = $request->input('bumiputera');
              $religion = $request->input('religion');
              $religion_others = $request->input('religion_others');

              $marital = $request->input('marital');
              $dependents = $request->input('dependents');
              $education = $request->input('education');
              
               $country_dob = $request->input('country_dob');
               $address_correspondence = $request->input('checker');
              $resident = $request->input('resident');
            
             $basic = Basic::where('id_praapplication', $id_praapplication)
             ->update(['country' => $country, 
              'country_origin' => $country_origin, 
               'country_others' => $country_others, 
              'citizen' => $citizen, 
              'new_ic' => $new_ic, 
              'dob' => $dob,
             'old_ic' => $old_ic, 
             'country_dob' => $country_dob,
             'name' => $name,
             'title' => $title, 
             'title_others' => $title_others,
             'gender' => $gender, 
             'marital' => $marital,
              'dependents' => $dependents,
             'race' => $race, 
             'race_others' => $race_others,
              'bumiputera' => $bumiputera, 
             'religion' => $religion , 
             'religion_others' => $religion_others,
             'marital' => $marital, 
             'police_number' => $police_number, 
             'address'=> $address,
              'address2'=> $address2,
               'address3'=> $address3,
              'postcode'=> $postcode,
               'state'=> $state, 
               'corres_address1'=> $corres_address1, 
               'corres_address2'=> $corres_address2, 
               'corres_address3'=> $corres_address3, 
               'corres_state'=> $corres_state, 
               'corres_postcode'=> $corres_postcode, 
               'corres_homephone'=> $corres_homephone,
                'corres_mobilephone'=> $corres_mobilephone, 
                'education'=> $education,
                 'ownership'=> $ownership,
                 'address_correspondence'=> $address_correspondence,
                 'resident'=>$resident ]);
            


    

        }

        else if($id == '2') {

            $id_praapplication = $request->input('id_praapplication');

            $empname = $request->input('empname');
            $emptype = $request->input('emptype');
            $emptype_others = $request->input('emptype_others');

            $position = $request->input('position');
            $dept_name = $request->input('dept_name');
            $division = $request->input('division');
            $address = $request->input('address');
            $address2 = $request->input('address2');
            $address3 = $request->input('address3');
            $state =  $request->input('state');
            $postcode =  $request->input('postcode');
            $nature_business = $request->input('nature_business');
            $joined2 =  $request->input('joined');
            $joined2 = str_replace('/', '-', $joined2);
            $joined =  date('Y-m-d', strtotime($joined2));
            $office_phone = $request->input('office_phone');
            $office_fax = $request->input('office_fax');
            $working_exp = $request->input('working_exp');
            $empstatus = $request->input('empstatus');
            
                
            $empinfo = Empinfo::where('id_praapplication', $id_praapplication)
            ->update(['empname' => $empname,  'emptype' => $emptype, 'emptype_others' => $emptype_others,  'position' => $position, 'dept_name' => $dept_name, 'division' => $division,'address' => $address, 'address2' => $address2, 'address3' => $address3, 'state' => $state, 'postcode' => $postcode,  'nature_business' => $nature_business, 'joined' => $joined, 'office_phone' => $office_phone, 'office_fax' => $office_fax, 'working_exp' => $working_exp,  'empstatus' => $empstatus ]);



        }
        
        else if($id == '3') {

            $id_praapplication = $request->input('id_praapplication');

            
            $name = $request->input('name');
            $homephone =  $request->input('homephone');
            $mobilephone = $request->input('mobilephone');
            $emptype = $request->input('emptype');
            $emptype_others = $request->input('emptype_others');
           
        
            $spouse = Spouse::where('id_praapplication', $id_praapplication)
          ->update(['name' => $name, 'homephone' => $homephone, 'mobilephone' => $mobilephone, 'emptype' => $emptype, 'emptype_others' => $emptype_others ]);

            //reference
            $name1 = $request->input('name1');
            $mobilephone1 =  $request->input('mobilephone1');
            $home_phone1 =  $request->input('home_phone1');
            $relationship1 =  $request->input('relationship1');
            $name2 = $request->input('name2');
            $mobilephone2 =  $request->input('mobilephone2');
            $home_phone2 =  $request->input('home_phone2');
            $relationship2 =  $request->input('relationship2');


     

            $reference = Reference::where('id_praapplication', $id_praapplication)->update(['name1' => $name1, 'mobilephone1' => $mobilephone1, 'relationship1' => $relationship1, 'name2' => $name2, 'mobilephone2' => $mobilephone2, 'relationship2' => $relationship2, 'home_phone1' => $home_phone1,'home_phone2' => $home_phone2]);

             //v



        }
       else if($id == '4') {

            $id_praapplication = $request->input('id_praapplication');

            
            $financial = Financial::where('id_praapplication', $id_praapplication)
            ->update(['monthly_income' => $request->input('monthly_income'), 
                    'other_income' => $request->input('other_income'), 
                    'total_income' => $request->input('total_income') ]);

            

          }


        else if($id == '5') {

            $id_praapplication = $request->input('id_praapplication');

            // Checking If Not Empty
            $check = Commitments::where('id_praapplication', $id_praapplication)->count();

            if($check>0) {
              $commitments = Commitments::where('id_praapplication', $id_praapplication)
              ->update(['name1' => $request->input('name1'), 
                    'monthly_payment1' => $request->input('monthly_payment1'), 
                    'financing1' => $request->input('financing1'),
                    'remaining1' => $request->input('remaining1'),

                    'name2' => $request->input('name2'), 
                    'monthly_payment2' => $request->input('monthly_payment2'), 
                    'financing2' => $request->input('financing2'),
                    'remaining2' => $request->input('remaining2'),
                    'remark' => $request->input('remark'),

                     ]);
            }
            else {
              $commitments = new Commitments;
              $commitments->id_praapplication = $request->input('id_praapplication');
              $commitments->name1 = $request->input('name1');
              $commitments->monthly_payment1 = $request->input('monthly_payment1');
              $commitments->financing1 = $request->input('financing1');
              $commitments->remaining1 = $request->input('remaining1');
              $commitments->name2 = $request->input('name2');
              $commitments->monthly_payment2 = $request->input('monthly_payment2');
              $commitments->financing2 = $request->input('financing2');
              $commitments->remaining2 = $request->input('remaining2');
              $commitments->remark = $request->input('remark');
              $commitments->save();
            }

            return response()->json(['check' => $check]);

     

          }

        else if($id == '6') {

            $id_praapplication = $request->input('id_praapplication');

            $package = $request->input('package');
            $product_bundling = $request->input('product_bundling');
            $product_bundling_specify = $request->input('product_bundling_specify');
            $cross_selling = $request->input('cross_selling');
            $cross_selling_specify = $request->input('cross_selling_specify');

            $takaful_coverage = $request->input('takaful_coverage');
            $takaful_coverage_specify = $request->input('takaful_coverage_specify');

            $financing_amount = $request->input('financing_amount');
            $tenure = $request->input('tenure');

            $financial = Financial::where('id_praapplication', $id_praapplication)
            ->update(['product_bundling' => $product_bundling, 'product_bundling_specify' => $product_bundling_specify,  'cross_selling' => $cross_selling,  'cross_selling_specify' => $cross_selling_specify, 'takaful_coverage' => $takaful_coverage ,'takaful_coverage_specify' => $takaful_coverage_specify]);

            $Loanamount = LoanAmmount::where('id_praapplication', $id_praapplication)
            ->update(['package' => $package, 'loanammount' => $financing_amount,  'new_tenure' => $tenure ]);


           

        }

        else if($id == '7') {

            $id_praapplication = $request->input('id_praapplication');

            $purpose_facility = $request->input('purpose_facility');
            $type_customer = $request->input('type_customer');
            $payment_mode = $request->input('payment_mode');
            $bank_name = $request->input('bank_name');
            $account_no = $request->input('account_no');

            $bank1 = $request->input('bank1');
            $bank2 = $request->input('bank2');

            $bank3 = $request->input('bank3');
            $bank4 = $request->input('bank4');

            $bank5 = $request->input('bank5');
            $bank6 = $request->input('bank6');

            $financial = Financial::where('id_praapplication', $id_praapplication)
            ->update(['purpose_facility' => $purpose_facility, 'type_customer' => $type_customer,  'payment_mode' => $payment_mode,  'bank_name' => $bank_name, 'account_no' => $account_no ,'bank1' => $bank1, 'bank2' => $bank2 ,'bank1' => $bank1, 'bank3' => $bank3 ,'bank4' => $bank4, 'bank5' => $bank5 ,'bank6' => $bank6]);

    
           

        }
       
        else if($id == '9') {
           
            $id_praapplication = $request->input('id_praapplication');

            $document6 =  $request->input('document6_v');
            $document7 =  $request->input('document7_v');
            $document9 =  $request->input('document9_v');
  
           
            $document6_save = Document::where('id_praapplication', $id_praapplication)->where('type',  '6' )
            ->update(['verification' => $document6]);
            $document7_save = Document::where('id_praapplication', $id_praapplication)->where('type',  '7' )
            ->update(['verification' => $document7]);
          
            $document9_save = Document::where('id_praapplication', $id_praapplication)->where('type',  '9' )
            ->update(['verification' => $document9]);
      
        }

       
        
    }
    
    public function term(Request $request) {

               $id_praapplication = $request->input('id_praapplication');
          $declaration = $request->input('declaration');
          $verification_remark = $request->input('remark');
      $branch = $request->input('branch');
          $id = $id_praapplication;
            $user = Auth::user();
            $user_aktif = Auth::user()->name;


        // cek kredit
            $count_credit = Credit::where('id_praapplication', $id_praapplication)->count();
            if($count_credit<1) {
              $credit = new Credit ;
              $credit->fullname = $request->input('fullname');
              $credit->mykad =  $request->input('mykad');
              $credit->passport =  $request->input('passport');
              $credit->relationship =  $request->input('relationship');
              $credit->id_praapplication = $id_praapplication;
              $credit->save();
            }
            else {
               $credit = Credit::where('id_praapplication', $id_praapplication)->update(['fullname' => $request->input('fullname'), 
              'mykad' => $request->input('mykad'),
              'passport' => $request->input('passport'),
              'relationship' => $request->input('relationship')]);
            }

            // cek pep
            $count_pep = Pep::where('id_praapplication', $id_praapplication)->count();
            if($count_pep<1) {
              $pep = new Pep ;
              $pep->name1 = $request->input('name1');
              $pep->relationship1 =  $request->input('relationship1');
              $pep->status1 =  $request->input('status1');
              $pep->prominent1 =  $request->input('prominent1');
              //2
              $pep->name2 = $request->input('name2');
              $pep->relationship2 =  $request->input('relationship2');
              $pep->status2 =  $request->input('status2');
              $pep->prominent2 =  $request->input('prominent2');
              //3
              $pep->name3 = $request->input('name3');
              $pep->relationship3 =  $request->input('relationship3');
              $pep->status3 =  $request->input('status3');
              $pep->prominent3 =  $request->input('prominent3');

              $pep->id_praapplication = $id_praapplication;
              $pep->save();
            }
            else {
              $pep = Pep::where('id_praapplication', $id_praapplication)->update(['name1' => $request->input('name1'), 
                  'relationship1' => $request->input('relationship1'),
                  'status1' => $request->input('status1'),
                  'prominent1' => $request->input('prominent1'),
                  'name2' => $request->input('name2'), 
                  'relationship2' => $request->input('relationship2'),
                  'status2' => $request->input('status2'),
                  'prominent2' => $request->input('prominent2'),
                  'name3' => $request->input('name3'), 
                  'relationship3' => $request->input('relationship3'),
                  'status3' => $request->input('status3'),
                  'prominent3' => $request->input('prominent3'),
                  'id_praapplication' => $request->input('id_praapplication')

                  ]);
            }

             $subs = Subscription::where('id_praapplication', $id_praapplication)->update(['sub_wealth' => $request->input('sub_wealth'), 
              'gpa' => $request->input('gpa'),
               'hasanah' => $request->input('hasanah'),
              'annur' => $request->input('annur'),
              'wasiat' => $request->input('wasiat')]);

         $foreigner = Foreigner::where('id_praapplication', $id_praapplication)->update(['foreign1' => $request->input('foreign1'), 
              'foreign2' => $request->input('foreign2'),
              'foreign3' => $request->input('foreign3'),
              'country' => $request->input('country'),
              'foreign4' => $request->input('foreign4')]);
          
          if ($declaration==2) { // IF APPROVE
           
     // return response()->json(['status' => "Application Approved!"]);
     // sent email to branch
     $branchname = Branch::where('id', $branch)->first()->branchname;
     $basic = Basic::latest('created_at')->where('id_praapplication',  $id_praapplication )->limit('1')->first(); 
     $cus_name = $basic->name;
     $cus_ic  = $basic->new_ic;
    
     // Ibu Pejabat Email List
     $ho = User::latest('created_at')->where('role',  '3' )->get();
     foreach ($ho as $a) {
      $emailho = $a->email;
      $nameho = $a->name;
       Mail::send('mail.verification_approved_ho', compact('branchname','nameho','cus_name','cus_ic'), function ($message) use ($emailho, $nameho) {
       $message->from('mbsb@ezlestari.com.my', 'mbsb@ezlestari.com.my');
       $message->subject('[MBSB] New Application has Route to Branch');
        $message->to($emailho, $nameho);
        //  echo 'email ='.$emailho;
       
       });
       
     } 
      // Branch Email list
      
      $branch_user = User::latest('created_at')->where('role',  '2' )->where('id_branch',  $branch )->get();
      foreach ($branch_user as $b) {
      $emailbranch = $b->email;
      $namebranch = $b->email;
      
       Mail::send('mail.verification_approved', compact('branchname','cus_name','cus_ic'), function ($message) use ($emailbranch, $namebranch) {
       $message->from('mbsb@ezlestari.com.my', 'mbsb@ezlestari.com.my');
       $message->subject('[MBSB] New Application has Route to Your Branch');
        $message->to($emailbranch, $namebranch);
        //  echo 'email ='.$emailho;
       
       });

       
       
     }
      // Set Time Log
          date_default_timezone_set("Asia/Kuala_Lumpur");
        $time_name = date('Ymd');
        $time_log = date('Y-m-d H:i:s');
        $log_download = new Log_download;
        
        $user = Auth::user();
        $id_user =  $user->id;
        $log_download->id_praapplication   =  $id;
        $log_download->id_user   =  $id_user;
        $log_download->Activity   =  'Application Route to '.$branchname.' Branch';
        $log_download->downloaded_at   =  $time_log;
        $log_download->log_remark   =  $verification_remark;
        // $log_download->log_reason  =  $reason;
        $log_download->save();
     
      $term = Term::where('id_praapplication', $id_praapplication)->update(['verification_status' => '99', 'verification_result' => '2', 'verification_remark' => $verification_remark, 'verified_by' => $user_aktif, 'id_branch' => $branch  ]);
       return redirect('admin')->with('message', 'Application Successfully Routed to Branch');
      // return response()->json(['status' => "Application Approved !"]);
     
      }
      else if ($declaration==3) { //IF REJECT
         // Set Time Log
          date_default_timezone_set("Asia/Kuala_Lumpur");
        $time_name = date('Ymd');
        $time_log = date('Y-m-d H:i:s');
        $log_download = new Log_download;
        
        $user = Auth::user();
        $id_user =  $user->id;
        $log_download->id_praapplication   =  $id;
        $log_download->id_user   =  $id_user;
        $log_download->Activity   =  'Application Rejected';
        $log_download->downloaded_at   =  $time_log;
        $log_download->log_remark   =  $verification_remark;
        // $log_download->log_reason  =  $reason;
        $log_download->save();
            $term = Term::where('id_praapplication', $id_praapplication)->update(['verification_status' => '0', 'verification_result' => $declaration, 'verification_remark' => $verification_remark, 'verified_by' => $user_aktif, 'id_branch' => $branch   ]);
            return response()->json(['status' => "Waiting User Response!"]);
          }
      else {
            $term = Term::where('id_praapplication', $id_praapplication)->update(['verification_status' => '0', 'verification_result' => $declaration, 'verification_remark' => $verification_remark, 'verified_by' => $user_aktif, 'id_branch' => $branch   ]);
            return response()->json(['status' => "Waiting Document!"]);
          }
     
     
          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
