<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Basic;
use App\Model\Log_download;
use App\Model\Blocked_ic;
use App\Model\Basic_v;
use App\Model\Empinfo;
use App\Model\Empinfo_v;
use App\Model\LoanAmmount;
use App\Model\LoanAmmount_v;
use App\Model\Contact;
use App\Model\Contact_v;
use App\Model\Loan;
use App\Model\Tenure;
use App\Model\Spouse;
use App\Model\Spouse_v;
use App\Model\Reference;
use App\Model\Reference_v;
use App\Model\Financial;
use App\Model\Financial_v;
use App\Model\Postcode;
use App\Model\Package;
use App\Model\PraApplication;
use App\Model\Employment;
use App\Model\Document;
use App\Model\Document_v;
use App\Model\Term;
use App\Model\Branch;
use App\Model\Settlement;
use App\Model\Credit;
use App\Model\Pep;
use App\Model\Commitments;
use App\Model\Foreigner;
use App\Model\Subscription;
use App\ScoreRating;
use App\ScoreCard;
use App\Dsr_a;
use App\Dsr_b;
use App\Dsr_c;
use App\Dsr_d;
use App\Dsr_e;
use App\RiskRating;
use App\Model\Country;
use App\Model\Occupations;
use App\Model\Position;
use App\Model\Relationship;
use App\Model\Title;
use Ramsey\Uuid\Uuid;
use Input;
use Mail;
use DateTime;
use App;
use PDF;
use DB;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\MOMBsc;
use App\Soap\Response\MOMBscResponse;
use App\Soap\Request\MOMRef;
use App\Soap\Response\MOMRefResponse;
use App\Soap\Response\MOMEmpInfResponse;
use App\Soap\Response\MOMSpResponse;
use App\Soap\Response\MOMCommitResponse;
use App\Soap\Response\MOMCallBscResponse;
use App\Soap\Response\MOMDocResponse;
use App\Soap\Response\MOMCTransResponse;
use App\Soap\Response\MOMFinResponse;
use App\Soap\Response\MOMDSRAResponse;
use App\Soap\Response\MOMDSRBResponse;
use App\Soap\Response\MOMDSRCResponse;
use App\Soap\Response\MOMDSREResponse;

use App\Soap\Response\MOMLnResponse;
use App\Soap\Response\MOMRskResponse;
use App\Soap\Response\MOMTrmResponse;

use OneSignal;
use Storage;
use File;
use App\Model\Waps;
use App\Model\CodeIDType;
use App\Model\CodeJobStatus;
use App\Model\CodeWorkSec;

class AdminController extends Controller
{
        private $testing;
        private $base_url;
        private $subject_t;
        protected $soapWrapper;

          /**
           * SoapController constructor.
           *
           * @param SoapWrapper $soapWrapper
           */
    public function __construct(SoapWrapper $soapWrapper)
    {
        $this->soapWrapper = $soapWrapper;
        $this->middleware('auth'); 
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function move(Request $request) 
    {
        //$user = Auth::user();
        $id_pra      = $request->input('id_praapplications');
        $ics      = $request->input('id_praapplications');

        $doc_7 = Document::latest('created_at')->where('id_praapplication', $id_pra)->where('type',7)->limit(1)->first();
        $doc_6 = Document::latest('created_at')->where('id_praapplication', $id_pra)->where('type',6)->limit(1)->first();
        $doc_9 = Document::latest('created_at')->where('id_praapplication', $id_pra)->where('type',9)->limit(1)->first();
        
        Storage::disk('ftp')->makeDirectory($ics, $mode = 0777, true, true);

        // foreach($documents as $val) {
              //$item = array_map(null, $documents->id,$documents->id,$documents->id,$documents->id);

             
              
                //$q = 'C:/xampp/htdocs/dataentry/dataentry/storage/app/public/731209120911/';
                //$new_path = public_path().'/documents/'.substr($val[0],10);
                //$file_local = public_path().'/uploads/file/'. $ics.'/'.$name;
               // $file_local = Storage::disk('local')->get($ics.'/'.$val->upload);
                //$file_sftp = Storage::disk('sftp')->put($ics.'/'.$val->upload, $file_local);



               //  $move = Storage::copy($file_local, $file_sftp);

                //Storage::disk('sftp')->makeDirectory($ics, $mode = 0777, true, true);
             
           /*$full_path_source7 = Storage::disk('local')->get($ics.'/'.$doc_7->upload);
            $full_path_dest7 = Storage::disk('sftp')->put($ics.'/'.$doc_7->upload, $full_path_source7);
            File::move($full_path_source7, $full_path_dest7);

             $full_path_source6 = Storage::disk('local')->get($ics.'/'.$doc_6->upload);
            $full_path_dest6 = Storage::disk('sftp')->put($ics.'/'.$doc_6->upload, $full_path_source6);
            File::move($full_path_source6, $full_path_dest6);

              $full_path_source9 = Storage::disk('local')->get($ics.'/'.$doc_9->upload);
            $full_path_dest9 = Storage::disk('sftp')->put($ics.'/'.$doc_9->upload, $full_path_source9);
            File::move($full_path_source9, $full_path_dest9);*/

           
            

           // $file_local9 = Storage::disk('local')->get($ics.'/'.$doc_9->upload);
            $file_local9 = public_path().'/storage/uploads/file/'. $ics.'/'.$doc_9->upload;
            $file_sftp9 = Storage::disk('ftp')->put($ics.'/'.$doc_9->upload, $file_local9);

            // if (!(file_exists($file_sftp9))) {  
                $move9 = Storage::move($file_local9, $file_sftp9);

            //} 
           

           
            /*$file_local6 = Storage::disk('local')->get($ics.'/'.$doc_6->upload);
            $file_sftp6 = Storage::disk('sftp')->put($ics.'/'.$doc_6->upload, $file_local6);

             if (!(file_exists($file_sftp6))) {  
                $move6 = Storage::move($file_local6, $file_sftp6);
            }  


            $file_local7 = Storage::disk('local')->get($ics.'/'.$doc_7->upload);
            $file_sftp7 = Storage::disk('sftp')->put($ics.'/'.$doc_7->upload, $file_local7);

             if (!(file_exists($file_sftp7))) {   
               $move7 = Storage::move($file_local7, $file_sftp7);
            }*/
            
              // }
            
              
        
           
          /*$new_path = public_path().'/documents/'.$file;
        $old_path = public_path().'/process_folder/'. $refno.'/'.$file;
        $move = File::move($new_path, $old_path);*/

        return redirect('/admin')->with('message', 'success!!');
    }

    public function index()
    {
        $user = Auth::user();
      if ($user->role==7){
          return redirect('home');
      }

        if($user->role=='1') {
                // IF admin Step 2 (tugasnya Routed to Branch)
                $terma = Term::where('status', '1')->wherein('verification_result', ['1','2','3'])->orderBy('file_created','DESC')->get();
                $blocked_ic = Blocked_ic::get();
                return view('admin.index', compact('user','terma','blocked_ic'));   
         }
          else if($user->role=='18') {
                // IF admin Step 2 (tugasnya Routed to Branch)
                $terma = Term::where('status', '1')->wherein('verification_result', ['1','2','3'])->orderBy('file_created','DESC')->get();
                $blocked_ic = Blocked_ic::get();
                return view('admin.index', compact('user','terma','blocked_ic'));   
         }
         else if ($user->role=='2') {
               // If Branch
               
               
                $log_download2 = Log_download::where('id_user',$user->id)->get();
                $last_download = Log_download::where('id_user', $user->id)->max('id');
                $terma = Term::where('status', '1')->wherein('verification_result', ['2','5'])->where('id_branch',$user->id_branch)->orderBy('file_created','DESC')->get();
                $blocked_ic = Blocked_ic::get();
                return view('branch.index', compact('user','log_download2','terma','blocked_ic'));   
         }
         else if($user->role=='3') {
                // IF HQ User
                $terma = Term::where('status', '1')->wherein('verification_result', ['2','5'])->orderBy('file_created','DESC')->get();
                $blocked_ic = Blocked_ic::get();
                return view('hq.index', compact('user','terma','blocked_ic'));   
         }
         else if($user->role=='4') {
                 // IF Admin Step 1 (tugasnya verifikasi doang)
               // $terma = Term::wherein('status', ['1','99','77'])->wherein('verification_result', ['0','1','2'])->where('verification_result_by_bank','!=','2')->orderBy('file_created','DESC')->with('praapplication.user')->where("referral_id","0")->get();
            $terma = Term::wherein('status', ['1','99','77'])->wherein('verification_result', ['0','1','2'])->where('verification_result_by_bank','!=','2')->orderBy('file_created','DESC')->with('praapplication.user')->get();

                 // IF Admin Step 1 (tugasnya verifikasi doang)
              //  $termas = Term::wherein('status', ['1','99','77'])->wherein('verification_result', ['0','1','2'])->where('verification_result_by_bank','!=','2')->orderBy('file_created','DESC')->with('praapplication.user')->where("referral_id","0")->get();
            $termas = Term::wherein('status', ['1','99','77'])->wherein('verification_result', ['0','1','2'])->where('verification_result_by_bank','!=','2')->orderBy('file_created','DESC')->with('praapplication.user')->get();
                // $terma = Term::wherein('status', ['1','99','77'])->wherein('verification_result', ['0','1','2'])->where('verification_result_by_bank','!=','1')->where('verification_result_by_bank','!=','2')->orderBy('file_created','DESC')->with('praapplication.user')->where("referral_id","0")->get();
                /*$terma = Term::where("referral_id","0")->wherein('status', ['1','99','77'])->wherein('verification_result', ['0','1','2'])->where('verification_result_by_bank','!=','1')->where('verification_result_by_bank','!=','2')->orderBy('file_created','DESC')->with('praapplication.user')->get();
                $blocked_ic = Blocked_ic::get();*/
                $soap_Wrapper = new SoapWrapper;
              
                 $this->soapWrapper->add('cds', function ($service) {
                  $service
                    //->wsdl('http://219.92.49.250/MOMWS/MOMCallWS.asmx?WSDL');
                  ->wsdl('http://moaqs.com/MOMWS/MOMCallWS.asmx?WSDL');
                });

                 //http://192.168.0.123/MOMWS/MOMCallWS.asmx?WSDL
                foreach ($termas as $termas) {
                     $param=array(
                            'AppNo'   => $termas->id_praapplication,
                            'txnCode' => 'ST003',
                            'ActCode' => 'R',
                            'usr' => 'momwapsuat',
                            'pwd' => 'J@ctL71N$#'
                        );
                      $response = $this->soapWrapper->call('cds.MOMCallStat',array($param));

                /*foreach ($response as $key => $object) {
                            $ApvIns = $object->ApvIns;
                            $ApvAm = $object->ApvAm;
                        }*/

                   /* foreach( $response as $row )
                    {
                        var_dump( $row->ApvStat, $row->ApvAm );
                    }

                }*/
                $smsMessages = is_array( $response->MOMCallStatResult)
            ? $response->MOMCallStatResult
            : array( $response->MOMCallStatResult );

                foreach ( $smsMessages as $smsMessage )
                {
                   // echo "<br>$smsMessage->ApvIns";
                     $validate           = Waps::latest('created_at')->where('id_praapplication', $termas->id_praapplication)->count();
                     $result=$smsMessage->ApvStat;
                      if((($result=='DECL')||($result=='APVA')|| ($result=='REJM') || ($result=='ACCP') ) && ($validate == 0) && ($termas->verification_status == 1) && ($termas->status == 1)  ){
                       // if($result=='DECL'){
                        //if($termas->status_waps=='0'){
                     
        
                   
                        $waps = new Waps;
                        $waps->id_praapplication   =  $smsMessage->AppNo;
                        $waps->ic =$smsMessage->IdNo;
                        $waps->name =  $smsMessage->Nme;
                         $waps->AppvAmt =$smsMessage->ApvAm;
                        $waps->AppvInst =  $smsMessage->ApvIns;
                         $waps->AppvRate =$smsMessage->ApvRt;
                        $waps->AppvDt =  $smsMessage->ApvDt;
                        $waps->AppvTen =  $smsMessage->Tnr;
                        $waps->AppvStat =  $result;
                        $waps->save();
                        
                        $term_update = Term::where('id_praapplication', $termas->id_praapplication)->update(['verification_result_by_bank' => '1' ,'status_waps'=>'1']);
                            Mail::send('mail.status_waps', compact('fullname'), function ($message) use ($termas) {
                            $message->from('novrizaleka@gmail.com', 'Personal loan Co-Opbank MBSB');
                            $message->subject('Loan has Approved');
                            $message->to($termas->email, $termas->fullname);
                            }); 
                        
                             OneSignal::sendNotificationToAll("New Status", $url = 'http://mbsb.insko.my/onesignal/public/admin/detail_pra/'.$termas->id_praapplication, $data = null);
                            }
                        //}
                    
                   

                }
             }

                return view('admin2.index', compact('user','terma','blocked_ic','reminders','response','result','smsMessages','smsMessage','soap_Wrapper'));  
         }
       else if($user->role=='8') {
                 // IF Admin Step 1 (tugasnya verifikasi doang)
                $terma = Term::wherein('status', ['1','99','77'])->wherein('verification_result', ['0','1','2'])->where('verification_result_by_bank','!=','2')->orderBy('file_created','DESC')->with('praapplication.user')->where("referral_id","0")->get();

                 // IF Admin Step 1 (tugasnya verifikasi doang)
                $termas = Term::wherein('status', ['1','99','77'])->wherein('verification_result', ['0','1','2'])->where('verification_result_by_bank','!=','2')->orderBy('file_created','DESC')->with('praapplication.user')->where("referral_id","0")->get();
                // $terma = Term::wherein('status', ['1','99','77'])->wherein('verification_result', ['0','1','2'])->where('verification_result_by_bank','!=','1')->where('verification_result_by_bank','!=','2')->orderBy('file_created','DESC')->with('praapplication.user')->where("referral_id","0")->get();
                /*$terma = Term::where("referral_id","0")->wherein('status', ['1','99','77'])->wherein('verification_result', ['0','1','2'])->where('verification_result_by_bank','!=','1')->where('verification_result_by_bank','!=','2')->orderBy('file_created','DESC')->with('praapplication.user')->get();
                $blocked_ic = Blocked_ic::get();*/
                $soap_Wrapper = new SoapWrapper;
              
                 $this->soapWrapper->add('cds', function ($service) {
                  $service
                    //->wsdl('http://219.92.49.250/MOMWS/MOMCallWS.asmx?WSDL');
                  ->wsdl('http://moaqs.com/MOMWS/MOMCallWS.asmx?WSDL');
                });

                 //http://192.168.0.123/MOMWS/MOMCallWS.asmx?WSDL
                foreach ($termas as $termas) {
                     $param=array(
                            'AppNo'   => $termas->id_praapplication,
                            'txnCode' => 'ST003',
                            'ActCode' => 'R',
                            'usr' => 'momwapsuat',
                            'pwd' => 'J@ctL71N$#'
                        );
                      $response = $this->soapWrapper->call('cds.MOMCallStat',array($param));

                /*foreach ($response as $key => $object) {
                            $ApvIns = $object->ApvIns;
                            $ApvAm = $object->ApvAm;
                        }*/

                   /* foreach( $response as $row )
                    {
                        var_dump( $row->ApvStat, $row->ApvAm );
                    }

                }*/
                $smsMessages = is_array( $response->MOMCallStatResult)
            ? $response->MOMCallStatResult
            : array( $response->MOMCallStatResult );

                foreach ( $smsMessages as $smsMessage )
                {
                   // echo "<br>$smsMessage->ApvIns";
                     $validate           = Waps::latest('created_at')->where('id_praapplication', $termas->id_praapplication)->count();
                     $result=$smsMessage->ApvStat;
                      if((($result=='DECL')||($result=='APVA')|| ($result=='REJM') || ($result=='ACCP') ) && ($validate == 0) && ($termas->verification_status == 1) && ($termas->status == 1)  ){
                       // if($result=='DECL'){
                        //if($termas->status_waps=='0'){
                     
        
                   
                        $waps = new Waps;
                        $waps->id_praapplication   =  $smsMessage->AppNo;
                        $waps->ic =$smsMessage->IdNo;
                        $waps->name =  $smsMessage->Nme;
                         $waps->AppvAmt =$smsMessage->ApvAm;
                        $waps->AppvInst =  $smsMessage->ApvIns;
                         $waps->AppvRate =$smsMessage->ApvRt;
                        $waps->AppvDt =  $smsMessage->ApvDt;
                        $waps->AppvTen =  $smsMessage->Tnr;
                        $waps->AppvStat =  $result;
                        $waps->save();
                        
                        $term_update = Term::where('id_praapplication', $termas->id_praapplication)->update(['verification_result_by_bank' => '1' ,'status_waps'=>'1']);
                            Mail::send('mail.status_waps', compact('fullname'), function ($message) use ($termas) {
                            $message->from('novrizaleka@gmail.com', 'Personal loan Co-Opbank MBSB');
                            $message->subject('Loan has Approved');
                            $message->to($termas->email, $termas->fullname);
                            }); 
                        
                             OneSignal::sendNotificationToAll("New Status", $url = 'http://mbsb.insko.my/onesignal/public/admin/detail_pra/'.$termas->id_praapplication, $data = null);
                            }
                        //}
                    
                   

                }
             }

                return view('admin2.index', compact('user','terma','blocked_ic','reminders','response','result','smsMessages','smsMessage','soap_Wrapper'));  
         }
         
         else if($user->role=='5') {
            $terma = Term::where('referral_id',$user->id)->wherein('status', ['0','1','99','88','77'])->where('mo_stage','>=','0')->orderBy('file_created','DESC')->get();   
            $termas = Term::where('referral_id',$user->id)->wherein('status', ['0','1','99','88','77'])->where('mo_stage','>=','0')->orderBy('file_created','DESC')->get();   
                // IF USERS == AGENT
              $soap_Wrapper = new SoapWrapper;
              
                 $this->soapWrapper->add('cds', function ($service) {
                  $service
                    //->wsdl('http://219.92.49.250/MOMWS/MOMCallWS.asmx?WSDL');
                  ->wsdl('http://moaqs.com/MOMWS/MOMCallWS.asmx?WSDL');
                });
                foreach ($termas as $termas) {
                    $userId= '70b3f22c-db26-4d05-be06-2604778bafc2';
                   /* if($termas->referral_id==$user->id){
                       OneSignal::sendNotificationToAll("New Status for a ",   $userId, $url = 'http://localhost:8080/onesignal/public/admin/detail_pra/'.$termas->id_praapplication, $data = null);
                   }*/
                     $param=array(
                            'AppNo'   => $termas->id_praapplication,
                            'txnCode' => 'ST003',
                            'ActCode' => 'R',
                            'usr' => 'momwapsuat',
                            'pwd' => 'J@ctL71N$#'
                        );
                      $response = $this->soapWrapper->call('cds.MOMCallStat',array($param));

                /*foreach ($response as $key => $object) {
                            $ApvIns = $object->ApvIns;
                            $ApvAm = $object->ApvAm;
                        }*/

                   /* foreach( $response as $row )
                    {
                        var_dump( $row->ApvStat, $row->ApvAm );
                    }

                }*/
                $smsMessages = is_array( $response->MOMCallStatResult)
            ? $response->MOMCallStatResult
            : array( $response->MOMCallStatResult );

                foreach ( $smsMessages as $smsMessage )
                {
                   // echo "<br>$smsMessage->ApvIns";
                    $result=$smsMessage->ApvStat;



                     $validate           = Waps::latest('created_at')->where('id_praapplication', $termas->id_praapplication)->count();

                      if((($result=='DECL')||($result=='APVA')|| ($result=='REJM') || ($result=='ACCP') ) && ($validate == 0) && ($termas->verification_status == 1) && ($termas->status == 1)  ){
                       // if($result=='DECL'){
                        //if($termas->status_waps=='0'){
                     
        
                   
                        $waps = new Waps;
                        $waps->id_praapplication   =  $smsMessage->AppNo;
                        $waps->ic =$smsMessage->IdNo;
                        $waps->name =  $smsMessage->Nme;
                         $waps->AppvAmt =$smsMessage->ApvAm;
                        $waps->AppvInst =  $smsMessage->ApvIns;
                         $waps->AppvRate =$smsMessage->ApvRt;
                        $waps->AppvDt =  $smsMessage->ApvDt;
                        $waps->AppvTen =  $smsMessage->Tnr;
                        $waps->AppvStat =  $result;
                        $waps->save();
                        
                        $term_update = Term::where('id_praapplication', $termas->id_praapplication)->update(['verification_result_by_bank' => '1' ,'status_waps'=>'1']);
                            Mail::send('mail.status_waps', compact('fullname'), function ($message) use ($termas) {
                            $message->from('novrizaleka@gmail.com', 'Personal loan Co-Opbank MBSB');
                            $message->subject('Loan has Approved');
                            $message->to($termas->email);
                            }); 
                        
                             //OneSignal::sendNotificationToAll("New Status", $url = 'http://localhost:8080/onesignal/public/admin', $data = null);

                             //admin/detail_pra
                            OneSignal::sendNotificationToAll("New Status for ".$smsMessage->IdNo, $url = 'http://localhost:8080/onesignal/public/admin/detail_pra/'.$termas->id_praapplication, $data = null);
                            }
                        //}
                    
                    /* if($result=='0'){


                     

                     if($termas->status_waps=='0'){
                        $term_update = Term::where('id_praapplication', $termas->id_praapplication)->update(['verification_result_by_bank' => '1' ,'status_waps'=>'1']);
                          Mail::send('mail.status_waps', compact('fullname'), function ($message) use ($termas) {
                          $message->from('novrizaleka@gmail.com', 'Personal loan Co-Opbank MBSB');
                          $message->subject('Loan has Approved');
                          $message->to($termas->email, $termas->fullname);
                           }); 
                      }
                     }*/
                     /*
                     if(($result=='0') AND ($termas->status_waps=='0'))
                     {
                        $term_update = Term::where('id_praapplication', $termas->id_praapplication)->update(['verification_result_by_bank' => '1' ,'status_waps'=>'1']);
                          Mail::send('mail.status_waps', compact('fullname'), function ($message) use ($termas) {
                          $message->from('novrizaleka@gmail.com', 'Personal loan Co-Opbank MBSB');
                          $message->subject('Loan has Approved');
                          $message->to($termas->email, $termas->fullname);
                           }); 
                      
                     }

                    else if(($result=='1') AND ($termas->status_waps=='1'))

                    {
                        $term_update = Term::where('id_praapplication', $termas->id_praapplication)->update(['verification_result_by_bank' => '1' ,'status_waps'=>'2']);
                          Mail::send('mail.status_waps', compact('fullname'), function ($message) use ($termas) {
                          $message->from('novrizaleka@gmail.com', 'Personal loan Co-Opbank MBSB');
                          $message->subject('Loan has Approved');
                          $message->to($termas->email, $termas->fullname);
                           }); 
                      }
                     
                     else if(($result=='2') AND ($termas->status_waps=='1'))

                    {
                        $term_update = Term::where('id_praapplication', $termas->id_praapplication)->update(['verification_result_by_bank' => '1' ,'status_waps'=>'1']);
                          Mail::send('mail.status_waps', compact('fullname'), function ($message) use ($termas) {
                          $message->from('novrizaleka@gmail.com', 'Personal loan Co-Opbank MBSB');
                          $message->subject('Loan has Approved');
                          $message->to($termas->email, $termas->fullname);
                           }); 
                      }*/
                     

                   

                }
             }

                 
                return view('mo.index', compact('user','terma','response','result','smsMessages','smsMessage','soap_Wrapper'));  
        }
        else if($user->role=='10') {
            $terma = Term::where("referral_id","!=","0")->wherein('status', ['0','1','99','88','77'])->where('mo_stage','>=','1')->orderBy('file_created','DESC')->get();
 $user = Auth::user();
            return view('admin_mbsb.index', compact('user','terma'));
         }

    }

    public function direct_application() {
        $user = Auth::user();
         $terma = Term::where("referral_id","0")->wherein('status', ['1','99','77'])->wherein('verification_result', ['0','1','2'])->where('verification_result_by_bank','!=','1')->where('verification_result_by_bank','!=','2')->orderBy('file_created','DESC')->with('praapplication.user')->get();
          return view('admin_mbsb.direct_application', compact('user','terma')); 
    }

    public function stats() {
         $user = Auth::user();
         $terma = Term::where("referral_id","0")->wherein('status', ['1','99','77','88'])->wherein('verification_result', ['0','1','2','3'])->orderBy('file_created','DESC')->get();

         return view('admin2.stats', compact('user','terma'));  
    }

    public function activities($id_pra) {
         $user = Auth::user();
         $terma = Term::where('id_praapplication',$id_pra)->wherein('status', ['0','1','99','77','88'])->wherein('verification_result', ['0','1','2','3'])->orderBy('file_created','DESC')->with('praapplication.user')->get();

         return view('admin2.activities', compact('user','terma'));  
    }

     public function get_document($id_pra,$name) {
         $user = Auth::user();
         $terma = Term::where('id_praapplication',$id_pra)->wherein('status', ['0','1','99','77','88'])->wherein('verification_result', ['0','1','2','3'])->orderBy('file_created','DESC')->with('praapplication.user')->get();

         $doc = Document::where('id_praapplication',$id_pra)->where('upload',$name)->limit('1')->first();

         return view('admin2.get_document', compact('user','terma','doc','name'));  
    }
    
        
     public function step1_verification($id_pra, $view=FALSE) {
        $user = Auth::user();
        if ($user->role==1 AND $user->role==4 ) {
                    return redirect('admin');   
        }
        else if ($user->role==0) {
                    return redirect('home');    
        }
        $email =  $user->email;
       
       $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
       
    //$id_pra = $pra->id;

        $status = Term::where('id_praapplication', $id_pra)->first()->status;
        $verification_remark = Term::where('id_praapplication', $id_pra)->first()->verification_remark;
        $basic_v = Basic_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $contact_v = Contact_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $empinfo_v = Empinfo_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount_v = LoanAmmount_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $spouse_v = Spouse_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference_v = Reference_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $financial_v = Financial_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $basic = Basic::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first(); 
        $name = strtoupper($basic->name);
        $employment = Employment::get();
        $document1 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
        $document2 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '2' )->first();
        $document3 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '3' )->first();
        $document4 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '4' )->first();
        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
         $document10 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '10' )->first();
         $document11 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '11' )->first();
        //$customer = User::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();         
        $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        
        $id_type= $empinfo->first()->employment_id;
        $total_salary = $empinfo->first()->salary + $empinfo->first()->allowance ;
         $total_salary = $empinfo->first()->salary + $empinfo->first()->allowance  ;
          $zbasicsalary = $pra->basicsalary + $pra->allowance;
        $zdeduction =  $pra->deduction;
        
        $loan = Loan::where('id_type',$id_type)
                  ->where('min_salary','<=',$total_salary)
                  ->where('max_salary','>=',$total_salary)->limit('1')->get();
        
        $id_loan= $loan->first()->id;  
        if($basic->id_type==1){
            $icnumber= $basic->new_ic;
            $tanggal = substr($icnumber,4, 2);
            $bulan =  substr($icnumber,2, 2);
            $tahun = substr($icnumber,0, 2); 
             $jantina = substr($icnumber,10, 2); 
            if($tahun > 30) {

                $tahun2 = "19".$tahun;
            }
            else {
                 $tahun2 = "20".$tahun;

            }
           
            $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 
            $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
            $oDateNow = new DateTime();
            $oDateBirth = new DateTime($lahir);
            $oDateIntervall = $oDateNow->diff($oDateBirth);

            $umur = 61 - $oDateIntervall->y;
            $statusx = 1;

            $birthday = $tahun2.'-'.$bulan.'-'.$tanggal; 
              // Convert Ke Date Time
            $biday = new DateTime($birthday);
            $today = new DateTime();
            
            $diff = $today->diff($biday);
            $diff_age = $diff->y;

             $dobirth = $tahun2.'-'.$bulan.'-'.$tanggal;
            $today = date("Y-m-d");
            $diff = date_diff(date_create($dobirth), date_create($today));
            $usia = $diff->format('%y');
        }
        else{
            $icnumber= $basic->dob;
            $tanggal = substr($icnumber,0, 2);
                $bulan =  substr($icnumber,3, 2);
                $tahun = substr($icnumber,8, 2); 
             $jantina = $basic->gender; 
            if($tahun > 30) {

                $tahun2 = "19".$tahun;
            }
            else {
                 $tahun2 = "20".$tahun;

            }
           
            $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 
            $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
            $oDateNow = new DateTime();
            $oDateBirth = new DateTime($lahir);
            $oDateIntervall = $oDateNow->diff($oDateBirth);

            $umur = 61 - $oDateIntervall->y;
            $statusx = 1;

            $birthday = $basic->dob; 
              // Convert Ke Date Time
            $biday = new DateTime($birthday);
            $today = new DateTime();
            
            $diff = $today->diff($biday);
            $diff_age = $diff->y;

             $today = date("Y-m-d");
            $diff = date_diff(date_create($birthday), date_create($today));
            $usia = $diff->format('%y');
        }
         
   
        $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->get();   
        
        $contact = Contact::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->get();
        $data = Basic::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

        $spouse = Spouse::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference = Reference::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $financial = Financial::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $settlement = Settlement::latest('created_at')->where('id_praapplication',  $id_pra )->get();


        $term = Term::where('id_praapplication',  $id_pra )->first(); //term
        $branch = Branch::latest('created_at')->where('status',  '1' )->get();
        $cal_bulan = $financial->first()->cal_bulan;
        if ($cal_bulan=='0000-00-00') {
         $cal_bulan = '';   
        }
        else {
         $cal_bulan = date('F Y', strtotime($cal_bulan));   
        }
        
        $dsr_a = Dsr_a::where('id_praapplication',$id_pra)->first();
        $dsr_b = Dsr_b::where('id_praapplication',$id_pra)->first();
        $dsr_c = Dsr_c::where('id_praapplication',$id_pra)->first();
        $dsr_d = Dsr_d::where('id_praapplication',$id_pra)->first();
        $dsr_e = Dsr_e::where('id_praapplication',$id_pra)->first();
        $riskrating = RiskRating::where('id_praapplication',$id_pra)->first();

        $sector = $dsr_a->sector;
        $salary_deduction = ScoreRating::where('sector',$sector)->where('variable','Salary Deduction')->get();
        $age = ScoreRating::where('sector',$sector)->where('variable','Age')->get();
        $edu_level = ScoreRating::where('sector',$sector)->where('variable','Education Level')->get();
        $emp = ScoreRating::where('sector',$sector)->where('variable','Employment')->get();
        $position = ScoreRating::where('sector',$sector)->where('variable','Position')->get();
        $marital = ScoreRating::where('sector',$sector)->where('variable','Marital Status')->get();
        $spouse_emp = ScoreRating::where('sector',$sector)->where('variable','Spouse Employment')->get();
        $property_loan = ScoreRating::where('sector',$sector)->where('variable','Property Loan Remaining')->get();
        $adverse_ccris = ScoreRating::where('sector',$sector)->where('variable','Adverse CCRIS Parameters')->get();
        if ($view=='verify') {
            
            return view('admin.step1_verification', compact('settlement','cal_bulan','name','view','loancal','user','branch','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary',
                                                        'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','statusx','document10','document11','zdeduction','zbasicsalary','salary_deduction','age','edu_level','emp','position','marital','spouse_emp','property_loan','adverse_ccris','dsr_a','dsr_b','dsr_c','dsr_d','dsr_e','riskrating','diff_age','usia'));        
               
        }
        else if ($view=='view') {
            
             return view('admin.step1_verification', compact('codesettlepty','settlement','cal_bulan','name','view','user','branch','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary',
                                                         'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','statusx','document10','document11','zdeduction','zbasicsalary','salary_deduction','age','edu_level','emp','position','marital','spouse_emp','property_loan','adverse_ccris','dsr_a','dsr_b','dsr_c','dsr_d','dsr_e','riskrating','diff_age','usia'));         
            
        }
        
       
              
    }

    public function scoring(Request $request) {
        $user = Auth::user();

  

          $dsr = ScoreRating::where('variable','DSR')->where('sector',$request->sector)
                  ->where('float_value','<=',$request->dsr)->where('float_value2','>=',$request->dsr)->first();

          $dsr_score = $dsr->score;

          $ndi = $request->ndi;

          if($ndi>=0 && $ndi<=499.99) {
            $ndi_score=0;
          }
          else if($ndi>=500.00 && $ndi<=1000.00) {
            $ndi_score=20;
          
          }
          else if($ndi>=1000.01) {
            $ndi_score=40;
          
          }


          $salary_deduction = ScoreRating::where('variable','Salary Deduction')->where('sector',$request->sector)
                  ->where('id',$request->salary_deduction)->first();
          $salded_score = $salary_deduction->score;

          $age = ScoreRating::where('variable','Age')->where('sector',$request->sector)
                  ->where('id',$request->age)->first();
          $age_score = $age->score;

          $edu_level = ScoreRating::where('variable','Education Level')->where('sector',$request->sector)
                  ->where('id',$request->edu_level)->first();
          $edu_score = $edu_level->score;

          $emp = ScoreRating::where('variable','Employment')->where('sector',$request->sector)
                  ->where('id',$request->emp)->first();
          $emp_score = $emp->score;

          $position = ScoreRating::where('variable','Position')->where('sector',$request->sector)
                  ->where('id',$request->position)->first();
          $position_score = $position->score;

          $marital = ScoreRating::where('variable','Marital Status')->where('sector',$request->sector)
                  ->where('id',$request->marital)->first();
          $marital_score = $marital->score;

          $spouse_emp = ScoreRating::where('variable','Spouse Employment')->where('sector',$request->sector)
                  ->where('id',$request->spouse_emp)->first();
          $spouse_score = $spouse_emp->score;

          $property_loan = ScoreRating::where('variable','Property Loan Remaining')->where('sector',$request->sector)
                  ->where('id',$request->property_loan)->first();
          $property_score = $property_loan->score;

          $adverse_ccris = ScoreRating::where('variable','Adverse CCRIS Parameters')->where('sector',$request->sector)
                  ->where('id',$request->adverse_ccris)->first();
          $ccris_decision = $adverse_ccris->float_value;
          $ccris_score = $adverse_ccris->score;

          $total_score = $dsr_score + $ndi_score + $salded_score + $age_score + $edu_score + $emp_score + $position_score + $marital_score + $spouse_score + $property_score + $ccris_score;

          $scorecard = $this->grading($total_score);
          $grading = $scorecard->grade;
          $decision = $scorecard->decision;

          if($request->decision=="FAIL. New BIRO Customer, Gross Income < RM3,000.") {
               $final_decision = "FAIL. New BIRO Customer, Gross Income < RM3,000."; 
          }
          else if($request->decision=="FAIL. New BIRO Customer, NDI < RM1,300.") {
               $final_decision = "FAIL. New BIRO Customer, NDI < RM1,300."; 
          }
          else if($request->decision=="FAIL. Existing BIRO Customer, Gross Income < RM1,500.") {
               $final_decision = "FAIL. Existing BIRO Customer, Gross Income < RM1,500."; 
          }
          else if($request->decision=="FAIL. Existing BIRO Customer, NDI < RM1,000.") {
               $final_decision = "FAIL. Existing BIRO Customer, NDI < RM1,000."; 
          }
          else if($request->decision=="FAIL. Non-Biro Customer, Gross Income < RM3,000.") {
               $final_decision = "FAIL. Non-Biro Customer, Gross Income < RM3,000."; 
          }
          else if($request->decision=="FAIL. Non-Biro Customer, NDI < RM1,300.") {
               $final_decision = "FAIL. Non-Biro Customer, NDI < RM1,300."; 
          }

          
          else {

            if($ccris_decision ==1.0000) {
              $final_decision = "REJECT";  
            }
            else {
               $final_decision = $decision;  
            }

          }

           return response()->json([
              'ccris_decision' => $ccris_decision,
              'dsr_score' => $dsr_score, 
              'ndi_score' => $ndi_score, 
              'salded_score' => $salded_score, 
              'age_score' => $age_score,
              'edu_score' => $edu_score,
              'emp_score' => $emp_score,
              'position_score' => $position_score,
              'marital_score' => $marital_score,
              'spouse_score' => $spouse_score,
              'property_score' => $property_score,
              'ccris_score' => $ccris_score,
              'total_score' => $total_score,
              'grading' => $grading,
              'final_decision' => $final_decision
              ]);  
        
        
    }

    public function grading($total_score) {
        $grading = ScoreCard::where('score','<=',$total_score)->where('score2','>=',$total_score)->first();

        return $grading;

    }

    public function sector(Request $request) {
        
        $dsr_a = Dsr_a::where('id_praapplication', $request->id_praapplication)
          ->update(['sector' => $request->sector]);

    }


    public function reject()
    {
        $user = Auth::user();
        if($user->role=='1' || $user->role=='8') {
            
              //  $terma = Term::where('status', '1')->where('verification_result', '3')->orWhere('verification_result_by_bank','2')->orderBy('file_created','DESC')->get();
                 $terma = Term::where('verification_result', '3')->orWhere('verification_result_by_bank','2')->where('referral_id','0')->orderBy('file_created','DESC')->get();
                $blocked_ic = Blocked_ic::get();
      
          return view('admin.reject', compact('user','terma','blocked_ic'));   
         }
     
    }

    public function approved()
    {
        $user = Auth::user();
        if($user->role=='1' || $user->role=='8') {
            
                 $terma = Term::where('verification_result','2')->where('referral_id','0')->orderBy('file_created','DESC')->get();

            
                return view('admin.approved', compact('user','terma','blocked_ic'));   
         }
     
    }
  
  public function docsreject()
    {
        $user = Auth::user();
        if($user->role=='1' || $user->role=='8') {
              
                $terma = Term::where('status', '88')->where('referral_id','0')->orderBy('file_created','DESC')->get();
                $blocked_ic = Blocked_ic::get();
      
          return view('admin.docsreject', compact('user','terma','blocked_ic'));   
         }
     
    }
 
    
    public function user_view($id_pra) {
         $user = Auth::user();
        
    }
	
    public function routeback_status()
    {
        $user = Auth::user();
		if($user->role=='1') {
				// IF admin Step 2 (tugasnya Routed to Branch)
				
				$terma = Term::where('status', '0')->where('edit', '0')->where('verification_result', '4')->orderBy('file_created','DESC')->get();
				$blocked_ic = Blocked_ic::get();
				return view('admin2.routeback_status', compact('user','terma','blocked_ic')); 	  
		 }

		 else if($user->role=='4') {
				// IF Admin Step 1 (tugasnya verifikasi doang)
				$terma = Term::where('status', '0')->where('edit', '0')->where('verification_result', '4')->orderBy('file_created','DESC')->get();
				$blocked_ic = Blocked_ic::get();
				return view('admin2.routeback_status', compact('user','terma','blocked_ic')); 	
		 }
    }
	

 public function detail($id_pra, $view=FALSE) {
        $user = Auth::user();
        if ($user->role==2 AND $user->role==3 ) {
                    return redirect('admin');   
        }
        else if ($user->role==0) {
                    return redirect('home');    
        }
        $email =  $user->email;
       
       $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
    //$id_pra = $pra->id;

        $status = Term::where('id_praapplication', $id_pra)->first()->status;
        $verification_result = Term::where('id_praapplication', $id_pra)->first()->verification_result;
        $verification_remark = Term::where('id_praapplication', $id_pra)->first()->verification_remark;
        $basic_v = Basic_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $contact_v = Contact_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $empinfo_v = Empinfo_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount_v = LoanAmmount_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $spouse_v = Spouse_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference_v = Reference_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $financial_v = Financial_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $basic = Basic::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first(); 
        $name = strtoupper($basic->name);
        $employment = Employment::get();
        $document1 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
        $document2 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '2' )->first();
        $document3 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '3' )->first();
        $document4 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '4' )->first();
        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
         $document10 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '10' )->first();
         $document11 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '11' )->first();
        //$customer = User::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();         
        $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
          $dsr_b = Dsr_b::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        
        $id_type= $empinfo->first()->employment_id;
        $total_salary = $empinfo->first()->salary + $empinfo->first()->allowance ;
        
        $loan = Loan::where('id_type',$id_type)
                  ->where('min_salary','<=',$total_salary)
                  ->where('max_salary','>=',$total_salary)->limit('1')->get();
        
        $id_loan= $loan->first()->id;  
        
         $icnumber= $pra->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;
        $statusx = 1;

   
          $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->get();   
        
        $contact = Contact::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->get();
        $data = Basic::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

        $spouse = Spouse::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference = Reference::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $financial = Financial::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

         $credit = Credit::where('id_praapplication', $id_pra)->first(); 
           $pep = Pep::where('id_praapplication', $id_pra)->first(); 
           $commitments = Commitments::where('id_praapplication', $id_pra)->first(); 

    $cal_bulan = $financial->first()->cal_bulan;
    if ($cal_bulan=='0000-00-00') {
     $cal_bulan = ''; 
    }
    else {
     $cal_bulan = date('F Y', strtotime($cal_bulan)); 
    }
    
    
        $term = Term::where('id_praapplication',  $id_pra )->first(); //term
        $branch = Branch::latest('created_at')->where('status',  '1' )->get();

        $today = date("d/m/Y");
          $country = Country::get(); //term
          $country2 = Country::get();
        $occupation = Occupations::get();
        $nature_business = Occupations::get();
        $position = Position::get();
        $relationship = Relationship::get();
        $relationship2 = Relationship::get();
        $title = Title::get();
        $nationality = Country::get();
        $empstatus = CodeJobStatus::get();
        $foreigner      = Foreigner::where('id_praapplication',$id_pra)->first();
            $subs           = Subscription::where('id_praapplication',$id_pra)->first();
         if($status >= 1) {
            return view('admin.user_detail', compact('view','name','cal_bulan','user','branch','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary',
                                                     'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','statusx','document10','document11','today','credit','pep','commitments','country','occupation','position','relationship','relationship2','title','dsr_b','nature_business','nationality','empstatus','foreigner','subs','country2'));        
                       
        }
        elseif(($status == 0) AND ($verification_result == 4))  {
              return view('admin.user_detail', compact('view','name','cal_bulan','user','branch','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary',
                                                     'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','statusx','document10','document11','today','credit','pep','commitments','country','occupation','position','relationship','relationship2','title','dsr_b','nature_business','nationality','empstatus','foreigner','subs','country2'));   

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function downloadzip($id_pra) {
        $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
        $basic = Basic::latest('created_at')->where('id_praapplication',$id_pra)->limit('1')->first();
          $dcmt = DB::table('document')->select(DB::raw(" max(id) as id"))->where('id_praapplication',$id_pra)->groupBy('type')->pluck('id');
        $filess = Document::whereIn('id', $dcmt)->get(); 
        $files = Document::where('id_praapplication',$id_pra)->get(); 
        $prafullname =  str_replace(" ","%20",$pra->fullname);  
            

        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
        $url = url('')."/uploads/file/".$id_pra."/"; 
        if((!empty($document5->upload)) OR (!empty($document6->upload)) OR (!empty($document7->upload)) OR (!empty($document8->upload)) OR (!empty($document9->upload))) {
                # create new zip opbject
                $zip = new \ZipArchive();
                
                # create a temp file & open it
                $tmp_file = tempnam('.','');
                $zip->open($tmp_file, \ZipArchive::CREATE);
                
                # loop through each file
                foreach($files as $file){
                        $url2 = $url.$file->upload;
                        $url2 = str_replace(' ', '%20', $url2);
                        
                    
                           if (!function_exists('curl_init')){ 
                                die('CURL is not installed!');
                            }
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url2);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $output = curl_exec($ch);
                            curl_close($ch);
                        $download_file = $output;
                                        
                        $type = substr($url2, -5, 5); 
                        #add it to the zip
                        $zip->addFromString(basename($url.$file->upload),$download_file);
                }
                
                # close zip
                $zip->close();
        
        
                // Set Time Download
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $time_name = date('Ymd');
                $time_download = date('Y-m-d H:i:s');
                $log_download = new Log_download;
                
                $user = Auth::user();
                $id_user =  $user->id;
                $log_download->id_praapplication   =  $id_pra;
                $log_download->id_user   =  $id_user;
                $log_download->Activity   =  'Download ZIP File';
                $log_download->type   =  'Zip Archive';
                $log_download->downloaded_at   =  $time_download;
                $log_download->save();
                
                # send the file to the browser as a download
                header('Content-disposition: attachment; filename=DOC-'.$time_name.'-'.$basic->new_ic.'.zip');
                header('Content-type: application/zip');
                readfile($tmp_file);
                
        }
        else {
                
                 return redirect('admin')->with('error', 'Cannot to ZIP caused document are empty !');
                //Return redirect('admin')->with('error',$url);
        }
        
    }

       public function downloadpdf($id_pra) {



        $user = Auth::user();
         $email =  $user->email;
       
       $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
    //$id_pra = $pra->id;

        $status = Term::where('id_praapplication', $id_pra)->first()->status;
        $verification_remark = Term::where('id_praapplication', $id_pra)->first()->verification_remark;
        $basic_v = Basic_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $contact_v = Contact_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $empinfo_v = Empinfo_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount_v = LoanAmmount_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $spouse_v = Spouse_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference_v = Reference_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $financial_v = Financial_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $basic = Basic::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first(); 
        $employment = Employment::get();
        $document1 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
        $document2 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '2' )->first();
        $document3 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '3' )->first();
        $document4 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '4' )->first();
        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
        //$customer = User::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();         
        $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

         $pra = PraApplication::latest('created_at')->where('id',  $id_pra )->limit('1')->first();
        
        $id_type= $empinfo->first()->employment_id;
        $total_salary = $empinfo->first()->salary + $empinfo->first()->allowance ;
        
        $loan = Loan::where('id_type',$id_type)
                  ->where('min_salary','<=',$total_salary)
                  ->where('max_salary','>=',$total_salary)->limit('1')->get();
        
        $id_loan= $loan->first()->id;  
        
        $tenure = Tenure::where('id_loan',$id_loan)->get();   
        
        $contact = Contact::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first();
        $data = Basic::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

        $spouse = Spouse::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $reference = Reference::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $financial = Financial::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $term = Term::where('id_praapplication',  $id_pra )->first(); //term
         $commitments = Commitments::where('id_praapplication', $id_pra)->first(); 
    
      // Set Time Download
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $time_name = date('Ymd');
        $time_download = date('Y-m-d H:i:s');
        $log_download = new Log_download;
        
        $user = Auth::user();
        $id_user =  $user->id;
        $log_download->id_praapplication   =  $id_pra;
        $log_download->id_user   =  $id_user;
        $log_download->Activity   =  'View PDF Document';
        $log_download->type   =  'PDF Document';
        $log_download->downloaded_at   =  $time_download;
        $log_download->save();
        $dsr_a = Dsr_a::where('id_praapplication',$id_pra)->first();
        $dsr_b = Dsr_b::where('id_praapplication',$id_pra)->first();
        $dsr_c = Dsr_c::where('id_praapplication',$id_pra)->first();
        $dsr_d = Dsr_d::where('id_praapplication',$id_pra)->first();
        $dsr_e = Dsr_e::where('id_praapplication',$id_pra)->first();
        $foreigner = Foreigner::where('id_praapplication',$id_pra)->first();
        $subs = Subscription::where('id_praapplication',$id_pra)->first();
      $pdf = PDF::loadView('pdf.app_form', compact('user','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary', 'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','commitments','dsr_a','pra','foreigner','subs'))->setPaper('a4');
        //return $pdf->loadView('FORM-'.$time_name.'-'.$basic->new_ic.'.pdf');
  
         return $pdf->stream('report - '.$basic->new_ic.'.pdf', array('Attachment'=>false));

         /* return view('pdf.app_form', compact('user','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary', 'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','commitments','dsr_a'));   */
       }
       

               public function allbranch() {
                $user = Auth::user();
                if ($user->role<>4) {
                    return redirect('admin')->with("message", "User ".$user->email." has no access authorization");   
                }
                $branch = Branch::Where('status','1')->orderBy('branchname','ASC')->get();
                return view('branch.allbranch', compact('branch','user'));     
        }
        
        public function addbranch(Request $request) {
            
                $branch = new Branch ;
                $code  = $request->input('branch_code');
                $validate = Branch::latest('created_at')->where('branch_code', $code)->count();
                if ($validate > 0) {
                      return redirect('admin/allbranch/index')->with('error', 'Failed Added, Branch '.$code.' already exits');
                }
                else {
                    $branch->branch_code   =   $request->input('branch_code');
                      $branch->branchname   =   $request->input('Name');
                      $branch->address   =  $request->input('Address');
                      $branch->phone   =  $request->input('Phone');
                        $branch->status   =  '1';
                      $branch->fax   = $request->input('Fax');;
                      $branch->save();
                      return redirect('admin/allbranch/index')->with('success', 'Branch '.$name.' Successfully Added');
                }

        }
        
        public function editbranch(Request $request) {
                $branch = new Branch ;
                $code  = $request->input('branch_code');
                $validate = Branch::latest('created_at')->where('branch_code', $code)->count();
                        $branch->branch_code   =   $request->input('branch_code');
                        $branchname   =   $request->input('Name');
                        $address   =  $request->input('Address');
                        $phone   =  $request->input('Phone');
                        $fax   = $request->input('Fax');
                        $status   = $request->input('Status');
                        $idBranch   = $request->input('idBranch');
                        $branch = Branch::where('id', $idBranch)->update(['branch_code' => $branch_code,'branchname' => $branchname, 'address' => $address, 'phone' => $phone, 'fax' => $fax, 'status' => $status   ]);
                        return redirect('admin/allbranch/index')->with('success', 'Branch '.$name.' Successfully Edited');
                

        }
    
        public function deletebranch(Request $request) {
                $branch = new Branch ;
                $name  = $request->input('branchname');
                $idBranch   = $request->input('idBranch');
                $branch = Branch::where('id', $idBranch)->update(['status' => '2'   ]);
                return redirect('admin/allbranch/index')->with('success', 'Branch '.$name.' Successfully Deleted');
                

        }
        
        public function allbranch_user() {
                $user = Auth::user();
                if ($user->role<>4) {
                    return redirect('admin');   
                }
                $user_branch = User::Where('role','2')->Where('active','1')->orderBy('created_at','DESC')->get();
                $branch = Branch::Where('status','1')->orderBy('id','DESC')->get();
                return view('branch.allbranch_user', compact('branch','user_branch','user'));     
        }
    
        public function editbranch_user(Request $request) {
                $branch_user = new User ;
                $name   =   $request->input('Name');
                $email   =  $request->input('Email');
                $id_branch   =  $request->input('branch');
                $pass   = $request->input('Password');
                $pc   = $request->input('PasswordConfirmation');
                $id   = $request->input('idUser');
            
                if((empty($pass)) AND  (empty($pc))) {
                    $branch_user = User::where('id', $id)->update(['name' => $name, 'email' => $email, 'id_branch' => $id_branch ]);
                     return redirect('admin/allbranch_user/index')->with('success', 'User '.$name.' Successfully Edited');
                }
                else {
                    if($pass==$pc) {
                        $pass_enc = bcrypt($pass);
                       $branch_user = User::where('id', $id)->update(['name' => $name, 'password' => $pass_enc, 'email' => $email, 'id_branch' => $id_branch ]); 
                        return redirect('admin/allbranch_user/index')->with('success', 'User '.$name.' and Password are Successfully Edited');
                    }
                    else {
                        
                     return redirect('admin/allbranch_user/index')->with('error', 'Password confirmation does not match password');
                    }
                }
                
               
                

        }
    
        public function deletebranch_user(Request $request) {
                $branch_user = new User ;
                $name  = $request->input('name');
                $id   = $request->input('id');
                $branch_user = User::where('id', $id)->update(['active' => '2'   ]);
                return redirect('admin/allbranch_user/index')->with('success', 'User '.$name.' Successfully Deleted');
                

        }
        
        public function allhq_user() {
                $user = Auth::user();
                if ($user->role<>4) {
                    return redirect('admin');   
                }
                $user_hq = User::Where('role','3')->orderBy('created_at','DESC')->get();
                return view('hq.allhq_user', compact('user_hq','user'));     
        }

        public function addbranch_user(Request $request) {
                $user_branch = new User ;
                $email = $request->input('Email');
                $validate = User::latest('created_at')->where('email', $email)->count();
                if ($validate > 0) {
                      return redirect('admin/allbranch_user/index')->with('error', 'Failed Added, Email already exits');
                }
                else {
                      $id = Uuid::uuid4()->getHex(); 
                      $user_branch->id   =  $id;
                      $user_branch->name   =   $request->input('Name');
                      $user_branch->email   =  $request->input('Email');
                      $user_branch->password   =  bcrypt($request->input('Password'));
                      $user_branch->role   =  '2';
                      $user_branch->active   =  '1';
                      $user_branch->id_branch   =  $request->input('Branch');
                      $user_branch->save();
                      return redirect('admin/allbranch_user/index')->with('success', 'Branch User Successfully Added');
                }

        }
        
        public function addhq_user(Request $request) {
                $user_hq = new User ;
                $email = $request->input('Email');
                $validate = User::latest('created_at')->where('email', $email)->count();
                if ($validate > 0) {
                      return redirect('admin/allhq_user/index')->with('error', 'Failed Added, Email already exits');
                }
                else {
                      $id = Uuid::uuid4()->getHex(); 
                      $user_hq->id   =  $id;
                      $user_hq->name   =   $request->input('Name');
                      $user_hq->email   =  $request->input('Email');
                      $user_hq->password   =  bcrypt($request->input('Password'));
                      $user_hq->role   =  '3';
                      $user_hq->active   =  '1';
                      $user_hq->save();
                      return redirect('admin/allhq_user/index')->with('success', 'HQ Bank Persatuan User Successfully Added');
                }

        }
        
        public function changed_bybank(Request $request) {
                $id_praapplication = $request->input('id_praapplication');
                $status = $request->input('status');
                $branchname = $request->input('branchname');
                $cus_name = $request->input('cus_name');
                $cus_ic = $request->input('cus_ic');
                $remark_bank = $request->input('remark_bank');
                $reason = $request->input('reason');
                $aproved_tenure = $request->input('tenure');
                $aproved_loan = $request->input('loan');
                $cus_email = $request->input('cus_email');
                $basic = Basic::latest('created_at')->where('id_praapplication',  $id_praapplication )->limit('1')->first();
                $validate = Term::where('id_praapplication', $id_praapplication)->where('verification_result_by_bank', $status)->where('remark_bank', $remark_bank)->where('rejected_reason', $reason)->where('aproved_tenure', $aproved_tenure)->where('aproved_loan', $aproved_loan)->count();
                if ($validate > 0) {
                      return redirect('admin')->with('error', 'Nothing Changed');
                }
                
                // Set Time Activity
                        if ($status==1) {
                                $s_string = "Approved";
                                $reason ="";
                                
                        }
                        elseif ($status==2) {
                                $s_string = "Rejected";
                                $aproved_tenure = 0;
                                $aproved_loan = 0;
                        }
                        elseif ($status==3) {
                                $s_string = "Pending Approval";
                                $reason ="";
                            
                        }
        
                        date_default_timezone_set("Asia/Kuala_Lumpur");
                        $time_name = date('Ymd');
                        $time_download = date('Y-m-d H:i:s');
                        $log_download = new Log_download;
                        
                        $user = Auth::user();
                        $id_user =  $user->id;
                        $log_download->id_praapplication   =  $id_praapplication;
                        $log_download->id_user   =  $id_user;
                        $log_download->Activity   =  'Change Status : '.$s_string;
                        $log_download->downloaded_at   =  $time_download;
                        $log_download->log_remark   =  $remark_bank;
                        $log_download->log_reason  =  $reason;
                        $log_download->save();
                        $email = "loan@ezlestari.com.my";
                        $fullname = "loan@ezlestari.com.my";
                        $kantor_cabang = $branchname;
                    
                        
                        // Send to Admin Email
                        
        
                      /*  Mail::send('mail.notifikasi_status_bank', compact('s_string','branchname','cus_name','cus_ic','remark_bank','aproved_loan','aproved_tenure','status','reason'), function ($message) use ($email, $fullname) {
                        $message->from('no-reply@ezlestari.com.my', 'no-reply@ezlestari.com.my');
                        $message->subject('[EZLESTARI.COM.MY] Application Status Changed');
                        $message->to($email, $fullname);
                        $message->to('loan@ezlestari.com.my', 'loan@ezlestari.com.my');
                        
                      
                         $ho = User::latest('created_at')->where('role',  '3' )->get();
                         foreach ($ho as $a) {
                             $emailho = $a->email;
                             $message->to($emailho, $emailho);
                            //  echo 'email ='.$emailho;
                         } 
                        
                        });
                        
                    
                        Mail::send('mail.notifikasi_status_customer', compact('s_string','branchname','cus_name','cus_ic','remark_bank','aproved_loan','aproved_tenure','status','reason'), function ($message) use ($cus_email, $cus_name) {
                        $message->from('loan@ezlestari.com.my', 'loan@ezlestari.com.my');
                        $message->subject('[EZLESTARI.COM.MY] Status to be updated as per branch status');
                        $message->to($cus_email, $cus_name);

                
                        });
						*/
                        
                //$name  = $request->input('Name');
                $term = Term::where('id_praapplication', $id_praapplication)->update(['verification_result_by_bank' => $status, 'remark_bank' => $remark_bank , 'rejected_reason' => $reason, 'aproved_loan' => $aproved_loan, 'aproved_tenure' => $aproved_tenure]);
                return redirect('admin')->with('success', 'Application Commited');
                

        }
        
        
        
        public function confirm_approved(Request $request) {
                $id_praapplication = $request->input('id_praapplication');

                // Set Time Activity
            
                        date_default_timezone_set("Asia/Kuala_Lumpur");
                        $time_name = date('Ymd');
                        $time_download = date('Y-m-d H:i:s');
                        $log_download = new Log_download;
                        
                        $user = Auth::user();
                        $id_user =  $user->id;
                        $log_download->id_praapplication   =  $id_praapplication;
                        $log_download->id_user   =  $id_user;
                        $log_download->Activity   =  'Change Status : Confirm Approved ';
                        $log_download->downloaded_at   =  $time_download;
                        $log_download->save();
                //$name  = $request->input('Name');
                $term = Term::where('id_praapplication', $id_praapplication)->update(['confirm_approved' => '1', 'verification_result' => '5']);
                        return redirect('admin')->with('success', 'Application Approved Successfully');
                

        }
        
        public function test($id_pra) {



        $user = Auth::user();
         $email =  $user->email;
       
       $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
    //$id_pra = $pra->id;

        $status = Term::where('id_praapplication', $id_pra)->first()->status;
        $verification_remark = Term::where('id_praapplication', $id_pra)->first()->verification_remark;
        $basic_v = Basic_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $contact_v = Contact_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $empinfo_v = Empinfo_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount_v = LoanAmmount_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $spouse_v = Spouse_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference_v = Reference_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $financial_v = Financial_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $basic = Basic::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first(); 
        $employment = Employment::get();
        $document1 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
        $document2 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '2' )->first();
        $document3 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '3' )->first();
        $document4 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '4' )->first();
        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
        //$customer = User::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();//         
        $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        
        $id_type= $empinfo->first()->employment_id;
        $total_salary = $empinfo->first()->salary + $empinfo->first()->allowance  ;
        
        $loan = Loan::where('id_type',$id_type)
                  ->where('min_salary','<=',$total_salary)
                  ->where('max_salary','>=',$total_salary)->limit('1')->get();
        
        $id_loan= $loan->first()->id;  
        
        $tenure = Tenure::where('id_loan',$id_loan)->get();   
        
        $contact = Contact::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first();
        $data = Basic::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

        $spouse = Spouse::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $reference = Reference::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $financial = Financial::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $term = Term::where('id_praapplication',  $id_pra )->first(); //term
    
  

    return view('pdf.form', compact('user','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary',
                                                         'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term'));  
       }
       
       public function report($startdate, $finishdate, $status_selected, $branch_selected) {
                $user = Auth::user();
                if ($user->role<>1) {
                    return redirect('admin');   
                }
    
                $branch = Branch::latest('created_at')->where('status',  '1' )->get();
                return view('report.index', compact('startdate','finishdate','status_selected','branch_selected','branch','user'));     
        }
        
        public function report_first() {
                $user = Auth::user();
                if ($user->role<>1) {
                    return redirect('admin');   
                }
                $startdate=0;
                $finishdate =0;
                $branch_selected=0;
                $status_selected =0;
                $branch = Branch::latest('created_at')->where('status',  '1' )->get();
                return view('report.index', compact('startdate','finishdate','status_selected','branch_selected','branch','user'));     
        }
        
         public function report_result($startdate, $finishdate, $status, $branch) {
                $user = Auth::user();
                if ($user->role<>3 AND $user->role<>1) {
                    return redirect('admin');   
                }
            
              
            /*  echo $status;
                echo $startdate;
                echo $finishdate; */
                
                if($branch<>'99' AND $status<>'99') {
                // Kondisi bukan pilihan ALL
                $terma = Term::where('status', '1')->where('id_branch', $branch)->where('verification_result_by_bank', $status)->wherein('verification_result', ['2','5'])->orderBy('id','DESC')->get();
                $termaCount = Term::where('status', '1')->where('id_branch', $branch)->where('verification_result_by_bank', $status)->wherein('verification_result', ['2','5'])->orderBy('id','DESC')->count();
                
                $blocked_ic = Blocked_ic::get();
                        
                }
                else if($branch=='99' AND $status<>'99') {
                // Kondisi All branch
                        
                }
                else if($branch<>'99' AND $status=='99') {
                // Kondisi All Status
                        
                }
                else if($branch=='99' AND $status=='99') {
                // Kondisi Semua Pilihan All
                        
                } 
        
    
                $startdate_convert =  date('d-m-Y', strtotime($startdate));
            
                $finishdate_convert =  date('d-m-Y', strtotime($finishdate));
                if ($termaCount>0) {
                        return view('report.report_result', compact('terma','blocked_ic','user','startdate_convert','finishdate_convert','termaCount','startdate','finishdate','status','branch'));
                        
                }
                else {
                        return redirect('admin/report/'.$startdate.'/'.$finishdate.'/'.$status.'/'.$branch)->with('error', 'No data available in report');
                }
                        
                 /* $user = Auth::user();
                if($user->role=='3') {
                // IF HQ User
                $terma = Term::where('status', '1')->wherein('verification_result', ['2','5'])->orderBy('id','DESC')->get();
                $blocked_ic = Blocked_ic::get();
                return view('hq.index', compact('user','terma','blocked_ic'));    
                }    */
        }
		
		public function incomplated_doc() {
        $user = Auth::user();
			if($user->role=='4') {
					// IF admin Step 2 (tugasnya Routed to Branch)
					$terma = Term::where('status', '0')->orderBy('created_at','DESC')->get();
					$blocked_ic = Blocked_ic::get();
					
					$pickup = PraApplication::select('x.email','x.created_at','id')
					->from(DB::raw('(select * from praapplication ORDER BY created_at DESC) as x'))
					->groupBy('email')
					->orderBy('created_at', 'desc')->get();
					
					return view('admin2.incomplated_doc', compact('user','terma','blocked_ic','pickup'));   
			}
			
		}	

		
		public function tesquery()
		{
				$user = Auth::user();

        $req_ndi = 1000.01;

          $ndi = ScoreRating::select('score')->where('variable','Net Disposable Income')->where('sector','G')
                  ->where('float_value','<=',$req_ndi )->where('float_value2','>=',$req_ndi )->first();

		
				
				return $ndi->score;
                // $blocked_ic = Blocked_ic::get();
                //return view('admin.index', compact('user','terma','blocked_ic'));   
         }
        
        public function detail_pra($id_pra, $view=FALSE) {

        $user = Auth::user();
      
        $email =  $user->email;
       
       $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
    //$id_pra = $pra->id;

        $status = Term::where('id_praapplication', $id_pra)->first()->status;
        $verification_result = Term::where('id_praapplication', $id_pra)->first()->verification_result;
        $verification_remark = Term::where('id_praapplication', $id_pra)->first()->verification_remark;
        $basic_v = Basic_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $contact_v = Contact_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $empinfo_v = Empinfo_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount_v = LoanAmmount_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $spouse_v = Spouse_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference_v = Reference_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $financial_v = Financial_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $basic = Basic::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first(); 
        $name = strtoupper($basic->name);
        $employment = Employment::get();
        $document1 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
        $document2 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '2' )->first();
        $document3 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '3' )->first();
        $document4 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '4' )->first();
        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
         $document10 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '10' )->first();
         $document11 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '11' )->first();
        //$customer = User::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();         
        $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        
        $id_type= $empinfo->first()->employment_id;
        $total_salary = $empinfo->first()->salary + $empinfo->first()->allowance ;
        
        $loan = Loan::where('id_type',$id_type)
                  ->where('min_salary','<=',$total_salary)
                  ->where('max_salary','>=',$total_salary)->limit('1')->get();
        
        $id_loan= $loan->first()->id;  
        
         $icnumber= $pra->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;
        $statusx = 1;

   
          $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->get();   
        
        $contact = Contact::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->get();
        $data = Basic::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

        $spouse = Spouse::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference = Reference::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $financial = Financial::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

         $credit = Credit::where('id_praapplication', $id_pra)->first(); 
           $pep = Pep::where('id_praapplication', $id_pra)->first(); 
           $commitments = Commitments::where('id_praapplication', $id_pra)->first(); 

    $cal_bulan = $financial->first()->cal_bulan;
    if ($cal_bulan=='0000-00-00') {
     $cal_bulan = ''; 
    }
    else {
     $cal_bulan = date('F Y', strtotime($cal_bulan)); 
    }
    
    
        $term = Term::where('id_praapplication',  $id_pra )->first(); //term
        $branch = Branch::latest('created_at')->where('status',  '1' )->get();

        $today = date("d/m/Y");
          $country = Country::get(); //term
        $occupation = Occupations::get();
        $position = Position::get();
        $relationship = Relationship::get();
        $relationship2 = Relationship::get();
        $title = Title::get();
        $nationality = Country::get();
        
            return view('admin.detail_pra', compact('view','name','cal_bulan','user','branch','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary',
                                                     'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','statusx','document10','document11','today','credit','pep','commitments','country','occupation','position','relationship','relationship2','title','nationality'));        
                       
      
    }

     public function download_form($id_pra) 
    {
        $user  = Auth::user();
        $email = $user->email;
       
        $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
        //$id_pra = $pra->id;

        $status              = Term::where('id_praapplication', $id_pra)->first()->status;
        $verification_remark = Term::where('id_praapplication', $id_pra)->first()->verification_remark;
        $basic_v             = Basic_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $contact_v           = Contact_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $empinfo_v           = Empinfo_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount_v       = LoanAmmount_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $spouse_v            = Spouse_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference_v         = Reference_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $financial_v         = Financial_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $basic               = Basic::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first(); 
        $employment = Employment::get();
        $document1 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
        $document2 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '2' )->first();
        $document3 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '3' )->first();
        $document4 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '4' )->first();
        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
        //$customer = User::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();         
        $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        
        $id_type        = $empinfo->first()->employment_id;
        $total_salary   = $empinfo->first()->salary + $empinfo->first()->allowance ;
        
        $loan   = Loan::where('id_type',$id_type)
                  ->where('min_salary','<=',$total_salary)
                  ->where('max_salary','>=',$total_salary)->limit('1')->get();
        
        $id_loan    = $loan->first()->id;  
        
        $tenure     = Tenure::where('id_loan',$id_loan)->get();   
        
        $contact    = Contact::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first();
        $data       = Basic::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $loanamm    = loanAmmount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

        $spouse     = Spouse::latest('created_at')->where('id_praapplication', $id_pra )->limit('1')->first();
        $reference  = Reference::latest('created_at')->where('id_praapplication', $id_pra )->limit('1')->first();
        $financial  = Financial::latest('created_at')->where('id_praapplication', $id_pra )->limit('1')->first();
        $term       = Term::where('id_praapplication',  $id_pra )->first(); //term
        $commitments = Commitments::where('id_praapplication', $id_pra)->first(); 
    
       // Set Time Download
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $time_name = date('Ymd');
        $time_download = date('Y-m-d H:i:s');
        $log_download = new Log_download;
        
        $user = Auth::user();
        $id_user =  $user->id;
        $log_download->id_praapplication   =  $id_pra;
        $log_download->id_user   =  $id_user;
        $log_download->Activity   =  'View PDF Document';
        $log_download->type   =  'PDF Document';
        $log_download->downloaded_at   =  $time_download;
        $log_download->save();
        $dsr_a = Dsr_a::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first(); 
$foreigner = Foreigner::where('id_praapplication',$id_pra)->first();
        $subs = Subscription::where('id_praapplication',$id_pra)->first();
       $pdf = PDF::loadView('pdf.app_form', compact('user','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary','loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','commitments','dsr_a','foreigner','subs'))->setPaper('a4');
       return $pdf->download('FORM-'.$time_name.'-'.$basic->new_ic.'.pdf');
        //return view('pdf.form_mbsb', compact(user','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary', 'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','commitments','dsr_a'));
    }

    public function view_pdf($id)
    {
        $term  = Term::where('id_praapplication',$id)->limit('1')->first();

        return view('pdf.view',compact('term'));
    }
         

}
