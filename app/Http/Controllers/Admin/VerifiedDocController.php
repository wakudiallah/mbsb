<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Model\PraApplication;
use App\Model\Basic;
use App\Model\User;
use App\Model\Contact;
use App\Model\Empinfo;
use App\Model\LoanAmmount;
use App\Model\LoanCalculator;
use App\Model\LoanCalculator2;
use App\Model\Spouse;
use App\Model\Reference;
use App\Model\Financial;
use App\Model\Document;
use App\Model\Basic_v;
use App\Model\Contact_v;
use App\Model\Empinfo_v;
use App\Model\LoanAmmount_v;
use App\Model\Spouse_v;
use App\Model\Reference_v;
use App\Model\Financial_v;
use App\Model\Document_v;
use App\Model\Term;
use App\Model\Log_download;
use App\Model\Settlement;

use App\Dsr_a;
use App\Dsr_b;
use App\Dsr_c;
use App\Dsr_d;
use App\Dsr_e;
use App\RiskRating;

use App\Http\Controllers\Controller;
use DB;
use Input;
use Auth;
use Mail;
use Image;
Use File;
use Storage;

class VerifiedDocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
	{
	 $this->middleware('auth');	
      $this->middleware('admin');
	} 
	
    public function index()
    {
        
         
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
      

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        
      
       
    }
        

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

    	date_default_timezone_set("Asia/Kuala_Lumpur");
        $today = date('Y-m-d H:i:s');
      
    
		    
		if($id == '2') {

			// Loan Calculator Here
			$id_praapplication = $request->input('id_praapplication');

			// DSR_A
			$basic_salary1 		= $request->input('basic_salary1');
            $basic_salary1x 	= str_replace(',', '', $basic_salary1);

            $basic_salary2 		= $request->input('basic_salary2');
            $basic_salary2x 	= str_replace(',', '', $basic_salary2);

            $basic_salary3 		= $request->input('basic_salary3');
            $basic_salary3x 	= str_replace(',', '', $basic_salary3);

            $fixed_allowances1 	= $request->input('fixed_allowances1');
            $fixed_allowances1x = str_replace(',', '', $fixed_allowances1);

            $fixed_allowances2 	= $request->input('fixed_allowances2');
            $fixed_allowances2x = str_replace(',', '', $fixed_allowances2);

            $fixed_allowances3 	= $request->input('fixed_allowances3');
            $fixed_allowances3x = str_replace(',', '', $fixed_allowances3);

            $total_earnings1 	= $request->input('total_earnings1');
            $total_earnings1x 	= str_replace(',', '', $total_earnings1);

            $total_earnings2 	= $request->input('total_earnings2');
            $total_earnings2x 	= str_replace(',', '', $total_earnings2);

            $total_earnings3 	= $request->input('total_earnings3');
            $total_earnings3x 	= str_replace(',', '', $total_earnings3);

            $variable_income 	= $request->input('variable_income');
            $variable_incomex 	= str_replace(',', '', $variable_income);

            $gmix 				= $request->input('gmi_use');

            $epf1 				= $request->input('epf1');
            $epf1x 				= str_replace(',', '', $epf1);

            $epf2 				= $request->input('epf2');
            $epf2x 				= str_replace(',', '', $epf2);

            $epf3 				= $request->input('epf3');
            $epf3x 				= str_replace(',', '', $epf3);

            $socso1 			= $request->input('socso1');
            $socso1x 			= str_replace(',', '', $socso1);

            $socso2 			= $request->input('socso2');
            $socso2x 			= str_replace(',', '', $socso2);

           	$socso3 			= $request->input('socso3');
            $socso3x 			= str_replace(',', '', $socso3);

            $income_tax1 		= $request->input('income_tax1');
            $income_tax1x 		= str_replace(',', '', $income_tax1);

            $income_tax2 		= $request->input('income_tax2');
            $income_tax2x 		= str_replace(',', '', $income_tax2);

            $income_tax3 		= $request->input('income_tax3');
            $income_tax3x 		= str_replace(',', '', $income_tax3);

            $other_contribution1 = $request->input('other_contribution1');
            $other_contribution1x= str_replace(',', '', $other_contribution1);

            $other_contribution2 = $request->input('other_contribution2');
            $other_contribution2x= str_replace(',', '', $other_contribution2);

            $other_contribution3 = $request->input('other_contribution3');
            $other_contribution3x= str_replace(',', '', $other_contribution3);

            $total_deduction1 	= $request->input('total_deduction1');
            $total_deduction1x 	= str_replace(',', '', $total_deduction1);

            $total_deduction2 	= $request->input('total_deduction2');
            $total_deduction2x 	= str_replace(',', '', $total_deduction2);

            $total_deduction3 	= $request->input('total_deduction3');
            $total_deduction3x 	= str_replace(',', '', $total_deduction3);

            $additional_income 	= $request->input('additional_income');
            $additional_incomex = str_replace(',', '', $additional_income);

            $nmi                = $request->input('nmi');
            $nmix               = str_replace(',', '', $nmi);

            $ite 				        = $request->input('ite');
            $itex 				      = str_replace(',', '', $ite);

            $total_gross_income = $request->input('total_gross_income');
            $total_gross_incomex= str_replace(',', '', $total_gross_income);

            $total_net_income 	= $request->input('total_net_income');
            $total_net_incomex 	= str_replace(',', '', $total_net_income);
            $category =$request->input('category');
            $existing_mbsb =$request->input('existing_mbsb');
            $cif_no =$request->input('cif_no');

			if($request->sector=="G") {
					$dsr_a = Dsr_a::where('id_praapplication', $id_praapplication)
			          ->update(['sector' => $request->sector,  
			          	'gender' => $request->gender, 
			          	'age' => $request->age, 
			          	'category' => $category, 
			          	'existing_mbsb' => $existing_mbsb, 
			           	'basic_salary1' => $basic_salary1x,
			           	'basic_salary2' => $basic_salary2x,
			           	'basic_salary3' => $basic_salary3x,
			           	'fixed_allowances1' => $fixed_allowances1x,
			           	'fixed_allowances2' => $fixed_allowances2x,
			           	'fixed_allowances3'=> $fixed_allowances3x,
			           	'total_earnings1' => $total_earnings1x,
			           	'total_earnings2' => $total_earnings2x,
			           	'total_earnings3' => $total_earnings3x,
			           	'variable_income' => $variable_incomex,
			           	'gmi' => $gmix,
			           	'epf1' => $epf1x,
			           	'epf2' => $epf2x,
			           	'epf3' => $epf3x,
			           	'socso1' => $socso1x,
			           	'socso2' => $socso2x,
			           	'socso3' => $socso3x,
			           	'income_tax1' => $income_tax1x,
			           	'income_tax2' => $income_tax2x,
			           	'income_tax3' => $income_tax3x,
			           	'other_contribution1' => $other_contribution1x,
			           	'other_contribution2' => $other_contribution2x,
			           	'other_contribution3' => $other_contribution3x,
			           	'total_deduction1' => $total_deduction1x,
			           	'total_deduction2' => $total_deduction2x,
			           	'total_deduction3' => $total_deduction3x,
			           	'nmi' => $nmix,
			           	'additional_income' => $additional_incomex,
			           	'ite' => $itex,
			           	'sec1_verified_by' => $request->sec1_verified_by,
			           	'sec1_verified_time' => $today,
			           	'total_gross_income' => $total_gross_incomex,
			           	'total_net_income' => $total_net_incomex,
                        'cif_no' => $cif_no
			           	]);
			}
			else {

				$dsr_a = Dsr_a::where('id_praapplication', $id_praapplication)
			          ->update(['sector' => $request->sector,  
			          	'gender' => $request->gender, 
			          	'age' => $request->age, 
			        
			           	'basic_salary1' => $basic_salary1x,
			           	'basic_salary2' => $basic_salary2x,
			           	'basic_salary3' => $basic_salary3x,
			           	'fixed_allowances1' => $fixed_allowances1x,
			           	'fixed_allowances2' => $fixed_allowances2x,
			           	'fixed_allowances3'=> $fixed_allowances3x,
			           	'total_earnings1' => $total_earnings1x,
			           	'total_earnings2' => $total_earnings2x,
			           	'total_earnings3' => $total_earnings3x,
			           	'variable_income' => $variable_incomex,
			           	'gmi' => $gmix,
			           	'epf1' => $epf1x,
			           	'epf2' => $epf2x,
			           	'epf3' => $epf3x,
			           	'socso1' => $socso1x,
			           	'socso2' => $socso2x,
			           	'socso3' => $socso3x,
			           	'income_tax1' => $income_tax1x,
			           	'income_tax2' => $income_tax2x,
			           	'income_tax3' => $income_tax3x,
			           	'other_contribution1' => $other_contribution1x,
			           	'other_contribution2' => $other_contribution2x,
			           	'other_contribution3' => $other_contribution3x,
			           	'total_deduction1' => $total_deduction1x,
			           	'total_deduction2' => $total_deduction2x,
			           	'total_deduction3' => $total_deduction3x,
			           	'nmi' => $nmix,
			           	'additional_income' => $additional_incomex,
			           	'ite' => $itex,
			           	'sec1_verified_by' => $request->sec1_verified_by,
			           	'sec1_verified_time' => $today,
			           	'total_gross_income' => $total_gross_incomex,
			           	'total_net_income' => $total_net_incomex,
                        'cif_no' => $cif_no
			           	]);
			}
			
			$existing_loan 			= $request->input('existing_loan');
            $existing_loanx 		= str_replace(',', '', $existing_loan);

            $total_group_exposure 	= $request->input('total_group_exposure');
            $total_group_exposurex 	= str_replace(',', '', $total_group_exposure);

            $financing_amount 	= $request->input('financing_amount');
            $financing_amountx 	= str_replace(',', '', $financing_amount);

          	$dsr_b = Dsr_b::where('id_praapplication', $id_praapplication)
          		->update(['financing_amount' => $financing_amountx,  
	          	'tenure' => $request->tenure, 
	           	'rate' => $request->rate,
	           	'monthly_repayment' => $request->monthly_repayment,
	           	'existing_loan' => $existing_loanx,
	           	'total_group_exposure' => $total_group_exposurex
	           	]);

          	$mbsb_hire 	= $request->input('mbsb_hire');
            $mbsb_hirex 	= str_replace(',', '', $mbsb_hire);

            $mbsb_personal 	= $request->input('mbsb_personal');
            $mbsb_personalx 	= str_replace(',', '', $mbsb_personal);

            $de_angkasa 	= $request->input('de_angkasa');
            $de_angkasax 	= str_replace(',', '', $de_angkasa);

            $de_govhousing 	= $request->input('de_govhousing');
            $de_govhousingx 	= str_replace(',', '', $de_govhousing);

            $de_koperasi 	= $request->input('de_koperasi');
            $de_koperasix 	= str_replace(',', '', $de_koperasi);

            $de_nonbiro 	= $request->input('de_nonbiro');
            $de_nonbirox 	= str_replace(',', '', $de_nonbiro);

            $de_otherloan 	= $request->input('de_otherloan');
            $de_otherloanx 	= str_replace(',', '', $de_otherloan);

            $de_other 		= $request->input('de_other');
            $de_otherx 		= str_replace(',', '', $de_other);

            $ccris_housing 	= $request->input('ccris_housing');
            $ccris_housingx = str_replace(',', '', $ccris_housing);

            $ccris_creditcard = $request->input('ccris_creditcard');
            $ccris_creditcardx= str_replace(',', '', $ccris_creditcard);

            $ccris_hire 	= $request->input('ccris_hire');
            $ccris_hirex 	= str_replace(',', '', $ccris_hire);

            $ccris_otherdraft = $request->input('ccris_otherdraft');
            $ccris_otherdraftx= str_replace(',', '', $ccris_otherdraft);

            $ccris_other 	= $request->input('ccris_other');
            $ccris_otherx 	= str_replace(',', '', $ccris_other);

            $total_commitment 	= $request->input('total_commitment');
            $total_commitmentx 	= str_replace(',', '', $total_commitment);

          	$dsr_c = Dsr_c::where('id_praapplication', $id_praapplication)
          		->update(['mbsb_housing' => $request->mbsb_housing,  
	          	'mbsb_hire' => $mbsb_hirex, 
	           	'mbsb_personal' => $mbsb_personalx,
	           	'de_angkasa' => $de_angkasax,
	           	'de_govhousing' => $de_govhousingx,
	           	'de_koperasi' => $de_koperasix,
	           	'de_nonbiro' => $de_nonbirox,
	           	'de_otherloan' => $de_otherloanx,
	           	'de_other' => $de_otherx,
	           	'ccris_housing' => $ccris_housingx,
	           	'ccris_creditcard' => $ccris_creditcardx,
	           	'ccris_hire' => $ccris_hirex,
	           	'ccris_otherdraft' => $ccris_otherdraftx,
	           	'ccris_other' => $ccris_otherx,
	           	'total_commitment' => $total_commitmentx
           	]);

          $dsr_d = Dsr_d::where('id_praapplication', $id_praapplication)
          ->update(['sec2_verified_by' => $request->sec2_verified_by,  
          	'sec2_verified_time' => $today
           	]);

          	$net_income 	= $request->input('net_income');
            $net_incomex 	= str_replace(',', '', $net_income);

           $dsr_e = Dsr_e::where('id_praapplication', $id_praapplication)
          ->update(['total_debt' => $request->total_debt,  
          	'net_income' => $net_incomex, 
           	'dsr' => $request->dsr,
           	'dsr_bnm' => $request->dsr_bnm,
           	'ndi' => $request->ndi_use,
           	'decision' => $request->decision,
           	'data_save' => $today
           	]);




			$name = $request->input('name');
	
			$basic = Basic::where('id_praapplication', $id_praapplication)
			->update(['name' => $name, 'new_ic' => $request->new_ic]);

			$loan_amount = $request->input('loan_amount');

           $loanammount = loanAmmount::where('id_praapplication', $id_praapplication)
          ->update(['loanammount' => $loan_amount ]);

          if($request->input('id_tenure_hidden')==0) {
          	$loanammount = loanAmmount::where('id_praapplication', $id_praapplication)
          ->update(['id_tenure' => '20' ]);
          }




		
		}
		else if($id == '3') {

			// Loan Calculator Here
			$id_praapplication = $request->input('id_praapplication');
			
			$expire1 = str_replace('/', '-', $request->input('expire1'));
			$expire1_r =  date('Y-m-d', strtotime($expire1));
			$expire2 = str_replace('/', '-', $request->input('expire2'));
			$expire2_r =  date('Y-m-d', strtotime($expire2));
			$expire3 = str_replace('/', '-', $request->input('expire3'));
			$expire3_r =  date('Y-m-d', strtotime($expire3));
			$expire4 = str_replace('/', '-', $request->input('expire4'));
			$expire4_r =  date('Y-m-d', strtotime($expire4));
			
		$riskrating = RiskRating::where('id_praapplication', $id_praapplication)
          ->update(['salary_deduction' => $request->rr_salary_deduction,  
          	'age' => $request->rr_age, 
           	'education_level' => $request->rr_edu_level,
           	'employment' => $request->rr_emp,
           	'position' => $request->rr_position,
           	'marital_status' => $request->rr_marital,
           	'spouse_employment' => $request->rr_spouse_emp,
           	'property_loan' => $request->rr_property_loan,
           	'adverse_ccris' => $request->rr_adverse_ccris,
           	'total_score' => $request->total_score,
           	'grading' => $request->grading,
           	'decision' => $request->final_decision,
           	'scoring_by' => Auth::user()->name,
           	'date_scoring' => $today,
           	'reviewed_by' => Auth::user()->name,
           	'date_reviewed' => $today
           	]);
		  
			
		
		}
	}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function upload(Request $request,$id)
    {
        $user_id     = $request->input('user_id');
        $id_praapplication = $request->input('id_praapplication');
        $pra = PraApplication::latest('created_at')->where('id',$id_praapplication)->limit('1')->first();
        $nametest  = $pra->id;
        
       
          $ic = $pra->ic_number;
                  
                  $ic_number2 =  str_replace('/', '', $ic);

                 
                  if ($request->hasFile('file'.$id)) {
                  $name = $request->input('document'.$id);
                 $file = $request->file('file'.$id);
                $tipe_file   = $_FILES['file'.$id]['type'];
                //if($tipe_file == "application/pdf") {


                 $filename = str_random(15).'-'.$name.'-'.$file->getClientOriginalName();
                 $destinationPath = 'storage/uploads/file/'.$nametest.'/';
                 $file->move($destinationPath, $filename);


                  
                 $document = new Document;
                $document->name = $name;
                $document->type = $id;
                $document->upload = $filename; 
                $document->id_praapplication = $id_praapplication; 
                $document->save();
                   
                 return response()->json(['file' => "$filename"]);


              // }
            

                }  
    }
    public function uploads(Request $request,$id) {
		$user = Auth::user();
		$id_praapplication = $request->input('id_praapplication');
		$pra = PraApplication::latest('created_at')->where('id',$id_praapplication)->limit('1')->first();
		$nametest  = $pra->id;
		if ($request->hasFile('file'.$id)) {
			$name = $request->input('document'.$id);
			$file = $request->file('file'.$id);
			$tipe_file   = $_FILES['file'.$id]['type'];
			if($tipe_file == "application/pdf" || $tipe_file == "image/jpeg" || $tipe_file == "image/png" || $tipe_file == "image/bmp" || $tipe_file == "image/gif") {
				$filename = str_random(25).'-'.$name.'-'.$file->getClientOriginalName();
				//Storage::disk('ftp')->put($nametest, fopen($request->file('file'), 'r+'));
				 if($tipe_file != "application/pdf") {

                    if(!(File::exists('storage/uploads/file/'.$nametest.'/'))) {
                        $create_folder =  File::makeDirectory('storage/uploads/file/'.$nametest.'/', 0775);
                    }

                      $img = Image::make($request->file('file'.$id));

                      $path = public_path('storage/uploads/file/'.$nametest.'/'. $filename);
                 
                      $img->resize(800, null, function ($constraint) {
                          $constraint->aspectRatio();
                      })->encode('jpg', 25)->save($path);
                 }
                 else {
                    $destinationPath = 'storage/uploads/file/'.$nametest.'/';
                    $file->move($destinationPath, $filename);
                 }
				$document = new Document;
				$document->name = $name;
				$document->type = $id;
				$document->upload = $filename; 
				$document->id_praapplication = $id_praapplication; 
				$document->save();
				
				return response()->json(['file' => "$filename"]);
			}
		}  
    }

	public function Term(Request $request) {

          $id_praapplication = $request->input('id_praapplication');
          $status = $request->input('status');
          $verification_remark = $request->input('remark');
		  $branch = $request->input('branch');
          $id = $id_praapplication;
          $user = Auth::user();
          $user_aktif = Auth::user()->name;


          
      
		 if ($status==77) {
			    // Set Time Log
		  		date_default_timezone_set("Asia/Kuala_Lumpur");
				$time_name = date('Ymd');
				$time_log = date('Y-m-d H:i:s');
				$log_download = new Log_download;
				
				$user = Auth::user();
				$id_user =  $user->id;
				$log_download->id_praapplication   =  $id;
				$log_download->id_user   =  $id_user;
				$log_download->Activity   =  'DSR PASS';
				$log_download->downloaded_at   =  $time_log;
				$log_download->log_remark   =  $verification_remark;
				// $log_download->log_reason  =  $reason;
				$log_download->save();
				$hasil = "DSR PASS";
				$terms = Term::where('id_praapplication', $id_praapplication)->limit('1')->first();
				if($terms->referral_id=='0'){
					$term = Term::where('id_praapplication', $id_praapplication)->update(['status' => '77', 'verification_remark' => $verification_remark]);

					$cust = PraApplication::latest('created_at')->where('id',  $id_praapplication )->limit('1')->first();
     
		      		$email_cust = $cust->email;
		      		$name_cust = $cust->fullname;
		       		
		       		Mail::send('mail.cust_resultdocument', compact('email_cust','name_cust'), function ($message) use ($email_cust, $name_cust) {
				       $message->from('mbsb@netxpert.com.my', 'MBSB Personal Financing-i');
				       $message->subject('[MBSB] Document Approved');
				       $message->to($email_cust, $name_cust);
				   });
				}
				else{
					$term = Term::where('id_praapplication', $id_praapplication)->update(['status' => '77', 'verification_remark' => $verification_remark]);
				}

				
            	

          }
		  else if ($status==88) {
			    // Set Time Log
		  		date_default_timezone_set("Asia/Kuala_Lumpur");
				$time_name = date('Ymd');
				$time_log = date('Y-m-d H:i:s');
				$log_download = new Log_download;
				
				$user = Auth::user();
				$id_user =  $user->id;
				$log_download->id_praapplication   =  $id;
				$log_download->id_user   =  $id_user;
				$log_download->Activity   =  'DSR REJECT';
				$log_download->downloaded_at   =  $time_log;
				$log_download->log_remark   =  $verification_remark;
				// $log_download->log_reason  =  $reason;
				$log_download->save();
				$hasil = "DSR REJECT";
				
            	$term = Term::where('id_praapplication', $id_praapplication)->update(['status' => '88', 'verification_remark' => $verification_remark]);

                $terma= Term::where('id_praapplication', $id_praapplication)->limit('1')->first();

            	if($terma->referral_id=='0'){
	            	$cust = PraApplication::latest('created_at')->where('id',  $id_praapplication )->limit('1')->first();
		      		$email_cust = $cust->email;
		      		$name_cust = $cust->name;
		       		
		       		Mail::send('mail.cust_resultdocument_reject', compact('email_cust','name_cust'), function ($message) use ($email_cust, $name_cust) {
				       $message->from('mbsb@netxpert.com.my', 'MBSB Personal Financing-i');
				       $message->subject('[MBSB] Document Approved');
				       $message->to($email_cust, $name_cust);
				   });
		       	}
           
          }
          		$basic = Basic::latest('created_at')->where('id_praapplication',$id_praapplication)->limit('1')->first();
          		$pra = PraApplication::latest('created_at')->where('id',$id_praapplication)->limit('1')->first();

          		
		 return response()->json(['status' => $hasil]);
	
     
          
    }	
}

