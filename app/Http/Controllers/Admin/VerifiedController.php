<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Model\Basic;
use App\Model\Basic_v;
use App\Model\Empinfo;
use App\Model\Empinfo_v;
use App\Model\Spouse;
use App\Model\LoanAmmount;
use App\Model\LoanAmmount_v;
use App\Model\Reference;
use App\Model\Reference_v;
use App\Model\Financial;
use App\Model\Financial_v;
use App\Model\I_financing;
use App\Model\I_financing_v;
use App\Model\Document;
use App\Model\Document_v;
use App\Model\Term;
use App\Model\User;
use App\Model\Branch;
use App\Model\Log_download;
use App\Model\Commitments;
use App\Model\Credit;
use App\Model\PraApplication;
use App\Model\Pep;
use App\Dsr_a;
use App\Dsr_b;
use App\Dsr_c;
use App\Dsr_d;
use App\Dsr_e;
use App\RiskRating;
use Input;
use Mail;
use DB;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\MOMBsc;
use App\Soap\Response\MOMBscResponse;
use App\Soap\Response\MOMEmpInfResponse;
use App\Soap\Request\MOMRef;
use App\Soap\Response\MOMRefResponse;
use App\Soap\Response\MOMSpResponse;
use App\Soap\Response\MOMCommitResponse;
use App\Soap\Response\MOMFinResponse;

use App\Soap\Response\MOMDSRAResponse;
use App\Soap\Response\MOMDSRBResponse;
use App\Soap\Response\MOMDSRCResponse;
use App\Soap\Response\MOMDSREResponse;

use App\Soap\Response\MOMLnResponse;
use App\Soap\Response\MOMRskResponse;
use App\Soap\Response\MOMTrmResponse;

use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;
use Storage;
use File;
use App\Model\CodeIDType;
use App\Model\CodeJobStatus;
use App\Model\CodeWorkSec;
//use dateDB;
class VerifiedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected $soapWrapper;

  /**
   * SoapController constructor.
   *
   * @param SoapWrapper $soapWrapper
   */
  public function __construct(SoapWrapper $soapWrapper)
  {
    $this->soapWrapper = $soapWrapper;
    $this->middleware('auth');  
      $this->middleware('admin');
  }

    
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            
       
        if($id == '1') {
            // Update Field
              $id_praapplication = $request->input('id_praapplication');

              $title = $request->input('title');
                $title_others = $request->input('title_others');
              $name = $request->input('name');
              $new_ic = $request->input('new_ic');
              $old_ic = $request->input('old_ic');
              $dob2 = $request->input('dob');
              //$dob =  $this->dateDB($dob2);
              //$dob =  '2018-12-12';
              $police_number = $request->input('police_number');
              $gender = $request->input('gender');
              $country = $request->input('country');
               $country_others = $request->input('country_others');
                $country_origin = $request->input('country_origin');

              $address = $request->input('address');
              $address2 = $request->input('address2');
              $address3 = $request->input('address3');

              $address_correspondence = $request->input('checker');

              $postcode = $request->input('postcode');
              $state = $request->input('state');
              $state_code = $request->input('state_code');
              $ownership = $request->input('ownership');
              $corres_address1 = $request->input('corres_address1');
               $corres_address2 = $request->input('corres_address2');
                $corres_address3 = $request->input('corres_address3');
              $corres_state = $request->input('corres_state');
              $corres_state1 = $request->input('corres_state2');
              $corres_postcode = $request->input('corres_postcode');
              $corres_homephone = $request->input('corres_homephone');
              $corres_mobilephone = $request->input('corres_mobilephone');
             // $corres_email = $request->input('corres_email');
              $citizen = $request->input('citizen');
              $race = $request->input('race');
               $race_others = $request->input('race_others');

              $bumiputera = $request->input('bumiputera');
              $religion = $request->input('religion');
              $religion_others = $request->input('religion_others');

              $marital = $request->input('marital');
              $dependents = $request->input('dependents');
              $education = $request->input('education');
              
               $country_dob = $request->input('country_dob');
            
            
             $basic = Basic::where('id_praapplication', $id_praapplication)
             ->update(['country' => $country, 
              'country_origin' => $country_origin, 
               'country_others' => $country_others, 
              'citizen' => $citizen, 
              'new_ic' => $new_ic, 
              //'dob' => $dob,
             'old_ic' => $old_ic, 
             'country_dob' => $country_dob,
             'name' => $name,
             'title' => $title, 
             'title_others' => $title_others,
             'gender' => $gender, 
             'marital' => $marital,
              'dependents' => $dependents,
             'race' => $race, 
             'race_others' => $race_others,
              'bumiputera' => $bumiputera, 
             'religion' => $religion , 
             'religion_others' => $religion_others,
             'marital' => $marital, 
             'police_number' => $police_number, 
             'address'=> $address,
              'address2'=> $address2,
               'address3'=> $address3,
              'postcode'=> $postcode,
               'state'=> $state, 
               'state_code'=> $state_code, 
               'corres_address1'=> $corres_address1, 
               'corres_address2'=> $corres_address2, 
               'corres_address3'=> $corres_address3, 
               'corres_state'=> $corres_state, 
               'corres_state1'=> $corres_state1, 
               'corres_postcode'=> $corres_postcode, 
               'corres_homephone'=> $corres_homephone,
                'corres_mobilephone'=> $corres_mobilephone, 
                'education'=> $education,
                 'ownership'=> $ownership,
                 'address_correspondence'=> $address_correspondence]);
            
            // Update Verification
              $title_v = $request->input('title_v');
              $name_v = $request->input('name_basic_v');
              $name_prefed_v = $request->input('name_prefed_v');
              $new_ic_v = $request->input('new_ic_basic_v');
              $old_ic_v = $request->input('old_ic_basic_v');
              //$dob =  date('Y-m-d', strtotime($dob2));
              $dob_v = $request->input('dob_basic_v');
              $police_number_v = $request->input('police_number_v');
              $gender_v = $request->input('gender_v');
              $country_v = $request->input('country_v');
              $address_v = $request->input('address_v');
              $ownership_v = $request->input('ownership_v');
              $address_corres_v = $request->input('address_corres_v');
              $citizen_v = $request->input('citizen_v');
              $race_v = $request->input('race_v');
              $relegion_v = $request->input('relegion_v');
              $marital_v = $request->input('marital_v');
              $dependents_v = $request->input('dependents_v');
              $education_v = $request->input('education_v');
              $remark = $request->input('remark');

    
            $basic_v = Basic_v::where('id_praapplication', $id_praapplication)->update(['title' => $title_v, 'name' => $name_v, 'new_ic' => $new_ic_v, 'old_ic' => $old_ic_v, 'dob' => $dob_v, 'police_number' => $police_number_v,'gender' => $gender_v,'country' => $country_v, 'address' => $address_v , 'ownership' => $ownership_v , 'address_corres' => $address_corres_v, 'citizen' => $citizen_v, 'race' => $race_v , 'religion' => $relegion_v , 'marital' => $marital_v,
            'dependents' => $dependents_v, 'education'=> $education_v, 'remark' => $remark]); 
        }

        else if($id == '2') {

            $id_praapplication = $request->input('id_praapplication');

            $empname = $request->input('empname');
            $emptype = $request->input('emptype');
            $emptype_others = $request->input('emptype_others');

            $position = $request->input('position');
            $dept_name = $request->input('dept_name');
            $division = $request->input('division');
            $address = $request->input('address');
            $address2 = $request->input('address2');
            $address3 = $request->input('address3');
            $state =  $request->input('state');
             $state_code =  $request->input('state_code3');
            $postcode =  $request->input('postcode');
            $nature_business = $request->input('nature_business');
            $joined2 =  $request->input('joined');
            $joined2 = str_replace('/', '-', $joined2);
            $joined =  date('Y-m-d', strtotime($joined2));
            $office_phone = $request->input('office_phone');
            $office_fax = $request->input('office_fax');
            $working_exp = $request->input('working_exp');
            $empstatus = $request->input('empstatus');
            
            $emp_name_v = $request->input('emp_name_v');
            $employment_id_v = $request->input('employment_id_v');
            $employer_id_v = $request->input('employer_id_v');
            $emp_type_v = $request->input('empt_type_v');
            $position_v = $request->input('position_v');
            $dept_name_v = $request->input('dept_name_v');
            $division_v = $request->input('division_v');
            $address_v = $request->input('address_v');
            $nature_business_v = $request->input('nature_business_v');
            $joined2_v =  $request->input('joined_v');
            $office_phone_v = $request->input('office_phone_v');
            $office_fax_v = $request->input('office_fax_v');
            $working_exp_v = $request->input('working_exp_v');
            $empstatus_v = $request->input('empstatus_v');
            $remark = $request->input('remark');
                
            $empinfo = Empinfo::where('id_praapplication', $id_praapplication)
            ->update(['empname' => $empname,  'emptype' => $emptype, 'emptype_others' => $emptype_others,  'position' => $position, 'dept_name' => $dept_name, 'division' => $division,'address' => $address, 'address2' => $address2, 'address3' => $address3, 'state' => $state, 'state_code' => $state_code ,'postcode' => $postcode,  'nature_business' => $nature_business, 'joined' => $joined, 'office_phone' => $office_phone, 'office_fax' => $office_fax, 'working_exp' => $working_exp,  'empstatus' => $empstatus ]);

            $empinfo_v = Empinfo_v::where('id_praapplication', $id_praapplication)->update(['remark' => $remark]); 


        }
        
        else if($id == '3') {

            $id_praapplication = $request->input('id_praapplication');

            
            $name = $request->input('name');
            $homephone =  $request->input('homephone');
            $mobilephone = $request->input('mobilephone');
            $emptype = $request->input('emptype');
            $emptype_others = $request->input('emptype_others');
           
        
            $spouse = Spouse::where('id_praapplication', $id_praapplication)
          ->update(['name' => $name, 'homephone' => $homephone, 'mobilephone' => $mobilephone, 'emptype' => $emptype, 'emptype_others' => $emptype_others ]);

            //reference
            $name1 = $request->input('name1');
            $mobilephone1 =  $request->input('mobilephone1');
            $home_phone1 =  $request->input('home_phone1');
            $relationship1 =  $request->input('relationship1');
            $name2 = $request->input('name2');
            $mobilephone2 =  $request->input('mobilephone2');
            $home_phone2 =  $request->input('home_phone2');
            $relationship2 =  $request->input('relationship2');


     

            $reference = Reference::where('id_praapplication', $id_praapplication)->update(['name1' => $name1, 'mobilephone1' => $mobilephone1, 'relationship1' => $relationship1, 'name2' => $name2, 'mobilephone2' => $mobilephone2, 'relationship2' => $relationship2, 'home_phone1' => $home_phone1,'home_phone2' => $home_phone2]);

             //v
              $remark = $request->input('remark');
            $reference_v = Reference_v::where('id_praapplication', $id_praapplication)->update(['remark' => $remark]); 



        }
       else if($id == '4') {

            $id_praapplication = $request->input('id_praapplication');

            
            $financial = Financial::where('id_praapplication', $id_praapplication)
            ->update(['monthly_income' => $request->input('monthly_income'), 
                    'other_income' => $request->input('other_income'), 
                    'total_income' => $request->input('total_income') ]);

            
            $remark = $request->input('remark');
            $Financial_v = Financial_v::where('id_praapplication', $id_praapplication)->update(['remark' => $remark]);

          }


        else if($id == '5') {

            $id_praapplication = $request->input('id_praapplication');

            // Checking If Not Empty
            $check = Commitments::where('id_praapplication', $id_praapplication)->count();

            if($check>0) {
              $commitments = Commitments::where('id_praapplication', $id_praapplication)
              ->update(['name1' => $request->input('name1'), 
                    'monthly_payment1' => $request->input('monthly_payment1'), 
                    'financing1' => $request->input('financing1'),
                    'remaining1' => $request->input('remaining1'),

                    'name2' => $request->input('name2'), 
                    'monthly_payment2' => $request->input('monthly_payment2'), 
                    'financing2' => $request->input('financing2'),
                    'remaining2' => $request->input('remaining2'),
                    'remark' => $request->input('remark'),

                     ]);
            }
            else {
              $commitments = new Commitments;
              $commitments->id_praapplication = $request->input('id_praapplication');
              $commitments->name1 = $request->input('name1');
              $commitments->monthly_payment1 = $request->input('monthly_payment1');
              $commitments->financing1 = $request->input('financing1');
              $commitments->remaining1 = $request->input('remaining1');
              $commitments->name2 = $request->input('name2');
              $commitments->monthly_payment2 = $request->input('monthly_payment2');
              $commitments->financing2 = $request->input('financing2');
              $commitments->remaining2 = $request->input('remaining2');
              $commitments->remark = $request->input('remark');
              $commitments->save();
            }

            return response()->json(['check' => $check]);

     

          }

        else if($id == '6') {

            $id_praapplication = $request->input('id_praapplication');

            $package = $request->input('package');
            $product_bundling = $request->input('product_bundling');
            $product_bundling_specify = $request->input('product_bundling_specify');
            $cross_selling = $request->input('cross_selling');
            $cross_selling_specify = $request->input('cross_selling_specify');

            $takaful_coverage = $request->input('takaful_coverage');
            $takaful_coverage_specify = $request->input('takaful_coverage_specify');

            $financing_amount = $request->input('financing_amount');
            $tenure = $request->input('tenure');

            $financial = Financial::where('id_praapplication', $id_praapplication)
            ->update(['product_bundling' => $product_bundling, 'product_bundling_specify' => $product_bundling_specify,  'cross_selling' => $cross_selling,  'cross_selling_specify' => $cross_selling_specify, 'takaful_coverage' => $takaful_coverage ,'takaful_coverage_specify' => $takaful_coverage_specify]);

            $Loanamount = LoanAmmount::where('id_praapplication', $id_praapplication)
            ->update(['package' => $package, 'loanammount' => $financing_amount,  'new_tenure' => $tenure ]);

              //v
              $remark = $request->input('remark');
            $LoanAmmount_v = LoanAmmount_v::where('id_praapplication', $id_praapplication)->update(['remark' => $remark]);
           

        }

        else if($id == '7') {

            $id_praapplication = $request->input('id_praapplication');

            $purpose_facility = $request->input('purpose_facility');
            $type_customer = $request->input('type_customer');
            $payment_mode = $request->input('payment_mode');
            $bank_name = $request->input('bank_name');
            $account_no = $request->input('account_no');

            $bank1 = $request->input('bank1');
            $bank2 = $request->input('bank2');

            $bank3 = $request->input('bank3');
            $bank4 = $request->input('bank4');

            $bank5 = $request->input('bank5');
            $bank6 = $request->input('bank6');

            $financial = Financial::where('id_praapplication', $id_praapplication)
            ->update(['purpose_facility' => $purpose_facility, 'type_customer' => $type_customer,  'payment_mode' => $payment_mode,  'bank_name' => $bank_name, 'account_no' => $account_no ,'bank1' => $bank1, 'bank2' => $bank2 ,'bank1' => $bank1, 'bank3' => $bank3 ,'bank4' => $bank4, 'bank5' => $bank5 ,'bank6' => $bank6]);

             $remark = $request->input('remark');
            $settlement_remark = Financial_v::where('id_praapplication', $id_praapplication)->update(['settlement_remark' => $remark]);
           

        }
       
        else if($id == '8') {
           
             $id_pra = $request->input('id_praapplication');

          $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
        $basic = Basic::latest('created_at')->where('id_praapplication',$id_pra)->limit('1')->first();
          $dcmt = DB::table('document')->select(DB::raw(" max(id) as id"))->where('id_praapplication',$id_pra)->groupBy('type')->pluck('id');
        $files = Document::whereIn('id', $dcmt)->get(); 
        $filesd = Document::where('id_praapplication',$id_pra)->get(); 
        $prafullname =  str_replace(" ","%20",$pra->fullname);  
            

        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
        $url = url('')."/storage/uploads/file/".$id_pra."/"; 
       //$url = "https://www.mbsb.insko.my/public/storage/uploads/file/".$id_pra."/"; 
       
        if((!empty($document5->upload)) OR (!empty($document6->upload)) OR (!empty($document7->upload)) OR (!empty($document8->upload)) OR (!empty($document9->upload))) {
            $zip = new \ZipArchive();

 
            // store the public path
            $publicDir = public_path();
             $tmp_file = tempnam('.','');
            // Define the file name. Give it a unique name to avoid overriding.
            $zipFileName = $id_pra.'.zip';
              
            if ($zip->open($publicDir . '/storage/uploads/file/'.$id_pra.'/' . $zipFileName, \ZipArchive::CREATE) === true) {
                // Loop through each file
                foreach($files as $file){
                     $url2 = $url.$file->upload;
                     $url2 = str_replace(' ', '%20', $url2);

                        if (!function_exists('curl_init')){ 
                            die('CURL is not installed!');
                        }

                     $ch = curl_init();
                     curl_setopt($ch, CURLOPT_URL, $url2);
                     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                     $output = curl_exec($ch);
                     curl_close($ch);
                     $download_file = $output;

                     $type = substr($url2, -5, 5); 
                     #add it to the zip
                     $zip->addFromString(basename($url.$file->upload.'.'.$type),$download_file);
                }
                // close zip
                $zip->close();
            }

             // Download the file using the Filesystem API
             $filePath = $publicDir . '/storage/uploads/file/' . $zipFileName;

             if (file_exists($filePath)) {
                 return Storage::download($filePath);
             }
        
              
                
            }
    

            $document6 =  $request->input('document6_v');
            $document7 =  $request->input('document7_v');
            $document9 =  $request->input('document9_v');
  
           
            $document6_v = Document::where('id_praapplication', $id_pra)->where('type',  '6' )
            ->update(['verification' => $document6]);
            $document7_v = Document::where('id_praapplication', $id_pra)->where('type',  '7' )
            ->update(['verification' => $document7]);
          
            $document9_v = Document::where('id_praapplication', $id_pra)->where('type',  '9' )
            ->update(['verification' => $document9]);
      
        }

       
        
    }
    
    public function term(Request $request) {

               $id_praapplication = $request->input('id_praapplication');
          $declaration = $request->input('declaration');
          $verification_remark = $request->input('remark');
      $branch = $request->input('branch');
          $id = $id_praapplication;
            $user = Auth::user();
            $user_aktif = Auth::user()->name;


            $fullname = $request->input('fullname');
            $mykad = $request->input('mykad');
            $passport = $request->input('passport');
            $relationship = $request->input('relationship');
        // cek kredit
          /*  $count_credit = Credit::where('id_praapplication', $id_praapplication)->count();
            if($count_credit=='0') {
              $credit = new Credit ;
              $credit->fullname = $fullname;
              $credit->mykad =  $mykad;
              $credit->passport =  $passport;
              $credit->relationship =  $relationship;
              $credit->id_praapplication = $id_praapplication;
              $credit->save();
            }
            else if($count_credit=='1') {
               $credit = Credit::where('id_praapplication', $id_praapplication)->update(['fullname' => $fullname, 
              'mykad' => $mykad,
              'passport' => $passport,
              'relationship' => $relationship]);
            }

            $name1 = $request->input('name1');
            $relationship1 = $request->input('relationship1');
            $status1 = $request->input('status1');
            $prominent1 = $request->input('prominent1');

            $name2 = $request->input('name2');
            $relationship2 = $request->input('relationship2');
            $status2 = $request->input('status2');
            $prominent2 = $request->input('prominent2');

             $name3 = $request->input('name3');
            $relationship3 = $request->input('relationship3');
            $status3 = $request->input('status3');
            $prominent3 = $request->input('prominent3');

            // cek pep
            $count_pep = Pep::where('id_praapplication', $id_praapplication)->count();
            if($count_pep=='0') {
              $pep = new Pep ;
              $pep->name1 = $name1;
              $pep->relationship1 =  $relationship1;
              $pep->status1 =  $status1;
              $pep->prominent1 =  $prominent1;
              //2
              $pep->name2 = $name2;
              $pep->relationship2 =  $relationship2;
              $pep->status2 =  $status2;
              $pep->prominent2 =  $prominent2;
              //3
              $pep->name3 = $name3;
              $pep->relationship3 =  $relationship3;
              $pep->status3 =  $status3;
              $pep->prominent3 =  $prominent3;

              $pep->id_praapplication = $id_praapplication;
              $pep->save();
            }
            else  if($count_pep=='1'){
              $pep = Pep::where('id_praapplication', $id_praapplication)->update(['name1' => $request->name1, 
                  'relationship1' => $relationship1,
                  'status1' => $status1,
                  'prominent1' => $prominent1,
                  'name2' => $name2, 
                  'relationship2' => $relationship2,
                  'status2' => $status2,
                  'prominent2' => $prominent2,
                  'name3' => $name3, 
                  'relationship3' => $relationship3,
                  'status3' => $status3,
                  'prominent3' => $prominent3,
                  'id_praapplication' => $id_praapplication

                  ]);
            }*/


          
          if ($declaration==2) { // IF APPROVE
           
     // return response()->json(['status' => "Application Approved!"]);
     // sent email to branch
     $branchname = Branch::where('branch_code', $branch)->first()->branchname;
     $basic = Basic::latest('created_at')->where('id_praapplication',  $id_praapplication )->limit('1')->first(); 
     $cus_name = $basic->name;
     $cus_ic  = $basic->new_ic;
    
     // Ibu Pejabat Email List
     $ho = User::latest('created_at')->where('role',  '3' )->get();
     foreach ($ho as $a) {
      $emailho = $a->email;
      $nameho = $a->name;
       Mail::send('mail.verification_approved_ho', compact('branchname','nameho','cus_name','cus_ic'), function ($message) use ($emailho,$nameho) {
       $message->from('mbsb@ezlestari.com.my', 'mbsb@ezlestari.com.my');
       $message->subject('[MBSB] New Application has Route to Branch');
        $message->to($emailho, $nameho);
        //  echo 'email ='.$emailho;
       
       });
       
     } 
      // Branch Email list
      
      $branch_user = User::latest('created_at')->where('role',  '2' )->where('id_branch',  $branch )->get();
      foreach ($branch_user as $b) {
      $emailbranch = $b->email;
      
       Mail::send('mail.verification_approved', compact('branchname','cus_name','cus_ic'), function ($message) use ($emailbranch) {
       $message->from('mbsb@ezlestari.com.my', 'mbsb@ezlestari.com.my');
       $message->subject('[MBSB] New Application has Route to Your Branch');
        $message->to($emailbranch);
        //  echo 'email ='.$emailho;
       
       });

       
       
     }
      // Set Time Log
          date_default_timezone_set("Asia/Kuala_Lumpur");
        $time_name = date('Ymd');
        $time_log = date('Y-m-d H:i:s');
        $log_download = new Log_download;
        
        $user = Auth::user();
        $id_user =  $user->id;
        $log_download->id_praapplication   =  $id;
        $log_download->id_user   =  $id_user;
        $log_download->Activity   =  'Application Submitted to WAPS and Route to '.$branchname.' Branch';
        $log_download->downloaded_at   =  $time_log;
        $log_download->log_remark   =  $verification_remark;
        // $log_download->log_reason  =  $reason;
        $log_download->save();
        
      $terms = Term::where('id_praapplication', $id_praapplication)->update(['verification_status' => '1', 'verification_result' => $declaration, 'verification_remark' => $verification_remark, 'verified_by' => $user_aktif, 'id_branch' => $branch, 'status' => '1' ]);
         $prap = Basic::where('id_praapplication',$id_praapplication)->first();
            $ics = $prap->new_ic;
            $doc_7 = Document::latest('created_at')->where('id_praapplication', $id_praapplication)->where('type',7)->limit(1)->first();
            $doc_6 = Document::latest('created_at')->where('id_praapplication', $id_praapplication)->where('type',6)->limit(1)->first();
            $doc_9 = Document::latest('created_at')->where('id_praapplication', $id_praapplication)->where('type',9)->limit(1)->first();
        
           /* Storage::disk('sftp')->makeDirectory($ics, $mode = 0777, true, true);


            $file_local9 = Storage::disk('local')->get($ics.'/'.$doc_9->upload);
            $file_ftp9 = Storage::disk('sftp')->put($ics.'/'.$doc_9->upload, $file_local9);

            if (!(file_exists($file_ftp9))) {
                $move9 = Storage::move($file_local9, $file_ftp9);
            }    
           

            $file_local6 = Storage::disk('local')->get($ics.'/'.$doc_6->upload);
            $file_ftp6 = Storage::disk('sftp')->put($ics.'/'.$doc_6->upload, $file_local6);

            if (!(file_exists($file_ftp6))) {  
                $move6 = Storage::move($file_local6, $file_ftp6);
            }  


            $file_local7 = Storage::disk('local')->get($ics.'/'.$doc_7->upload);
            $file_ftp7 = Storage::disk('sftp')->put($ics.'/'.$doc_7->upload, $file_local7);
            
            if (!(file_exists($file_ftp7))) {  
               $move7 = Storage::move($file_local7, $file_ftp7);
            }*/
       // send to WAPS
            $pra        = Basic::where('id_praapplication',$id_praapplication)->first();
            $empinfo    = Empinfo::where('id_praapplication',$id_praapplication)->first();
            $spouse     = Spouse::where('id_praapplication',$id_praapplication)->first();
            $ref        = Reference::where('id_praapplication',$id_praapplication)->first();
            $fin        = Financial::where('id_praapplication',$id_praapplication)->first();
            $com        = Commitments::where('id_praapplication',$id_praapplication)->first();
            $loan_a      = LoanAmmount::where('id_praapplication',$id_praapplication)->first();
            
            $dsr_a        = Dsr_a::where('id_praapplication',$id_praapplication)->first();
            $dsr_b        = Dsr_b::where('id_praapplication',$id_praapplication)->first();
            $dsr_c        = Dsr_c::where('id_praapplication',$id_praapplication)->first();
            $dsr_e        = Dsr_e::where('id_praapplication',$id_praapplication)->first();
            $dsr_d        = Dsr_d::where('id_praapplication',$id_praapplication)->first();
            $risk         = RiskRating::where('id_praapplication',$id_praapplication)->first();
            $term         = Term::where('id_praapplication',$id_praapplication)->first();
            $doc6         = Document::latest('created_at')->where('id_praapplication',$id_praapplication)->where('type','6')->limit('1')->first();
             $doc7         = Document::latest('created_at')->where('id_praapplication',$id_praapplication)->where('type','7')->limit('1')->first();
             $doc9         = Document::latest('created_at')->where('id_praapplication',$id_praapplication)->where('type','9')->limit('1')->first();

               Storage::disk('ftp')->makeDirectory($id_praapplication, $mode = 0777, true, true);

             $zipFileName = $id_praapplication.'.zip';
            $file_local9 = public_path().'/storage/uploads/file/'. $id_praapplication.'/'.$zipFileName;
              //Storage::disk('image')->put($name, file_get_contents($file));
           // $file_local9 = Storage::disk('public')->get($id_praapplication.'/'.$zipFileName);
            $file_ftp9 = Storage::disk('ftp')->put($id_praapplication.'/'.$zipFileName, file_get_contents($file_local9));

            if (!(file_exists($file_ftp9))) {
                $move9 = Storage::move($file_local9, $file_ftp9);
            }  


            $this->soapWrapper->add('MOMBsc', function ($service) use($pra, $empinfo,$spouse,$ref,$fin,$com,$loan_a,$dsr_a,$dsr_b,$dsr_c,$dsr_d,$dsr_e,$risk,$term,$doc7,$doc9,$doc6){
            $service
                ->wsdl('http://moaqs.com/MOMWS/MOMWS.asmx?WSDL')
                ->trace(true)
                ->cache(WSDL_CACHE_NONE)
                //->header('http://www.w3.org/2001/XMLSchema-instance', 'Action', 'http://tempuri.org/MOMBsc')
                ->options([
                    'login' => 'momwapsuat',
                    'password' => 'J@ctL71N$#'
                    ])
                ->classmap([
                    MOMBsc::class,
                    MOMBscResponse::class,
                    MOMEmpInfResponse::class,
                    MOMSpResponse::class,
                    MOMRefResponse::class,
                    MOMFinResponse::class,
                    MOMCommitResponse::class
                ]);
            });
            
            $response_tm= $this->soapWrapper->call('MOMBsc.MOMTrm', [
                [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'TR001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'b_offid' => $term->mo_id,
                    'v_offid' => $term->verified_by,
                    'v_rmk' => $term->verification_remark,
                    'brid' => $term->id_branch
                ]
            ]);
            
            $response_com= $this->soapWrapper->call('MOMBsc.MOMCommit', [
                [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'CT001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'nm1' => $com->name1,
                    'mth_py1' => $com->monthly_payment1,
                    'fin1' => $com->financing1,
                    'rmn1' => $com->remaining1,
                     'nm2' => $com->name2,
                    'mth_py2' => $com->monthly_payment2,
                    'fin2' => $com->financing2,
                    'rmn2' => $com->remaining2,
                    'rmk' => $com->remark
                ]
            ]);
            
            $changesymbol = $pra->name;
            $changesymbol =  str_replace("'","`",$changesymbol);

            $address = $pra->address;
            $address =  str_replace("'","`",$address);

            $address2 = $pra->address2;
            $address2 =  str_replace("'","`",$address2);

            $address3 = $pra->address3;
            $address3 =  str_replace("'","`",$address3);

            $corres_address1 = $pra->corres_address1;
            $corres_address1 =  str_replace("'","`",$corres_address1);

            $corres_address2 = $pra->corres_address2;
            $corres_address2 =  str_replace("'","`",$corres_address2);

            $corres_address3 = $pra->corres_address3;
            $corres_address3 =  str_replace("'","`",$corres_address3);

            $title_others = $pra->title_others;
            $title_others =  str_replace("'","`",$title_others);

            $country_others = $pra->country_others;
            $country_others =  str_replace("'","`",$country_others);

            $race_others = $pra->race_others;
            $race_others =  str_replace("'","`",$race_others);

            $religion_others = $pra->religion_others;
            $religion_others =  str_replace("'","`",$religion_others);


            $response = $this->soapWrapper->call('MOMBsc.MOMBsc',[
                [
                'AppNo' => $pra->id_praapplication,
                'txnCode' => 'CI001',
                'ActCode' => 'A',
                'usr' => 'momwapsuat',
                'pwd' => 'J@ctL71N$#',
                'ctry' => $pra->country,
                'ctry_ori' => $pra->country_origin,
                'ctzn' => $pra->citizen,
                'n_id' => $pra->new_ic,
                'o_id' => $pra->old_ic,
                'ctry_dob' => $pra->country_dob,
                'p_id' => $pra->police_number,
                'dob' => $pra->dob,
                'nm' => $changesymbol,
                'nm_pref' => $pra->name_prefed,
                'ttl' => $pra->title,
                'ttl_oth' => $title_others,
                'gdr' => $pra->gender,
                'marit' => $pra->marital,
                'depend' => $pra->dependents,
                'add1' => $address,
                 'add2' => $address2,
                'add3' => $address3,
                'add4' => $pra->postcode,
                'add5' => $pra->state_code,
                'cor_add1' => $corres_address1,
                'cor_add2' => $corres_address2,
                'cor_add3' => $corres_address3,
                'cor_add4' => $pra->corres_postcode,
                 'cor_add5' => $pra->corres_state1,
                'cor_add6' => $pra->corres_homephone,
                'cor_add7' => $pra->corres_mobilephone,
                'cor_add8' => $pra->corres_email,
                 'own' => $pra->ownership,
                'rce' => $pra->race,
                'bpr' => $pra->bumiputera,
                'rel' => $pra->religion,
                'edc' => $pra->education,
                'ctry_oth' => $country_others,
                'rce_oth' => $race_others,
                'rel_oth' => $religion_others,
                'ctg' => $pra->category,
                ]
            ]);
           
            $empaddress = $empinfo->address;
            $empaddress =  str_replace("'","`",$empaddress);

            $empaddress2 = $empinfo->address2;
            $empaddress2 =  str_replace("'","`",$empaddress2);

            $empaddress3 = $empinfo->address3;
            $empaddress3 =  str_replace("'","`",$empaddress3);

            $division = $empinfo->division;
            $division =  str_replace("'","`",$division);

            $dept_name = $empinfo->dept_name;
            $dept_name =  str_replace("'","`",$dept_name);

            $emptype_others = $empinfo->emptype_others;
            $emptype_others =  str_replace("'","`",$emptype_others);

            $response_emp = $this->soapWrapper->call('MOMBsc.MOMEmpInf',[
                [
                'AppNo' => $pra->id_praapplication,
                'txnCode' => 'EI001',
                'ActCode' => 'A',
                'usr' => 'momwapsuat',
                'pwd' => 'J@ctL71N$#',
                'emp_nm' => $empinfo->empname,
                'emp_ty' => $empinfo->emptype,
                'emp_oth' => $emptype_others,
                'emp_id' => $empinfo->employment_id,
                'empr_id' => $empinfo->employer_id,
                'post' => $empinfo->position,
                'add1' => $empaddress,
                 'add2' => $empaddress2,
                'add3' => $empaddress3,
                'add4' => $empinfo->state_code,
                'add5' => $empinfo->postcode,
                'join1' => $empinfo->joined,
                'slry' => $empinfo->salary,
                'allw' => $empinfo->allowance,
                'ddc' => $empinfo->deduction,
                 'occu' => $empinfo->occupation,
                'occu_sec' => $empinfo->occupation_sector,
                'emp_stat' => $empinfo->empstatus,
                'dept_nm' => $dept_name,
                 'dvs' => $division,
                'nob' => $empinfo->nature_business,
                'of_phn' => $empinfo->office_phone,
                'of_fx' => $empinfo->office_fax,
                'wrk_ex' => $empinfo->working_exp,
                ]
            ]);

            $changesymbol_sp = $spouse->name;
            $changesymbol_sp =  str_replace("'","`",$changesymbol_sp);

            $response_sp = $this->soapWrapper->call('MOMBsc.MOMSp',[
                [
                'AppNo' => $pra->id_praapplication,
                'txnCode' => 'CS001',
                'ActCode' => 'A',
                'usr' => 'momwapsuat',
                'pwd' => 'J@ctL71N$#',
                "nmSp"  => $changesymbol_sp,
                "hmphSp"  => $spouse->homephone,
                "mbphSp"  => $spouse->mobilephone,
                "emtySp"  => $spouse->emptype,
                "emtySp_ot"  => $spouse->emptype_others,
               ]
             ]);

            $changesymbol_rf1 = $ref->name1;
            $changesymbol_rf1 =  str_replace("'","`",$changesymbol_rf1);

            $changesymbol_rf2 = $ref->name2;
            $changesymbol_rf2 =  str_replace("'","`",$changesymbol_rf2);

            $response_ref = $this->soapWrapper->call('MOMBsc.MOMRef', [
                [
                'AppNo' =>$pra->id_praapplication,
                'txnCode' => 'CR001',
                'ActCode' => 'A',
                'usr' => 'momwapsuat',
                'pwd' => 'J@ctL71N$#',
                'nm1' => $changesymbol_rf1,
                'hm1' => $ref->home_phone1,
                'hp1' => $ref->mobilephone1,
                'rel1' => $ref->relationship1,
                'nm2' => $changesymbol_rf2,
                'hm2' => $ref->home_phone2,
                'hp2' => $ref->mobilephone2,
                'rel2' => $ref->relationship2,
                ]
            ]);

            $bank_name = $fin->bank_name;
            $bank_name =  str_replace("'","`",$bank_name);

            $bank1 = $fin->bank1;
            $bank1 =  str_replace("'","`",$bank1);

            $bank2 = $fin->bank2;
            $bank2 =  str_replace("'","`",$bank2);

            $bank3 = $fin->bank3;
            $bank3 =  str_replace("'","`",$bank3);

            $bank4 = $fin->bank4;
            $bank4 =  str_replace("'","`",$bank4);

            $bank5 = $fin->bank5;
            $bank5 =  str_replace("'","`",$bank5);

            $bank6 = $fin->bank6;
            $bank6 =  str_replace("'","`",$bank6);

            $product_bundling_specify = $fin->product_bundling_specify;
            $product_bundling_specify =  str_replace("'","`",$product_bundling_specify);

            $cross_selling_specify = $fin->cross_selling_specify;
            $cross_selling_specify =  str_replace("'","`",$cross_selling_specify);

            $takaful_coverage_specify = $fin->takaful_coverage_specify;
            $takaful_coverage_specify =  str_replace("'","`",$takaful_coverage_specify);


            $response_fin = $this->soapWrapper->call('MOMBsc.MOMFin', [
                [
                'AppNo' => $pra->id_praapplication,
                'txnCode' => 'FC001',
                'ActCode' => 'A',
                'usr' => 'momwapsuat',
                'pwd' => 'J@ctL71N$#',
                'mthInc' => $fin->monthly_income,
                'othInc' => $fin->other_income,
                'totInc' => $fin->total_income,
                'proBund' => $fin->product_bundling,
                'proBundSpec' => $product_bundling_specify,
                 'cSell' => $fin->cross_selling,
                'cSellSpec' => $cross_selling_specify,
                'tkfCov' => $fin->takaful_coverage,
                'tkfCovSpec' => $takaful_coverage_specify,
                'purFac' => $fin->purpose_facility,
                'tyCust' => $fin->type_customer,
                'pyMod' => $fin->payment_mode,
                'bNm' => $bank_name,
                'accNo' => $fin->account_no,
                'bnk1' => $bank1,
                'bnk2' => $bank2,
                'bnk3' => $bank3,
                'bnk4' => $bank4,
                'bnk5' => $bank5,
                'bnk6' => $bank6,
                ]
            ]);

            $comname1 = $com->name1;
            $comname1 =  str_replace("'","`",$comname1);

            $comfinancing1 = $com->financing1;
            $comfinancing1 =  str_replace("'","`",$comfinancing1);

            $comname2 = $com->name2;
            $comname2 =  str_replace("'","`",$comname2);

            $comfinancing2 = $com->financing2;
            $comfinancing2 =  str_replace("'","`",$comfinancing2);

            $response_com = $this->soapWrapper->call('MOMBsc.MOMCommit', [
                [
                'AppNo' => $pra->id_praapplication,
                'txnCode' => 'CT001',
                'ActCode' => 'A',
                'usr' => 'momwapsuat',
                'pwd' => 'J@ctL71N$#',
                'nm1' => $comname1,
                'mth_py1' => $com->monthly_payment1,
                'fin1' => $comfinancing1,
                'rmn1' => $com->remaining1,
                'nm2' => $comname2,
                'fin2' => $comfinancing2,
                'rmn2' => $com->remaining2,
                'rmrk' => $com->remark,
                'mth_py2' => $com->monthly_payment2,
                ]
            ]);
            /*DSR A*/
        
            $response_da = $this->soapWrapper->call('MOMBsc.MOMDSRA',[
                [
                'AppNo' => $pra->id_praapplication,
                'txnCode' => 'DA001',
                'ActCode' => 'A',
                'usr' => 'momwapsuat',
                'pwd' => 'J@ctL71N$#',
                'sec' => $dsr_a->sector,
                'gdr' => $dsr_a->gender,
                'cat' => $dsr_a->category,
                'ex_mb' => $dsr_a->existing_mbsb,
                'ag' => $dsr_a->age,
                'bc_sal1' => $dsr_a->basic_salary1,
                'bc_sal2' => $dsr_a->basic_salary2,
                'bc_sal3' => $dsr_a->basic_salary3,
                'fx_alw1' => $dsr_a->fixed_allowances1,
                'fx_alw2' => $dsr_a->fixed_allowances2,
                'fx_alw3' => $dsr_a->fixed_allowances3,
                'tot_en1' => $dsr_a->total_earnings1,
                'tot_en2' => $dsr_a->total_earnings2,
                'tot_en3' => $dsr_a->total_earnings3,
                'vr_in' => $dsr_a->variable_income,
                'gmi' => $dsr_a->gmi,
                'ep1' => $dsr_a->epf1,
                'ep2' => $dsr_a->epf2,
                'ep3' => $dsr_a->epf3,
                'sc1' => $dsr_a->socso1,
                'sc2' => $dsr_a->socso2,
                'sc3' => $dsr_a->socso3,
                'in_tx1' => $dsr_a->income_tax1,
                'in_tx2' => $dsr_a->income_tax2,
                'in_tx3' => $dsr_a->income_tax3,
                'ot_cn1' => $dsr_a->other_contribution1,
                'ot_cn2' => $dsr_a->other_contribution2,
                'ot_cn3' => $dsr_a->other_contribution3,
                'tot_dc1' => $dsr_a->total_deduction1,
                'tot_dc2' => $dsr_a->total_deduction2,
                'tot_dc3' => $dsr_a->total_deduction3,
                'nmi' => $dsr_a->nmi,
                'ad_in' => $dsr_a->additional_income,
                'ite' => $dsr_a->ite,
                'sc1_vby' => $dsr_a->sec1_verified_by,
                'sc1_vt' => $dsr_a->sec1_verified_time,
                'tot_gm' => $dsr_a->total_gross_income,
                'tot_nin' => $dsr_a->total_net_income
                ]
            ]);
            
             $response_db = $this->soapWrapper->call('MOMBsc.MOMDSRB', [
                [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'DB001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'fnam' => $dsr_b->financing_amount,
                    'ten' => $dsr_b->tenure,
                    'rt' => $dsr_b->rate,
                    'mt_rp' => $dsr_b->monthly_repayment,
                    'ex_ln' => $dsr_b->existing_loan,
                    'tot_gpx' =>$dsr_b->total_group_exposure
                ]
            ]);

             $response_dc = $this->soapWrapper->call('MOMBsc.MOMDSRC', [
                [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'DC001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'mb_ho' => $dsr_c->mbsb_housing,
                    'mb_hr' =>  $dsr_c->mbsb_hire,
                    'mb_p' => $dsr_c->mbsb_personal,
                    'd_ag' => $dsr_c->de_angkasa,
                    'd_gvh' => $dsr_c->de_govhousing,
                    'd_kp' => $dsr_c->de_koperasi,
                    'd_nb' => $dsr_c->de_nonbiro,
                    'd_ol' => $dsr_c->de_otherloan,
                    'd_ot' => $dsr_c->de_other,
                    'c_ho' => $dsr_c->ccris_housing,
                    'c_cd' => $dsr_c->ccris_creditcard,
                    'c_hr' => $dsr_c->ccris_hire,
                    'c_od' => $dsr_c->ccris_otherdraft,
                    'c_ot' => $dsr_c->ccris_other,
                    'tot_cm' => $dsr_c->total_commitment
                ]
            ]);

            $response_de = $this->soapWrapper->call('MOMBsc.MOMDSRE', [
                [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'DE001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'tot_db' => $dsr_e->total_debt,
                    'n_in' => $dsr_e->net_income,
                    'ds' => $dsr_e->dsr,
                    'ds_bn' => $dsr_e->dsr_bnm,
                    'ndi' => $dsr_e->ndi,
                    'dec' => $dsr_e->decision,
                    'd_sv' => $dsr_e->data_save,
                    'sc2_vby' => $dsr_d->sec2_verified_by,
                    'sc2_vt' => $dsr_d->sec2_verified_time,
                ]
            ]);

            $response_rk = $this->soapWrapper->call('MOMBsc.MOMRsk', [
                [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'DE001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'ds' => $dsr_e->dsr,
                    'ndi' => $dsr_e->ndi,
                    'sl_dn' => $risk->salary_deduction,
                    'ag' => $risk->age,
                    'edlv' => $risk->education_level,
                    'emp' => $risk->employment,
                    'pos' => $risk->position,
                    'mart' => $risk->marital_status,
                    'sp_ep' => $risk->spouse_employment,
                    'pr_ln' => $risk->adverse_ccris,
                    'ad_c' => $risk->adverse_ccris,
                    'tot_sc' => $risk->total_score,
                    'grd' => $risk->grading,
                    'dcs' => $risk->decision,
                    'sc_by' => $risk->scoring_by,
                    'd_sc' => $risk->date_scoring,
                    'rv_by' => $risk->reviewed_by,
                    'rv_d' => $risk->date_reviewed,
                ]
            ]);

            $changesymbol_st = $pra->name;
            $changesymbol_st =  str_replace("'","`",$changesymbol_st);

            $response_stat = $this->soapWrapper->call('MOMBsc.MOMStat',[
                [
                'AppNo' => $pra->id_praapplication,
                'txnCode' => 'ST001',
                'ActCode' => 'A',
                'usr' => 'momwapsuat',
                'pwd' => 'J@ctL71N$#',
                'ApNm' => $changesymbol_st,
                'ApID' => $pra->new_ic,
                'ApStat' => 'GLAN',
                ]
            ]);

            $response_ln = $this->soapWrapper->call('MOMBsc.MOMLn', [
                [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'LN001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'pack' => $loan_a->package,
                    'lnamt' => $loan_a->loanammount,
                    'nwten' => $loan_a->new_tenure,
                    'mxln' => $loan_a->maxloan,
                    'inst' => $loan_a->installment,
                    'rt' => $loan_a->rate,
                ]
            ]);
            
            $response_doc7 = $this->soapWrapper->call('MOMBsc.MOMDoc',[
                [
                'AppNo' => $pra->id_praapplication,
                'txnCode' => 'DC001',
                'ActCode' => 'A',
                'usr' => 'momwapsuat',
                'pwd' => 'J@ctL71N$#',
                'nm' => $doc7->name,
                'dwld' => $doc7->download,
                'upl' => $doc7->upload,
                'ty' => $doc7->type,
                'vrf_flg' => $doc7->verification,
                'vfr' => $doc7->verified_by
                ]
            ]);

              $response_doc6 = $this->soapWrapper->call('MOMBsc.MOMDoc',[
                [
                'AppNo' => $pra->id_praapplication,
                'txnCode' => 'DC001',
                'ActCode' => 'A',
                'usr' => 'momwapsuat',
                'pwd' => 'J@ctL71N$#',
                'nm' => $doc6->name,
                'dwld' => $doc6->download,
                'upl' => $doc6->upload,
                'ty' => $doc6->type,
                'vrf_flg' => $doc6->verification,
                'vfr' => $doc6->verified_by
                ]
            ]);

              $response_doc9 = $this->soapWrapper->call('MOMBsc.MOMDoc',[
                [
                'AppNo' => $pra->id_praapplication,
                'txnCode' => 'DC001',
                'ActCode' => 'A',
                'usr' => 'momwapsuat',
                'pwd' => 'J@ctL71N$#',
                'nm' => $doc9->name,
                'dwld' => $doc9->download,
                'upl' => $doc9->upload,
                'ty' => $doc9->type,
                'vrf_flg' => $doc9->verification,
                'vfr' => $doc9->verified_by
                ]
            ]);
                
         
var_dump($response);
            var_dump($response_emp);
             
             var_dump($response_sp);
             var_dump($response_fin);
             var_dump($response_com);
              dd($response);
            dd($response_emp);
           
            dd($response_sp);
            dd($response_fin);
            dd($response_ref);
            dd($response_com);

       return redirect('admin')->with('message', 'Application Successfully Routed to Branch');
      // return response()->json(['status' => "Application Approved !"]);
     
      }
      else if ($declaration==3) { //IF REJECT
         // Set Time Log
          date_default_timezone_set("Asia/Kuala_Lumpur");
        $time_name = date('Ymd');
        $time_log = date('Y-m-d H:i:s');
        $log_download = new Log_download;
        
        $user = Auth::user();
        $id_user =  $user->id;
        $log_download->id_praapplication   =  $id;
        $log_download->id_user   =  $id_user;
        $log_download->Activity   =  'Application Rejected';
        $log_download->downloaded_at   =  $time_log;
        $log_download->log_remark   =  $verification_remark;
        // $log_download->log_reason  =  $reason;
        $log_download->save();
            $term = Term::where('id_praapplication', $id_praapplication)->update(['verification_status' => '1', 'verification_result' => $declaration, 'verification_remark' => $verification_remark, 'verified_by' => $user_aktif, 'id_branch' => $branch   ]);
            return response()->json(['status' => "Waiting User Response!"]);
          }
      else {
            $term = Term::where('id_praapplication', $id_praapplication)->update(['verification_status' => '1', 'verification_result' => $declaration, 'verification_remark' => $verification_remark, 'verified_by' => $user_aktif, 'id_branch' => $branch   ]);
            return response()->json(['status' => "Waiting Document!"]);
          }
     
     
          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
