<?php

namespace App\Soap\Response;

class MOMTrmResponse
{
  /**
   * @var string
   */
  protected $MOMTrmResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMTrmResult)
  {
    $this->MOMTrmResult = $MOMTrmResult;
  }

  /**
   * @return string
   */
  public function getMOMTrmResult()
  {
    return $this->MOMTrmResult;
  }
}