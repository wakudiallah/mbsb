<?php

namespace App\Soap\Response;

class MOMCTransResponse
{
  /**
   * @var string
   */
  protected $MOMCTransResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMCTransResult)
  {
    $this->MOMCTransResult = $MOMCTransResult;
  }

  /**
   * @return string
   */
  public function getMOMCTransResult()
  {
    return $this->MOMCTransResult;
  }
}