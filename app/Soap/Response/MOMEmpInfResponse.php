<?php

namespace App\Soap\Response;

class MOMEmpInfResponse
{
  /**
   * @var string
   */
  protected $MOMEmpInfResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMEmpInfResult)
  {
    $this->MOMEmpInfResult = $MOMEmpInfResult;
  }

  /**
   * @return string
   */
  public function getMOMEmpInfResult()
  {
    return $this->MOMEmpInfResult;
  }
}