<?php

namespace App\Soap\Request;

class MOMBscRequest2
{
  /**
   * @var string
   */
  protected $ctry;

  /**
   * @var string
   */
  protected $ctry_ori;

  /**
   * @var string
   */
  protected $ctzn;

  /**
   * @var string
   */
  protected $n_id;

  /**
   * @var string
   */
  protected $o_id;
  protected $ctry_dob;
  protected $p_id;
  protected $dob;
  protected $nm;
  protected $nm_pref;
  protected $ttl;
  protected $ttl_oth;

  protected $gdr;
  protected $marit;
  protected $depend;
  protected $add1;
  protected $add2;
  protected $add3;
  protected $add4;
  protected $add5;

  protected $cor_add1;
  protected $cor_add2;
  protected $cor_add3;
  protected $cor_add4;
  protected $cor_add5;
  protected $cor_add6;
  protected $cor_add7;
  protected $cor_add8;

  protected $own;
  protected $rce;
  protected $bpr;
  protected $rel;
  protected $edc;
  protected $ctry_oth;
  protected $rce_oth;
  protected $rel_oth;

  /**
   * @var string
   */
  
  /**
   * @var string
   */

  /**
   * GetConversionAmount constructor.
   *
   * @param string $CurrencyFrom
   * @param string $CurrencyTo
   * @param string $RateDate
   * @param string $Amount
   */
  public function __construct($ctry, $ctry_ori, $ctzn, $n_id, $o_id, $ctry_dob, $p_id, $dob, $nm, $nm_pref, $ttl, $ttl_oth, $gdr, $marit, $depend, $add1, $add2, $add3, $add4, $add5, $cor_add1, $cor_add8, $cor_add7, $cor_add6, $cor_add5, $cor_add4, $cor_add3, $cor_add2, $own, $rce, $bpr, $rel, $edc, $ctry_oth, $rce_oth, $rel_oth, $ctg, $appno)
  {
    $this->ctry        = $ctry;
    $this->ctry_ori     = $ctry_ori;
    $this->ctzn     = $ctzn;
    $this->n_id       = $n_id;
    $this->o_id      = $o_id;
    $this->ctry_dob      = $ctry_dob;

     $this->p_id        = $p_id;
    $this->dob     = $dob;
    $this->nm     = $nm;
    $this->nm_pref       = $nm_pref;
    $this->ttl      = $ttl;
    $this->ttl_oth      = $ttl_oth;

     $this->gdr        = $gdr;
    $this->marit     = $marit;
    $this->depend     = $depend;
    $this->add1       = $add1;
    $this->add2      = $add2;
    $this->add3      = $add3;
     $this->add4        = $add4;
    $this->add5     = $add5;
    $this->cor_add1     = $cor_add1;
    $this->cor_add2       = $cor_add2;
    $this->cor_add3      = $cor_add3;
    $this->cor_add4      = $cor_add4;
    $this->cor_add5     = $cor_add5;
    $this->cor_add6       = $cor_add6;
    $this->cor_add7      = $cor_add7;
    $this->cor_add8      = $cor_add8;

    $this->own       = $own;
    $this->rce      = $rce;
    $this->bpr      = $bpr;
    $this->rel     = $rel;
    $this->edc       = $edc;
    $this->ctry_oth      = $ctry_oth;
    $this->rce_oth      = $rce_oth;
    $this->ctg      = $ctg;
    $this->app_no      = $app_no;

  }

  /**
   * @return string
   */
  public function getctry()
  {
    return $this->ctry;
  }

  /**
   * @return string
   */
  public function getctry_ori()
  {
    return $this->ctry_ori;
  }

  /**
   * @return string
   */
  public function getctzn()
  {
    return $this->ctzn;
  }

  /**
   * @return string
   */
  public function getn_id()
  {
    return $this->n_id;
  }

  /**
   * @return string
   */
  public function geto_id()
  {
    return $this->o_id;
  }
   public function getctry_dob()
  {
    return $this->ctry_dob;
  }

   public function getp_id()
  {
    return $this->p_id;
  }

   public function getdob()
  {
    return $this->dob;
  }

   public function getnm()
  {
    return $this->nm;
  }
   public function getnm_pref()
  {
    return $this->nm_pref;
  }
   public function getttl()
  {
    return $this->ttl;
  }
   public function getttl_oth()
  {
    return $this->ttl_oth;
  }
   public function getgdr()
  {
    return $this->gdr;
  }
   public function getmarit()
  {
    return $this->marit;
  } public function getdepend()
  {
    return $this->depend;
  }

   public function getadd1()
  {
    return $this->add1;
  } 
   public function getadd2()
  {
    return $this->add2;
  } 
   public function getadd3()
  {
    return $this->add3;
  } 
   public function getadd4()
  {
    return $this->add4;
  } 
   public function getadd5()
  {
    return $this->add5;
  } 

  public function getcor_add1()
  {
    return $this->cor_add1;
  }
    public function getcor_add2()
  {
    return $this->cor_add2;
  }
    public function getcor_add3()
  {
    return $this->cor_add3;
  }
    public function getcor_add4()
  {
    return $this->cor_add4;
  }
    public function getcor_add5()
  {
    return $this->cor_add5;
  }
    public function getcor_add6()
  {
    return $this->cor_add6;
  }
    public function getcor_add7()
  {
    return $this->cor_add7;
  }
    public function getcor_add8()
  {
    return $this->cor_add8;
  }
   public function getown()
  {
    return $this->own;
  }
   public function getrce()
  {
    return $this->rce;
  }
   public function getbpr()
  {
    return $this->bpr;
  }
   public function getrel()
  {
    return $this->rel;
  }

   public function getedc()
  {
    return $this->edc;
  }
   public function getctry_oth()
  {
    return $this->ctry_oth;
  }
    public function getrce_oth()
  {
    return $this->rce_oth;
  }
    public function getrel_oth()
  {
    return $this->rel_oth;
  }
    public function getctg()
  {
    return $this->ctg;
  }
    public function getapp_no()
  {
    return $this->app_no;
  }

  /**
   * @return string
   */
 
}