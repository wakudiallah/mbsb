<?php
//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

$page_title = "List of Anti Attrition";

$page_css[] = "your_style.css";
include("asset/inc/header.php");
include("asset/inc/nav.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
    <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
        @if (Session::has('error'))
            <div class="alert adjusted alert-danger fade in">
                <button class="close" data-dismiss="alert">
                     ×
                </button>
                <i class="fa-fw fa-lg fa fa-exclamation"></i>
                <strong>{{ Session::get('error') }}</strong> 
            </div>
        @elseif (Session::has('success'))
            <div class="alert adjusted alert-success fade in">
                <button class="close" data-dismiss="alert">
                     ×
                </button>
                 <i class="fa-fw fa-lg fa fa-exclamation"></i>
                 <strong>{{ Session::get('success') }}</strong> 
            </div>
        @endif

        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget well" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>Widget Title </h2>          
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                        <div class="widget-body no-padding">
                            <table id="dt_basic" class="table table-striped table-bordered table-hover"  width="100%">
                                <thead>                             
                                    <tr>
                                        <th>No.</th>
                                        <th>IC Number</th>
                                        <th>Name</th>
                                        <th>New Loan Offer</th>
                                        <th>New DSR</th>
                                        <th>Profit to Earn</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @foreach ($antiattrition as $anti)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{$anti->CustName}}</td>
                                        <td>{{$anti->CustIDNo}}</td>
                                        <td>{{$anti->NewLoanOffer}}</td>
                                        <td>{{$anti->NewDSR}}</td>
                                        <td>{{$anti->ProfitToEarn}}</td>
                                        <td> 
                                         <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/send_to_mom')}}" >
                                             <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                             <input type="hidden" name="CustIDNo" value="{{ $anti->CustIDNo}}">
<a href="{{ url('/clrt/loan-offer-detail-assigned/'.$anti->ACID) }}" class='btn btn-sm btn-success'>Detail</a>

 @if(!empty($anti->Basic->Terma->status==1) AND ($anti->Basic->Terma->mo_stage==1))
                                                       <span data-toggle="tooltip" title="{{$anti->Basic->Terma->verification_remark}}" class='label label-default'>Check DSR</span>
                                                @elseif(!empty($anti->Basic->Terma->status==88))
                                                <span data-toggle="tooltip" title="{{$anti->Basic->Terma->verification_remark}}" class='label label-danger'>Rejected</span>
                                                 @elseif($anti->Basic->Terma->status==77)
                                                <span data-toggle="tooltip" title="{{$anti->Basic->Terma->verification_remark}}" class='label label-danger'>Complete  form</span>
                                                @else 
                                        @if(($anti->Basic->Terma->status==0) AND ($anti->Basic->Terma->verification_result ==0))
                                            <span data-toggle="tooltip" title="{{$anti->Basic->Terma->verification_remark}}" class='label label-warning'>Select Tenure</span>
                                                    @elseif($anti->Basic->Terma->verification_result ==1)                                                
                                                        <span data-toggle="tooltip" title="{{$anti->Basic->Terma->verification_remark}}"  class='label label-success'>Submitted to WAPS</span>
                                                    @elseif($anti->Basic->Terma->verification_result ==2)
                                                        <span data-toggle="tooltip" title="{{$anti->Basic->Terma->verification_remark}}"  class='label label-primary'>Routed to Branch / Submitted to WAPS</span>
                                                        <br>
                                                        @if($anti->Basic->Terma->id_branch=='0')
                                                         <div align='center'> - </div> 
                                                        @else                                            
                                                        <i>{{ $anti->Basic->Terma->Branch->branchname }}</i>
                                                        @endif 
                                                
                                                    @elseif(($anti->Basic->Terma->verification_result ==3))
                                                      <span data-toggle="tooltip" title="{{$anti->Basic->Terma->verification_remark}}" class='label label-danger'>Rejected</span>
                                                 
                                                    @elseif(($anti->Basic->Terma->verification_result ==4))
                                                      <span data-toggle="tooltip" title="{{$anti->Basic->Terma->verification_remark}}"  class='label label-default'>Route Back to User</span>
                                                 
                                                    @endif
                                                    @endif
                                                   
                                            <!-- <button class="btn btn-sm btn-success" onclick="thanks()" type="submit" ><i class="material-icons">file_download</i> Generate & Send to MBSB</button>-->
                                            
                                         </form>
                                        </td>
                                        <td>
                                             @if(($anti->Basic->Terma->status==1) AND ($anti->Basic->Terma->mo_stage==1))
                                                    <a href="{{ url('/admin/step1/'.$anti->Basic->Terma->id_praapplication.'/verify') }}" class='btn btn-sm btn-success' ><i class='fa fa-file'></i> Check DSR</a>
                                                @elseif($anti->Basic->Terma->status=='77')
                                                   <a href="{{ url('/moform/'.$anti->Basic->Terma->id_praapplication.'/verify') }}" class='btn btn-sm btn-primary' ><i class='fa fa-file'></i> Complete  form</a><br>
                                                    <a href="{{ url('/admin/step1/'.$anti->Basic->Terma->id_praapplication.'/view') }}" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>
                                                @elseif(($anti->Basic->Terma->status==0) AND ($anti->Basic->Terma->verification_result==0))
                                                    <a href="{{ url('/clrt/select-tenure/'.$anti->ACID) }}" class='btn btn-sm btn-success' ><i class='fa fa-file'></i> Select Tenure</a>


                                                 @elseif(($anti->Basic->Terma->status==1) AND ($anti->Basic->Terma->mo_stage==2))
                                                   <a href="{{ url('/moform/'.$anti->Basic->Terma->id_praapplication.'/view') }}" class='btn btn-sm btn-default fa fa-book' >View Form</a>
                                                          <a href="{{ url('/admin/step1/'.$anti->Basic->Terma->id_praapplication.'/view') }}" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>

                                                     <!--<a href="{{ url('/add_document/'.$anti->Basic->Terma->id_praapplication.'/verify') }}" class='btn btn-sm btn-default fa fa-book' >Remark Documents</a>-->
                                                <!--tambahan-->
                                                @elseif($anti->Basic->Terma->status=='99')
                                                    @if(($anti->Basic->Terma->mo_stage=='2') AND ($anti->Basic->Terma->verification_result=='0'))
                                                        <!--<a href="{{ url('/admin/user_detail/'.$anti->Basic->Terma->id_praapplication.'/verify') }}" class='btn btn-sm btn-primary fa fa-book' >Verify  Form</a>-->
                                                             <a href="{{ url('/moform/'.$anti->Basic->Terma->id_praapplication.'/view') }}" class='btn btn-sm btn-default fa fa-book' >View Form</a>
                                                          <a href="{{ url('/admin/step1/'.$anti->Basic->Terma->id_praapplication.'/view') }}" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>
                                                    @elseif(($anti->Basic->Terma->mo_stage=='2') AND ($anti->Basic->Terma->verification_result=='2'))
                                                          <a href="{{ url('/moform/'.$anti->Basic->Terma->id_praapplication.'/view') }}" class='btn btn-sm btn-default fa fa-book' >View Form</a>
                                                          <a href="{{ url('/admin/step1/'.$anti->Basic->Terma->id_praapplication.'/view') }}" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>
                                                    @elseif(($anti->Basic->Terma->mo_stage=='2') AND ($anti->Basic->Terma->verification_result=='3'))
                                                          <a href="{{ url('/moform/'.$anti->Basic->Terma->id_praapplication.'/view') }}" class='btn btn-sm btn-default fa fa-book' >View Form</a>
                                                          <a href="{{ url('/admin/step1/'.$anti->Basic->Terma->id_praapplication.'/view') }}" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>
                                                    @endif 

                                               
                                                @elseif($anti->Basic->Terma->status==88)
                                                    <a href="{{ url('/admin/step1/'.$anti->Basic->Terma->id_praapplication.'/view') }}" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>
                                                @endif

                                                @if(($anti->Basic->Terma->status==1) AND ($result!='GLAN') AND ($anti->Basic->Terma->verification_result==2) )
                                                     <a href="{{url('/')}}/admin/detail_pra/{{$anti->Basic->Terma->id_praapplication}}" class='btn btn-sm btn-default fa fa-book' >Detail</a>
                                                @endif

                                                        @if($anti->Basic->Terma->edit=='1')
                                                            @if($anti->Basic->Terma->verification_result !=3)
                                                                @if($anti->Basic->Terma->verification_result !=2)
                                                                    <a href="{{url('/')}}/form/approveedit/{{$anti->Basic->Terma->id_praapplication}}" 
                                                                    onclick="return confirm('Are you sure ?')"  class='btn btn-sm btn-warning'>Approve RB</a>
                                                                @endif 
                                                            @endif 
                                                        @endif 

                                        </td>
                                    </tr>
                                    <?php
                                        $i++; ?>
                                    @endforeach
                                </tbody>
                            </table>

                            <script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
                            <script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
                            <script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
                            <script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
                            <script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

                            <script type="text/javascript">
                                $(document).ready(function() {
                                    pageSetUp();
                                    /* BASIC ;*/
                                        var responsiveHelper_dt_basic = undefined;
                                        var responsiveHelper_datatable_fixed_column = undefined;
                                        var responsiveHelper_datatable_col_reorder = undefined;
                                        var responsiveHelper_datatable_tabletools = undefined;
                                        
                                        var breakpointDefinition = {
                                            tablet : 1024,
                                            phone : 480
                                        };
                                    /* END BASIC */
                                        
                                    /* COLUMN SHOW - HIDE */
                                        $('#datatable_col_reorder').dataTable({
                                            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                                                    "t"+
                                                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                                            "autoWidth" : true,
                                            "preDrawCallback" : function() {
                                                // Initialize the responsive datatables helper once.
                                                if (!responsiveHelper_datatable_col_reorder) {
                                                    responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
                                                }
                                            },
                                            "rowCallback" : function(nRow) {
                                                responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                                            },
                                            "drawCallback" : function(oSettings) {
                                                responsiveHelper_datatable_col_reorder.respond();
                                            }           
                                        });
                                        /* END COLUMN SHOW - HIDE */
                                    })
                            </script>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>

<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    $('#dt_basic').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth" : true,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_dt_basic) {
                responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_dt_basic.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_dt_basic.respond();
        }
    });     
</script>
<script>
    $(document).ready(function() {
        // show the alert
        window.setTimeout(function() {
        $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
        });
        }, 2800); 
    });
</script>




          
