<?php
//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

$page_title = "List of Anti Attrition";

$page_css[] = "your_style.css";
include("asset/inc/header.php");
include("asset/inc/nav.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
    <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   	@if (Session::has('error'))
            <div class="alert adjusted alert-danger fade in">
                <button class="close" data-dismiss="alert">
                     ×
                </button>
                <i class="fa-fw fa-lg fa fa-exclamation"></i>
                <strong>{{ Session::get('error') }}</strong> 
            </div>
        @elseif (Session::has('success'))
            <div class="alert adjusted alert-success fade in">
                <button class="close" data-dismiss="alert">
                     ×
                </button>
                 <i class="fa-fw fa-lg fa fa-exclamation"></i>
                 <strong>{{ Session::get('success') }}</strong> 
            </div>
        @endif

        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="jarviswidget well" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>Widget Title </h2>          
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                        <div class="widget-body no-padding">
                        <form method="POST" class="form-horizontal" id="popup-validation" action="{{ url('/send_to_mom')}}" >
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <section>
                                    &nbsp;&nbsp;
                                    <label for="country">Manager</label>
                                    &nbsp;&nbsp;<select name="state" class="form-control" style="width:350px">
                                        <option value="">--- Select Manager ---</option>
                                            @foreach($manager as $manager)
                                                <option value="{{ $manager->id }}">
                                                    {{ $manager->name }}
                                                </option>
                                            @endforeach
                                    </select>
                                </section>
                                <section>&nbsp;&nbsp;<br></section>
                                <section>
                                    &nbsp;&nbsp;<label for="city">Marketing Officer</label>
                                        &nbsp;&nbsp;<select name="city" class="form-control" style="width:350px"></select>
                                </section>
                                <section>&nbsp;&nbsp;<br></section>
                            <table id="dt_basic" class="table table-striped table-bordered table-hover"  width="100%">
								<thead> 			                
									<tr>
										<th>No.</th>
                                        <th>IC Number</th>
                                        <th>Name</th>
										<th>Phone</th>
                                        <th>Submit Date</th>
                                        <th>Download</th>
									</tr>
								</thead>
                                <tbody>
    								<?php $i=1; ?>
                                    @foreach ($antiattrition as $antiattrition)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{$antiattrition->ACID}}</td>
                                        <td>{{$antiattrition->CustName}}</td>
                                        <td>{{$antiattrition->CustIDNo}}</td>
                                        <td>{{$antiattrition->ACID}}</td>
                                        <td><!--<a href="{{url('/')}}/send_to_mo" onclick="return confirm('Are you sure ?')"  class='btn btn-sm btn-warning'>Assign to MO</a>-->
                                            <div class="demo-checkbox">
                                                    <input type="checkbox" id="basic_checkbox_{{$i}}" name="CustIDNo[]" value="{{$antiattrition->CustIDNo}}" class="invitation-friends Bike" />

                                                    <label for="basic_checkbox_{{$i}}"></label>

                                                     <input type="hidden" id="id_cus" name="id_cus[]" value="{{$antiattrition->CustIDNo}}" />
                                                     <input type="hidden" id="ic" name="ic[]" value="{{$antiattrition->CustIDNo}}" />
                                                    <input type="hidden"  name="ci[]" value="{{$antiattrition->CustIDNo}}">
                                                    <input type="hidden"  name="ktp[]" value="{{$antiattrition->CustIDNo}}">

                                                     <input type="hidden"  name="ids[]" value="">
                                                     <input type="hidden"  name="sta[]" value="">
                                                </div>
                                        </td>
                                    </tr>
                                    <?php
                                        $i++; ?>
                                    @endforeach
								</tbody>
                            </table>
                            <div class="col-md-2 col-md-offset-10">
                                            <button class="btn btn-sm btn-success" onclick="thanks()" type="submit" ><i class="material-icons">Assign</button>
                                        </div>
                                    </form>

                            <script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
                            <script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
                            <script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
                            <script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
                            <script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

                            <script type="text/javascript">
                            	$(document).ready(function() {
                            		pageSetUp();
                            		/* BASIC ;*/
                            			var responsiveHelper_dt_basic = undefined;
                            			var responsiveHelper_datatable_fixed_column = undefined;
                            			var responsiveHelper_datatable_col_reorder = undefined;
                            			var responsiveHelper_datatable_tabletools = undefined;
                            			
                            			var breakpointDefinition = {
                            				tablet : 1024,
                            				phone : 480
                            			};
                                    /* END BASIC */
                            			
                            		/* COLUMN SHOW - HIDE */
                                        $('#datatable_col_reorder').dataTable({
                                            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                                                    "t"+
                                                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                                            "autoWidth" : true,
                                            "preDrawCallback" : function() {
                                                // Initialize the responsive datatables helper once.
                                                if (!responsiveHelper_datatable_col_reorder) {
                                                    responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
                                                }
                                            },
                                            "rowCallback" : function(nRow) {
                                                responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                                            },
                                            "drawCallback" : function(oSettings) {
                                                responsiveHelper_datatable_col_reorder.respond();
                                            }           
                                        });
                                        /* END COLUMN SHOW - HIDE */
                                    })
                            </script>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>

<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
	var responsiveHelper_dt_basic = undefined;
	var responsiveHelper_datatable_fixed_column = undefined;
	var responsiveHelper_datatable_col_reorder = undefined;
	var responsiveHelper_datatable_tabletools = undefined;
	
	var breakpointDefinition = {
		tablet : 1024,
		phone : 480
	};
	$('#dt_basic').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
			"t"+
			"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_dt_basic) {
				responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_dt_basic.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_dt_basic.respond();
		}
	});		
</script>
<script>
    $(document).ready(function() {
        // show the alert
        window.setTimeout(function() {
        $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
        });
        }, 2800); 
    });
</script>



<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="state"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url:  "<?php  print url('/'); ?>/myform/ajax/"+stateID,

                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="city"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="city"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="city"]').empty();
            }
        });
    });
</script>

          
