<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title =  "Submited Report";


$page_css[] = "your_style.css";
include("asset/inc/header.php");


include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
    <?php
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	    @if (Session::has('error'))
        <div class="alert adjusted alert-danger fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa-lg fa fa-exclamation"></i>
            <strong>{{ Session::get('error') }}</strong> 
        </div>
        @elseif (Session::has('success'))
        <div class="alert adjusted alert-success fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa-lg fa fa-exclamation"></i>
            <strong>{{ Session::get('success') }}</strong> 
        </div>
        @endif

        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
            <div class="jarviswidget well" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>Widget Title </h2>       
                </header>

                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body no-padding">
                        {!! Form::open(['url' => 'clrt/new_loan_offer_view','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                        <fieldset>
    						<section>
                                <label class="label">Select Task</label>
                                <label class="select">
                                    <i class="icon-append"></i>
                                    <select name='task'>
										<option value="-99"> Select </option>
                                        @foreach ($jobrun as $jobrun)
                                            <option value="{{ $jobrun->id }}">{{ $jobrun->JobDesc }}</option>
                                        @endforeach
									</select>
                                    <b class="tooltip tooltip-bottom-right">Select Task</b>
                                </label>
                            </section>
                            <section>
                                <label class="label">Job Sector</label>
                                <label class="select">
                                    <i class="icon-append"></i>
                                    <select name='jobsector'>
                                        <option value="-99"> Select </option>
                                        <option value="P"> Private Sector </option>
                                        <option value="G"> Government </option>
                                    </select>
                                    <b class="tooltip tooltip-bottom-right">Job Sector</b>
                                </label>
                            </section>
                            <section>
                                <label class="label">Select State</label>
                                <label class="select">
                                    <i class="icon-append"></i>
                                    <select name='state'>
                                        <option value="-99"> Select </option>
                                        @foreach ($state as $state)
                                            <option value="{{ $state->clrt_name }}">{{ $state->clrt_name }}</option>
                                        @endforeach
                                    </select>
                                    <b class="tooltip tooltip-bottom-right">Select Task</b>
                                </label>
                            </section>
                            <section>
                                <label class="label">NettProceed</label>
                                <label class="input">
                                    <i class="icon-append"></i>
                                    <input type="text" name="net1" placeholder="From"  class="form-control"  required>
                                    <b class="tooltip tooltip-bottom-right">NettProceed</b>
                                </label>
                            </section>
                            <section>
                                <label class="label">To</label>
                                <label class="input">
                                    <i class="icon-append"></i>
                                    <input type="text" name="net2" placeholder="To"  class="form-control"  required>
                                    <b class="tooltip tooltip-bottom-right">To</b>
                                </label>
                            </section>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                         </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="submit" class="btn btn-primary">Generate</button>
                    {!! Form::close() !!} 
                    </div>
                    <br><br><br><br>
                </div>
            </div>
        </article>
    </div>
</div>
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>
    
    <script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Name: {
                    required : true,
                    minlength : 1,
                    maxlength : 50
                },
                TelephoneNumber: {
                    required : true,
                    minlength : 11,
                    maxlength : 11
                }
            },

            // Messages for form validation
             messages : {

                    Name: {
                    required : 'Please enter Branch Name',
                    email : 'Please enter a VALID email address'
                }
                    }
        });

    });
</script>
 <script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
            
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();


                $('.startdate').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });

        </script>



          
