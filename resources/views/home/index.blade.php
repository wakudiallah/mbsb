<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Submit Application";


$page_css[] = "your_style.css";
include("asset/inc/header.php");





?>
<style>
.not-active {
   pointer-events: none;
   cursor: default;
}
table.border {
    border-collapse: separate;
    border-spacing: 10px; /* cellspacing */
    *border-collapse: expression('separate', cellSpacing = '10px');
}

td.border {
    padding: 10px; /* cellpadding */
}

</style>

    <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <div id="content">
<section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
                
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                
                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"
                
                                -->
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-check"></i> </span>
                                    <h2>Application form </h2>
                
                                </header>
                
                                <!-- widget div-->
                                <div>
                
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                
                                    </div>
                                    <!-- end widget edit box -->
                
                                    <!-- widget content -->
                                    <div class="widget-body">
                          
                                        <div class="row">
                                            <form id="wizard-1" novalidate="novalidate">
                                                <div id="bootstrap-wizard-1" class="col-sm-12">
                                                    <div class="form-bootstrapWizard">
                                                        <ul class="bootstrapWizard form-wizard" >
                                                             <li data-target="#step1" style="width:20%;" >
                                                                <a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title  hidden-xs">Personal Particulars (Main Applicant)/<i> Butir-Butir Peribadi (Pemohon Utama)</i> </span> </a>
                                                            </li>
                                                            <li data-target="#step2" class="not-active" style="width:20%;">
                                                                <a href="#tab2" data-toggle="tab"> <span class="step">2</span> <span class="title hidden-xs">Employment Details (Main Applicant)/<i> Butir-Butir Pekerjaan (Pemohon Utama)</i> </span> </a>
                                                            </li>
                                                            <li data-target="#step3" class="not-active" style="width:20%;">
                                                                <a href="#tab3" data-toggle="tab"> <span class="step">3</span> <span class="title hidden-xs">Particulars of Spouse & Emergency Contact**/<i> Maklumat Suami-Isteri & Rujukan Kecemasan**</i> </span> </a>
                                                            </li>
                                                         
                                                            <li data-target="#step4" class="not-active" style="width:20%;">
                                                                <a href="#tab4" data-toggle="tab"> <span class="step">4</span> <span class="title hidden-xs"> Upload Document/<i> Muat Naik Dokumen </i></span> </a>
                                                            </li>
                                                             <li data-target="#step5" class="not-active" style="width:20%;">
                                                                <a href="#tab5" data-toggle="tab"> <span class="step">5</span> <span class="title hidden-xs">Declaration/Disclosure by Applicant/<i>  Perakuan/Pendedahan Oleh Pemohon </i></span> </a>
                                                            </li>
                                                       </ul>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                   
                                                    <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <ul class="pager wizard no-margin">
                                                                        <!--<li class="previous first ">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                                                        </li>-->
                                                                        <li class="next">
                                                                            <a href="javascript:void(0);" class="btn btn-lg txt-color-blue"><span class="hidden-xs"> Seterusnya / </span><i>Next</i> </a>
                                                                        </li>
                                                                        <li class="previous ">
                                                                            <a href="javascript:void(0);" class="btn btn-lg btn-default"><span class="hidden-xs"> Sebelum / </span><i> Previous </i> </a>
                                                                        </li>
                                                                        <!--<li class="next last">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                                                        </li>-->
                                                                        
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab1">
                                                         @foreach($data as $data)
                                                            <br>
                                                               <b>Loan Amount : RM {{number_format($dsr_b->financing_amount)}} | Tenure : {{$dsr_b->tenure}} Year(s) | Package : @if($pra->package->id=="1") Mumtaz-<i><font size="1">i</font></i> @else Afdhal-<i><font size="1">i</font></i> @endif</b><br> 
                                                            <h3><strong>Step 1 </strong> - PERSONAL PARTICULARS (MAIN APPLICANT) /  <i> BUTIR-BUTIR PERIBADI (PEMOHON UTAMA)</i> </h3>
                                                            <div class="col-md-6">
                                                                <br>
                                                        <div class="form-group">
                                                        <label>1. Salutation / <i>Gelaran</i> :</label>
                                                            {{csrf_field()}}
                                                            <input type="hidden" name="id_praapplication" value="{{$pra->id}}"/>
                                                           <select class="select2" name="title" id="salutation" required="required">
                                                             <option selected=""></option>
                                                            @foreach ($title as $title)
                                                                @if($data->title !=NULL)
                                                                    <?php 
                                                                        if($data->title==$title->title_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$title->title_code}}">{{$title->title_desc}}</option>
                                                                @else
                                                                    <option value="{{$title->title_code}}">{{$title->title_desc}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
<br>
                                                               <input type="text" class="form-control" placeholder="Specify / Sila Nyatakan" name="title_others" id="salutation_others" required @if($data->title=="Others") value="{{$data->title_others}}" @endif >
                                                        </div>

                                                        <div class="form-group">
                                                        <label>2. Full Name (As per ID Document / <i>Nama Penuh (Seperti dalam Dokumen Pengenalan Diri)</i> :</label>
                                                                  <input type="text" minlength="2" maxlength="200"  id="fullname" name="name" value="{{$data->name}}" class="form-control" onkeyup="this.value = this.value.toUpperCase()" onkeypress="return fullname(event);">
                                                        </div>

                                                         @if($data->id_type=='1')
                                                            <div class="form-group">
                                                                <label>3. My Kad No. / <i>No MyKad</i> :</label>
                                                                 <input  type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==12) return false;" id="new_ic" name="new_ic" minlength="12" maxlength="12" placeholder="IC Number"  @if (Session::has('icnumber'))  value="{{ Session::get('icnumber') }}" @endif  value="{{$data->new_ic}}" class="form-control">
                                                            </div>
                                                            @else
                                                             <div class="form-group">
                                                                <label>3. My Kad No. / <i>No MyKad</i> :</label>
                                                                 <input  type="text" id="new_ic" onkeyup="this.value = this.value.toUpperCase()" name="new_ic"  placeholder="Other"  @if (Session::has('other'))  value="{{ Session::get('other') }}" @endif minlength="6" maxlength="12" value="{{$data->new_ic}}" class="form-control">
                                                            </div>
                                                            @endif

                                                        <div class="form-group">
                                                        <label>4. Date Of Birth / <i>Tarikh Lahir</i> :  </label>
                                                            <?php 
                                                                $lahir =  date('d/m/Y', strtotime($data->dob));
                                                            ?>
                                                            <input type="text" maxlength="14" name="" value="{{ $lahir  }}"  class="form-control" id="dob" readonly="readonly"s />
                                                        </div>
                                                        <div class="form-group">
                                                        <label>5. Country of Birth/ <i>Tempat Lahir</i> :</label>
                                                          
                                                            <select name="country_dob" id="country_dob" class="select2">
                                                              <option disabled="" selected="">SELECT</option> 
                                                            @foreach ($country as $country)
                                                                @if($data->country_dob !=NULL)
                                                                    <?php 
                                                                        if($data->country_dob==$country->country_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$country->country_code}}">{{$country->country_desc}}</option>
                                                                @else
                                                                    <option value="{{$country->country_code}}">{{$country->country_desc}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select><br><br>
                                                        State / <i>Negeri</i>
                                                          <select name="state_dob" id="state_dob" class="select2">
                                                              <option disabled="" selected="">SELECT</option> 
                                                            @foreach ($state as $state)
                                                                @if($data->state_dob !=NULL)
                                                                    <?php 
                                                                        if($data->state_dob==$state->state_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$state->state_code}}">{{$state->state_name}}</option>
                                                                @else
                                                                    <option value="{{$state->state_code}}">{{$state->state_name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                       
                                                    </div>
                                                        <div class="form-group">
                                                        <label>6. Police  / Military No./<i>No. Polis / Tentera </i> :</label>
                                                            <input type="text" maxlength="15" id="police_number" name="police_number" value="{{$data->police_number}}" class="form-control" onkeypress="return isNumberKey(event)">
                                                        </div>

                                                        <div class="form-group">
                                                        <label>7. Gender / <i>Jantina</i> :</label>
                                                             <select class="select2" name="gender" id="gender" required="requeired">
                                                                @if(!empty($data->gender))
                                                                    @if($data->gender=='M')
                                                                    <option value="M">Male / <i>Lelaki </i></option>
                                                                    @elseif($data->gender=='F')
                                                                    <option value="F">Female / <i>Perempuan </i></option>
                                                                    @endif
                                                                @endif
                                                                    <option value="M">Male / <i>Lelaki </i></option>
                                                                    <option value="F">Female / <i>Perempuan </i></option>
                                                            </select>
                                                    </div>
                                                    
                                                     <div class="form-group">
                                                        <label>8. Home Address / <i>Alamat Rumah</i> </label>
                                                        <br>
                                                            <input type="text" class="form-control"  name="address" required="requeired" value="{{$data->address}}" placeholder="Address Line 1" minlength="2" maxlength="40" onkeyup="this.value = this.value.toUpperCase()">
                                                         <br>
                                                        <input type="text" class="form-control"  name="address2"  placeholder="Address Line 2" value="{{$data->address2}}" onkeyup="this.value = this.value.toUpperCase()">

                                                         <br>
                                                        <input type="text" class="form-control"  name="address3"  placeholder="Address Line 3" value="{{$data->address3}}" onkeyup="this.value = this.value.toUpperCase()">
                                                         <label> Postcode/ <i>Poskod :</i></label>
                                                            <br><input type="text" maxlength="5" required="" name="postcode" id="postcode" class="form-control" onkeypress="return isNumberKey(event)" value="{{$data->postcode}}"  />
                                                        <label>State / <i>Negeri :</i></label><br>
                                                            <input type="text"    maxlength="50"  id="state" name="state" class="form-control" value="{{$data->state}}" >

                                                            <input type="hidden"    maxlength="50"  id="state_code" name="state_code" class="form-control" value="{{$data->state_code}}">
                                                     </div>
                                                    
                                                    <div class="form-group">
                                                    <label>9. Ownership Status / <i>Taraf Pemilikan</i>:</label>
                                                        <select class=" form-control" name="ownership" id="ownership" required="requeired">

                                                                @if(!empty($data->ownership))

                                                                    @if($data->ownership=='OW')

                                                                        <option value="OW">Owned /<i> Milik Sendiri</i> </option>

                                                                    @elseif($data->ownership=='LP')

                                                                        <option value="LP">Living with Parents/Relatives/<i> Tinggal bersama keluarga/saudara </i></option>

                                                                    @elseif($data->ownership=='RT')

                                                                        <option value="RT">Rented /<i> Sewa</i></option>

                                                                    @endif

                                                                    <option value="OW">Owned /<i> Milik Sendiri</i> </option>

                                                                    <option value="LP">Living with Parents/Relatives/<i> Tinggal bersama keluarga/saudara </i></option>

                                                                    <option value="RT">Rented /<i> Sewa</i></option>

                                                               @else

                                                                    <option selected=""></option>

                                                                    <option value="OW">Owned /<i> Milik Sendiri</i> </option>

                                                                    <option value="LP">Living with Parents/Relatives/<i> Tinggal bersama keluarga/saudara </i></option>

                                                                    <option value="RT">Rented /<i> Sewa</i></option>

                                                                @endif

                                                            </select>
                                                    </div>        
                                                    </div>
                                                    <div class="col-md-6">
                                                    <br>
                                                    <div class="form-group">
                                                            <label>10. Resident Status / <i>Status Pemastautin </i> :</label>
                                                                <select class=" form-control" id="resident" name="resident" required>
                                                                    @if(!empty($data->resident))
                                                                        @if($data->resident=='Y')
                                                                            <option value="Y">Resident / <i>Pemastautin</i></option>
                                                                        @elseif($data->resident=='N')
                                                                            <option value="N">Non Resident / <i>Bukan Pemastautin</i></option>
                                                                        @endif
                                                                            <option value="Y">Resident / <i>Pemastautin</i></option>
                                                                            <option value="N">No / <i>Tidak</i></option>
                                                                    @else
                                                                        <option selected=""></option>
                                                                        <option value="Y">Resident / <i>Pemastautin</i></option>
                                                                        <option value="N">No / <i>Bukan Pemastautin</i></option>
                                                                    @endif
                                                                </select>
                                                        </div>

                                                        <div class="form-group">
                                                            <label>11. Nationality / <i>Kewarganegaraan</i> :</label>
                                                                <select class=" form-control" name="nationality" id="nationality"required>
                                                                    @if(!empty($data->nationality))
                                                                        @if($data->nationality=='MY')
                                                                            <option value="MY">Malaysian / <i>Malaysia</i></option>
                                                                        @elseif($data->nationality=='PR')
                                                                            <option value="PR">Permanent Resident / <i>Penduduk Tetap</i></option>
                                                                        @elseif($data->nationality=='NC')
                                                                            <option value="NC">Non Citizen / <i>Bukan Warganegara</i></option>
                                                                        @endif
                                                                            <option value="MY">Malaysian / <i>Malaysia</i></option>
                                                                             <option value="PR">Permanent Resident / <i>Penduduk Tetap</i></option>
                                                                             <option value="NC">Non Citizen / <i>Bukan Warganegara</i></option>
                                                                    @else
                                                                        <option selected=""></option>
                                                                        <option value="MY">Malaysian / <i>Malaysia</i></option>
                                                                         <option value="PR">Permanent Resident / <i>Penduduk Tetap</i></option>
                                                                         <option value="NC">Non Citizen / <i>Bukan Warganegara</i></option>
                                                                    @endif
                                                                </select>
                                                        </div>
                                                     

                                                    <div class="form-group">
                                                        <div id="country_origin">
                                                            <label>Country Origin / <i>Negara Asal</i> :</label>
                                                                <select name="country_origin" id="country_origin" class="select2" required="required">
                                                                    <option disabled="" selected="">SELECT</option> 
                                                                        @foreach ($nationality as $nationality)
                                                                            @if($data->nationality !=NULL)
                                                                                <?php 
                                                                                    if($data->nationality==$nationality->country_code) {
                                                                                      $selected = "selected";
                                                                                    }
                                                                                    else {
                                                                                       $selected = "";
                                                                                    } 
                                                                                ?>
                                                                                    <option {{$selected}} value="{{$nationality->country_code}}">{{$nationality->country_desc}}</option>
                                                                            @else
                                                                                <option value="{{$nationality->country_code}}">{{$nationality->country_desc}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                   
                                                    <div class="form-group">
                                                    <label>12. Race / <i>Bangsa </i> :</label>
                                                        <select class=" form-control" id="race" name="race" required="requeired">



                                                                    @if(!empty($data->race))

                                                                        @if($data->race=='MAL')

                                                                            <option value="MAL">Malay / <i>Melayu</i></option>

                                                                        @elseif($data->race=='IND')

                                                                            <option value="IND">Indian / <i>India</i></option>

                                                                        @elseif($data->race=='CHN')

                                                                            <option value="CHN" >Chinese / <i>China</i></option>

                                                                        @elseif($data->race=='ASK')

                                                                            <option value="ASK" >Other Bumiputera (Sabah & Sarawak) / <i>Lain-Lain Bumiputera (Sabah & Sarawak)</i></option>

                                                                        @elseif($data->race=='ASM')

                                                                            <option value="ASM" >Orang Asli (Sem. Malaysia)</option>

                                                                        @elseif($data->race=='EUR')

                                                                            <option value="EUR" >

                                                                        @elseif($data->race=='IBN')Serani </option>

                                                                            <option value="IBN" >Iban </option>

                                                                        @elseif($data->race=='KDZ')

                                                                            <option value="KDZ" >Kadazan </option>

                                                                        @elseif($data->race=='PUN')

                                                                            <option value="PUN" >Punjabi </option>

                                                                        @elseif($data->race=='OTL')

                                                                            <option value="OTL">Others Local (Specify)/ Lain-lain Lokal (Nyatakan)</option>

                                                                        @elseif($data->race=='OTH')

                                                                            <option value="OTH">Others Foreign (Specify)/ Lain-lain Orang Asing (Nyatakan)</option>

                                                                        @endif

                                                                        <option value="MAL">Malay / <i>Melayu</i></option>

                                                                        <option value="IND">Indian / <i>India</i></option>

                                                                        <option value="CHN" >Chinese / <i>China</i></option>

                                                                        <option value="ASK" >Other Bumiputera (Sabah & Sarawak) / <i>Lain-Lain Bumiputera (Sabah & Sarawak)</i></option>

                                                                        <option value="ASM" >Orang Asli (Sem. Malaysia)</option>

                                                                        <option value="EUR" >Serani </option>

                                                                        <option value="IBN" >Iban </option>

                                                                        <option value="KDZ" >Kadazan </option>

                                                                        <option value="PUN" >Punjabi </option>

                                                                        <option value="OTL">Others Local (Specify)/ Lain-lain Lokal (Nyatakan)</option>

                                                                        <option value="OTH">Others Foreign (Specify)/ Lain-lain Orang Asing (Nyatakan)</option>

                                                                    @else

                                                                        <option selected=""></option>

                                                                        <option value="MAL">Malay / <i>Melayu</i></option>

                                                                        <option value="IND">Indian / <i>India</i></option>

                                                                        <option value="CHN" >Chinese / <i>China</i></option>

                                                                        <option value="ASK" >Other Bumiputera (Sabah & Sarawak) / <i>Lain-Lain Bumiputera (Sabah & Sarawak)</i></option>

                                                                        <option value="ASM" >Orang Asli (Sem. Malaysia)</option>

                                                                        <option value="EUR" >Serani </option>

                                                                        <option value="IBN" >Iban </option>

                                                                        <option value="KDZ" >Kadazan </option>

                                                                        <option value="PUN" >Punjabi </option>

                                                                        <option value="OTL">Others Local (Specify)/ Lain-lain Lokal (Nyatakan)</option>

                                                                        <option value="OTH">Others Foreign (Specify)/ Lain-lain Orang Asing (Nyatakan)</option>

                                                                    @endif

                                                                </select>


                                                                 <input type="text" class="form-control" placeholder="Specify / Sila Nyatakan" name="race_others" id="race_others" required @if($data->race=="OTH") value="{{$data->race_others}}" @endif >
                                                    </div>

                                                    <div class="form-group">
                                                    <label>13. Religion / <i>Agama</i> :</label>
                                                         <select class=" form-control" id="religion" name="religion" required>

                                                                    @if(!empty($data->religion))

                                                                        @if($data->religion=='I')

                                                                        <option value="I">Islam / <i>Muslim</i></option>

                                                                        @elseif($data->religion=='H')

                                                                       <option value="H">Hindu /<i> Hindu</i></option>

                                                                        @elseif($data->religion=='C')

                                                                        <option value="C">Christian /<i> Kristian<i> </option>

                                                                        @elseif($data->religion=='B')

                                                                        <option value="B">Buddhist /<i>Buddha </i></option>

                                                                        @elseif($data->religion=='S')

                                                                        <option value="S">Sikh</option>

                                                                        @elseif($data->religion=='O')

                                                                       <option value="O">Others/ Lain-lain</i></option>

                                                                        @endif

                                                                        <option value="I">Islam / <i>Muslim</i></option>

                                                                        <option value="H">Hindu /<i> Hindu</i></option>

                                                                        <option value="C">Christian /<i> Kristian<i> </option>

                                                                        <option value="B">Buddhist /<i>Buddha </i></option>

                                                                         <option value="S">Sikh</option>

                                                                        <option value="O">Others/ Lain-lain</i></option>

                                                                    @else

                                                                        <option selected=""></option>

                                                                        <option value="I">Islam / <i>Muslim</i></option>

                                                                        <option value="H">Hindu /<i> Hindu</i></option>

                                                                        <option value="C">Christian /<i> Kristian<i> </option>

                                                                        <option value="B">Buddhist /<i>Buddha </i></option>

                                                                         <option value="S">Sikh</option>

                                                                        <option value="O">Others/ Lain-lain</i></option>

                                                                    @endif

                                                                </select>

                                                                  <input type="text" class="form-control" placeholder="Specify / Sila Nyatakan" name="religion_others" id="religion_others" required @if($data->religion=="O") value="{{$data->religion_others}}" @endif >
                                                    </div>
                                                    <div class="form-group">

                                                            <label>14. Bumiputera Status / <i>Status Bumiputera </i> :</label>

                                                                <select class=" form-control" id="bumiputera" name="bumiputera" required>

                                                                    @if(!empty($data->bumiputera))

                                                                        @if($data->bumiputera=='Y')

                                                                            <option value="Y">Yes / <i>Ya</i></option>

                                                                        @elseif($data->bumiputera=='N')

                                                                            <option value="N">No / <i>Tidak</i></option>

                                                                        @endif

                                                                            <option value="Y">Yes / <i>Ya</i></option>

                                                                            <option value="N">No / <i>Tidak</i></option>

                                                                    @else

                                                                        <option selected=""></option>

                                                                        <option value="Y">Yes / <i>Ya</i></option>

                                                                        <option value="N">No / <i>Tidak</i></option>

                                                                    @endif

                                                                </select>

                                                        </div>


                                                    <div class="form-group">
                                                    <label>15. Marital Status / <i>Taraf Perkahwinan </i> :</label>
                                                          <select class=" form-control" name="marital" id="marital" required="required">

                                                                @if(!empty($data->marital))

                                                                    @if($data->marital=='D')

                                                                    <option value="D">Divorced / <i>Balu</i></option>

                                                                    @elseif($data->marital=='M')

                                                                   <option value="M">Married / <i>Berkahwin</i></option>

                                                                    @elseif($data->marital=='W')

                                                                    <option value="W">Widower / <i>Balu</i></option>

                                                                    @elseif($data->marital=='S')

                                                                    <option value="S"> Single/ <i>Bujang</i></option>

                                                                    @elseif($data->marital=='U')

                                                                     <option value="U">Unknown / <i>Tidak diketahui</i></option>

                                                                    @endif

                                                                    <option value="S"> Single/ <i>Bujang</i></option>

                                                                    <option value="M">Married / <i>Berkahwin</i></option>

                                                                    <option value="D">Divorced / <i>Balu</i></option>

                                                                    <option value="W">Widower / <i>Balu</i></option>

                                                                    <option value="U">Unknown / <i>Tidak diketahui</i></option>

                                                                @else

                                                                    <option selected=""></option>

                                                                    <option value="S"> Single/ <i>Bujang</i></option>

                                                                    <option value="M">Married / <i>Berkahwin</i></option>

                                                                    <option value="D">Divorced / <i>Balu</i></option>

                                                                    <option value="W">Widower / <i>Balu</i></option>

                                                                    <option value="U">Unknown / <i>Tidak diketahui</i></option>

                                                                @endif

                                                                </select>
                                                    </div>

                                                    <div class="form-group">
	                                                    <label>16. No. of Dependants / <i>Jumlah Tanggungan</i>
	                                                         <input type="text" class="form-control" maxlength="2" value="{{$data->dependents}}" name="dependents" onkeypress="return isNumberKey(event)" required="requeired">
	                                                    </label>
                                                    </div>
                                                      <div class="form-group">
                                                              <div class="form-group">
                                                            <label>17. Education Level / <i>Taraf Pendidikan</i> :</label>
                                                                <select class="select2" id="education" name="education" required="requeired">

                                                                  @if(!empty($data->education))

                                                                        @if($data->education=='DG')

                                                                            <option value="DG">Degree /<i>Ijazah</i></option>

                                                                            @elseif($data->education=='DI')

                                                                            <option value="DI">Diploma /<i>Diploma</i></option>

                                                                            @elseif($data->education=='PH')

                                                                            <option value="PH">PhD /<i>Kedoktoran</i></option>

                                                                            @elseif($data->education=='MA')

                                                                            <option value="MA">Masters /<i>Ijazah Sarjana</i></option>

                                                                            @elseif($data->education=='PS')

                                                                             <option value="PS">Primary /<i>Rendah</i></option>

                                                                            @elseif($data->education=='SS')

                                                                            <option value="SS">Secondary /<i>Menengah</i></option>

                                                                             @elseif($data->education=='Z')

                                                                             <option value="Z">Not Specified /<i>Tidak ditentukan</i></option>

                                                                        @endif

                                                                            <option value="PS">Primary /<i>Rendah</i></option>

                                                                            <option value="SS">Secondary /<i>Menengah</i></option>

                                                                            <option value="DI">Diploma /<i>Diploma</i></option>

                                                                            <option value="DG">Degree /<i>Ijazah</i></option>

                                                                            <option value="MA">Masters /<i>Ijazah Sarjana</i></option>

                                                                            <option value="PH">PhD /<i>Kedoktoran</i></option>

                                                                            <option value="Z">Not Specified /<i>Tidak ditentukan</i></option>

                                                                     @else

                                                                         <option disabled="" selected="">SELECT</option>

                                                                        <option value="PS">Primary /<i>Rendah</i></option>

                                                                        <option value="SS">Secondary /<i>Menengah</i></option>

                                                                        <option value="DI">Diploma /<i>Diploma</i></option>

                                                                        <option value="DG">Degree /<i>Ijazah</i></option>

                                                                        <option value="MA">Masters /<i>Ijazah Sarjana</i></option>

                                                                        <option value="PH">PhD /<i>Kedoktoran</i></option>

                                                                        <option value="Z">Not Specified /<i>Tidak ditentukan</i></option>

                                                                    @endif
                                                                    </select>
                                                          
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach

                                                    <div class="tab-pane" id="tab2">
                                                  
                                                    <br>
                                                         <b>Loan Amount : RM {{number_format($dsr_b->financing_amount)}} | Tenure : {{$dsr_b->tenure}} Year(s) | Package : @if($pra->package->id=="1") Mumtaz-<i><font size="1">i</font></i> @else Afdhal-<i><font size="1">i</font></i> @endif</b><br> 
                                                    <h3><strong>Step 2</strong> - EMPLOYMENT DETAILS (MAIN APPLICANT) / <i> BUTIR-BUTIR PEKERJAAN (PEMOHON UTAMA)</i> </h3>
                                                    {{csrf_field()}}
                                                     <input type="hidden" name="id_praapplication" value="{{$pra->id}}"/>
                                                            @foreach($empinfo as $empinfo)
                                                    <div class="col-md-6">
                                                                <br>
                                        
                                                     <div class="form-group">
                                                    <label>1. Occupation / <i> Pekerjaan </i> :</label>
                                                      

                                                         <select name="occupation" id="occupation" class="select2" required="">
                                                             <option selected=""></option>
                                                            @foreach ($occupation as $occupation)
                                                                @if($empinfo->occupation !=NULL)
                                                                    <?php 
                                                                        if($empinfo->occupation==$occupation->occupation_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$occupation->occupation_code}}">{{$occupation->occupation_desc}}</option>
                                                                @else
                                                                    <option value="{{$occupation->occupation_code}}">{{$occupation->occupation_desc}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                       
                                                        
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                    <label>2. Name of Employer/Business / <i>Nama Majikan/Perniagaan </i> :</label>
                                                        @if(!empty($empinfo->empname))
                                                        <input type="text" name="emp_name" maxlength="66"  
                                                            value="{{$empinfo->empname}}" class="form-control" required="requeired">
                                                        @else
                                                            <input type="text" name="emp_name" maxlength="66"  
                                                                value="{{$pra->employer->name}}" class="form-control" required="requeired">  
                                                        @endif
                                                    </div>
                                                     <div class="form-group">
                                                        <label>3. MBSB Staff / <i>Kakitangan MBSB</i> :</label>
                                                            <select class=" form-control" id="mbsb_staff" name="mbsb_staff" required>
                                                                @if(!empty($empinfo->mbsb_staff))
                                                                    @if($empinfo->mbsb_staff=='Y')
                                                                    <option value="Y">Yes / <i>Ya</i></option>
                                                                    @elseif($empinfo->mbsb_staff=='N')
                                                                    <option value="N">No /<i> Tidak</i></option>
                                                                    @endif

                                                                    <option value="Y">Yes / <i>Ya</i></option> 
                                                                    <option value="N">No /<i> Tidak</i></option>
                                                                @else
                                                                    <option selected=""></option>
                                                                    <option value="Y">Yes / <i>Ya</i></option> 
                                                                    <option value="N">No /<i> Tidak</i></option>
                                                                @endif
                                                            </select>
                                                           <div id="staff_no">
                                                            <label>Staff No / <i>No Kakitangan</i> :</label>
                                                            
                                                            <input type="text" class="form-control" placeholder="Specify / Sila Nyatakan" name="staff_no" id="staff_no" required @if($empinfo->mbsb_staff=="Y") value="{{$empinfo->staff_no}}" @endif >
                                                        </div>

                                                      

                                                    </div>
                                                    <div class="form-group">
                                                    <label>4.  Address of Employer/Business / <i>Alamat Majikan/Perniagaan</i> :</label>
                                                        <input type="text" class="form-control" name="address" value="{{$empinfo->address}}" required="required" placeholder="Address Line 1" minlength="2" maxlength="40" onkeyup="this.value = this.value.toUpperCase()"><br>

                                                         <input type="text" class="form-control" name="address2" value="{{$empinfo->address2}}" placeholder="Address Line 2" minlength="2" maxlength="40" onkeyup="this.value = this.value.toUpperCase()"><br>

                                                          <input type="text" class="form-control" name="address3" value="{{$empinfo->address3}}" placeholder="Address Line 3" onkeyup="this.value = this.value.toUpperCase()"><br>
                                                           <label> Postcode/ <i>Poskod :</i></label>
                                                                <br><input type="text"  value="{{$empinfo->postcode}}" maxlength="5" name="postcode3" id="postcode3" class="form-control" onkeypress="return isNumberKey(event)" required="" />
                                                            <label>State / <i>Negeri :</i></label><br>
                                                                <input type="text"   value="{{$empinfo->state}}"   maxlength="50"  id="state3" name="state3" class="form-control" re>
                                                    </div> 
                                                    
                                                    <div class="form-group">
                                                    <label>5. Start Date of Work / <i>Tarikh Mula Bekerja</i> :</label>

                                                        <?php 
                                                            if($empinfo->joined=="0000-00-00") {
                                                                $joined =  " ";
                                                            }
                                                            else {
                                                                $joined =  date('d/m/Y', strtotime($empinfo->joined));
                                                            }
                                                            $today=date('d/m/Y');
                                                            
                                                        ?>
                                                        @if(!empty($empinfo->joined))
                                                        <input type="text"  name="joined"  data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  value="{{$joined}}"
                                                         class="form-control startdate" name="joined" / required="">
                                                         @else
                                                          <input type="text"  name="joined"  data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  value=""
                                                         class="form-control startdate" name="joined" required="">
                                                         @endif
                                                    </div>

                                                    <div class="form-group">
                                                        <label>6. Position / <i>Jawatan</i> :</label>
                                                        <select name="position" id="position" class="select2" required="">
                                                             <option selected=""></option>
                                                            @foreach ($position as $position)
                                                                @if($empinfo->position !=NULL)
                                                                    <?php 
                                                                        if($empinfo->position==$position->position_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$position->position_code}}">{{$position->position_desc}}</option>
                                                                @else
                                                                    <option value="{{$position->position_code}}">{{$position->position_desc}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                     <div class="form-group">
                                                        <label>7. Office Telephone / <i>Telefon Pejabat :</i></label><br>
                                                            <input type="text" id="office_phone" minlength="6" maxlength="14"   name="office_phone" class="form-control" onkeypress="return isNumberKey(event)" value="{{$empinfo->office_phone}}" required="" />
                                                    </div>

                                                    </div> </div>
                                                    @endforeach 

                                                    <div class="tab-pane" id="tab3">
                                                    <br>
                                                         <b>Loan Amount : RM {{number_format($dsr_b->financing_amount)}} | Tenure : {{$dsr_b->tenure}} Year(s) | Package : @if($pra->package->id=="1") Mumtaz-<i><font size="1">i</font></i> @else Afdhal-<i><font size="1">i</font></i> @endif</b><br> 
                                                    <h3><strong>Step 3</strong> - PARTICULARS OF SPOUSE & EMERGENCY CONTACT**/ <i> MAKLUMAT SUAMI-ISTERI & RUJUKAN KECEMASAN**</i></h3>  
                                                    <input type="hidden" name="id_praapplication" value="{{$pra->id}}">
                                                    {{csrf_field()}}
                                                          <div class="col-md-6"  id="tab99">
                                                                <br>
                                                           <h4><strong>PARTICULAR OF SPOUSE / <i>MAKLUMAT SUAMI-ISTERI</i></strong></h4>
                                                   <div id="spouse-group">
                                                        <div class="form-group" id="spouse_name" >
                                                        @foreach($spouse as $spouse)
                                                        <label>Full Name   / <i>  Nama Penuh   :  </i>  </label>
                                                            <input type="text" maxlength="66" class="form-control" value="{{$spouse->name}}" name="name" id="" required="required">
                                                        </div>

                                                         <div class="form-group">
                                                                <label>Home/Office Telephone / <i>Telefon Rumah/Pejabat :</i></label><br>
                                                                    <input type="text" id="home_phone" minlength="6" maxlength="14"   name="homephone" class="form-control" value="{{$spouse->homephone}}" onkeypress="return isNumberKey(event)"/>
                                                        </div>
                                                        <div class="form-group">
                                                                <label>Mobile Phone / <i>Telefon Bimbit :</i></label><br>
                                                                    <input type="text" id="handphone" minlength="6" maxlength="14"   name="mobilephone" class="form-control" value="{{$spouse->mobilephone}}" onkeypress="return isNumberKey(event)"/>
                                                       
                                                        </div>

                                                        <div class="form-group" id="spouse_occupation">
                                                        <label>Employment Type/<i>Jenis Pekerjaan </i> :</label>
                                                            <select class="select2"  name="emptype" id="spouse_emptype" required="required">
                                                            @if(!empty($spouse->emptype))
                                                                <option  >{{$spouse->emptype}}</option>
                                                            @endif
                                                                <option>Self - Employed / <i>Bekerja Sendiri</i></option>
                                                                <option>Unemployed/ <i>Tidak Bekerja</i></option>
                                                                <option>Salaried - Private Sector/ <i>Bergaji - Sektor Swasta</i></option>
                                                                <option>Professional/ <i>Profesional</i></option>
                                                                <option>Salaried - Government/ <i>Bergaji - Kerajaan</i></option>
                                                                <option>Others (Specify)/ Lain-lain (Nyatakan)</option>
                                                        </select>
                                                           <input type="text" class="form-control" placeholder="Sila Nyatakan" id="spouse_emptype_others" name="emptype_others" required @if($spouse->emptype=="Others (Specify)/ Lain-lain (Nyatakan)") value="{{$spouse->emptype_others}}"  @endif >
                                                           <input type="text" class="form-control" id="spouse_emptype_kosong">
                                                    </div>
                                                </div>
                                                           @endforeach

                                                    <?php $sumref=1; ?>
                              
                                                    <br>
                                                    <h4><strong>EMERGENCY CONTACT** / RUJUKAN KECEMASAN** </strong></h4>
                                                    <b>** (Family Members/Relatives not staying with you)</b> / <i>(Ahli Keluarga/Saudara terdekat yang tidak tinggal bersama anda)</i><br><br>

                                                    <b>
                                                    Contact 1 / <i> Rujukan 1 </i></b>
                                                    <div class="form-group">
                                                    <label>Full Name / <i> Nama Penuh </i> </label>
                                                            <input type="text" maxlength="66" id="name1" name="name1" value="{{$reference->name1}}" class="form-control" required="required" onkeyup="this.value = this.value.toUpperCase()">
                                                    </div>

                                                    <div class="form-group">
                                                    <label>Home Telephone/ <i> Telefon Rumah </i>:</label>
                                                        <input type="text" minlength="6" maxlength="14"   value="{{$reference->home_phone1}}" id="home_phone1" name="home_phone1" class="form-control" onkeypress="return isNumberKey(event)"  />
                                                    </div> 

                                                    <div class="form-group">
                                                    <label>Mobile Phone / <i> Telefon Bimbit </i>:</label>
                                                        <input type="text" minlength="6" maxlength="14"   value="{{$reference->mobilephone1}}" id="mobilephone1" name="mobilephone1" class="form-control" onkeypress="return isNumberKey(event)" required="required" />
                                                    </div> 

                                                    <div class="form-group">
                                                    <label>Relationship / <i> Hubungan</i> </label>
                                                            <select name="relationship1" id="relationship1" class="select2" required="">
                                                             <option selected=""></option>
                                                            @foreach ($relationship as $relationship)
                                                                @if($reference->relationship1 !=NULL)
                                                                    <?php 
                                                                        if($reference->relationship1==$relationship->relationship_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$relationship->relationship_code}}">{{$relationship->relationship_desc}}</option>
                                                                @else
                                                                    <option value="{{$relationship->relationship_code}}">{{$relationship->relationship_desc}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <b>Contact 2 / <i> Rujukan 2 </i></b>
                                                    <div class="form-group">
                                                    <label>Full Name / <i> Nama Penuh </i> </label>
                                                            <input type="text" maxlength="66" id="name2" name="name2" value="{{$reference->name2}}" class="form-control" required="required" onkeyup="this.value = this.value.toUpperCase()">
                                                    </div>

                                                    <div class="form-group">
                                                    <label>Home Telephone/ <i> Telefon Rumah </i>:</label>
                                                        <input type="text" minlength="6" maxlength="14"   value="{{$reference->home_phone2}}" id="home_phone2" name="home_phone2" class="form-control" onkeypress="return isNumberKey(event)">
                                                    </div> 

                                                    <div class="form-group">
                                                    <label>Mobile Phone / <i> Telefon Bimbit </i>:</label>
                                                        <input type="text" minlength="6" maxlength="14"   value="{{$reference->mobilephone2}}" id="mobilephone2" name="mobilephone2" class="form-control" onkeypress="return isNumberKey(event)" required="required" />
                                                    </div> 

                                                    <div class="form-group">
                                                    <label>Relationship / <i> Hubungan</i> </label>
                                                            <select name="relationship2" id="relationship2" class="select2" required="">
                                                             <option selected=""></option>
                                                            @foreach ($relationship2 as $relationship2)
                                                                @if($reference->relationship2 !=NULL)
                                                                    <?php 
                                                                        if($reference->relationship2==$relationship2->relationship_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$relationship2->relationship_code}}">{{$relationship2->relationship_desc}}</option>
                                                                @else
                                                                     <option value="{{$relationship2->relationship_code}}">{{$relationship2->relationship_desc}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                  
                                                 
                                                    </div>
                                                    </div>

                                                    
                                                
                                                          

                                                         
                                                          <div class="tab-pane" id="tab4">
                                                            <br>
                                                            
                                                                   <b>Loan Amount : RM {{number_format($dsr_b->financing_amount)}} | Tenure : {{$dsr_b->tenure}} Year(s) | Package : @if($pra->package->id=="1") Mumtaz-<i><font size="1">i</font></i> @else Afdhal-<i><font size="1">i</font></i> @endif</b><br> 
                                                            <h3><strong>Step 4</strong> -  Upload Document / <i> Muat Naik dokumen</i> (<i>Document in PDF/JPG/PNG Format / Dokumen dalam format PDF/JPG/PNG</i>) </h3>
                                                            <fieldset>
                                            
                                                                 @if($financial->debt_consolidation>0)
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Settlement Letter / <i> Penyata Penyelesaian Awal</i></label>
                                                    <div class="col-md-8">
                                                         <input id="fileupload1"  @if(empty($document1->name)) required @endif   class="btn btn-default" type="file" name="file1" >
                                                        <input type="hidden" name="document1"  id="documentx1"  value="Salinan Penyata Penyelesaian Awal">
                                                        &nbsp; <span id="document1"> </span> 
                                                        @if(!empty($document1->name))
                                                            <span id="document1a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $user->name)}}/{{$document1->upload}}"> {{$document1->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                                                        @endif
                                                        <br><br>
                                                       If more than one document, please upload below/ <br><i>Jika dokumen penyata penyelesaian awal lebih daripada satu, sila muat naik dibawah </i>
                                                                                                      
                                                         <input id="fileupload2"  class="btn btn-default" type="file" name="file2" >
                                                        <input type="hidden" name="document2"  id="documentx2"  value="Salinan Penyata Penyelesaian Awal (optional 1)">
                                                        &nbsp; <span id="document2"> </span> 
                                                        @if(!empty($document2->name))
                                                            <span id="document2a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $user->name)}}/{{$document2->upload}}"> {{$document2->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                                                        @endif

                                                        <input id="fileupload3"  class="btn btn-default" type="file" name="file3" >
                                                        <input type="hidden" name="document3"  id="documentx3"  value="Salinan Penyata Penyelesaian Awal  (optional 2)">
                                                        &nbsp; <span id="document3"> </span> 
                                                        @if(!empty($document3->name))
                                                            <span id="document3a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $user->name)}}/{{$document3->upload}}"> {{$document3->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                                                        @endif
            
             
                                                    </div>
                                                </div>
                                                @endif
                                                  <br> &nbsp; </br>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Salinan Kad Pengenalan *</label>
                                                    <div class="col-md-8">
                            <input id="fileupload6"  @if(empty($document6->name)) required @endif  class="btn btn-default" type="file" name="file6"  >
                            <input type="hidden" name="document6"   id="documentx6"  value="Salinan Kad Pengenalan">
                            &nbsp; <span id="document6"> </span> 
                            <input type='hidden' value='@if(!empty($document6->name)){{$document6->upload}} @endif' id='a6' name='a6'/>
                            @if(!empty($document6->name))
                              <span id="document6a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id)}}/{{$document6->upload}}"> {{$document6->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                            @endif
                                  
                                                    </div>
                                                </div>
                                                      <br> &nbsp; </br>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Surat Pengesahan Majikan</label>
                                                    <div class="col-md-8">
                                                        <input id="fileupload9"  @if(empty($document9->name)) required @endif  class="btn btn-default" type="file" name="file9"  >
                                                        <input type="hidden" name="document9"   id="documentx9"  value="Surat Pengesahan Majikan">
                                                        &nbsp; <span id="document9"> </span> 
                                                        @if(!empty($document9->name))
                                                            <span id="document9a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id)}}/{{$document9->upload}}"> {{$document9->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                                                        @endif
                                                                    
                                                    </div>
                                                </div>
        
                                               
                                                </fieldset>
                                              
                                            </div>
                                             
                                            <div class="tab-pane" id="tab5">

                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input name="id_praapplication" type="hidden" id="id_praapplication" value="{{$data->id_praapplication}}" >
                                                <br>
                                                       <b>Loan Amount : RM {{number_format($dsr_b->financing_amount)}} | Tenure : {{$dsr_b->tenure}} Year(s) | Package : @if($pra->package->id=="1") Mumtaz-<i><font size="1">i</font></i> @else Afdhal-<i><font size="1">i</font></i> @endif</b><br> 
                                                <h3><strong>Step 5</strong> -DECLARATION/DISCLOSURE BY APPLICANT / <i>PERAKUAN/PENDEDAHAN OLEH PEMOHON </i>  </h3>
                                                           
                                                    <div class="row">
                                                        <div class="col-lg-1">
                                                        </div>
                                                        <div align="justify" class="col-lg-10">

                                                            <table border="1">
                                                                <tr>
                                                                    <td class="border" align="left" bgcolor="#0055a5">  
                                                                        <font color="white"> <b>7.1) PURCHASE APPLICATION AND PROMISE TO BUY</b>/ <i>PERMOHONAN BELIAN DAN AKUJANJI UNTUK MEMBELI </i></font>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="border">
                                                                        <b><br>Upon approval of financing and my acceptance to the Terms and Conditions, I hereby order and request MBSB to purchase commodity with the purchase price that will be approved by MBSB as per SMS 'Aqad and promise to buy the same commodity at cost plus profit, depending on the financing facility and I will be held responsible for any violation to the agreement.</b><br> 
                                                                        <i>Setelah mendapat kelulusan pembiayaan dan penerimaan saya kepada Terma dan Syarat, saya dengan ini membuat pesanan dan memohon MBSB untuk membeli komoditi dengan harga belian yang akan diluluskan oleh MBSB seperti tertera di dalam 'Aqad SMS dan berjanji untuk membeli komoditi tersebut pada kos tambah keuntungan, bergantung kepada kelulusan pembiayaan, di mana saya bertanggungjawab sepenuhnya akibat mengingkari perjanjian ini.</i><br><br>
                                                                    <table width="90%" border="0">
                                                                         <tr>
                                                                             <td valign="top" width="40%">
                                                                                 <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Name</b><br>
                                                                                            <i>Nama</i>
                                                                                        </td>
                                                                                         <td>
                                                                                            <input type="text" class="form-control" value="{{$data->name}}" name="" disabled/>
                                                                                        </td>

                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td>
                                                                                             <b>MyKad No.</b><br>
                                                                                            <i>No. MyKad</i>
                                                                                         </td>
                                                                                         <td>
                                                                                            <input type="text" class="form-control" value="{{$data->new_ic}}" name="" disabled/>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td>
                                                                                             <b>Date.</b><br>
                                                                                            <i>Tarikh</i>
                                                                                         </td>
                                                                                          <td>

                                                                                            <input type="text" class="form-control" value="{{$today}}" name="" disabled/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                             </td>
                                                                             <td valign="top">
                                                                                    <table border="1" width="500" height="100">
                                                                                        <tr>
                                                                                            <td align="center"><h2>
                                                                                                <input type="checkbox" value="1" name="purchase_application" @if($term->purchase_application>0) checked @endif ><b> I Agree</b> / Saya Bersetuju</h2>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                             </td>
                                                                         </tr>
                                                                    </table>
                                                                       <br>


                                                                       
                                                                    
                                                                                
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                          <!-- APPOINTMENT --><br>

                                                            <table border="1">
                                                          <tr>
                                                          <td class="border" align="left" bgcolor="#0055a5">  <font color="white"> <b>7.2) APPOINTMENT OF MBSB AS A SALES AGENT</b> / <i>PERLANTIKAN MBSB SEBAGAI EJEN JUALAN</i></font></td>
                                                          </tr>
                                                          <tr>
                                                        
                                                          <td class="border">

                                                          <b><br>Subject to MBSB's acceptance, I hereby irrevocably and unconditionally appoint MBSB as my agent under the shariah contract of Wakalah to sell the commodities  to any third party purchaser/commodity trader as MBSB may deem fit and in accordance with such terms acceptable to MBSB. I shall be bound by any contract or agreement that MBSB may enter into with the said third party purchaser/trader for the purpose of the sale of the commodities on my behalf. I hereby agree to pay MBSB a sum of RM36.04 (inclusive of Goods and Services Tax) as Wakalah Fee. I hereby  undertake to indemnify MBSB to make good in full all losses, costs and expenses resulting from any claims, proceedings, actions, requests or any form of damages that MBSB may suffer or incur as a result of fulfilling MBSB's agency function as set out above.</b><br> 
                                                          <i>Tertakluk kepada penerimaan MBSB, saya dengan ini secara tidak boleh batal dan tanpa syarat melantik MBSB sebagai ejen saya di bawah kontrak syariah Wakalah untuk menjual komoditi kepada mana-mana pembeli/peniaga komoditi pihak ketiga sebagaimana yang difikirkan patut oleh MBSB dan mengikut apa-apa terma yang diterima oleh MBSB. Saya akan terikat dengan mana-mana kontrak atau perjanjian yang dibuat oleh MBSB dengan pembeli/peniaga komoditi pihak ketiga tersebut bagi tujuan penjualan komoditi bagi pihak saya. Saya dengan ini bersetuju untuk membayar MBSB sebanyak <b>RM36.04</b> (termasuk Cukai Barangan dan Perkhidmatan) sebagai fi Wakalah. Saya dengan ini mengaku janji untuk menanggung segala kerugian MBSB dengan membayar sepenuhnya semua kerugian, kos, dan perbelanjaan yang timbul daripada apa-apa tuntutan, prosiding, tindakan, permintaan atau apa-apa bentuk kerosakan yang mungkin dialami atau ditanggung oleh MBSB akibat memenuhi fungsi agensi seperti yang dinyatakan di atas.</i><br><br>

                                                        <table width="90%" border="0">
                                                             <tr>
                                                                 <td valign="top" width="40%">

                                                                   <table>
                                                                        <tr>
                                                                            <td>
                                                                                <b>Name</b><br>
                                                                                <i>Nama</i>
                                                                            </td>
                                                                             <td>
                                                                                <input type="text" class="form-control" value="{{$data->name}}" name="" disabled/>
                                                                            </td>

                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>MyKad No.</b><br>
                                                                                <i>No. MyKad</i>
                                                                             </td>
                                                                             <td>
                                                                                <input type="text" class="form-control" value="{{$data->new_ic}}" name="" disabled/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>Date.</b><br>
                                                                                <i>Tarikh</i>
                                                                             </td>
                                                                              <td>

                                                                                <input type="text" class="form-control" value="{{$today}}" name="" disabled/>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <table border="1" width="500" height="100">
                                                                            <tr>
                                                                                <td align="center"><h2><input type="checkbox" name="appointment_mbsb" value="1" @if($term->appointment_mbsb>0) checked @endif ><b> I Agree</b> / Saya Bersetuju</h2>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                  </td>
                                                              </tr>
                                                          </table>

                                                            </td>

                                                        
                                                          </tr>
                                                          </table>

                                                        <br>
                                                          
                                                      <table border="1">
                                                          <tr>
                                                          <td class="border" align="left" bgcolor="#0055a5">  <font color="white"> <b>7.3) DECLARATION/DISCLOSURE BY APPLICANT/CO-APPLICANT/GUARANTOR </b> / <i>PERAKUAN/PENDEDAHAN OLEH PEMOHON/PEMOHON BERSAMA/PENJAMIN</i></font></td>
                                                          </tr>
                                                          <tr>
                                                        
                                                          <td class="border">
                                                        <ol type='1'>
                                                          <li> <b>I/We hereby declare that I/we am/are NOT a bankrupt.<br></b>
                                                                <i> Saya/Kami mengesahkan bahawa saya/kami TIDAK muflis. 
                                                                <br></i><br>
                                                          </li>
                                                          <li>
                                                                 <b> I/We declare that the information furnished in this form are completely true and accurate and I/we have not withheld any information which may prejudice my/our financing application or have a bearing on your financing decision.</b> <br><i>Saya/Kami mengesahkan bahawa maklumat yang disediakan di dalam borang ini adalah benar, tepat, dan lengkap dan saya/kami tidak menyembunyikan sebarang maklumat yang mungkin prejudis terhadap 
                                                                 permohonan pembiayaan saya/kami atau mempunyai kesan ke atas keputusan 
                                                                 pembiayaan anda.<br></i><br></li>
                                                          <li> <b> I/We hereby authorize you/your representative to obtain the relevant information relating to this application from any relevant source as deemed suitable by MBSB but not limited to any bureaus or agencies established by Bank Negara Malaysia ("BNM") or other parties. The authorization to obtain the relevant information is also extended to prospective guarantors, security providers, authorized depository agent and other party relating to this application as deemed necessary by MBSB.</b><br><i>Saya/Kami memberi kebenaran kepada MBSB untuk mendapatkan maklumat yang relevan terhadap permohonan ini dari sumber-sumber yang relevan dan yang dianggap sesuai oleh MBSB termasuk dan tidak terhad kepada mana-mana biro atau agensi yang ditubuhkan oleh Bank Negara Malaysia ("BNM") atau pihak yang lain. Kebenaran untuk mendapatkan maklumat yang relevan juga merangkumi bakal penjamin dan/atau pemberi sekuriti dan mana-mana wakil penyimpan yang dibenarkan dan pihak yang berkenaan dengan permohonan ini yang dianggap sesuai oleh MBSB. <br></i><br> </li>

                                                          <li> <b>I/We understand that MBSB reserves the absolute right to approve or decline this application as MBSB deems fit without assigning any reason.</b><br>
                                                          <i>Saya/Kami faham bahawa pihak MBSB mempunyai hak mutlak untuk meluluskan atau menolak permohonan tanpa menyatakan sebarang alasan. <br></i><br></li>

                                                          <li> <b>I/We expressly irrevocably consent and authorize MBSB to furnish all relevant information relating to or arising from or in connection with financing facilities to any subsidiary companies of MBSB, its agents and/or such person or BNM, Cagamas Berhad and debt collection agents or such other authority or body established by BNM, or such other authority having jurisdiction over MBSB as MBSB may absolutely deem fit and such other authority as may be authorized by law. </b><br> <i>Saya/Kami bersetuju tanpa hak menarik balik dan tanpa syarat memberi kebenaran kepada MBSB untuk mendedahkan sebarang maklumat yang diperlukan berkaitan dengan atau berbangkit daripada apa-apa hubungan dengan kemudahan pembiayaan kepada mana-mana anak syarikat MBSB, ejennya, dan/atau mana-mana individu atau BNM, Cagamas Berhad dan ejen pemungut hutang atau mana-mana pihak berkuasa lain yang mempunyai bidang kuasa ke atas MBSB di mana MBSB secara mutlak berhak memberi kata putus dan mana-mana pihak berkuasa yang dibenarkan selaras dengan undang-undang. <br></i><br></li>

                                                          <li><b>I/We hereby further expressly irrevocably consent and authorize MBSB to seek any information concerning me/us with or from any credit reference/reporting agencies, including but not limited to CCRIS, CTOS, RAMCI, FIS and/or Inland Revenue Authorities or any authority as MBSB may from time to time deem fit.</b> <br> <i>Saya/Kami bersetuju tanpa hak menarik balik dan tanpa syarat memberi kebenaran kepada MBSB untuk mendapatkan sebarang maklumat berkaitan saya/kami daripada mana-mana agensi rujukan kredit, termasuk dan tidak terhad kepada CCRIS, CTOS, RAMCI, FIS dan/atau Lembaga Hasil Dalam Negeri atau mana-mana pihak berkuasa yang  diputuskan oleh MBSB dari masa ke semasa.<br></i><br></li>

                                                          <li><b>I/We also acknowledge that it is a requirement that all information relating to this application must be transmitted and/or updated to the Central Credit Reference Information System ("CCRIS"), a database maintained by BNM as and when necessary.</b> <br> <i>Saya/Kami juga mengesahkan bahawa semua maklumat berkaitan permohonan ini mestilah dimaklumkan dan/atau dikemaskini kepada Sistem Maklumat Rujukan Kredit Pusat ("CCRIS"), pengkalan data yang diuruskan oleh BNM apabila perlu.<br></i><br></li>

                                                          <li><b>I/We shall comply with MBSB's requirements in respect of my/our application and I/we understand that MBSB's offer of the financing shall be subject to MBSB performing the necessary verification.</b> <br> <i>Saya/Kami akan mematuhi segala keperluan MBSB untuk permohonan saya/kami dan saya/kami memahami bahawa tawaran pembiayaan oleh MBSB adalah tertakluk kepada pengesahan yang diperlukan oleh MBSB<br></i><br></li>

                                                          <li><b>I/We hereby undertake to notify and/or inform the emergency contact person and my/our spouse that their personal data has been provided to MBSB and undertake to indemnify and hold MBSB harmless in the event of any legal repercussions arising from my/our failure and/or to notify the said emergency contact person/spouse. </b><br> <i>Saya/Kami dengan ini bersetuju untuk memberitahu dan/atau menghubungi penama rujukan kecemasan dan suami isteri bahawa data peribadi mereka telah diberi kepada MBSB dan berjanji tidak akan mengambil sebarang tindakan ke atas MBSB sekiranya timbul sebarang tindakan undang-undang daripada kegagalan saya/kami untuk memberitahu dan/atau menghubungi penama rujukan kecemasan/suami isteri.<br></i><br></li>

                                                          <li><b>This application form and all supporting documents that were submitted to MBSB shall be the sole property of MBSB and MBSB is entitled to retain the same irrespective of whether my/our application is approved or rejected by MBSB. </b><br> <i>Borang permohonan ini dan semua dokumen sokongan yang telah diserahkan kepada MBSB adalah hak milik mutlak MBSB dan MBSB berhak untuk mengekalkan semua dokumen tanpa mengira samaada permohonan saya/kami diluluskan atau ditolak oleh MBSB.<br></i><br></li>

                                                          <li><b>I/We hereby irrevocably agree to waive my/our rights to a refund where the amount is not exceeding RM5.00 arising from but not limited  to early settlement or closure of my/our financing account. I/We further consent and authorize MBSB to donate the said amount to charitable organisations deemed appropriate by MBSB.</b><br> <i>Saya/Kami bersetuju tanpa hak menarik balik bagi bayaran balik di mana jumlahnya tidak melebihi RM5.00, hasil daripada tetapi tidak terhad kepada penyelesaian awal atau penutupan akaun pembiayaan saya/kami. Saya/Kami seterusnya membenar dan memberi kuasa kepada MBSB untuk menderma jumlah tersebut kepada badan-badan kebajikan yang difikirkan wajar oleh pihak MBSB.<br></i><br></li>

                                                        </ol>
                                                         
                                                         
                                                         
                                                         
                                                         
                                                          
                                                          
                                                          
                                                          
                                                         
                                                          </td>
                                                            </tr>
                                                          </table>


                                                          <!-- PERMISSION TO DEDUCT --><br>

                                                           <table border="1">
                                                          <tr>
                                                            <td class="border" align="left" bgcolor="#0055a5">  <font color="white"><b>7.4) PERMISSION TO DEDUCT FROM PERSONAL FINANCING-i FACILITY</b> / <i>KEBENARAN PEMOTONGAN DARI KEMUDAHAN PEMBIAYAAN PERIBADI-i</i></font></td>
                                                          </tr>
                                                          <tr>
                                                        
                                                          <td>
                                                            <ol type="1">

                                                                <li>
                                                                     <b>I hereby authorize and allow MBSB to deduct and pay directly from the financing amount approved by MBSB, the following :</b><br>
                                                                     <i>Saya dengan ini memberi kuasa dan membenarkan MBSB melakukan pemotongan / pembayaran secara terus daripada jumlah pembiayaan yang diluluskan oleh MBSB, seperti berikut:</i></li>
                                                                      <ol type="i">
                                                                        <li><b>Security Deposit</b>/<i> Deposit Sekuriti</i>.</li>
                                                                        <li><b> GCTT Takaful Contribution (if applicable)</b>/<i> Sumbangan Takaful GCTT (sekiranya berkenaan)</i>.</li>
                                                                        <li><b>Wakalah Fee</b>/<i> Fi Wakalah</i>.</li>
                                                                        <li><b>Redemption for other financing (if applicable)</b>/<i> Penyelesaian pembiayaan lain (sekiranya berkenaan)</i>.</li>
                                                                        <li><b>Banca Takaful Product (if applicable)</b>/<i> Produk Banca Takaful (sekiranya berkenaan)</i>.</li>
                                                                        <li><b>Interbank Giro (IBG) Fee</b>/<i> Fi Giro Antara Bank (IBG)</i>.</li>
                                                                        <li><b>Other charges (if applicable)</b>/<i> Lain-lain caj (sekiranya berkenaan)</i>.</li>
                                                                    </ol><br>
                                                                </li>

                                                                <li> 
                                                                    <b>The deductions to be made are subject to the package taken by me and based on the terms imprinted in the SMS 'aqad.</b>
                                                                    <br><i>Potongan yang akan dibuat adalah tertakluk kepada pakej yang diambil oleh saya dan sebagaimana yang tertera  di dalam 'aqad SMS.</i><br><br>
                                                                </li>

                                                                <li>
                                                                    <b>I also agree that the net financing amount (after deductions) will be paid by MBSB to me by crediting my account details of which I have provided or by any other mode deemed suitable by MBSB.</b>
                                                                    <br> <i>Saya juga bersetuju bahawa baki amaun pembiayaan di atas (selepas potongan) akan dibayar oleh MBSB kepada saya dengan mengkreditkan akaun saya mengikut butiran yang telah disertakan atau melalui apa-apa cara yang difikirkan sesuai oleh MBSB.</i><br><br>
                                                                </li>

                                                                <li>
                                                                    <b>I also confirm and undertake that this authorization is irrevocable in the event the amount to settle other debts has been paid to any third party on my behalf by MBSB  and I further agree and undertake to pay all indebtedness owing to MBSB in the event I cancel the facility.</b>
                                                                    <br> <i>Saya juga dengan ini akujanji bahawa kebenaran ini adalah kebenaran tidak boleh batal sekiranya kesemua amaun tebus hutang telah pun dibayar kepada mana-mana pihak ketiga bagi pihak saya oleh MBSB dan saya juga bersetuju serta berakujanji akan membayar kesemua amaun tebus hutang tersebut kepada MBSB jika saya membatalkan kemudahan tersebut.  </i><br><br>
                                                                </li>

                                                                <li>
                                                                    <b>I shall be responsible to pay in full any cost incurred. I hereby further undertake to keep MBSB fully indemnified and make good in full all losses, costs and expenses resulting from any claims, proceedings, actions, requests or any form of damages that MBSB may suffer or incur as a result of fulfilling its function as set out above.</b>
                                                                    <br><i>Saya akan bertanggungjawab untuk membayar sepenuhnya semua kos yang ditanggung. Saya juga mengaku janji akan menanggung segala kerugian MBSB dengan membayar sepenuhnya  semua kerugian, kos dan perbelanjaan yang timbul daripada apa-apa tuntutan, prosiding, tindakan, permintaan atau apa-apa bentuk kerosakan yang mungkin dialami atau ditanggung oleh MBSB akibat memenuhi fungsi seperti yang dinyatakan di atas.</i>
                                                                </li>

                                                            </ol>
                                                        
                                                         </td>
                                                        </tr>
                                                      </table>

                                                       <!-- Credit Transactions --><br>

                                                        <table border="1" >
                                                          <tr>
                                                                <td class="border" align="left" bgcolor="#0055a5">  <font color="white"><b>7.5) CREDIT TRANSACTIONS AND EXPOSURES WITH CONNECTED PARTIES/ </b>/ <i>TRANSAKSI KREDIT DAN PENDEDAHAN DENGAN PIHAK BERKAITAN </i></font> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="border">
                                                                    <b><br> Do you have any immediate family or close relatives (including parents, brother/sister and their spouses, dependent's spouse and own/step/adopted child) that are employees of MBSB?</b><br> 
                                                                    <i>Adakah anda mempunyai ahli keluarga atau saudara terdekat (termasuk ibubapa, abang/kakak/adik dan pasangan, pasangan dibawah tanggungan, anak dan saudara tiri/angkat) yang sedang bekerja dengan MBSB? </i><br><br>
                                                                     <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" id="credit_yes" name="credit_transactions" value="1" @if($term->credit_transactions>0) checked @endif >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" id="credit_no" name="credit_transactions" value="0" @if($term->credit_transactions==0) checked @endif >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                    <br>
                                                                    <b>If Yes, please complete the information below:/</b><i> Jika Ya, sila lengkapkan maklumat dibawah:</i><br><br>
                                                                    <table width="40%">
                                                                        <tr>
                                                                            <td>
                                                                                <b>Full Name</b><br>
                                                                                <i>Nama Penuh</i>
                                                                            </td>
                                                                             <td>
                                                                                <input type="text" onkeyup="this.value = this.value.toUpperCase()" onkeypress="return fullname(event);" class="form-control" name="fullname" @if(!empty($credit->fullname))  value="{{$credit->fullname}}" @endif />
                                                                            </td>

                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>MyKad No.</b><br>
                                                                                <i>No. MyKad</i>
                                                                             </td>
                                                                             <td>
                                                                                <input type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==12) return false;"  minlength="12" maxlength="12" class="form-control" @if(!empty($credit->mykad))   value="{{$credit->mykad}}" @endif  name="mykad" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>Passport No.</b><br>
                                                                                <i>No. Passport</i>
                                                                             </td>
                                                                              <td>

                                                                                <input type="text" class="form-control" @if(!empty($credit->passport))  value="{{$credit->passport}}" @endif name="passport" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>Relationship</b><br>
                                                                                <i>Hubungan</i>
                                                                             </td>
                                                                              <td>

                                                                                <input type="text" class="form-control" @if(!empty($credit->relationship))   value="{{$credit->relationship}}" @endif  name="relationship" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                              
                                                            </tr>
                                                        </table>


                                                          <!-- Consent --><br>

                                                        <table border="1" >
                                                          <tr>
                                                                <td align="left" class="border" bgcolor="#0055a5">  <font color="white"><b>7.6) CONSENT FOR CROSS-SELLING, MARKETING, PROMOTIONS, ETC</b>/ <i>PERSETUJUAN UNTUK JUALAN SILANG, PEMASARAN, PROMOSI DAN LAIN-LAIN </i></font> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="border">
                                                                    <b><br> I/We expressly consent and authorize MBSB to process any information that I/we have provided to MBSB for the purposes of cross-selling, marketing and promotions including disclosure to its strategic partners or such persons or third parties as MBSB deem fit.</b><br> 
                                                                    <i>Saya/Kami mengesahkan bahawa saya/kami memberi kebenaran dan kuasa kepada MBSB yang tidak boleh dibatal tanpa kebenaran untuk mendedahkan sebarang maklumat yang telah saya/kami kemukakan kepada MBSB bagi tujuan jualan silang, pemasaran dan promosi termasuk pendedahan kepada rakan strategik atau mana-mana individu atau pihak ketiga yang difikirkan wajar oleh MBSB.</i><br><br>
                                                                     <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" name="consent_for" id="consent_for_yes" value="1" @if($term->consent_for>0) checked @endif >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" name="consent_for" id="consent_for_no" value="0" @if($term->consent_for==0) checked @endif >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                              
                                                            </tr>
                                                        </table>

                                                       

                                                        <!-- High Networth --><br>

                                                        <table border="1"> 
                                                            <tr>
                                                                 <td align="left" class="border" bgcolor="#0055a5">  <font color="white">  <b>7.7) HIGH NETWORTH INDIVIDUAL CUSTOMER ("HNWI")</b>/ <i>INDIVIDU YANG BERPENDAPATAN TINGGI ("HNWI") </i></font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border">
                                                                    <br> <b> HNWI means an individual whose total net personal assets, or total net joint assets with his or her spouse, exceeds RM3 million or its equivalent in foreign currencies, excluding the value of the individual's primary residence. Calculation of HNWI is total asset less total liabilities.</b><br> 
                                                                    <i>HNWI bermaksud seseorang individu di mana jumlah bersih aset-aset peribadi, atau jumlah bersih aset-aset bersama dengan pasangan, melebihi RM3 juta atau yang setaraf dengannya dalam mata awang asing, tidak termasuk nilai kediaman utama individu tersebut. Pengiraan HNWI adalah berdasarkan jumlah keseluruhan aset tolak jumlah keseluruhan liabiliti.</i><br>

                                                                    <br> <b>Does your total net personal assets or total net joint assets with your spouse exceeds RM3 million?</b></br>
                                                                    <i>Adakah jumlah bersih aset-aset peribadi anda atau jumlah bersih aset bersama dengan pasangan anda melebihi RM3 juta?. </i><br><br>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" name="high_networth" id="high_networth_yes" value="1" @if($term->high_networth>0) checked @endif >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" name="high_networth" id="high_networth_no" value="0" @if($term->high_networth==0) checked @endif >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                           
                                                          </table>

                                                          <!-- POLITICALY EXPOSED PERSON --><br>

                                                        <table border="1"> 
                                                            <tr>
                                                                 <td align="left" class="border" bgcolor="#0055a5">  <font color="white">  <b>7.8) POLITICALLY EXPOSED PERSON ("PEP")</b>/ <i> INDIVIDU BERKAITAN POLITIK ("PEP") </i></font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border">
                                                                    <br> <b> PEP - Individuals who are or who have been entrusted with prominent public functions domestically or internationally. Family members of PEPs are defined as those who may be expected to influence or be influenced by that PEP, as well as dependents of the PEP. This includes the PEP’s:</b><br> 
                                                                    <i>PEP – seseorang Individu yang diamanahkan dengan “Fungsi Awam Yang Penting” samaada domestik atau antarabangsa. Ahli keluarga PEP adalah ditakrifkan sebagai mereka yang dijangka boleh mempengaruhi atau dipengaruhi oleh PEP tersebut dan juga tanggungan PEP. Ianya termasuk:</i><br>
                                                                    <ol type="i">
                                                                        <li>
                                                                            <b>Spouse and dependents of the spouse;</b><br><i>Pasangan suami atau isteri berserta tanggungannya;</i>
                                                                        </li>
                                                                        <li>
                                                                            <b>Child (including step children and adopted children) and spouse of the child;</b><br>
                                                                            <i>Anak (termasuk anak tiri atau anak angkat yang sah) berserta pasangan suami atau isteri kepada anak-anak tersebut;</i>
                                                                        </li>
                                                                        <li>
                                                                            <b>Parent; and</b><br>
                                                                            <i>Ibu bapa; dan</i>
                                                                        </li>
                                                                        <li>
                                                                            <b>Brother or sister and their spouses.</b><br>
                                                                            <i>Adik beradik berserta pasangan suami atau isteri mereka. </i>
                                                                        </li>
                                                                    </ol>

                                                                    <b>Definition of Related Closed Associates of PEPs:/</b> Definisi Kenalan-Kenalan yang Berkait Rapat dengan PEPs:
                                                                    <ul>
                                                                        <li>
                                                                            <b>Related close associate to PEP is defined as individual who is closely connected to a PEP, either socially or professionally.</b><br>
                                                                            <i>Kenalan-kenalan yang berkait rapat dengan PEP ditakrifkan sebagai individu yang saling berhubung dan berkait rapat dengan PEP, samada secara sosial atau profesional. </i>
                                                                        </li>
                                                                    </ul><br>
                                                                     <b>For Individual</b> / <i>Untuk Individu:</i>
                                                                     <ol type="1">
                                                                        <li>
                                                                            <b>Are you a PEP or a Family Member(s) of PEP or a Related Close Associate(s) of PEP?</b>
                                                                            <br>
                                                                            <i>Adakah anda seorang PEP atau ahli keluarga PEP atau kenalan-kenalan berkait rapat dengan PEP?</i>
                                                                             <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="radio" name="politically" value="1" id="politically_yes" @if($term->politically>0) checked @endif >
                                                                                    </td>
                                                                                    <td><b>Yes</b>/Ya</td>
                                                                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                                                                    <td>
                                                                                        <input type="radio" name="politically" id="politically_no" value="0" @if($term->politically==0) checked @endif >
                                                                                    </td>
                                                                                    <td><b>No</b>/Tidak</td>
                                                                                </tr>
                                                                            </table><br>
                                                                            <b>If Yes, please complete the information below:/</b><i> Jika Ya, sila lengkapkan maklumat di bawah:</i><br>
                                                                            <table border="1" width="100%">
                                                                                <tr align="center">
                                                                                    <td width="5%">No.</td>
                                                                                    <td><b>Name</b><br><i>Nama</i></td>
                                                                                    <td><b>Relationship with customer</b><br>
                                                                                    <i>Hubungan dengan Pelanggan</i></td>
                                                                                    <td><b>Status**</b><br><i>Status**</i></td>
                                                                                    <td><b>Prominent Public Position</b><br><i>Kedudukan Awam yang Penting</i></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="no1" value="1" readonly>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="name1" @if(!empty($pep->name1))  value="{{$pep->name1}}" @endif>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="relationship1" @if(!empty($pep->relationship1))  value="{{$pep->relationship1}}" @endif>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="status1" @if(!empty($pep->status1))  value="{{$pep->status1}}" @endif>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="prominent1" @if(!empty($pep->prominent1))  value="{{$pep->prominent1}}" @endif>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="no2" value="2" readonly>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="name2" @if(!empty($pep->name2))  value="{{$pep->name2}}" @endif>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="relationship2" @if(!empty($pep->relationship2))  value="{{$pep->relationship2}}" @endif>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="status2" @if(!empty($pep->status2))  value="{{$pep->status2}}" @endif>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="prominent2" @if(!empty($pep->prominent2))  value="{{$pep->prominent2}}" @endif>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="no3" value="3" readonly>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="name3" @if(!empty($pep->name3))  value="{{$pep->name3}}" @endif>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="relationship3" @if(!empty($pep->relationship3))  value="{{$pep->relationship3}}" @endif>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="status3" @if(!empty($pep->status3))  value="{{$pep->status3}}" @endif>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" class="form-control" name="prominent3" @if(!empty($pep->prominent3))  value="{{$pep->prominent3}}" @endif>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <b>** Currently holding/ is actively seeking/ is being considered/ Previously holding</b><br><i>** Memegang jawatan buat masa ini/ masih aktif mencari/ dalam pertimbangan/ memegang jawatan sebelum ini</i>

                                                                        </li>
                                                                     </ol>
                                                                </td>
                                                            </tr>

                                                           
                                                          </table>


                                                          <!-- FOREIGN EXCHANGE --><br>

                                                        <table border="1"> 
                                                            <tr>
                                                                 <td align="left"  class="border" bgcolor="#0055a5">  <font color="white">  <b>7.9) DECLARATION RELATING TO FOREIGN EXCHANGE ADMINISTRATION RULES ISSUED BY BANK NEGARA MALAYSIA/</b>/ <i>/ PENGISYTIHARAN BERKAITAN DENGAN PERATURAN PENTADBIRAN PERTUKARAN ASING YANG DIKELUARKAN OLEH BANK NEGARA MALAYSIA</i></font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border">
                                                                    <br> <b>I hereby /</b>
                                                                    <i>Saya dengan ini:</i><br><br>
                                                                    <li type="a" style="font-weight: bold; margin-left: 30px;"><strong>Declare and conﬁrm that all personal data and information provided in and in connection with this Personal Financing-i Application and any other banking documents I/we may sign or enter into from time to time is true and correct in full compliance with the Islamic Financial Services Act 2013 and Central Bank of Malaysia Act 2009. I/we shall be fully responsible for any inaccurate, untrue or incomplete information provided in this Form. I/we also authorise the Bank to make this information available to Bank Negara Malaysia in compliance with Islamic Financial Services Act 2013 and Central Bank of Malaysia Act 2009.</strong></li>
                                                                    <li type="a" style="font-weight: bold; margin-left: 30px;"><strong>Agree and undertake that we shall at all times comply and be responsible for the compliance with BANK NEGARA MALAYSIA&rsquo;s (BNM) previling ForeignExchange Administration Notices &amp; Rules as made available at BNM's website : www.bnm.gov.my<br ></strong><em style="font-weight: normal;">Bersetuju dan mengaku bahawa kami akan sentiasa mematuhi dan bertanggungjawab terhadap pematuhan bagi Notis &amp; Peraturan Pentadbiran Pertukaran Asing yang dibuat oleh BANK NEGARA MALAYSIA (BNM) seperti yang terdapat di laman web BNM: <a href="http://www.bnm.gov.my/">www.bnm.gov.my</a></em></li>
                                                                    <br>
                                                    <table>
                                                         <tr>
								                            <td align="justify">
								                                <input type="checkbox" value="1" name="foreigner1" @if($foreigner->foreigner1>0) checked @endif >  <b>I have been granted permanent residence status in the country of</b><i> / Saya telah diberikan status kediaman tetap di negara</i>
								                            </td>
								                        </tr>
								                        <tr>
								                            <td>
								                            	<select name="for_country" id="for_country" class="select2" required="required">
                                                            <option disabled="" selected="">SELECT</option> 
                                                            @foreach ($country2 as $country_nat)
                                                                @if($foreigner->country !=NULL)
                                                                    <?php 
                                                                        if($foreigner->country==$country_nat->country_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option {{$selected}} value="{{$country_nat->country_code}}">{{$country_nat->country_desc}}</option>
                                                                @else
                                                                    <option value="{{$country_nat->country_code}}">{{$country_nat->country_desc}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
								                            </td>
								                        </tr>
                                                        <tr>
								                            <td align="justify">
								                                <input type="checkbox" value="1" name="foreigner2" @if($foreigner->foreigner2>0) checked @endif >  <b>I reside in Malaysia more than 182 days in a calendar year</b><i> Saya tinggal di Malaysia lebih daripada 182 hari dalam satu tahun kalendar</i>
								                            </td>
								                        </tr>
								                        <tr>
								                            <td align="justify">
								                                <input type="checkbox" value="1" name="foreigner3" @if($foreigner->foreigner3>0) checked @endif > <b>I have not been granted any permanent residence status in a territory outside Malaysia./ </b><i>Saya tidak pernah diberikan mana-mana status kediaman tetap di wilayah luar Malaysia</i>
								                            </td>
								                        </tr>
								                        <tr>
								                            <td align="justify">
								                                <input type="checkbox" value="1" name="foreigner4" @if($foreigner->foreigner4>0) checked @endif > 
								                                <b>I reside outside Malaysia more than 182 days in a calendar year/</b><i> Saya tinggal di luar Malaysia lebih daripada 182 hari dalam satu tahun kalendar</i>
								                            </td>
								                        </tr>
								                    </table>
                                                                </td>
                                                            </tr>
                                                        </table><br>

                                                            <table border="1" >
                                                          <tr>
                                                                <td align="left" class="border" bgcolor="#0055a5">  <font color="white"><b>7.10) DECLARATION FOR SUBSCRIPTION OF WEALTH MANAGEMENT PRODUCT/</b>/ <i>/  PERAKUAN UNTUK LANGGANAN PRODUK PENGURUSAN KEKAYAAN</i></font> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="border">
                                                                    <b><br> Do you wish to subscribe to the following Wealth Management Product(s)&nbsp; /&nbsp; </b>
                                                                    <i>Adakah anda ingin melanggan Produk Pengurusan Kekayaan yang berikut:</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                     <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" name="sub_wealth" id="sub_wealth" value="1" @if($subs->sub_wealth>0) checked @endif >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" name="sub_wealth" id="sub_wealth" value="0" @if($subs->sub_wealth==0) checked @endif >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                             <tr>
                                                        
                                                          <td>
                                                            <ol type="1">
                                                            	<li>
                                                                    <b>Declaration if agree to subscribe to Group Personal Accident (GPA) Takaful Plan/ </b><br>
                                                                     <i>Pengakuan sekiranya bersetuju untuk mengambil pelan Takaful Kemalangan Diri Berkelompok (TKDB)</i></li>
                                                                      <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" name="gpa" id="gpa" value="1" @if($subs->gpa>0) checked @endif >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" name="gpa" id="gpa" value="0" @if($subs->gpa==0) checked @endif >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                </li>

                                                                <li> 
                                                                    <b>Declaration if agree to subscribe to AN-NUR Takaful Plan/</b>
                                                                    <br><i> Pengakuan sekiranya bersetuju untuk mengambil pelan Takaful AN-NUR</i>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" name="annur" id="annur" value="1" @if($subs->annur>0) checked @endif >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" name="annur" id="annur" value="0" @if($subs->annur==0) checked @endif >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                </li>

                                                                <li>
                                                                    <b>Declaration if agree to subscribe to HASANAH Takaful Plan/</b>
                                                                    <br> <i>Pengakuan sekiranya bersetuju untuk mengambil pelan Takaful HASANAH </i>s
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" name="hasanah" id="hasanah" value="1" @if($subs->hasanah>0) checked @endif >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" name="hasanah" id="hasanah" value="0" @if($subs->hasanah==0) checked @endif >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                </li>

                                                                <li>
                                                                    <b>Declaration if agree to subscribe to WASIAT Product/</b>
                                                                    <br> <i>Pengakuan sekiranya bersetuju untuk mengambil WASIAT</i>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" name="wasiat" id="wasiat" value="1" @if($subs->wasiat>0) checked @endif >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" name="wasiat" id="wasiat" value="0" @if($subs->wasiat==0) checked @endif >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                </li>

                                                            </ol>
                                                        
                                                         </td>
                                                        </tr>
                                                        </table><br>

                                                          

                                                           

                                                          <table border="1"> 
                                                            <tr>
                                                                 <td align="left"  class="border" bgcolor="#0055a5">  <font color="white">  <b>7.11) FOR GOODS AND SERVICES TAX ("GST") EFFECTIVE 1 APRIL 2015</b>/ <i>UNTUK CUKAI BARANGAN DAN PERKHIDMATAN (“CBP”) BERKUATKUASA 1 APRIL 2015 </i></font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border">
                                                                    <br> <b>I/We hereby agree that I/We shall be liable for all Goods and Services Tax (GST) payable in connection with this application or any account or any service in connection therein and MBSB shall be authorized to debit my/our account for the same.</b><br> 
                                                                    <i>Saya/Kami bersetuju bahawa saya/kami akan bertanggungjawab ke atas Cukai Barangan dan Perkhidmatan (“CBP”) yang berkaitan dengan permohonan ini atau mana-mana akaun atau apa-apa perkhidmatan yang berkaitan dan MBSB dibenarkan untuk mendebit akaun saya/kami bagi tujuan yang sama.</i><br><br>
                                                                  

                                                                   
                                                                </td>
                                                            </tr>
                                                           
                                                          </table>

                                                          <!-- FOR GOODS AND SERVICES--><br>

                                                        <table border="1"> 
                                                            <tr>
                                                                 <td class="border" align="left" bgcolor="#0055a5">  <font color="white">  <b>7.12) PRODUCT DISCLOSURE SHEET</b>/ <i>LEMBARAN PENJELASAN PRODUK</i></font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border">
                                                                    <br> <b>I/We hereby declare that I/ we have been briefed on the information contained in the Product Disclosure Sheet that has been given to me/us for the product applied herein</b><br> 
                                                                    <i>Saya/Kami mengesahkan bahawa saya/kami telah diberitahu mengenai maklumat yang terkandung di dalam Lembaran Penjelasan Produk yang telah diberikan kepada saya/kami berkaitan dengan produk yang dipohon di sini.</i><br><br>
                                                                    <table width="90%" border="0">
                                                                         <tr>
                                                                             <td valign="top" width="40%">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Name</b><br>
                                                                                            <i>Nama</i>
                                                                                        </td>
                                                                                         <td>
                                                                                            <input type="text" class="form-control" value="{{$data->name}}" name="" disabled/>
                                                                                        </td>

                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td>
                                                                                             <b>MyKad No.</b><br>
                                                                                            <i>No. MyKad</i>
                                                                                         </td>
                                                                                         <td>
                                                                                            <input type="text" class="form-control" value="{{$data->new_ic}}" name="" disabled/>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td>
                                                                                             <b>Date.</b><br>
                                                                                            <i>Tarikh</i>
                                                                                         </td>
                                                                                          <td>

                                                                                            <input type="text" class="form-control" value="{{$today}}" name="" disabled/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                             <td valign="top">
                                                                                    <table border="1" width="500" height="100">
                                                                                        <tr>
                                                                                            <td align="center"><h2>
                                                                                                 <input type="checkbox" value="1" name="product_disclosure" @if($term->product_disclosure>0) checked @endif > <b>I Agree</b> / Saya Bersetuju</h2>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                   

                                                                   
                                                                </td>
                                                            </tr>

                                                           
                                                          </table>


                                                           


                                                          </div>
                                                            <div class="col-lg-1"></div>

                                                          </div>

                                                          <div class="row">
                                                          <div class="col-lg-1">
                                                          </div>
                                                            <div align="justify" class="col-lg-10">

                                                   
                                                             <br> <br> <br> 
                                                   
                                         
                                                         
                                                    

                                                           

                                                

                                                            <p align="center"><a id="agree" class="btn btn-primary btn-lg" >
                                                            <i class="fa fa-send"></i> Hantar / <i> Submit </i>
                                                           </a> </p>

                                                          </div>
                                                          <div class="col-lg-1">
                                                          </div>
                                                           </div>



                                                        
                                                         
                                                        

                                                          
                                                          
                                                        </div>


                                                      

                                                     
                
                                                       <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <ul class="pager wizard no-margin">
                                                                        <!--<li class="previous first ">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                                                        </li>-->
                                                                        <li class="next">
                                                                            <a href="javascript:void(0);" class="btn btn-lg txt-color-blue"><span class="hidden-xs">  Seterusnya / </span><i> Next </i> </a>
                                                                        </li>
                                                                        <li class="previous ">
                                                                            <a href="javascript:void(0);" class="btn btn-lg btn-default"><span class="hidden-xs">  Sebelum / </span><i> Previous </i> </a>
                                                                        </li>
                                                                        <!--<li class="next last">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                                                        </li>-->
                                                                        
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                
                                    </div>
                                    <!-- end widget content -->
                
                                </div>
                                <!-- end widget div -->
                
                            </div>
                            <!-- end widget -->
                
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET ENffD -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
            </div>
            <br>
                <div class="page-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                   
                </div>

                <div class="col-xs-6 col-sm-6 text-right hidden-xs">
                    <div class="txt-color-black inline-block">
                        <span class="txt-color-black">NetXpert Data Solution © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
        
        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        
        $(document).ready(function() {
            
            pageSetUp();
            
            
    
            //Bootstrap Wizard Validations

              var $validator = $("#wizard-1").validate({
                
                rules: {
                  email: {
                    required: false,
                    email: "Your email address must be in the format of name@domain.com"
                  },
                  fname: {
                    required: true
                  },
                  lname: {
                    required: true
                  },
                  country: {
                    required: true
                  },
                  city: {
                    required: false
                  },
                  postcode: {
                    required: false,
                    minlength: 4
                  },
                  wphone: {
                    required: true,
                    minlength: 10
                  },
                  hphone: {
                    required: true,
                    minlength: 10
                  }
                },
                
                messages: {
                  fname: "Please specify your First name",
                  lname: "Please specify your Last name",
                  email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                  }
                },
                
                highlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                  if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                  } else {
                    error.insertAfter(element);
                  }
                }
              });
              
              $('#bootstrap-wizard-1').bootstrapWizard({
                'tabClass': 'form-wizard',
                'onNext': function (tab, navigation, index) {
                  var $valid = $("#wizard-1").valid();
                  if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                  } else {

                   

                      $.ajax({

                            type: "PUT",
                           
                            url: '{{ url('/form/') }}'+'/'+index,
                            data: $('#tab'+index+' :input').serialize(),

                            cache: false,
                            beforeSend: function () {
                               
                            },

                            success: function () {
                             
                            
                             

                            },
                            error: function () {
                               
                                 

                            }
                        });
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                      'complete');
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                    .html('<i class="fa fa-check"></i>');
                  }
                }
              });
              
        
            // fuelux wizard
              var wizard = $('.wizard').wizard();
              
              wizard.on('finished', function (e, data) {
                //$("#fuelux-wizard").submit();
                //console.log("submitted!");
                $.smallBox({
                  title: "Congratulations! Your form was submitted",
                  content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                  color: "#5F895F",
                  iconSmall: "fa fa-check bounce animated",
                  timeout: 4000
                });
                
              });

        
        })

        </script>

        <!-- Your GOOGLE ANALYTICS CODE Below -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
            
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();


                $('.startdate').datepicker({
                dateFormat : 'dd/mm/yy',
                changeYear: true,
                changeMonth: true,
                // yearRange: '1950:2017',
                 yearRange: "-70:+0",
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });

                 $('.date').datepicker({
                dateFormat : 'dd/mm/yy',
                changeYear: true,
                changeMonth: true,
                // yearRange: '1950:2017',
                   yearRange: "-70:+0",
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });


        </script>


<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

      <script type="text/javascript">
       
$( "#postcode2" ).change(function() {
    var postcode = $('#postcode2').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        //$("#city").val(data[k].post_office );
                        $("#state2").val(data[k].state.state_name );
                    });

                }
            });

   
});

</script>

  <script type="text/javascript">

$( "#postcode" ).keyup(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

    
                        $("#state").val(data[k].state.state_name );
                         $("#state_code").val(data[k].state.state_code );
                  
                    });

                }
            });

   
});
</script>

      <script type="text/javascript">
       
$( "#postcode3" ).change(function() {
    var postcode = $('#postcode3').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        //$("#city").val(data[k].post_office );
                        $("#state3").val(data[k].state.state_name );
                    });

                }
            });

   
});

</script>

<?php for ($x = 1; $x <= 12; $x++) {  ?>

<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'form/upload/{{$x}}';
    $('#fileupload{{$x}}').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
             var text = $('#documentx{{$x}}').val();
            $("#document{{$x}}").html("<a target='_blank' href='"+"{{url('/uploads/file/')}}/{{$user->name}}/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
             $("#document{{$x}}a").hide();
            

             document.getElementById("fileupload{{$x}}").required = false;
             
            
            

        }
       
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        
});
</script>

<?php } ?>


<script type="text/javascript">

$( "#agree" ).click(function() {
     var icnumber = $('#new_ic').val();



    var purchase_application = $('input[name="purchase_application"]:checked').val();
    var appointment_mbsb = $('input[name="appointment_mbsb"]:checked').val();
    var credit_yes= $('#credit_yes:checked').val();
    var credit_no = $('#credit_no:checked').val();
    var consent_for_yes = $('#consent_for_yes:checked').val();
    var consent_for_no = $('#consent_for_no:checked').val();
    var high_networth_yes = $('#high_networth_yes:checked').val();
    var high_networth_no = $('#high_networth_no:checked').val();
    var politically_yes = $('#politically_yes:checked').val();
    var politically_no = $('#politically_no:checked').val();

    
    
    var product_disclosure = $('input[name="product_disclosure"]:checked').val();   // 7.10

    

if(purchase_application>0) {

    if(appointment_mbsb>0) {

        if(credit_yes==1 || credit_no==0) {

                if(consent_for_yes==1 || consent_for_no==0) {

                    if(high_networth_yes==1 || high_networth_no==0) {

                        if(politically_yes==1 || politically_no==0) {

                                 
                                        if(product_disclosure>0) {
                                            var r = prompt('Please enter your ic number');
                                            if (r == icnumber) {
                                                          $.ajax({
                                                            type: "POST",                       
                                                                url: '{{ url('/form/term/') }}',
                                                                data: $('#tab5 :input').serialize(),
                                                                cache: false,
                                                                beforeSend: function () {                             
                                                                },
                                                                 success: function () {   
                                                                    alert("Success!");
                                                                    location.reload();                             
                                                                },
                                                                error: function () {                                                               
                                                                }
                                                            });                      
                                              }
                                              else {
                                                    bootbox.alert("Incorrect, Please Retype Your IC Number !");
                                              }
                                        }
                                        else {
                                             bootbox.alert('PLEASE SELECT AN OPTION ON THE PRODUCT DISCLOSURE SHEET AGREEMENT (7.12) ');   
                                        }
                                
                               
                            
                        }
                        else {
                             bootbox.alert('PLEASE SELECT AN OPTION ON THE POLITICALLY EXPOSED PERSON ("PEP") (7.8) '); 
                        }
                       
                    }
                    else {
                        bootbox.alert('PLEASE SELECT AN OPTION ON THE HIGH NETWORTH INDIVIDUAL CUSTOMER ("HNWI") (7.7) '); 
                    }
                        
                }
                else {
                     bootbox.alert('PLEASE SELECT AN OPTION ON THE CONSENT FOR CROSS-SELLING, MARKETING, PROMOTIONS, ETC (7.6) ');
                }
            
        }
        else {
            bootbox.alert('PLEASE SELECT AN OPTION ON THE CREDIT TRANSACTIONS AND EXPOSURES WITH CONNECTED PARTIES  (7.5) ');
        }
    }
    else {
        bootbox.alert('YOU MUST AGREE TO THE APPOINTMENT OF MBSB AS A SALES AGENT AGREEMENT (7.2) ');
    }
}
else { //else purchase application
    bootbox.alert('YOU MUST AGREE TO THE PURCHASE APPLICATION AND PROMISE TO BUY AGREEMENT (7.1)');
}

   
});
</script>


 <script type="text/javascript">

$( ".income" ).change(function() {


if(  $('#income1').val() ) {
    var income1 = $('#income1').val();
}
else {
    var income1 = 0;
}
if(  $('#income2').val() ) {
    var income2 = $('#income2').val();
}
else {
    var income2 = 0;
}

if(  $('#income3').val() ) {
    var income3 = $('#income3').val();
}
else {
    var income3 = 0;
}

if(  $('#outcome2').val() ) {
    var outcome2 = $('#outcome2').val();
}
else {
    var outcome2 = 0;
}

if(  $('#outcome3').val() ) {
    var outcome3 = $('#outcome3').val();
}
else {
    var outcome3 = 0;
}
if(  $('#outcome4').val() ) {
    var outcome4 = $('#outcome4').val();
}
else {
    var outcome4 = 0;
}



var total = parseFloat(income1) + parseFloat(income2)  + parseFloat(income3) ;
    $("#income4").val(parseFloat(total).toFixed(2));

var total2 = parseFloat(outcome2)  + parseFloat(outcome3) + parseFloat(outcome4)   ;
    $("#outcome5").val( parseFloat(total2).toFixed(2));

   ;
    $("#totalincome").val( parseFloat(total - total2).toFixed(2));

   
});
</script>
<script type="text/javascript">
    
$(document).ready(function() {

var found = [];
    $("select[name='title'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='gender'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='ownership'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });


var found = [];
    $("select[name='country'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='race'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='bumiputera'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='religion'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='marital'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='education'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='emptype'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='empstatus'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='spouse_emptype'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

});


</script>




<script>
function isNumberKey(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          if (key.length == 0) return;
          var regex = /^[0-9.,\b]+$/;
          if (!regex.test(key)) {
              theEvent.returnValue = false;
              if (theEvent.preventDefault) theEvent.preventDefault();
          }
}
</script>

<script>
function toFloat(z) {
    var x = document.getElementById(z);
    x.value = parseFloat(x.value).toFixed(2);
}
</script>


<script>
function alertWithoutNotice(message){
    setTimeout(function(){
        alert(message);
    }, 1000);
}
</script>

@if($data->marital != 'Married / Berkahwin')
  <script type="text/javascript">

  $(document).ready(function() {
    

        $("#spouse-group :input").attr("disabled", true);
            $("#statuskawin").show();
            $("#spouse_emptype").hide();
             $("#spouse_emptype_others").hide();
            $("#spouse_emptype_kosong").show();
            
            
   
});
  </script>
  @else 
  <script type="text/javascript">

  $(document).ready(function() {
    

     $("#spouse-group :input").attr("disabled", false);
            $("#statuskawin").hide();
            $("#spouse_emptype").show();
            $("#spouse_emptype_kosong").hide();

            @if($spouse->emptype != 'Others (Specify)/ Lain-lain (Nyatakan)')

                $("#spouse_emptype_others").hide();
            @else 
        
                $("#spouse_emptype_others").show();       
            @endif 
   
});
  </script>

@endif 

  <script type="text/javascript">

$( "#marital" ).change(function() {
    var status = $('#marital').val();
    var spouse_emptype = $('#spouse_emptype').val();
     
    if(status  != 'Married / Berkahwin' ) {
       
        
        $("#spouse-group :input").attr("disabled", true);
            $("#statuskawin").show();
              $("#spouse_emptype_kosong").show();
            $("#spouse_emptype_others").hide();
             $("#spouse_emptype").hide();
    }
    else {
   
      
        $("#spouse-group :input").attr("disabled", false);
            $("#statuskawin").hide();
            $("#spouse_emptype").show();
            $("#spouse_emptype_kosong").hide();

            if(spouse_emptype  != 'Others (Specify)/ Lain-lain (Nyatakan)') {
       
                $("#spouse_emptype_others").hide();
            }
            else {

                $("#spouse_emptype_others").show();
            }

    }

   
});
</script>

<script type="text/javascript">
function minmax(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 0; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}

function minmax_tempoh(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 24; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}

</script>


@if($data->title != 'OTH')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#salutation_others").hide();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#salutation_others").show();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#salutation" ).change(function() {
    var salutation = $('#salutation').val();
     
    if(salutation  != 'OTH' ) {
       
            $("#salutation_others").hide();
    }
    else {
   

            $("#salutation_others").show();
    }

   
});
</script>

@if($empinfo->emptype != 'Others (Specify) / Lain-lain (Nyatakan)')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#emptype_others").hide();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#emptype_others").show();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#emptype" ).change(function() {
    var emptype = $('#emptype').val();
     
    if(emptype  != 'Others (Specify) / Lain-lain (Nyatakan)' ) {
       
            $("#emptype_others").hide();
    }
    else {
   

            $("#emptype_others").show();
    }

   
});
</script>

@if($spouse->emptype != 'Others (Specify)/ Lain-lain (Nyatakan)')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#spouse_emptype_others").hide();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#spouse_emptype_others").show();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#spouse_emptype" ).change(function() {
    var emptype = $('#spouse_emptype').val();
     
    if(emptype  != 'Others (Specify)/ Lain-lain (Nyatakan)') {
       
            $("#spouse_emptype_others").hide();
    }
    else {
   

            $("#spouse_emptype_others").show();
    }

   
});
</script>


@if($financial->product_bundling != '1')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#product_bundling_specify").hide();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#product_bundling_specify").show();
       
      });
  </script>

@endif 

<script type="text/javascript">

$( "#pb_yes" ).change(function() {
    var pb_yes = $('#pb_yes').val();
      $("#product_bundling_specify").show();
});

$( "#pb_no" ).change(function() {
    var pb_no = $('#pb_no').val();  

     $("#product_bundling_specify").hide();

   
});
</script>

@if($financial->cross_selling != '1')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#cross_selling_specify").hide();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#cross_selling_specify").show();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#cs_yes" ).change(function() {
    var cs_yes = $('#cs_yes').val();
      $("#cross_selling_specify").show();
});


$( "#cs_no" ).change(function() {
    var cs_no = $('#cs_no').val();  

     $("#cross_selling_specify").hide();

   
});

</script>



@if($data->country == 'Non-Citizen (Specify)/ Bukan Warganegara (Nyatakan)')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#country_others").show();
                $("#country_origin").hide();
       
      });
  </script>
  @elseif($data->country == 'Permanent Resident / Penduduk Tetap')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#country_origin").show();
                $("#country_others").hide();
       
      });
  </script>
  @else 

      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#country_origin").hide();
                $("#country_others").hide();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#country" ).change(function() {
    var country = $('#country').val();
     
    if(country  == 'Non-Citizen (Specify)/ Bukan Warganegara (Nyatakan)') {
       
            $("#country_others").show();
            $("#country_origin").hide();
    }
    else if(country  == 'Permanent Resident / Penduduk Tetap') {
       
            $("#country_origin").show();
            $("#country_others").hide();
       
    }
    else {
   

            $("#country_origin").hide();
            $("#country_others").hide();
    }

   
});
</script>
@if($basic->country_dob != 'MY')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#state_dob").hide();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#state_dob").show();
       
      });
  </script>

@endif 
@if(($data->race == 'OTH') || ($data->race == 'OTL'))
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#race_others").show();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#race_others").hide();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#race" ).change(function() {
    var race = $('#race').val();
     
    if((race  == 'OTH') ||(race  == 'OTL')) {
       
            $("#race_others").show();
    }
    else {
   

            $("#race_others").hide();
    }

   
});
</script>

@if($data->religion != 'O')
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#religion_others").hide();
       
      });
  </script>
  @else 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#religion_others").show();
       
      });
  </script>

@endif 

  <script type="text/javascript">

$( "#religion" ).change(function() {
    var religion = $('#religion').val();
     
    if(religion  != 'O') {
       
            $("#religion_others").hide();
    }
    else {
   

            $("#religion_others").show();
    }

   
});
</script>



<script type="text/javascript">
    $(document).ready(function(){
    $("#fullname").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });
    $("#name1").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });
    $("#name2").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });
    $("#spousename1").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });
});
</script>
@if($data->nationality != 'NC')
  <script type="text/javascript">
      $(document).ready(function() {
        $("#country_origin").hide();
      });
  </script>
@else 
<script type="text/javascript">
 $(document).ready(function() {
  $("#country_origin").show();
});
  </script>
@endif 

<script type="text/javascript">
$( "#nationality" ).change(function() {
    var nationality = $('#nationality').val();
    if(nationality  != 'NC' ) {
            $("#country_origin").hide();
    }
    else {
        $("#country_origin").show();
    }
});
</script>

@if($empinfo->mbsb_staff != 'Y')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#staff_no").hide();
        });
   </script>
@else 
    <script type="text/javascript">
        $(document).ready(function() {
            $("#staff_no").show();
        });
    </script>
@endif 

<script type="text/javascript">
$( "#mbsb_staff" ).change(function() {
    var mbsb_staff = $('#mbsb_staff').val();
    if(mbsb_staff  != 'Y') {
        $("#staff_no").hide();
    }
    else {
        $("#staff_no").show();
    }
});
</script>