<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Personal Financing-i | MBSB Bank ";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>

        <div id="main" role="main">
<br><br><br><br>
      <!-- MAIN CONTENT -->
      <div id="content" class="container">
                     @if (Session::has('message'))
    

    <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
     <i class="fa-fw fa-lg fa fa-exclamation"></i>
      <strong>{{ Session::get('message') }}</strong> 
    </div>
            
            @endif             
            @if (count($errors) > 0)
    <div class="alert alert-danger">
          <button class="close" data-dismiss="alert">
         ×
    </button>  
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<article class="col-sm-12 col-md-12 col-lg-6">
            <!-- Widget ID (each widget will need unique ID)-->
      <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
        <!-- widget options:
          usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
          
          data-widget-colorbutton="false" 
          data-widget-editbutton="false"
          data-widget-togglebutton="false"
          data-widget-deletebutton="false"
          data-widget-fullscreenbutton="false"
          data-widget-custombutton="false"
          data-widget-collapsed="true" 
          data-widget-sortable="false"
          
        -->
        <header>
          <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
          <h2>Contact form </h2>        
          
        </header>

        <!-- widget div-->
        <div>
          
          <!-- widget edit box -->
          <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->
            
          </div>

          <!-- end widget edit box -->
          
          <!-- widget content -->
          
          <div class="widget-body no-padding">
            
                 {!! Form::open(['url' => 'contact/contact_save','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
              
              
              <fieldset>          
                <div class="row">
                  <section class="col col-6">
                    <label class="label">Name</label>
                    <label class="input">
                      <i class="icon-append fa fa-user"></i>
                      <input type="text" name="named" id="named" required>
                    </label>
                  </section>
                  <section class="col col-6">
                    <label class="label">E-mail</label>
                    <label class="input">
                      <i class="icon-append fa fa-envelope-o"></i>
                      <input type="email" name="emaild" id="emaild" required>
                    </label>
                  </section>
                </div>
                
                <section>
                  <label class="label">Subject</label>
                  <label class="input">
                    <i class="icon-append fa fa-tag"></i>
                    <input type="text" name="subject" required>
                  </label>
                </section>
                
                <section>
                  <label class="label">Message</label>
                  <label class="textarea">
                    <i class="icon-append fa fa-comment"></i>
                    <textarea rows="4" name="pesan" required></textarea>
                  </label>
                </section>
                
              
              </fieldset>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <footer>
                <button type="submit" class="btn btn-primary">Send</button>
              </footer>
              
            
              {!! Form::close() !!}         
            
          </div>
          <!-- end widget content -->
          
        </div>
        <!-- end widget div -->
        
      </div>
      <!-- end widget -->               
  <div class="">
            @include('auth.login_modal') 
        </div>

    </article>
    <!-- END COL -->
    <article class="col-sm-12 col-md-12 col-lg-6">
     <h3><b><font color='#a90329!important'>MORE INFORMATION</font></b></h3>
<p>HEAD OFFICE</p>

<p>Telephone:+(603) 2096 3000 Address:Ground Floor, Wisma Mbsb, 48, Jalan Dungun, Bukit Damansara, 50490 Kuala Lumpur, Federal Territory of Kuala Lumpur
     </p>
    </article>
    
  
          
        </div>
        </div>
      
       
  

    
        

<!-- ==========================CONTENT ENDS HERE ========================== -->

 <div class="page-footer">
        <div class="row">
            <div class="col-xs-4 col-sm-4 text-left ">
                <div class="txt-color-black inline-block">
                    <span id="logo"> <a href="<?php print url('/') ?>"> <img src="<?php echo ASSETS_URL; ?>/img/islam.png" alt="SmartAdmin"  style="margin-top: -17px !important; width: 150px"> </a></span>
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 text-right ">
                <div class="txt-color-black inline-block">
                    <span class="txt-color-black">Copyright © <?php echo date('Y'); ?> MBSB Bank Berhad</span><br>
                    <span class="txt-color-black">Registration No: 200501033981 (716122-P). All Rights Reserved.</span>
                </div>
            </div>
        </div>
    </div> 
    <?
include("asset/inc/scripts_nofooter.php");
?>
<!--Add the following script at the bottom of the web page (before </body></html>)
<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=21343764"></script>--><script type="text/javascript">
        function test() {
            var form = document.getElementById('uploadform');
            form.submit();
        };
    </script>

