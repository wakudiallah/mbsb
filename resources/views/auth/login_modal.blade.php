
<style type="text/css">
    
   

.center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 27%;
  margin-bottom: 20px;
}

</style>


<!-- Modal HTML -->




<div id="myModal" class="modal fade  bd-example-modal-lg" tabindex="-100" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="background-color: #ffffff !important">
      
      <div class="modal-body">

        <div class="row">
        <div class="col-md-12">
          <img src="https://mbsb.insko.my/public/asset/img/logo.jpg" class="img img-responsive center" width="20%" height="10%" style="text-align: center">
        </div>
        <div class="col-md-6">
          
        </div>
      </div>

      <form id="login-form" class="form" action="{{ url('auth/login') }}" method="post">

            <input type="hidden" name="_token" value="{{csrf_token() }}">

          <span id="latitude"></span>
              <span id="longitude"></span>
              <span id="location"></span>

          <div class="form-group">
              <label for="username" class="text-info">Email:</label><br>
              <input type="text" name="email" id="username" class="form-control">

          </div>
          <div class="form-group">
              <label for="password" class="text-info">Password:</label><br>
              <input type="password" name="password" id="password" class="form-control">
          </div>
          <div class="form-group">
            <div class="row">

              <div class="col-md-6">
                 <label for="remember-me" class="text-info"><span>Remember me</span> <span><input id="remember-me" name="remember-me" type="checkbox"></span></label><br>
              </div>

              <div class="col-md-6" style="float: right">
                <a href="{{url('/')}}/password/email" class="text-info" style="float: right !important">Forgot Password</a>
              </div>
             

              </div>
             
          </div>

          <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-2 col-lg-2">
              <img src="{{asset('img/islam.png')}}" class="img img-responsive" width="60%" height="20%" style="margin-bottom: 20px">
            </div>
            <div class="col-md-10" align="center">
              <div style="margin-right: 43px !important">
              
              <input type="submit" name="submit" class="btn btn-primary btn-md" value="Submit">

        </form>



          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
            </div>
          </div>
          <div id="register-link" class="text-right">
              
          </div>
        </div>
    </div>
  </div>
</div>                         