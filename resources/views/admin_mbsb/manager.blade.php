<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Manager List";


$page_css[] = "your_style.css";
include("asset/inc/header.php");


$page_nav["master"]["sub"]["package"]["active"] = true;
include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   @include('sweetalert::alert')
       <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><br>
            <a href='' data-toggle='modal' data-target='#addBranch' class='btn btn-success'><i class='fa fa-plus'></i> Add Manager</a>
            <div class="modal fade" id="addBranch" tabindex="-1" role="dialog" aria-labelledby="addBranch" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;
                            </button>
                            <h4 class="modal-title" id="addBranch">New Manager</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(['url' => 'save_manager','method' => 'post','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <fieldset>
                                <section>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" id="Name" name="name" placeholder="Full Name" required onkeyup="this.value = this.value.toUpperCase()" onkeypress="return FullName(event);">
                                        <b class="tooltip tooltip-bottom-right">Full Name</b>
                                    </label>
                                </section>
                                <section>           
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" id="manager_id" name="manager_id" placeholder="Manager ID" required autocomplete="off">
                                        <b class="tooltip tooltip-bottom-right">Manager ID</b>
                                    </label>
                                </section>
                                <section>       
                                     <label class="input">
                                        <i class="icon-append fa fa-phone"></i>
                                        <input type="text" id="phone" name="phone" placeholder="Phone No" required i pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==12) return false;" minlength="7" maxlength="14" >
                                        <b class="tooltip tooltip-bottom-right">Phone No</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope"></i>
                                        <input type="Email" id="Email" name="email" placeholder="Email" required>
                                        <b class="tooltip tooltip-bottom-right">Email</b>
                                    </label>
                                </section>
                                <section>
                                    Set Password ?
                                    <label class="select">
                                        <select name="set_password" id="set_password">
                                            <option value="1" >Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                        <b class="tooltip tooltip-bottom-right">Set Password?</b>
                                    </label>
                                </section>
                                <div id="section_password">
                                    <section>
                                        <label class="input">
                                            <i class="icon-append fa fa-lock"></i>
                                            <input type="password" id="password" name="password" placeholder="Password" required autocomplete="off">
                                            <b class="tooltip tooltip-bottom-right">Password</b>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="input">
                                            <i class="icon-append fa fa-lock"></i>
                                            <input type="password" id="password_confirmation" name="password_confirmation" placeholder="Password Confirmation" required>
                                            <b class="tooltip tooltip-bottom-right">Password Confirmation</b>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" name="submit" class="btn btn-primary">
                                Submit
                            </button>
                        {!! Form::close() !!}   
                        </div>
                    </div>
                </div>
            </div> <br><br>

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget well" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>Widget Title </h2>     
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body no-padding">
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
							<thead>
                                <tr>
                                    <th>No</th>
                                    <th>Manager ID</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Created at</th>
                                    <th>Status</th>
                                    <th>Detail/Action</th>
                                </tr>
							</thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach ($manager as $a)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $a->Manager->employer_id }}</td>
                                    <td>{{ $a->Manager->name }}</td>
                                    <td>{{ $a->email }}</td>
                                    <td>{{ $a->Manager->phoneno }}</td>
                                    <td>{{ $a->created_at }}</td>
                                    <td>
                                        @if($a->active==1) 
                                        <span class='label label-success'><b>Active</b></span>
                                        @else
                                         <span class='label label-default'><b>Not Active</b></span>
                                        @endif
                                    </td>
                                    <td> <a href='#' class="btn btn-primary btn-sm" data-toggle='modal' data-target='#myModal{{$a->id}}'> <i class="fa fa-cog fa-lg"></i> </a> &nbsp; | &nbsp; 
										<!--
                                        <a href='#' data-toggle='modal' data-target='#deleteModal{{$i}}'> <i class="fa fa-trash-o fa-lg"></i> </a>
										 
										<div class="modal fade" id="deleteModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal{{$i}}" aria-hidden="true">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
															&times;
														</button>
														<h4 class="modal-title" id="DeleteBranch"><b>Are You Sure to Delete  
                                                          {{ $a->name }} ? </b></h4>
													</div> 
													<div class="modal-body">
                                                        {!! Form::open(['url' => 'agent/'.$a->id,'method' => 'DELETE' ]) !!}
                                                        <div align='right'>
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <input type="hidden" name="name" value="{{ $a->name }}">
    														<button type="button" class="btn btn-default" data-dismiss="modal">
    															No
    														</button>
														    <button type="submit" name="submit" class="btn btn-primary">
																Yes
															</button>
                                                        </div>
                                                        {!! Form::close() !!}   
													</div>
												</div>
											</div>
										</div> -->
                                    </td>
                                </tr>
                            <?php $i++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div><br><br><br><br>
                </div>
            </div>
        </article>
    </div>
</div>
<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            
            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };

        $('#datatable_col_reorder').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "autoWidth" : true,
            "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_datatable_col_reorder) {
                    responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
                }
            },
            "rowCallback" : function(nRow) {
                responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
            },
            "drawCallback" : function(oSettings) {
                responsiveHelper_datatable_col_reorder.respond();
            }           
        });
    })
</script>
<script>
  /* BASIC ;*/
    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    $('#dt_basic').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth" : true,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_dt_basic) {
                responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_dt_basic.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_dt_basic.respond();
        }
    });  
</script>

<script>
    $(document).ready(function() {
    // show the alert
        window.setTimeout(function() {
        $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
            $(this).remove(); 
        });
        }, 2800);
    });
</script>
    
<script type="text/javascript">
    // Validation
    $(function() {
        runAllForms();
        $("#smart-form-register3").validate({
            rules : {
                Email: {
                    required : true,
                    email : true
                },
                Password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                 Branch : {
                    required : true
                },
                PasswordConfirmation : {
                    required : true,
                    minlength : 3,
                    maxlength : 20,
                    equalTo : '#Password'
                }
            },
            // Messages for form validation
             messages : {
                    Email: {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
                },
                    Branch: {
                    required : 'Please select a Branch'
                },
                Password : {
                    required : 'Please enter your password'
                },
                PasswordConfirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
            }
        });
    });
</script>


@foreach ($manager as $b)
<div class="modal fade" id="myModal{{$b->id}}" tabindex="-1" role="dialog" aria-labelledby="myModal{{$i}}" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="addBranch"> {{ $b->name }} </h4>
            </div>
            <div class="modal-body">
             {!! Form::open(['url' => 'agent_verify','class' => 'smart-form client-form', 'id' =>'smart-form-register3','method'=>'POST', 'enctype' =>'multipart/form-data' ]) !!} 
              <fieldset>
              <input type="hidden" name="marketing_id" value="{{$b->id}}">
                    <section >
                        <label class="label">Full Name: </label>
                        <label class="input">
                            <i class="icon-append fa fa-user"></i>
                            <input type="text" id="fullname" name="fullname" placeholder="Full Name " value="{{ $b->name }}" required onkeyup="this.value = this.value.toUpperCase()">
                            <b class="tooltip tooltip-bottom-right">Full Name</b>
                        </label>
                    </section>
                    <section>
                        <label class="label">Manager ID: </label>
                        <label class="input">
                            <i class="icon-append fa fa-address-card"></i>
                            <input type="text" maxlength="12" minlength="12" id="employer_id" name="employer_id" placeholder="Manager ID " value="{{ $b->Manager->employer_id }}" required>
                            <b class="tooltip tooltip-bottom-right">Manager ID</b>
                        </label>
                    </section>
                    <section>
                        <label class="label">Phone No: </label>
                        <label class="input">
                        
                            <i class="icon-append fa fa-mobile fa-lg"></i>
                            <input type="text" id="phoneno" name="phoneno" placeholder="Handphone" value="{{ $b->Manager->phoneno }}" required>
                            <b class="tooltip tooltip-bottom-right">Handphone</b>
                        </label>
                    </section>
                    <section>
                        <label class="label">Current Status:</label>
                        <label class="input">
                            @if($b->active==0) 
                            <span class='label label-warning'><b>Inactive</b></span>
                            @elseif($b->active==1) 
                             <span class='label label-success'><b>Active</b></span>
                            @endif
                        </label>
                    </section>
                    <section>
                        <label class="label">Set New Status:</label>
                        <label class="select">
                            <i class="icon-append fa fa-history"></i>
                            <select name="status" id="status{{$b->id}}">
                                <option value="-99" selected disabled>-Select-</option> 
                                <option value="0">Inactive</option>
                                <option value="1">Active </option>
                            </select>
                            <b class="tooltip tooltip-bottom-right">Set New Status</b>
                        </label>
                    </section>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </fieldset>
            </div>
            <div class="modal-footer">
                <button type="submit" name="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancel
                </button>
                {!! Form::close() !!}   
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endforeach

<script type="text/javascript">
    $(document).ready(function(){
        $("#phone").keypress(function(event){
            var inputValue = event.charCode;
            if(!(inputValue >= 48 && inputValue <= 57)){
                event.preventDefault();
            }
        });
    });
</script>

<script type="text/javascript">
    $( "#set_password" ).change(function() {
        var set_password = $('#set_password').val();
            if(set_password == 1) {
                $("#section_password").show();
                $("#password").prop('required',true);
                $("#password_confirmation").prop('required',true);
            }
        else {
            $("#section_password").hide();
            $("#password").prop('required',false);
            $("#password_confirmation").prop('required',false);
        }   
    });
</script>