<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Direct Customer Application List";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
        @if (Session::has('error'))
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('error') }}</strong> 
        </div>
        @elseif (Session::has('success'))
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('success') }}</strong> 
        </div>
            
      @endif
      
      <section id="widget-grid" class="">
                 
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-8 col-lg-8">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Current Statistics of Direct Customer Application</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                              
                        <?php   
                                function ringgit($nilai, $pecahan = 0) 
                                    { 
                                        return number_format($nilai, $pecahan, '.', ','); 
                                    }
                                
                                $sumloan= 0;
                                $sumapp= 0;                     
                        
                                $sumapproved= 0;
                                $sumloan_approved= 0; 
                                
                                $sumrejected= 0; 
                                $sumloan_rejected= 0; 
                                
                                $sumprocess= 0;
                                $sumloan_process= 0; 
                                
                                $sumpendingapproval= 0; 
                                $sumloan_pendingapproval= 0;

                                $sumpending2ndadmin= 0; 
                                $sumloan_pending2ndadmin= 0; 

                                $sumpendingadmin= 0; 
                                $sumloan_pendingadmin= 0; ?>
                                
                        @foreach ($terma as $termb)
                            <?php $sumloan = $sumloan + $termb->PraApp->loanamount;
                                    $sumapp = $sumapp + 1; ?>
                            @if($termb->verification_result_by_bank ==1)
                                    <?php   $sumapproved = $sumapproved + 1; 
                                            $sumloan_approved = $sumloan_approved + $termb->PraApp->loanamount; 
                                    ?>
                            @endif
                            @if(($termb->verification_result_by_bank ==2) OR ($termb->verification_result ==3))
                                    <?php   $sumrejected = $sumrejected + 1; 
                                            $sumloan_rejected = $sumloan_rejected + $termb->PraApp->loanamount; 
                                    ?>
                            @endif
                            @if($termb->verification_result_by_bank ==3)
                                    <?php   $sumpendingapproval = $sumpendingapproval + 1;
                                            $sumloan_pendingapproval = $sumloan_pendingapproval + $termb->PraApp->loanamount; 
                                    ?>
                            @endif
                             @if($termb->verification_result_by_bank ==0 AND $termb->verification_result ==2)
                                    <?php   $sumprocess = $sumprocess + 1; 
                                            $sumloan_process = $sumloan_process + $termb->PraApp->loanamount; 
                                    ?>
                            @endif
                            @if($termb->verification_result ==1)
                                    <?php   $sumpending2ndadmin = $sumpending2ndadmin + 1; 
                                            $sumloan_pending2ndadmin = $sumloan_pending2ndadmin + $termb->PraApp->loanamount; 
                                    ?>
                            @endif
                            @if($termb->verification_result ==0)
                                    <?php   $sumpendingadmin = $sumpendingadmin + 1; 
                                            $sumloan_pendingadmin = $sumloan_pendingadmin + $termb->PraApp->loanamount; 
                                    ?>
                            @endif
                            
                        @endforeach
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Total Application</th>
                                            <th>Total Loan Amount Request</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Total Approved by Branch </td>
                                            <td>{{ $sumapproved }}</td>
                                            <td>RM {{ ringgit($sumloan_approved) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total Rejected by Branch or Admin </td>
                                            <td>{{ $sumrejected }}</td>
                                            <td>RM {{ ringgit($sumloan_rejected) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total Pending Approval </td>
                                            <td>{{ $sumpendingapproval }}</td>
                                            <td>RM {{ ringgit($sumloan_pendingapproval) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total In Process </td>
                                            <td>{{ $sumprocess }}</td>
                                            <td>RM {{ ringgit($sumloan_process) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total Pending Verification  </td>
                                            <td>{{ $sumpendingadmin }}</td>
                                            <td>RM {{ ringgit($sumloan_pendingadmin) }}</td>
                                        </tr>
                                        <tr>
                                            <td><b>All</b></td>
                                            <td><b>{{ $sumapp }}</b></td>
                                            <td><b>RM {{ ringgit($sumloan) }}</b></td>
                                        </tr>                                     
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
                        
                        
        <section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Direct Customer Application List</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                              
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>                         
                                                <tr>
                                                    <th>No.</th>
                                                    <th class="hidden-xs">IC Number</th>
                                                    <th>Name</th>
                                                    <th class="hidden-xs">Phone</th>
                                                 
                                                    <th>Submit Date</th>
                                                    <th>Status</th>
                                                 
                                                    <th>Last Activites</th>
                                                   
                                                     <th class="hidden-xs">Download</th>
                                                      

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=1; ?>
                                                 @foreach ($terma as $term)
                                                   
                                              <tr>
                                              
                                                <td>
                                                   {{ $i }}                              
                                                </td>
                                                  
                                                  <td class="hidden-xs">
                                              {{ $term->Basic->new_ic }}
                                              <input type='hidden' id='ic99{{$i}}' name='ic' value='{{ $term->Basic->new_ic }}'/>
                                         
                                                  
                                                  
                                                  </td>
                                                <td>{{ $term->Basic->name }}</td>
                                                <td class="hidden-xs">{{ $term->PraApplication->phone }}</td>
                                                <td>{{$term->file_created }}</td>
                                                <td><div align='center'>
                                               
                                                    @if($term->verification_result ==0)
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}" class='label label-warning'>Pending Verification</span>
                                                    @elseif($term->verification_result ==1)                                                
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-success'>Ready for 2nd Verification</span>
                                                    @elseif($term->verification_result ==2)
                                                        <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-primary'>Routed to Branch</span>
                                                        <br>
                                                        @if($term->id_branch=='0')
                                                         <div align='center'> - </div> 
                                                        @else                                            
                                                        <i>{{ $term->Branch->branchname }}</i>
                                                        @endif 
                                                
                                                    @elseif($term->verification_result ==3)
                                                      <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-danger'>Rejected</span>
                                                 
                                                    @elseif($term->verification_result ==4)
                                                      <span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-default'>Route Back to User</span>
                                                 
                                                    @endif
                                                    
                        
                                                       </div>
                                                </td>
                                         
                                            
                                               
                                                   <td class="hidden-xs">
                                                  <a href="JavaScript:newPopup('{{url('/')}}/activities/{{$term->id_praapplication}}');"   class='btn btn-sm btn-default'><i class="fa fa-search"></i></a>
                                                  </td>
                                      
                                              
                                                 <td class="hidden-xs"> <a href="{{url('/')}}/admin/downloadzip/{{$term->id_praapplication}}"
                                                ><img width='24' height='24' src="{{ url('/') }}/asset/img/zip.png"></img></a>&nbsp;&nbsp; 
                                                 @if($term->status!=99 AND $term->status!=88 AND $term->status!=77)
                                                  <a href="{{url('/')}}/admin/downloadpdf/{{$term->id_praapplication}}"
                                                ><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png"></img></a>
                                                @endif
                                                 </td>
                                            </tr>
                                              <?php
                                              $i++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>
                         
                                                    <script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



        <script type="text/javascript">
        
        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        
        $(document).ready(function() {
            
            pageSetUp();
        
            
            /* // DOM Position key index //
        
            l - Length changing (dropdown)
            f - Filtering input (search)
            t - The Table! (datatable)
            i - Information (records)
            p - Pagination (paging)
            r - pRocessing 
            < and > - div elements
            <"#id" and > - div with an id
            <"class" and > - div with a class
            <"#id.class" and > - div with an id and class
            
            Also see: http://legacy.datatables.net/usage/features
            */  
    
            /* BASIC ;*/
                var responsiveHelper_dt_basic = undefined;
                var responsiveHelper_datatable_fixed_column = undefined;
                var responsiveHelper_datatable_col_reorder = undefined;
                var responsiveHelper_datatable_tabletools = undefined;
                
                var breakpointDefinition = {
                    tablet : 1024,
                    phone : 480
                };
    
                
    
            /* END BASIC */
            
                /* COLUMN SHOW - HIDE */
    $('#datatable_col_reorder').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "autoWidth" : true,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_col_reorder) {
                responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_datatable_col_reorder.respond();
        }           
    });
    
    /* END COLUMN SHOW - HIDE */
        })
        </script>
          
                            

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
                var responsiveHelper_dt_basic = undefined;
                var responsiveHelper_datatable_fixed_column = undefined;
                var responsiveHelper_datatable_col_reorder = undefined;
                var responsiveHelper_datatable_tabletools = undefined;
                
                var breakpointDefinition = {
                    tablet : 1024,
                    phone : 480
                };
                $('#dt_basic').dataTable({
                      
                        "scrollX": true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                        "t"+
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth" : true,
                    "preDrawCallback" : function() {
                        // Initialize the responsive datatables helper once.
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback" : function(nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback" : function(oSettings) {
                        responsiveHelper_dt_basic.respond();
                    }
                });
                
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>

<script type="text/javascript">
// Popup window code
function newPopup(url) {
    popupWindow = window.open(
        url,'popUpWindow','height=400,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes')
}
</script>




          
