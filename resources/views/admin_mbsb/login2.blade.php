<!DOCTYPE html>
<html lang="en">
<head>
	<title>Personal Financing-i Administrator</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{url('/asset')}}/img/favicon/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('/asset/login/')}}/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('/asset/login/')}}/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('/asset/login/')}}/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('/asset/login/')}}/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{url('/asset/login/')}}/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('/asset/login/')}}/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{url('/asset/login/')}}/css/util.css">
	<link rel="stylesheet" type="text/css" href="{{url('/asset/login/')}}/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<script>
var apiGeolocationSuccess = function(position) {
  showLocation(position);
};

var tryAPIGeolocation = function() {
  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDCa1LUe1vOczX1hO_iGYgyo8p_jYuGOPU", function(success) {
    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
  })
  .fail(function(err) {
    
  });
};

var browserGeolocationSuccess = function(position) {
  showLocation(position);
};

var browserGeolocationFail = function(error) {
  switch (error.code) {
    case error.TIMEOUT:
      alert("Browser geolocation error !\n\nTimeout.");
      break;
    case error.PERMISSION_DENIED:
      if(error.message.indexOf("Only secure origins are allowed") == 0) {
        tryAPIGeolocation();
      }
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Browser geolocation error !\n\nPosition unavailable.");
      break;
  }
};

var tryGeolocation = function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      browserGeolocationSuccess,
      browserGeolocationFail,
      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
  }
};

tryGeolocation();

function showLocation(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
  var _token = $('#_token').val();
  $.ajax({
    type:'POST',
    url: "{{url('/')}}/getLocation",
    data: { latitude: latitude, _token : _token, longitude : longitude },
    success:function(data){
            if(data){
              $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");
              $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");
               $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");
                 

            }else{
                $("#location").html('Not Available');
            }
    }
  });
}
</script>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('{{url('/asset/login/')}}/images/office.png');">

	 
			<div class="wrap-login100 p-t-190 p-b-30">

				{!! Form::open(['url' => 'mbsb_login','class' => 'login100-form validate-form smart-form client-form' ]) !!}
							{!! csrf_field() !!}
				          	<span id="latitude"></span>
                            <span id="longitude"></span>
                            <span id="location"></span>

					<div class="login100-form-avatar">
						<img src="{{url('/asset/img/')}}/mbsb_small.png" alt="AVATAR">
					</div>

					<span class="login100-form-title p-t-20 p-b-45">
						MBSB Administration

						@if (count($errors) > 0)
   
            @foreach ($errors->all() as $error)
                <br><font size="1" color="red">{{ $error }}</font>
            @endforeach

		@endif
					</span>

		

					<div class="wrap-input100 validate-input m-b-10" data-validate = "Username is required">
						<input class="input100" type="email" name="email" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock"></i>
						</span>
					</div>

					<div class="container-login100-form-btn p-t-10">
						<button class="login100-form-btn">
							Login
						</button>
					</div>

					<div class="text-center w-full p-t-25 p-b-230">
						<a href="{{url('/')}}/password/email" class="txt1">
							Forgot Username / Password?
						</a>
					</div>

					<div class="text-center w-full">
						 <span class="txt1">Copyright © <?php echo date('Y'); ?> MBSB Bank Berhad</span><br>
                    <span class="txt1">Registration No: 200501033981 (716122-P). All Rights Reserved.</span>
					</div>
				{!! Form::close() !!} 
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="{{url('/asset/login/')}}/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="{{url('/asset/login/')}}/vendor/bootstrap/js/popper.js"></script>
	<script src="{{url('/asset/login/')}}/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="{{url('/asset/login/')}}/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="{{url('/asset/login/')}}/js/main.js"></script>

</body>
</html>