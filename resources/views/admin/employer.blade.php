<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title =  "Employer Master";


$page_css[] = "your_style.css";
include("asset/inc/header.php");


$page_nav["master"]["sub"]["bankpersatuan"]["sub"]["branchmaster"]["active"] = true;
include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	    @if (Session::has('error'))
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('error') }}</strong> 
        </div>
        @elseif     (Session::has('message'))
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('message') }}</strong> 
        </div>
            
      @endif

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            
                            <br>
                            <a href='' data-toggle='modal' data-target='#addBranch' class='btn btn-success'><i class='fa fa-bank'></i> Add Employer</a>
                            	<!-- Modal -->
                                <div class="modal fade" id="addBranch" tabindex="-1" role="dialog" aria-labelledby="addBranch" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="addBranch">Add Employer</h4>
                                            </div>
                                            <div class="modal-body">
                                             {!! Form::open(['url' => 'employer','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                                              <fieldset>
                                                    <section >
                                                          <label class="label"> Name </label>
                                                             <label class="input">
                                                                <i class="icon-append fa fa-suitcase"></i>
                                                                <input type="text" id="name" name="name" placeholder="Branch Name " required>
                                                                <b class="tooltip tooltip-bottom-right">Employer Name</b>
                                                            </label>
                                                        </section>
                                                  <section >
														<label class="label">Employment Type </label>
																				<label class="input">
																			   
																					<select name='type'  required class='form-control'>
																				@foreach($employment as $employment2)
																		<option value="{{$employment2->id}}" > {{$employment2->name }}</option>

																				@endforeach
																						  
																			
																					</select>
																					<b class="tooltip tooltip-bottom-right">Select Employment Type</b>
																				</label>
																			</section>
                                                        
                                                       
                                                       
                                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                 </fieldset>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Cancel
                                                </button>
                                                <button type="submit" name="submit" class="btn btn-primary">
                                                               Submit
                                                            </button>
                                                           
                                                   {!! Form::close() !!}   
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            
                            <br><br>

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                                <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                    
                                    data-widget-colorbutton="false" 
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true" 
                                    data-widget-sortable="false"
                                    
                                -->
                                
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
										<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Name</th>
                                                    <th> Type</th>
                                                   
													<th>Action</th>
                                               
                                                </tr>
											</thead>
                                            <tbody>
                                            <?php $i=1; ?>
                                            @foreach ($employer as $a)
                                                <tr>
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $a->name }}</td>
                                                    <td>{{ $a->employment->name }}</td>
                                                  
                                                
													<td> <a href='#' data-toggle='modal' data-target='#myModal{{$i}}'>Edit</a>
													 <!-- Modal -->
													<div class="modal fade" id="myModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="myModal{{$i}}" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		&times;
																	</button>
																	<h4 class="modal-title" id="addBranch">Branch {{ $a->branchname }} </h4>
																</div>
																<div class="modal-body">
																 {!! Form::open(['url' => 'employer/update','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
																  <fieldset>
																		
																			<section >
                                                          <label class="label"> Name </label>
                                                             <label class="input">
                                                                <i class="icon-append fa fa-suitcase"></i>
                                                                <input type="text" id="name" name="name" placeholder="Branch Name "  value="{{$a->name }}" required>
                                                                <b class="tooltip tooltip-bottom-right">Employer Name</b>
                                                            </label>
                                                        </section>
                                                  <section >
														<label class="label">Employment Type </label>
																				<label class="input">
																			   
																					<select name='type'  required class='form-control'>
																				
										

							@foreach($employment as $employment23)
							@if($employment23->id==$a->employment_id)
							  <?php $selected ="selected"; ?>
							@else
								<?php $selected =" "; ?>
							 @endif
							  
							<option value="{{$employment23->id}}" {{ $selected }}> {{$employment23->name }}</option>

							@endforeach
																						  
																						  
																			
																					</select>
																					<b class="tooltip tooltip-bottom-right">Select Employment Type</b>
																				</label>
																			</section>		
																		   
																		 <input type="hidden" name="_token" value="{{ csrf_token() }}">
																		  <input type="hidden" name="id_employer" value="{{ $a->id }}">
																	 </fieldset>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">
																		Cancel
																	</button>
																	<button type="submit" name="submit" class="btn btn-primary">
																				   Submit
																				</button>
																			   
																	   {!! Form::close() !!}   
																</div>
															</div><!-- /.modal-content -->
														</div><!-- /.modal-dialog -->
													</div><!-- /.modal -->
													
												&nbsp;
												
													<a href='#' data-toggle='modal' data-target='#delete{{$i}}'>Delete</a>
													 <!-- Modal -->
													<div class="modal fade" id="delete{{$i}}" tabindex="-1" role="dialog" aria-labelledby="delete{{$i}}" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		&times;
																	</button>
																	<h4 class="modal-title" id="delete"><b>Warning</b></h4>
																</div>
																<div class="modal-body">
																 {!! Form::open(['url' => 'employer/delete','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
																 Are you sure you want to delete ?
																		 <input type="hidden" name="_token" value="{{ csrf_token() }}">
																		  <input type="hidden" name="id_employer" value="{{ $a->id }}">
																	
																</div>
																<div class="modal-footer">
																	
																	<button type="submit" name="submit" class="btn btn-primary">
																				   Yes
																				</button>
																				  <button type="button" class="btn btn-default" data-dismiss="modal">
																		No
																	</button>
																			   
																	   {!! Form::close() !!}   
																</div>
															</div><!-- /.modal-content -->
														</div><!-- /.modal-dialog -->
													</div><!-- /.modal -->
													</td>
                                                  
                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
											</tbody>
                                        </table>
																					   
										
													<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



		<script type="text/javascript">
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
		
			
			/* // DOM Position key index //
		
			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing 
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class
			
			Also see: http://legacy.datatables.net/usage/features
			*/	
	
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				
	
			/* END BASIC */
			
				/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */
		})
		</script>
          
													
													

									</div>
									<br><br><br><br>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>
    
    <script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Name: {
                    required : true,
                    minlength : 1,
                    maxlength : 50
                },
                TelephoneNumber: {
                    required : true,
                    minlength : 11,
                    maxlength : 11
                }
            },

            // Messages for form validation
             messages : {

                    Name: {
                    required : 'Please enter Branch Name',
                    email : 'Please enter a VALID email address'
                }
                    }
        });

    });
</script>
<script>    
function confirmDelete(url) {
    if (confirm("Are you sure you want to delete this?")) {
        window.location.href=url;
    } else {
        false;
    }       
}
</script>

