<?php
//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");
$page_title = "Documents Verification";

$page_css[] = "your_style.css";

include("asset/inc/header.php");
?>

<style>
    .not-active {
        pointer-events: none;
        cursor: default;
    }
</style>



<?php
    include("asset/inc/nav.php");
    //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
    //$breadcrumbs["New Crumb"] => "http://url.com"
    $breadcrumbs["Home"] = "";
    include("asset/inc/ribbon.php");
?>

<div id="main" role="main">
    <div id="content">
        <section id="widget-grid" class="">
            <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-sm-12 col-md-12 col-lg-12">
                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-check"></i> </span>
                        <h2>DSR Checking </h2>
                    </header>
                <div>
                <div class="jarviswidget-editbox"></div>
                <div class="widget-body">
                    <div class="row">
                        <form id="wizard-1" novalidate="novalidate">
                            <div id="bootstrap-wizard-1" class="col-sm-12">
                                <div class="form-bootstrapWizard">
                                    <ul class="bootstrapWizard form-wizard">
                                        <li data-target="#step1">
                                            <a href="#tab1" data-toggle="tab"> <span class="step">1</span> <span class="title">Documents</span> </a>
                                        </li>
                                        <li data-target="#step2" class="not-active">
                                            <a href="#tab2" data-toggle="tab"> <span class="step">2</span> <span class="title">Loan Calculator</i></span> </a>
                                        </li>
                                        <li data-target="#step3" class="not-active">
                                            <a href="#tab3" data-toggle="tab"> <span class="step">3</span> <span class="title">Scoring</span> </a>
                                        </li>
                                        <li data-target="#step4" class="not-active">
                                            <a href="#tab4" data-toggle="tab"> <span class="step">4</span> <span class="title">Route</span> </a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <ul class="pager wizard no-margin">
                                                <li class="next">
                                                    <a href="javascript:void(0);" class="btn btn-lg txt-color-green"> Seterusnya / <i> Next </i> </a>
                                                </li>
                                                <li class="previous">
                                                    <a href="javascript:void(0);" class="btn btn-lg btn-default"> Sebelum / <i> Previous </i> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane" id="tab2"><br>
                                        <h3><strong>Loan Calculator</strong></h3>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input name="id_praapplication" type="hidden" id="id_pra" value="{{$pra->id}}" >
                                        <fieldset><br>
                                            <div class="container" style="">
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <font color="red">This Customer select <b>{{$pra->employer->employment->name}}</b> and choose <b>{{$pra->package->name}}</b> package when register</font>
                                                        <table width="70%" class='table table-hover' border='0' cellspacing="2" cellpadding="2" style="width: 97% !important">
                                                            <thead>
                                                                <tr>
                                                                    <td width="40%"><h3><b>Choose Customer Sector</b></h3></td>
                                                                    <td width="30%">
                                                                        <select name="sector" id="sector" class="fin form-control">
                                                                            <option disabled selected>-Select Customer Sector-</option>
                                                                            @if($dsr_a->sector=="G")
                                                                            <option selected value="G">Government Biro/Non-Biro</option>
                                                                            <option value="P">Private</option>
                                                                            @elseif($dsr_a->sector=="P")
                                                                            <option value="G">Government Biro/Non-Biro</option>
                                                                            <option selected value="P">Private</option>
                                                                            @else
                                                                            <option value="G">Government Biro/Non-Biro</option>
                                                                            <option value="P">Private</option>
                                                                            @endif
                                                                        </select>
                                                                    </td>
                                                                    <td width="30%"></td>
                                                                </tr>
                                                            </thead>
                                                        </table><br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class='col-lg-10'>
                                                        <div class="form-group">
                                                            @if($financial->first()->debt_consolidation>0) 
                                                            <div id="add_redemption_button" style='display:block'>
                                                                <div class="form-group">
                                                                    <label><b>Settlement Instruction</b></label><br>
                                                                    <table id="setData" border='1' class='table table-hover table-borderd col-md-11 col-lg-11'> 
                                                                        <tbody>
                                                                            <tr bgcolor="#e6f3ff"> 
                                                                                <td>Institusi Kewangan / Koperasi</td><td>Ansuran Bulanan (RM)</td>
                                                                                <td>Baki Pembayaran (RM)</td>
                                                                            </tr> 
                                                                            <?php $z=0;?>
                                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                            <input type="hidden" id="id_praapplication" name="id_praapplication" value="{{$pra->id}}"/>
                                                                            @foreach ($settlement as $set)
                                                                            <tr>
                                                                                <td width='55%'><input type='text' class="form-control"  name='set_installment[]' value='{{$set->institution}}' /></td>
                                                                                <td width='25%'><input type='text' class="form-control"  name='set_installment[]' value='{{$set->installment}}' /></td>
                                                                                <td width='25%'><input type='text' value='{{$set->amount}}' class="form-control"  name='set_amount[]' value='' /></td>
                                                                            </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table><br>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {!! Form::close() !!} 
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-11">
                                                        <p>PERSONAL FINANCING: DEBT SERVICE RATIO (DSR) Calculation V4.9 - 24.5.2016</p>
                                                        <p><b><u>Section A. Applicant Information</u></b></p><br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class='col-md-10'>
                                                        <table width="100%" class="table table-hover" border='0' cellspacing="2" cellpadding="2" style="width: 97% !important">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Applicant Name</td>
                                                                    <td colspan='2'><input name="name" style='text-transform:uppercase' type="text"  class="form-control" value='{{$name}}' id="name"></td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Applicant MyKad Number</td>
                                                                    <td colspan='2'><input name="new_ic" style='text-transform:uppercase' type="text"  class="form-control fn" value='{{$data->new_ic}}' id="new_ic" maxlength="12"></td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Gender <sup>*</sup></td>
                                                                    <td colspan='2'>
                                                                        <select class="form-control" name="gender" id="gender" required="requeired">
                                                                            @if(!empty($data->gender))
                                                                                @if($data->gender=='M')
                                                                                <option value="M">Male / <i>Lelaki </i></option>
                                                                                @elseif($data->gender=='F')
                                                                                <option value="F">Female / <i>Perempuan </i></option>
                                                                                @endif
                                                                            @endif
                                                                            <option value="M">Male / <i>Lelaki </i></option>
                                                                            <option value="F">Female / <i>Perempuan </i></option>
                                                                        </select>
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                   <tr>
                                                                    <td>Age</td>
                                                                    <td colspan='2'>
                                                                        <?php 
                                                                            if(!empty($data->age)) {
                                                                                $age_from_ic = $usia;
                                                                            }
                                                                            else {
                                                                                $age_from_ic = $usia;
                                                                            }
                                                                            ?>
                                                                        <input readonly name="age" maxlength="3" type="text" id="age" class="form-control" value='{{$age_from_ic}}' id="loan_amount">
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                @if($dsr_a->sector=="G")
                                                                <tr>
                                                                    <td>Category <sup>*</sup></td>
                                                                    <td colspan='2'>
                                                                        <select name="category" id="category" class="fin form-control" placeholder="Select Category" required>
                                                                            @if(!empty($dsr_a->category))
                                                                            <option>{{$dsr_a->category}}</option>
                                                                            @else
                                                                            <option disabled selected>-Select Category-</option>
                                                                            @endif
                                                                            <option>BIRO</option>
                                                                            <option>NON-BIRO</option>
                                                                        </select>
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                            <div class="form-group">
                                                                <tr>
                                                                    <td>Existing MBSB PF-I customer refinancing their existing PF-I facility with MBSB (internal overlap) <sup>*</sup></td>
                                                                    <td colspan='2'>
                                                                        <select name="existing_mbsb" id="existing_mbsb" class="fin form-control" placeholder="Select Option" required > 
                                                                            @if(!empty($dsr_a->existing_mbsb))
                                                                            <option  >{{$dsr_a->existing_mbsb}} </option>
                                                                            @else
                                                                            <option disabled selected>-Select Option-</option>
                                                                            @endif
                                                                            <option value="YES">YES</option>
                                                                            <option value="NO">NO</option>
                                                                        </select>
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                            </div>
                                                            
                                                                <tr id="otherFieldDiv">
                                                                    <td>Existing (CIF No.)</td>
                                                                    <td colspan='2'>
                                                                        <div class="form-group" >
                                                                        <input name="cif_no" style='text-transform:uppercase' type="text"  class="form-control" value='{{$dsr_a->cif_no}}' id="otherField" > </div>
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                           

                                                                        @else
                                                                
                                                                @endif
                                                               
                                                                <tr>
                                                                    <td>Loan Amount Request</td>
                                                                    <td colspan='2'><input name="loan_amount" readonly type="text"  class="form-control" value='{{$loanammount->loanammount}}' id="loan_amount" ></td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Loan Tenure Request (Years)</td>
                                                                    <td colspan='2'>
                                                                        @if($loanammount->id_tenure>0)
                                                                            <?php  $loantenure = $loanammount->Tenure->years;
                                                                            $id_tenure_hidden = $loanammount->Tenure->years; ?>
                                                                        @else
                                                                            <?php  $loantenure = 10; 
                                                                            $id_tenure_hidden = 0; ?>
                                                                        @endif
                                                                        <input name="tenure" type="text"  class="form-control" value='{{$loantenure}}' id="" readonly>
                                                                        <input name="id_tenure_hidden" type="hidden"  class="form-control" value='{{$id_tenure_hidden}}'>
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr bgcolor="#688F9E" style="width: 97% !important">
                                                                    <td><strong>Earnings</strong></td>
                                                                    <td>First Month</td>
                                                                    <td>Second Month</td>
                                                                    <td>Third Month</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Basic salary <sup>*</sup></td>
                                                                    <td>
                                                                        <!-- <input  type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="basic_salary1" value="{{number_format((float)$dsr_a->basic_salary1, 2)}}" id="basic_salary1" step=".01"> -->
                                                                        <?php
                                                                            $my_number = number_format($dsr_a->basic_salary1, 2);
                                                                        ?>
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" class="form-control Currency_basicsalary1 fn" id="a1" name="basic_salary1" placeholder="RM" required="required" value="{{number_format((float)$dsr_a->basic_salary1, 2)}}">
                                                                    </td>
                                                                    <td> 
                                                                        <!-- <input onchange="toFloat('basic_salary2')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="basic_salary2" value="{{number_format((float)$dsr_a->basic_salary2, 2)}}" id="basic_salary2">  -->
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" class="form-control Currency_basicsalary2 fn" id="a2" name="basic_salary2" placeholder="RM" required="required" value="{{number_format((float)$dsr_a->basic_salary2, 2)}}">
                                                                    </td>
                                                                    <td>
                                                                        <!-- <input onchange="toFloat('basic_salary3')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="basic_salary3" value="{{number_format((float)$dsr_a->basic_salary3, 2)}}" id="basic_salary3"> -->
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" class="form-control Currency_basicsalary3 fn" id="a3" name="basic_salary3" placeholder="RM" required="required" value="{{number_format((float)$dsr_a->basic_salary3, 2)}}">
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Fixed Allowances <sup>*</sup></td>
                                                                    <td>
                                                                        <!-- <input onchange="toFloat('fixed_allowances1')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fixed_allowances1" value="{{number_format((float)$dsr_a->fixed_allowances1, 2)}}" id="fixed_allowances1">-->
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" class="form-control Currency_allowance1 fn" id="b1" name="fixed_allowances1" placeholder="RM" value="{{number_format((float)$dsr_a->fixed_allowances1, 2)}}" required="required">
                                                                    </td>
                                                                    <td>  
                                                                        <!-- <input onchange="toFloat('fixed_allowances2')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fixed_allowances2" value="{{number_format((float)$dsr_a->fixed_allowances2, 2)}}" id="fixed_allowances2"> -->
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" class="form-control Currency_allowance2 fn" id="b2" name="fixed_allowances2" placeholder="RM" required="required" value="{{number_format((float)$dsr_a->fixed_allowances2, 2)}}">
                                                                    </td>
                                                                    <td>
                                                                        <!-- <input onchange="toFloat('fixed_allowances3')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="fixed_allowances3" value="{{number_format((float)$dsr_a->fixed_allowances3, 2)}}" id="fixed_allowances3"> -->  
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" class="form-control Currency_allowance3 fn" id="b3" name="fixed_allowances3" placeholder="RM" required="required" value="{{number_format((float)$dsr_a->fixed_allowances3, 2)}}"> 
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total</td>
                                                                    <td>
                                                                        <!-- <input readonly onchange="toFloat('total_earnings1')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="total_earnings1" value="{{number_format((float)$dsr_a->total_earnings1, 2)}}" id="total_earnings1">  -->
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" class="form-control total_nya" id="total_nya" placeholder="RM" name="total_earnings1" value="{{number_format((float)$dsr_a->total_earnings1, 2)}}" readonly>
                                                                    </td>
                                                                    <td> 
                                                                        <!-- <input readonly onchange="toFloat('total_earnings2')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="total_earnings2" value="{{number_format((float)$dsr_a->total_earnings2, 2)}}" id="total_earnings2"> -->
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" class="form-control total_nya2" id="total_nya2" placeholder="RM" name="total_earnings2" value="{{number_format((float)$dsr_a->total_earnings2, 2)}}" readonly>   
                                                                    </td>
                                                                    <td> 
                                                                        <!-- <input readonly onchange="toFloat('total_earnings3')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="total_earnings3" value="{{number_format((float)$dsr_a->total_earnings3, 2)}}" id="total_earnings3">  -->
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" class="form-control total_nya3" id="total_nya3" placeholder="RM" name="total_earnings3" value="{{number_format((float)$dsr_a->total_earnings3, 2)}}" readonly>    
                                                                    </td>
                                                                    <td> 
                                                                        <!-- <input readonly onchange="toFloat('total_earnings')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="total_earnings" value="{{number_format((float)$dsr_a->total_earnings, 2)}}" id="total_earnings"> -->
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" class="form-control total_earnings" id="total_earnings" placeholder="RM" name="total_earnings" value="{{number_format((float)$dsr_a->total_earnings3, 2)}}" readonly>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Variable Income</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>
                                                                        <font color="red">
                                                                        <!-- <input onchange="toFloat('variable_income')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" style="color:red;" placeholder="RM" name="variable_income" value="{{number_format((float)$dsr_a->variable_income, 2)}}" id="variable_income"> -->
                                                                        <input type="tel" onkeypress="return isNumberKey(event)"" style="color:red;" class="form-control variable_income fn" id="variable_income" name="variable_income" placeholder="RM" required="required" value="{{number_format((float)$dsr_a->variable_income, 2)}}"> 
                                                                        </font> 
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td><i>Gross Monthly Income </i>   </td>
                                                                    <td>
                                                                        <input readonly type="tel" onkeypress="return isNumberKey(event)" maxlength="10"  required="required" class="form-control Currency" placeholder="RM" name="gmi_hidden" value="{{number_format((float)$dsr_a->gmi, 2)}}" id="gmi">
                                                                        <input readonly type="hidden" onkeypress="return isNumberKey(event)" maxlength="10"  required="required" class="form-control Currency" placeholder="RM" name="gmi_use" value="{{number_format((float)$dsr_a->gmi, 2)}}" id="gmi_use">
                                                                    </td>
                                                                </tr>
    <!-- ============================== End of  Basic salary  ======================================== -->
                                                                <tr bgcolor="#688F9E">
                                                                    <td><strong>Deductions</strong></td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>EPF Contribution</td>
                                                                    <td>
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" maxlength="10"  required="required" class="form-control Currency" placeholder="RM" name="epf1" value="{{number_format((float)$dsr_a->epf1, 2)}}" id="epf1">
                                                                    </td>
                                                                    <td>
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="epf2" value="{{number_format((float)$dsr_a->epf2, 2)}}" id="epf2">
                                                                    </td>
                                                                    <td>
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="epf3" value="{{number_format((float)$dsr_a->epf3, 2)}}" id="epf3">
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Socso Contribution</td>
                                                                    <td>
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="socso1" value="{{number_format((float)$dsr_a->socso1, 2)}}" id="socso1">   
                                                                    </td>
                                                                    <td>  
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" maxlength="10"  required="required" class="form-control Currency" placeholder="RM" name="socso2" value="{{number_format((float)$dsr_a->socso2, 2)}}" id="socso2">
                                                                    </td>
                                                                    <td>
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="socso3" value="{{number_format((float)$dsr_a->socso3, 2)}}" id="socso3">
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Income Tax</td>
                                                                    <td>  
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="income_tax1" value="{{number_format((float)$dsr_a->income_tax1, 2)}}" id="income_tax1">
                                                                    </td>
                                                                    <td>
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="income_tax2" value="{{number_format((float)$dsr_a->income_tax2, 2)}}" id="income_tax2">   
                                                                    </td>
                                                                    <td>
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="income_tax3" value="{{number_format((float)$dsr_a->income_tax3, 2)}}" id="income_tax3">
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Other Contribution</td>
                                                                    <td>  
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="other_contribution1" value="{{number_format((float)$dsr_a->other_contribution1, 2)}}" id="other_contribution1">
                                                                    </td>
                                                                    <td>
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="other_contribution2" value="{{number_format((float)$dsr_a->other_contribution2, 2)}}" id="other_contribution2">
                                                                    </td>
                                                                    <td>
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="other_contribution3" value="{{number_format((float)$dsr_a->other_contribution3, 2)}}" id="other_contribution3">
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total deduction</td>
                                                                    <td>
                                                                        <input readonly type="text" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="total_deduction1" value="{{number_format((float)$dsr_a->total_deduction1, 2)}}" id="total_deduction1">   
                                                                    </td>
                                                                    <td>
                                                                        <input readonly type="text" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="total_deduction2" value="{{number_format((float)$dsr_a->total_deduction2, 2)}}" id="total_deduction2">
                                                                    </td>
                                                                    <td>
                                                                        <input readonly type="text" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="total_deduction3" value="{{number_format((float)$dsr_a->total_deduction3, 2)}}" id="total_deduction3">
                                                                    </td>
                                                                    <td>
                                                                        <input readonly type="text" maxlength="10" required="required" class="form-control Currency" placeholder="RM"  value="{{number_format((float)$dsr_a->total_deduction3, 2)}}" id="total_deduction">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Net Monthly Income </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>
                                                                        <input readonly type="text" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="nmi" value="{{number_format((float)$dsr_a->nmi, 2)}}" id="nmi">  
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr  bgcolor="#688F9E">
                                                                    <td><strong>Business Income / Other additional  Income </strong></td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Non-Salaried or Additional Income</td>
                                                                    <td>
                                                                        <input  type="tel" onkeypress="return isNumberKey(event)" maxlength="10"  required="required" class="form-control Currency" placeholder="RM" name="additional_income" value="{{number_format((float)$dsr_a->additional_income, 2)}}" id="additional_income">
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Income Tax Estimation for Non-Salaried Customer</td>
                                                                    <td>
                                                                        <input type="tel" onkeypress="return isNumberKey(event)" maxlength="10" required="required" class="form-control Currency" placeholder="RM" name="ite" value="{{number_format((float)$dsr_a->ite, 2)}}" id="ite">
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Verified by Processing Officer</td>
                                                                    <td colspan="2">
                                                                        <input readonly type="text" maxlength="" required="required" class="form-control fn" placeholder="RM" name="sec1_verified_by" value="{{$user->name}}" >
                                                                    </td>
                                                                    <td colspan="2"><i>* Processing Officer is current User Name login session</i></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Date and Time</td>
                                                                    <td colspan="2">
                                                                        <input readonly type="text" required="required" class="form-control fn" name="" value="Auto Update When Save" id="">     
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total Gross Income (RM)</td>
                                                                    <td colspan="2">
                                                                        <input readonly onchange="toFloat('total_gross_income')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="total_gross_income" value="{{number_format((float)$dsr_a->total_gross_income, 2)}}" id="total_gross_income">
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total Net Income (RM)</td>
                                                                    <td colspan="2">
                                                                        <input readonly onchange="toFloat('total_net_income')" type="text" maxlength="10" onkeypress="return isNumberKey(event)"  required="required" class="form-control fn" placeholder="RM" name="total_net_income" value="{{number_format((float)$dsr_a->total_net_income, 2)}}" id="total_net_income">
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <p><b><u>Section B - Financing Application</u></b></p><br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <table border="0" class='table table-hover' cellspacing="2" cellpadding="2">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="45%">Total Financing Amount</td>
                                                                    <td width="20%">
                                                                        <input name="financing_amount" value="{{number_format((float)$dsr_b->financing_amount, 2)}}" type="tel" required class="form-control fn" id="financing_amount" onchange="toFloat('financing_amount')" onkeypress="return isNumberKey(event)"></td>
                                                                    <td width="35%">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Tenure (Years)</td>
                                                                    <td><input name="tenure" value="{{$dsr_b->tenure}}" type="tel" required class="form-control fn" id="tenure"  onkeypress="return isNumberKey(event)"></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                    <input name="effective_rate" value="{{$pra->package->effective_rate}}" type="hidden" required class="form-control fn" id="effective_rate"  onkeypress="return isNumberKey(event)">
                                                                <tr>
                                                                    <td>Prescribed Rate/ Package ( % )</td>
                                                                    <td><input name="rate" value="{{number_format((float)$dsr_b->rate, 2)}}" type="tel" required class="form-control fn" id="rate" onchange="toFloat('rate')" readonly onkeypress="return isNumberKey(event)"></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Monthly Repayment (MBSB)</td>
                                                                    <td><input readonly name="monthly_repayment" value="{{$dsr_b->monthly_repayment}}" type="tel" required class="form-control fn" id="monthly_repayment"  onkeypress="return isNumberKey(event)"></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" style="font-style: italic;font-size: 12px"><sup>*</sup>{{$pra->package->effective_rate}} % setahun (Kadar Efektif) / {{$pra->package->flat_rate}} % setahun (Kadar Tetap untuk 3 tahun)</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Existing Loan/Financing Exposure with MBSB</td>
                                                                    <td><input name="existing_loan" value="{{number_format((float)$dsr_b->existing_loan, 2)}}" type="tel" required class="form-control fn" id="existing_loan" onchange="toFloat('existing_loan')" onkeypress="return isNumberKey(event)"></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total Group Exposure</td>
                                                                    <td><input readonly name="total_group_exposure" value="{{number_format((float)$dsr_b->total_group_exposure, 2)}}" type="text" required class="form-control fn" id="total_group_exposure" onchange="toFloat('total_group_exposure')" onkeypress="return isNumberKey(event)"></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <p><b><u>Section C- Commitment</u></b></p><br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <table border="0" class='table table-hover' cellspacing="2" cellpadding="2" style="width: 97% !important">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="45%"><b>MBSB Installments/Repayment</b></td>
                                                                    <td colspan="2">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="45%">Housing and Term Loan/Financing Commitment per month</td>
                                                                    <td width="20%"><input name="mbsb_housing" value="{{number_format((float)$dsr_c->mbsb_housing, 2)}}" type="tel" required class="form-control fn" id="mbsb_housing" onchange="toFloat('mbsb_housing')" onkeypress="return isNumberKey(event)"></td>
                                                                    <td width="35%">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Hire Purchase Commitment per month</td>
                                                                    <td><input name="mbsb_hire" value="{{number_format((float)$dsr_c->mbsb_hire, 2)}}" type="tel" required class="form-control fn" id="mbsb_hire" onchange="toFloat('mbsb_hire')" onkeypress="return isNumberKey(event)"></td><td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Personal Financing Commitment per month</td>
                                                                    <td><input name="mbsb_personal" value="{{number_format((float)$dsr_c->mbsb_personal, 2)}}" type="tel" required class="form-control fn" id="mbsb_personal" onchange="toFloat('mbsb_personal')" onkeypress="return isNumberKey(event)"></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="45%"><b>Deductions (from Payslip or other sources)</b></td>
                                                                    <td colspan="2">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="45%">ANGKASA /RAMCI Commitment per month</td>
                                                                    <td width="20%"><input name="de_angkasa" value="{{number_format((float)$dsr_c->de_angkasa, 2)}}" type="tel" onkeypress="return isNumberKey(event)" required class="form-control Currency" id="de_angkasa"></td>
                                                                    <td width="35%">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Government Housing Loan Commitmentper month</td>
                                                                    <td><input name="de_govhousing" value="{{number_format((float)$dsr_c->de_govhousing, 2)}}" type="tel" onkeypress="return isNumberKey(event)" required class="form-control Currency" id="de_govhousing" ></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>KOPERASI Commitment per month</td>
                                                                    <td><input name="de_koperasi" value="{{number_format((float)$dsr_c->de_koperasi, 2)}}" type="tel" onkeypress="return isNumberKey(event)" required class="form-control Currency" id="de_koperasi"></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Non Biro Commitment per month</td>
                                                                    <td><input name="de_nonbiro" value="{{number_format((float)$dsr_c->de_nonbiro, 2)}}" type="tel" onkeypress="return isNumberKey(event)" required class="form-control Currency" id="de_nonbiro"></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Other Loan Commitments per month</td>
                                                                    <td><input name="de_otherloan" value="{{number_format((float)$dsr_c->de_otherloan, 2)}}" type="tel" onkeypress="return isNumberKey(event)" required class="form-control Currency" id="de_otherloan"></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Other Commitments per month</td>
                                                                    <td><input name="de_other" value="{{number_format((float)$dsr_c->de_other, 2)}}" type="tel" onkeypress="return isNumberKey(event)" required class="form-control Currency" id="de_other"></td><td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="45%"><b>CCRIS</b></td>
                                                                    <td colspan="2">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="45%">Housing and Term Loan Commitment per month</td>
                                                                    <td width="20%"><input name="ccris_housing" value="{{number_format((float)$dsr_c->ccris_housing, 2)}}" type="tel" onkeypress="return isNumberKey(event)" required class="form-control Currency" id="ccris_housing"></td>
                                                                    <td width="35%">&nbsp;</td>
                                                                </tr>
                                                                <tr> 
                                                                    <td>Credit Card / Charge Card Commitment per month</td>
                                                                    <td><input name="ccris_creditcard" value="{{number_format((float)$dsr_c->ccris_creditcard, 2)}}" type="text" required class="form-control Currency" id="ccris_creditcard"></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Hire Purchase Commitment per month</td>
                                                                    <td><input name="ccris_hire" value="{{number_format((float)$dsr_c->ccris_hire, 2)}}" type="tel" onkeypress="return isNumberKey(event)" required class="form-control Currency" id="ccris_hire"></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Overdraft Commitment per month</td>
                                                                    <td><input name="ccris_otherdraft" value="{{number_format((float)$dsr_c->ccris_otherdraft, 2)}}" type="tel" onkeypress="return isNumberKey(event)" required class="form-control Currency" id="ccris_otherdraft"></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Other Loan Commitments per month</td>
                                                                    <td><input name="ccris_other" value="{{number_format((float)$dsr_c->ccris_other, 2)}}" type="tel" onkeypress="return isNumberKey(event)" required class="form-control Currency" id="ccris_other"></td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>Total Actual Commitment</b></td>
                                                                    <td>&nbsp;</td>
                                                                    <td><input readonly name="total_commitment" value="{{number_format((float)$dsr_c->total_commitment, 2)}}" type="tel" onkeypress="return isNumberKey(event)" required class="form-control Currency" id="total_commitment" ></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <p><b><u>Section D - Verification for Section B & C</u></b></p><br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <table border="0" class='table table-hover' cellspacing="2" cellpadding="2" style="width: 97% !important">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="45%">Verified by Processing Officer</td>
                                                                    <td>
                                                                        <?php 
                                                                            if(empty($dsr_d->sec2_verified_by)) {
                                                                                $sec2_verified_by = $user->name;
                                                                            }
                                                                            else {
                                                                                $sec2_verified_by = $dsr_d->sec2_verified_by;
                                                                            }
                                                                        ?>
                                                                        <input readonly type="text" name="sec2_verified_by" maxlength="" required="required" class="form-control fn" placeholder="RM"  value="{{$sec2_verified_by}}"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Date & Time</td>
                                                                    <td>
                                                                        <?php 
                                                                            if($dsr_d->sec2_verified_time=="2018-01-01 00:00:00") {
                                                                                $sec2_verified_time = "Auto Update When Save";
                                                                            }
                                                                            else {
                                                                                $sec2_verified_time = $dsr_d->sec2_verified_time;
                                                                            }
                                                                        ?>
                                                                        <input readonly type="text" name="sec2_verified_time" required="required" class="form-control fn" placeholder="RM" value="{{$sec2_verified_time}}" id=""></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <p><b><u>Section E - DSR Scoring</u></b></p><br>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <table border="0" class='table table-hover' cellspacing="2" cellpadding="2" style="width: 97% !important">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="45%">TOTAL DEBT</td>
                                                                    <td><input readonly name="total_debt" value="{{number_format((float)$dsr_e->total_debt, 2)}}" type="text" required class="form-control fn" id="total_debt" onchange="toFloat('total_debt')" onkeypress="return isNumberKey(event)"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="45%">NET INCOME</td>
                                                                    <td><input readonly name="net_income" value="{{number_format((float)$dsr_e->net_income, 2)}}" type="text" required class="form-control fn" id="net_income" onchange="toFloat('net_income')" onkeypress="return isNumberKey(event)"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="45%">DSR (Based on BNM Calculation)</td>
                                                                    <td>
                                                                        <input readonly name="dsr" value="{{$dsr_e->dsr}}" type="text" required class="form-control fn" id="dsr">
                                                                        <input readonly name="dsr_bnm" value="{{number_format((float)$dsr_e->dsr_bnm, 4, '.', '')}}" type="hidden" required class="form-control fn" id="dsr_bnm" onchange="toFloat('dsr_bnm')" onkeypress="return isNumberKey(event)">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="45%">NET DISPOSABLE INCOME (NDI)</td>
                                                                    <td>
                                                                        <input readonly name="ndi" value="{{number_format((float)$dsr_e->ndi, 2)}}" type="text" required class="form-control fn" id="ndi" onchange="toFloat('ndi')" onkeypress="return isNumberKey(event)">
                                                                        <input readonly name="ndi_use" value="{{number_format((float)$dsr_e->ndi, 2)}}" type="hidden" required class="form-control fn" id="ndi_use" onchange="toFloat('ndi_use')" onkeypress="return isNumberKey(event)">
                                                                     </td>
                                                                 </tr>
                                                                 <tr>
                                                                    <td width="45%">DECISION</td>
                                                                    <td><input readonly name="decision" value="{{$dsr_e->decision}}" type="text" required class="form-control fn" id="decision"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="45%">Date and Time </td>
                                                                    <td> 
                                                                        <?php 
                                                                        if($dsr_e->data_save=="2018-01-01 00:00:00") {
                                                                            $data_save = "Auto Update When Save";
                                                                        }
                                                                        else {
                                                                            $data_save = $dsr_e->data_save;
                                                                        }
                                                                        ?>
                                                                        <input readonly type="text" required="required" class="form-control fn" placeholder="RM" name="data_save" value="{{$data_save}}" id="">
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <!-- End Step 2 -->
                                    <div class="tab-pane" id="tab3">
                                        <br>
                                        <h3><strong>Net Disposable Income (NDI) Statement</strong></h3>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input name="id_praapplication" type="hidden"  value="{{$pra->id}}">
                                        <fieldset><br>
                                            <div class="container" style="">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <table border="0" class='table table-hover' cellspacing="2" cellpadding="2">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="45%">Basic Salary </td>
                                                                    <td><b><input readonly name="ndi_basic_salary" value="{{$dsr_a->basic_salary3}}" type="text" required class="form-control Currency" id="ndi_basic_salary" ></b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td >Fixed Allowance</td>
                                                                    <td>
                                                                        <input readonly name="ndi_fixed_allowance" value="{{number_format((float)$dsr_a->fixed_allowances3, 2, '.', '')}}" type="text" required class="form-control fn" id="ndi_fixed_allowance">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >Variable income</td>
                                                                    <td>
                                                                        <input readonly name="ndi_variable_income" value="{{number_format((float)$dsr_a->variable_income, 2, '.', '')}}" type="text" required class="form-control fn" id="ndi_variable_income">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >Non-Salaried Income</td>
                                                                    <td>
                                                                        <input readonly name="ndi_non_salaried" value="{{number_format((float)$dsr_a->additional_income, 2, '.', '')}}" type="text" required class="form-control fn" id="ndi_non_salaried">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Gross Income</td>
                                                                    <td>
                                                                        <b><input readonly name="ndi_gross_income" value="{{number_format((float)$dsr_a->total_gross_income, 2, '.', '')}}" type="text" required class="form-control fn" id="ndi_gross_income"></b>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><i>&nbsp;&nbsp; - Deduct: Statutory Deductions</i></td>
                                                                    <td>
                                                                        <input readonly name="ndi_deduction" value="({{number_format((float)$dsr_a->total_deduction3, 2, '.', '')}})" type="text" required class="form-control fn" id="ndi_deduction">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><i>&nbsp;&nbsp; - Deduct: Income Tax Estimation (Non-Salaried Income)</i></td>
                                                                    <td>
                                                                        <input readonly name="ndi_ite" value="({{number_format((float)$dsr_a->ite, 2, '.', '')}})" type="text" required class="form-control fn" id="ndi_ite">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >Net Income after Statutory Deductions</td>
                                                                    <td>
                                                                        <b><input readonly name="ndi_net_income" value="{{number_format((float)$dsr_a->total_net_income, 2, '.', '')}}" type="text" required class="form-control fn" id="ndi_net_income"></b>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><i>&nbsp;&nbsp; - Deduct: Current Banking Commitments</i></td>
                                                                    <td>
                                                                        <input readonly name="ndi_commitment" value="({{number_format((float)$dsr_c->total_commitment, 2, '.', '')}})" type="text" required class="form-control fn" id="ndi_commitment">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><i>&nbsp;&nbsp; - Deduct: Commitment to this proposal</i></td>
                                                                    <td>
                                                                        <input readonly name="ndi_repayment" value="({{number_format((float)$dsr_b->monthly_repayment, 2, '.', '')}})" type="text" required class="form-control fn" id="ndi_repayment" >
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Net Disposable Income</td>
                                                                    <td>
                                                                        <b> <input readonly name="ndi_ndi" value="{{number_format((float)$dsr_e->ndi, 2, '.', '')}}" type="text" required class="form-control fn" id="ndi_ndi"></b>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <h3><strong>Risk Rating for Personal Financing</strong></h3><br>
                                                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                                        <table border="0" class='table table-hover' cellspacing="2" cellpadding="2">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="45%">Debt Service Ratio (DSR)</td>
                                                                    <td>
                                                                        <input readonly name="rr_dsr" value="{{$dsr_e->dsr}}" type="text" required class="form-control fn" id="rr_dsr" onchange="toFloat('rr_dsr')" onkeypress="return isNumberKey(event)">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Net Disposable Income</td>
                                                                    <td>
                                                                        <input readonly name="rr_ndi" value="{{number_format((float)$dsr_e->ndi, 2, '.', '')}}" type="text" required class="form-control fn" id="rr_ndi">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >Salary Deduction</td>
                                                                    <td>
                                                                        <select name="rr_salary_deduction" id="rr_salary_deduction" placeholder="Please Select Salady Deduction" class="form-control" REQUIRED>
                                                                            <option value='noselect1' selected disabled>Please Select</option>
                                                                            @foreach($salary_deduction as $sal) 
                                                                                @if($riskrating->salary_deduction==$sal->id) 
                                                                                    <?php $selected = "selected";?>
                                                                                @else 
                                                                                    <?php $selected = ""; ?>
                                                                                @endif
                                                                                <option {{$selected}} value="{{$sal->id}}" {{$selected}}>{{$sal->value}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Age</td>
                                                                    <td>
                                                                        <select name="rr_age" id="rr_age" placeholder="Please Select Age" class="form-control" REQUIRED>
                                                                            <option value='noselect2' selected disabled>Please Select</option>
                                                                            @foreach($age as $rr_age) 
                                                                                @if($riskrating->age==$rr_age->id) 
                                                                                    <?php $selected = "selected"; ?>
                                                                                @else 
                                                                                    <?php $selected = ""; ?>
                                                                                @endif
                                                                                <option {{$selected}} value="{{$rr_age->id}}">{{$rr_age->value}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Education Level</td>
                                                                    <td>
                                                                        <select name="rr_edu_level" id="rr_edu_level" placeholder="Please Select Education Level" class="form-control" REQUIRED>
                                                                            <option value='noselect3' selected disabled>Please Select</option>

                                                                                @foreach($edu_level as $rr_edu_level)
                                                                                    @if($riskrating->education_level==$rr_edu_level->id) 
                                                                                        <?php $selected = "selected"; ?>
                                                                                    @else 
                                                                                        <?php $selected = ""; ?>
                                                                                    @endif 
                                                                                    <option {{$selected}} value="{{$rr_edu_level->id}}">{{$rr_edu_level->value}}</option>
                                                                                @endforeach
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Employment</td>
                                                                    <td>
                                                                        <select name="rr_emp" id="rr_emp" placeholder="Please Select" class="form-control" REQUIRED>
                                                                            <option value='noselect4' selected disabled>Please Select</option>
                                                                            @foreach($emp as $rr_emp) 
                                                                                @if($riskrating->employment==$rr_emp->id) 
                                                                                    <?php $selected = "selected"; ?>
                                                                                @else 
                                                                                    <?php $selected = ""; ?>
                                                                                @endif 
                                                                            <option {{$selected}} value="{{$rr_emp->id}}">{{$rr_emp->value}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Position</td>
                                                                    <td>
                                                                        <select name="rr_position" id="rr_position" placeholder="Please Select" class="form-control" REQUIRED>
                                                                            <option value='noselect5' selected disabled>Please Select</option>
                                                                                @foreach($position as $rr_position) 
                                                                                    @if($riskrating->position==$rr_position->id) 
                                                                                        <?php $selected = "selected"; ?>
                                                                                    @else 
                                                                                        <?php $selected = ""; ?>
                                                                                    @endif 
                                                                                    <option {{$selected}} value="{{$rr_position->id}}">{{$rr_position->value}}</option>
                                                                                @endforeach
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >Marital Status</td>
                                                                    <td>
                                                                        <select name="rr_marital" id="rr_marital" placeholder="Please Select" class="form-control" REQUIRED>
                                                                            <option value='noselect6' selected disabled>Please Select</option>
                                                                            @foreach($marital as $rr_marital) 
                                                                                @if($riskrating->marital_status==$rr_marital->id) 
                                                                                    <?php $selected = "selected"; ?>
                                                                                @else 
                                                                                    <?php $selected = ""; ?>
                                                                                @endif 
                                                                            <option {{$selected}} value="{{$rr_marital->id}}">{{$rr_marital->value}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Spouse Employment</td>
                                                                    <td>
                                                                        <select name="rr_spouse_emp" id="rr_spouse_emp" placeholder="Please Select" class="form-control" REQUIRED>
                                                                            <option value='noselect7' selected disabled>Please Select</option>
                                                                            @foreach($spouse_emp as $rr_spouse_emp) 
                                                                                @if($riskrating->spouse_employment==$rr_spouse_emp->id) 
                                                                                    <?php $selected = "selected"; ?>
                                                                                @else 
                                                                                    <?php $selected = ""; ?>
                                                                                @endif 
                                                                                <option {{$selected}} value="{{$rr_spouse_emp->id}}">{{$rr_spouse_emp->value}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >Property Loan Remaining</td>
                                                                    <td>
                                                                        <select name="rr_property_loan" id="rr_property_loan" placeholder="Please Select" class="form-control" REQUIRED>
                                                                            <option value='noselect8' selected disabled>Please Select</option>
                                                                            @foreach($property_loan as $rr_property_loan) 
                                                                                @if($riskrating->property_loan==$rr_property_loan->id) 
                                                                                    <?php $selected = "selected"; ?>
                                                                                @else 
                                                                                    <?php $selected = ""; ?>
                                                                                @endif
                                                                                <option {{$selected}} value="{{$rr_property_loan->id}}">{{$rr_property_loan->value}}</option>
                                                                             @endforeach
                                                                         </select>
                                                                     </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >Adverse CCRIS Parameters</td>
                                                                    <td>
                                                                        <select name="rr_adverse_ccris" id="rr_adverse_ccris" placeholder="Please Select" class="form-control" REQUIRED>
                                                                        <option value='noselect9' selected disabled>Please Select</option>
                                                                            @foreach($adverse_ccris as $rr_adverse_ccris) 
                                                                                @if($riskrating->adverse_ccris==$rr_adverse_ccris->id) 
                                                                                <?php $selected = "selected"; ?>
                                                                                @else 
                                                                                <?php $selected = ""; ?>
                                                                                 @endif
                                                                            <option {{$selected}} value="{{$rr_adverse_ccris->id}}">{{$rr_adverse_ccris->value}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> &nbsp;</td>
                                                                    <td>
                                                                        <a id="btn_scoring" class="btn btn-lg btn-success">Scoring</a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- new row -->
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <table border="0" class='table table-hover' cellspacing="2" cellpadding="2">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="45%">Total Score</td>
                                                                    <td>
                                                                        <input readonly name="total_score" value="{{$riskrating->total_score}}" type="text" required class="form-control fn" id="total_score">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td >Grading</td>
                                                                    <td>
                                                                        <input readonly name="grading" value="{{$riskrating->grading}}" type="text" required class="form-control fn" id="grading">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Decision</td>
                                                                    <td>
                                                                        <input readonly name="final_decision" value="{{$riskrating->decision}}" type="text" required class="form-control" id="final_decision">
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <!-- End Step 2 -->
                                    <div class="tab-pane" id="tab1">
                                        <br>
                                        <h3><strong>Documents</strong></h3>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <fieldset>
                                            <div class="container" style="">
                                                <input name="id_praapplication" type="hidden"  value="{{$pra->id}}">
                                                <div class="row">
                                                    <label class="col-md-6 control-label">Salinan Penyata Gaji  Bulan Terkini <sup>*</sup></label>
                                                    <div class="col-md-2">
                                                        <input id="fileupload7"   @if(empty($document7->name)) required  @endif   class="btn btn-default" type="file" name="file7">
                                                        <input type="hidden" name="document7"  id="documentx7"  value="SALINAN PENYATA GAJI UNTUK 3 BULAN TERKINI">
                                                        &nbsp; <span id="document7"> </span> 
                                                        @if(!empty($document7->name))
                                                           <span id="document7a"><a target='_blank' class="hidden-xs hidden-sm"  href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id)}}/{{$document7->upload}}"> {{$document7->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                                                           <a target='_blank' class="hidden-md hidden-lg" href="{{url('/')}}/admin/get_document/{{str_replace('/', '', $pra->id)}}/{{$document7->upload}}"> {{$document7->name}} 2</a>
                                                        @endif
                                                    </div>
                                                </div><br>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <div class="tab-pane" id="tab4">
                                        <br>
                                        <h3><strong>Approval/Rejected Documents</strong> </h3>
                                        <input name="send_status" id="send_status" type="hidden"  value="4">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-11 col-lg-11">
                                                <div class="form-group">
                                                    <label><b>Remark / Reason :</b></label>
                                                    <textarea onkeyup="this.value = this.value.toUpperCase()" id='remark_verification' class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 183px;" name='verification_remark'>{{$term->verification_remark}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="_token" id='token_term' value="{{ csrf_token()}}">
                                        <input name="id_praapplication" id="id_praapplication_term" type="hidden"  value="{{$pra->id}}">
                                        <input name="dec_sendapp" id="dec_sendapp" type="hidden"  value="1">
                                        
                                        @if($view!='view')
                                        <p class="center"><a id="sendapp" class="btn btn-primary">
                                            <i> Approve </i></a> 
                                            <a id="sendreject" class="btn btn-danger">
                                            <i> Rejected </i> </a> </p>
                                        @else
                                        <p class="center"><a href='{{url('/')}}/admin' class="btn btn-default">
                                            <i> Back to Dashboard</i></a> </p>
                                        @endif
                                    </div>
                                    <div class="form-actions">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <ul class="pager wizard no-margin">
                                                <li class="next">
                                                    <a href="javascript:void(0);" class="btn btn-lg txt-color-green"> Seterusnya / <i> Next </i> </a>
                                                </li>
                                                <li class="previous ">
                                                    <a href="javascript:void(0);" class="btn btn-lg btn-default"> Sebelum / <i> Previous </i> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</article>
</div>
</section>
</div> </div>
<br>

      


        @include('admin/js')

        
     <script type="text/javascript">
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    $(document).ready(function() {
        pageSetUp();
        //Bootstrap Wizard Validations
        var $validator = $("#wizard-1").validate({
            rules: {
                email: {
                    required: false,
                     email: "Your email address must be in the format of name@domain.com"
                },
                fname: {
                    required: true
                },
                lname: {
                    required: true
                },
                country: {
                    required: true
                },
                city: {
                    required: false
                },
                postcode: {
                    required: false,
                    minlength: 4
                },
                wphone: {
                    required: true,
                    minlength: 10
                },
                hphone: {
                    required: true,
                    minlength: 10
                }
            },
            messages: {
                fname: "Please specify your First name",
                lname: "Please specify your Last name",
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } 
                else {
                    error.insertAfter(element);
                }
            }
        });
        $('#bootstrap-wizard-1').bootstrapWizard({
            'tabClass': 'form-wizard',
            'onNext': function (tab, navigation, index) {
                var $valid = $("#wizard-1").valid();

                if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                } 
                else {
                    var new_ic_basic_v = $('input[name=new_ic_basic_v]:checked').val();
                    var name_v = $('input[name=name_v]:checked').val();
                    var handphone_v = $('input[name=handphone_v]:checked').val();
                    @if($view=='verify')
                        if (new_ic_basic_v=='0' || name_v=='0' || handphone_v=='0') {
                            alert('Please make sure all field are valid !');
                            $validator.focusInvalid();
                            return false;
                        }
                    @endif
                    $.ajax({
                        type: "PUT",
                        url: '{{ url('/verifiedDoc') }}'+'/'+index,
                        data: $('#tab'+index+' :input').serialize(),
                        cache: false,
                        beforeSend: function () {
                        },
                        success: function () {
                        },

                        error: function () {}
                    });
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                        'complete');
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                    .html('<i class="fa fa-check"></i>');
                }
            }
        });
        // fuelux wizard
        var wizard = $('.wizard').wizard();
            wizard.on('finished', function (e, data) {
            //$("#fuelux-wizard").submit();
            //console.log("submitted!");
            $.smallBox({
                title: "Congratulations! Your form was submitted",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
        });
    })
</script>
<!-- Your GOOGLE ANALYTICS CODE Below -->
<script type="text/javascript">
    var _gaq = _gaq || [];
     _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
     _gaq.push(['_trackPageview']);
     (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
         ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
         var s = document.getElementsByTagName('script')[0];
         s.parentNode.insertBefore(ga, s);
     })();
     $('.startdate').datepicker({
        dateFormat : 'dd/mm/yy',
        prevText : '<i class="fa fa-chevron-left"></i>',
         nextText : '<i class="fa fa-chevron-right"></i>',
         onSelect : function(selectedDate) {
            $('#finishdate').datepicker('option', 'minDate', selectedDate);
        }
    });
</script>

<?php 
    include("asset/inc/scripts.php"); 
?>

<script type="text/javascript">
    $( "#new_ic" ).keyup(function() {
        var ic      = $('#new_ic').val();
        var tanggal = ic.substr(4, 2);
        var bulan   = ic.substr(2, 2);
        var tahun   = ic.substr(0, 2);

        if(tahun > 30) {
            tahun2 = "19"+tahun;
        }
        else {
            tahun2 = "20"+tahun;
        }
        var dob = tahun2+'-'+bulan+'-'+tanggal; 
        dob = new Date(dob);
        var today = new Date();
        var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
        $("#age").val(age);
    });

    $( ".f3n" ).keyup(function() {
    // Income

        var fn_monthly_basic       = $('#fn_monthly_basic').val();
        var fn_govt_service        = $('#fn_govt_service').val();
        var fn_financial_incentive = $('#fn_financial_incentive').val();
        var fn_fixed_allowance     = $('#fn_fixed_allowance').val();
        var fn_housing_allowance   = $('#fn_housing_allowance').val();
        var fn_cost_of             = $('#fn_cost_of').val();
        var fn_special_allowance   = $('#fn_special_allowance').val();
        var fn_gross_income        = parseFloat(fn_monthly_basic) + parseFloat(fn_govt_service)  + parseFloat(fn_financial_incentive) + parseFloat(fn_fixed_allowance) + parseFloat(fn_housing_allowance) + parseFloat(fn_cost_of) + parseFloat(fn_special_allowance)  ;

        // Non fixed Variable

        var fn_overtime_allowance = $('#fn_overtime_allowance').val();
        var fn_travel             = $('#fn_travel').val();
        var fn_commission         = $('#fn_commission').val();
        var fn_business_income    = $('#fn_business_income').val();
        var fn_yearly_devidend    = $('#fn_yearly_devidend').val();
        var fn_food_allowance     = $('#fn_food_allowance').val();
        var fn_yearly_contractual = $('#fn_yearly_contractual').val();
        var fn_housing_building   = $('#fn_housing_building').val();
        var fn_non_fixed          = $('#fn_non_fixed').val();
        var fn_share_income       = $('#fn_share_income').val();
        var fn_total_income       = parseFloat(fn_overtime_allowance) + parseFloat(fn_travel)  + parseFloat(fn_commission)
              + parseFloat(fn_business_income) + parseFloat(fn_yearly_devidend) + parseFloat(fn_food_allowance) + parseFloat(fn_yearly_contractual)
              + parseFloat(fn_housing_building) + parseFloat(fn_non_fixed) + parseFloat(fn_share_income) + parseFloat(fn_gross_income) ;
        $("#fn_total_income").val(parseFloat(fn_total_income).toFixed(2));

        // Deductions Variable
        var fn_knwsp            = $('#fn_knwsp').val();
        var fn_income_tax       = $('#fn_income_tax').val();
        var fn_house_financing  = $('#fn_house_financing').val();
        var fn_others           = $('#fn_others').val();
        var fn_lembaga_tabung   = $('#fn_lembaga_tabung').val();
        var fn_insurance        = $('#fn_insurance').val();
        var fn_perkeso          = $('#fn_perkeso').val();
        var fn_zakat            = $('#fn_zakat').val();
        var fn_bpa              = $('#fn_bpa').val();
        var fn_asb              = $('#fn_asb').val();

        // Total Deductions
        var fn_total_deductions = parseFloat(fn_knwsp) + parseFloat(fn_income_tax) + parseFloat(fn_house_financing)
            + parseFloat(fn_others) + parseFloat(fn_lembaga_tabung) + parseFloat(fn_insurance) + parseFloat(fn_perkeso)
            + parseFloat(fn_zakat) + parseFloat(fn_bpa) + parseFloat(fn_asb);
            $("#fn_total_deductions").val(parseFloat(fn_total_deductions).toFixed(2));

        // Total net income
        var fn_total_income     = $('#fn_total_income').val();
        var fn_total_deductions = $('#fn_total_deductions').val();
        var fn_total_net        = parseFloat(fn_total_income) - parseFloat(fn_total_deductions);
            $("#fn_total_net").val(parseFloat(fn_total_net).toFixed(2));

        // Bayaran Balik
        var pemrumah1     = $('#pemrumah1').val();
        var pemrumah2    = $('#pemrumah2').val();
        var pemrumah3    = $('#pemrumah3').val();
        var kadkredit1   = $('#kadkredit1').val();
        var kadkredit2   = $('#kadkredit2').val();
        var kadkredit3   = $('#kadkredit3').val();
        var pempribadi   = $('#pempribadi').val();
        var pemperabot   = $('#pemperabot').val();
        var pemkenderaan = $('#pemkenderaan').val();
        var pemlain = $('#pemlain').val();
        var jumlahpembiayaan = parseFloat(pemrumah1) + parseFloat(pemrumah2)
            + parseFloat(pemrumah3) + parseFloat(kadkredit1) + parseFloat(kadkredit2) + parseFloat(kadkredit3) + parseFloat(pempribadi) + parseFloat(pemperabot) + parseFloat(pemkenderaan) + parseFloat(pemlain);

        $("#jumlahpembiayaan").val(parseFloat(jumlahpembiayaan).toFixed(2));

        // Langkah 1

        $("#pendapatan").val(parseFloat(fn_total_income).toFixed(2));
        $("#kwsp").val(parseFloat(fn_knwsp).toFixed(2));
        $("#perkeso").val(parseFloat(fn_perkeso).toFixed(2));
        $("#cukai_pendapatan").val(parseFloat(fn_income_tax).toFixed(2));
        $("#zakat_pendapatan").val(parseFloat(fn_zakat).toFixed(2));
        $("#lainlain1").val(parseFloat(fn_others).toFixed(2));

        var lainlain2= parseFloat(fn_knwsp) + parseFloat(fn_perkeso) + parseFloat(fn_income_tax)
        + parseFloat(fn_zakat) + parseFloat(fn_others);
        $("#lainlain2").val(parseFloat(lainlain2).toFixed(2));
        
        var jumlah_pendapatan= parseFloat(fn_total_income) - parseFloat(lainlain2);
        $("#jumlah_pendapatan").val(parseFloat(jumlah_pendapatan).toFixed(2));

        // Langkah 2

        $("#l2_pemrumah").val(parseFloat(fn_house_financing).toFixed(2));
        $("#l2_bps").val(parseFloat(fn_bpa).toFixed(2));
        $("#l2_pemdarib").val(parseFloat(jumlahpembiayaan).toFixed(2));
        var l2_jumlahbayaran= parseFloat(fn_house_financing) + parseFloat(fn_bpa) + parseFloat(jumlahpembiayaan);
        $("#l2_jumlahbayaran").val(parseFloat(l2_jumlahbayaran).toFixed(2));

        // Langkah 3

        var l3_bkr = $('#l3_bkr').val();
        var l3_koperasi = $('#l3_koperasi').val();
        var l3_bank = $('#l3_bank').val();
        var l3_lainlain = $('#l3_lainlain').val();
        var l3_jumlah = parseFloat(l3_bkr) + parseFloat(l3_koperasi) + parseFloat(l3_bank)  + parseFloat(l3_lainlain);
        $("#l3_jumlah").val(parseFloat(l3_jumlah).toFixed(2));

        // Langkah 4

        $("#l4_jumlahbayaran").val(parseFloat(l2_jumlahbayaran).toFixed(2));
        $("#l4_jumlah_pendapatan").val(parseFloat(jumlah_pendapatan).toFixed(2));

        var l4_dsr  = (parseFloat(l2_jumlahbayaran) / parseFloat(jumlah_pendapatan)) * 100;
        $("#l4_dsr").val(parseFloat(l4_dsr).toFixed(2));

        var l4_jumlahpinjam = $('#l4_jumlahpinjam').val();
        var l4_jumlahpinjam2= $('#l4_jumlahpinjam2').val();
        var l4_tempoh       = $('#l4_tempoh').val();
        var l4_kadar        = $('#l4_kadar').val();
        var l4_tempohbulan  = parseFloat(l4_tempoh) * 12;
        $("#l4_tempohbulan").val(parseInt(l4_tempohbulan));

        var l4_ansuran_total = parseFloat(l4_jumlahpinjam) * parseFloat(l4_tempoh)* (parseFloat(l4_kadar) / 100) + parseFloat(l4_jumlahpinjam);
        var l4_ansuran = parseFloat(l4_ansuran_total) / parseFloat(l4_tempohbulan);
        $("#l4_ansuran").val(parseFloat(l4_ansuran).toFixed(2));

        var l4_bayaranbalik = parseFloat(l2_jumlahbayaran) + parseFloat(l4_ansuran)- parseFloat(l3_jumlah);
        $("#l4_bayaranbalik").val(parseFloat(l4_bayaranbalik).toFixed(2));

        var l4_jumlah_pendapatan2 = parseFloat(jumlah_pendapatan);
        $("#l4_jumlah_pendapatan2").val(parseFloat(l4_jumlah_pendapatan2).toFixed(2));

        var l4_dsr2 = (parseFloat(l4_bayaranbalik) / parseFloat(l4_jumlah_pendapatan2)) * 100;
        $("#l4_dsr2").val(parseFloat(l4_dsr2).toFixed(2));
    });
</script>

<script>
function getDecision() {
    var category = $('#category').val();
    var existing_mbsb = $('#existing_mbsb').val();
    var ndi = $('#ndi_use').val();
    var gmi = $('#gmi_use').val();
    var dsr = $('#dsr_bnm').val();
    var sector = $('#sector').val();

        if(sector=="G") {
            if(category=="BIRO") {
                if(existing_mbsb=="NO") {
                    if(gmi<3000) {
                        $("#decision").val("FAIL. New BIRO Customer, Gross Income < RM3,000."); 
                    }
                    else if(gmi>=3000) { 
                        if(dsr>85.0000) {
                            $("#decision").val("FAIL. Max DSR 85%");
                        }
                        else {
                            if(ndi<1300) {
                                $("#decision").val("FAIL. New BIRO Customer, NDI < RM1,300.");
                            }
                            else if(isNaN(ndi)) {
                              $("#decision").val(" ");
                            }
                            else {
                                $("#decision").val("PASS");  
                            }
                        }            
                    }
                }
                else if(existing_mbsb=="YES") {
                    if(gmi<1500) {
                        $("#decision").val("FAIL. Existing BIRO Customer, Gross Income < RM1,500.");
                    }
                    else if(gmi>=1500) {
                        if(gmi>=2000 && gmi<=3000 && dsr>60.000) {
                            $("#decision").val("FAIL. Gross Income 2000 - 3000, Max DSR 60%");
                        }
                        else if(gmi>3000 && dsr>85.000) {
                            $("#decision").val("FAIL. Gross Income > 3000, Max DSR 85%");
                        }
                        else {
                            if(ndi<1000) {
                                $("#decision").val("FAIL. Existing BIRO Customer, NDI < RM1,000.");
                            }
                            else if(isNaN(ndi)) {
                                $("#decision").val(" ");
                            }
                            else {
                                $("#decision").val("PASS");  
                            }
                        }
                    }
                }
                else {
                    $("#decision").val("Please select Existing MBSB PF-I customer refinancing their existing PF-I facility with MBSB (internal overlap) option in Section A");   
                }
            }
            else if(category=="NON-BIRO") {
                if(gmi<3000) {
                    $("#decision").val("FAIL. Non-Biro Customer, Gross Income < RM3,000.");
                }
                else if(gmi>=3000) {
                    if(dsr>85.000) {
                        $("#decision").val("FAIL. Max DSR 85%");
                    }
                    else {
                       if(ndi<1300) {
                            $("#decision").val("FAIL. Non-Biro Customer, NDI < RM1,300.");
                        }
                         else if(isNaN(ndi)) {
                            $("#decision").val(" ");
                        }
                        else {
                            $("#decision").val("PASS");  
                        }
                    }
                }
            }
            else {
                $("#decision").val("Please select category option in Section A");  
            }
        }
        else if(sector=="P") {
            if(gmi<3500) {
                          $("#decision").val("FAIL. Private Gross Income < RM3,500.");
            }
        else if(gmi>=3500) {
            if(gmi<10000 && dsr>65.000) {
                $("#decision").val("FAIL. For Gross Income less than RM 10,000, Max DSR is 65%");
            }
            else if(gmi>=10000 && dsr>80.000) {
                $("#decision").val("FAIL. For Gross Income more than RM 10,000, Max DSR is 80%");
            }
            else {
                if(ndi<1300) {
                    $("#decision").val("FAIL. Customer's NDI < RM1,300.");
                }
                else if(isNaN(ndi)) {
                    $("#decision").val(" ");
                }
                else {
                    $("#decision").val("PASS");  
                }
            }
        }
    }
}

function ndi_statement() {
     /*     $("#ndi_basic_salary").val(parseFloat($('#a').val()).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
    $("#ndi_fixed_allowance").val(parseFloat($('#b').val()).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
    $("#ndi_variable_income").val(parseFloat($('#variable_income').val()).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
    $("#ndi_non_salaried").val(parseFloat($('#additional_income').val()).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
    $("#ndi_gross_income").val(parseFloat($('#total_gross_income').val()).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

    $("#ndi_deduction").val('('+parseFloat($('#total_deduction3').val()).toFixed(2)+')');
    $("#ndi_ite").val('('+parseFloat($('#ite').val()).toFixed(2)+')');
    $("#ndi_net_income").val(parseFloat($('#net_income').val()).toFixed(2));
    $("#ndi_commitment").val('('+parseFloat($('#total_commitment').val()).toFixed(2)+')');
    $("#ndi_repayment").val('('+parseFloat($('#monthly_repayment').val()).toFixed(2)+')');
    $("#ndi_ndi").val(parseFloat($('#ndi').val()).toFixed(2));
    */
    //risk rating
    $("#rr_dsr").val($('#dsr').val());
    $("#rr_ndi").val(parseFloat($('#ndi_use').val()).toFixed(2));
}

$( ".fin" ).change(function() {
    getDecision();
});

$( "#btn_scoring" ).click(function() {

    var sector           = $('#sector').val();
    var dsr              = $('#dsr_bnm').val();
    var ndi              = $('#rr_ndi').val();
    var salary_deduction = $('#rr_salary_deduction').val();
    var age              = $('#rr_age').val();
    var edu_level        = $('#rr_edu_level').val();
    var emp              = $('#rr_emp').val();
    var position         = $('#rr_position').val();
    var marital          = $('#rr_marital').val();
    var spouse_emp       = $('#rr_spouse_emp').val();
    var property_loan    = $('#rr_property_loan').val();
    var adverse_ccris    = $('#rr_adverse_ccris').val();
    var _token           = $('#_token').val();
    var decision         = $('#decision').val();

    if(salary_deduction== null) {
        bootbox.alert("Please Select Salary Deduction Option");
    }
    else if(age== null) {
        bootbox.alert("Please Select Age Option");
    }
    else if(edu_level== null) {
        bootbox.alert("Please Select Education Level Option");
    }
    else if(emp== null) {
        bootbox.alert("Please Select Employment Option");
    }

    else if(position== null) {
        bootbox.alert("Please Select Position Option");
    }
    else if(marital== null) {
        bootbox.alert("Please Select Marital Status Option");
    }
    else if(spouse_emp== null) {
        bootbox.alert("Please Select Spouse Employment Option");
    }
    else if(property_loan== null) {
        bootbox.alert("Please Select Property Loan Remaining Option");
    }
    else if(adverse_ccris== null) {
        bootbox.alert("Please Select Adverse CCRIS Parameters Option");
    }

    $.ajax({
        type: "POST",
        url: "{{ url('/admin/scoring/') }}",
        data: { sector: sector, dsr: dsr, _token : _token, ndi : ndi, salary_deduction : salary_deduction, age : age, edu_level : edu_level, emp : emp, position : position, marital : marital, spouse_emp : spouse_emp, property_loan : property_loan, adverse_ccris : adverse_ccris, decision : decision },
        cache: false,
        beforeSend: function () {
        },
        success: function (data) {
             $("#total_score").val(data.total_score );
             $("#grading").val(data.grading );
              $("#final_decision").val(data.final_decision );
              //  alert(data.total_score);
        },
        error: function () {}
        });              
    });

    $( "#sector" ).change(function() {
        var sector = $('#sector').val();
        var id_praapplication = $('#id_pra').val();
        var _token = $('#_token').val();
        $.ajax({
            type: "POST",
            url: "{{ url('/admin/sector/') }}",
            data: { sector: sector, id_praapplication: id_praapplication, _token : _token },
            cache: false,
            beforeSend: function () {
            },
            success: function (data) {
                location.reload();
                getDecision();
                //  alert(data.total_score);
            },
            error: function () {
            }
        });              
    });

    function minmax(value) 
    {
        if(parseInt(value) < 0 || isNaN(parseInt(value))) 
            return 0; 
        else if(parseInt(value) > 1000000) 
            return 100; 
        else return value;

    }
</script>

<script>
    $('.startmonth').datepicker({
        changeMonth: true,
        changeYear: true,
        changeDate: false,

        dateFormat: 'MM yy',
          onClose: function(dateText, inst) { 
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
            }
        });

   $(".startmonth").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });
</script>

<script src="{{url('/')}}/js/file/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="{{url('/')}}/js/file/jquery.fileupload.js"></script>

<?php for ($x = 1; $x <= 12; $x++) {  ?>

<script>
    $(function () {
        'use strict';
         var url = window.location.hostname === 'blueimp.github.io' ?'//jquery-file-upload.appspot.com/' : '{{url('/')}}/verifiedDoc/upload/{{$x}}';
         $('#fileupload{{$x}}').fileupload({
         url: url,
         dataType: 'json',
        success: function ( data) {
            var text = $('#documentx{{$x}}').val();
            $("#document{{$x}}").html("<a target='_blank' href='"+"{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->id)}}/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
            $("#document{{$x}}a").hide();
            alert(text+' telah berjaya dimuat naik');
            document.getElementById("fileupload{{$x}}").required = false;
        }
    }).prop('disabled', !$.support.fileInput)
         .parent().addClass($.support.fileInput ? undefined : 'disabled');
     });
</script>

<?php } ?>
<script type="text/javascript">
    $( "#agree" ).click(function() {

        var icnumber    = $('#icnumber').val();
        var kebenaran   = $('#kebenaran:checked').val();
        var declaration = $('#declaration:checked').val();
        var r           = prompt("Please enter your ic number" );
        var notice      = $('input[name="notice"]:checked').val();

        if (r == icnumber) {
            if(kebenaran > 0 && notice > 0 && declaration > 0) {
             $.ajax({
                type: "POST",
                url: '{{ url('/form/term/') }}',
                data: $('#tab9 :input').serialize(),
                cache: false,
                beforeSend: function () {},
                success: function () {
                    alert("success");
                    location.reload();
                },
                error: function () {}
                 });
        }
        else {
            alert("You must agree to the terms and conditions");
        }
        }
        else {
        alert("Incorrect, Please Retype Your IC Number !");
        }
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var found = [];
        $("select option").each(function() {
            if($.inArray(this.value, found) != -1) $(this).remove();
            found.push(this.value);
        });
    });
</script>

<script>
function isNumberKey(evt) {

          var theEvent = evt || window.event;

          var key = theEvent.keyCode || theEvent.which;

          key = String.fromCharCode(key);

          if (key.length == 0) return;

          var regex = /^[0-9.,\b]+$/;

          if (!regex.test(key)) {

              theEvent.returnValue = false;

              if (theEvent.preventDefault) theEvent.preventDefault();

          }





}





</script>





<script>

function toFloat(z) {

    var x = document.getElementById(z);

    //x.value = parseFloat(x.value).toFixed(2);

    x.value =parseFloat(x.value).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');

}

</script>





<script>

function alertWithoutNotice(message){

    setTimeout(function(){

        alert(message);

    }, 1000);

}

</script>







<script type="text/javascript">
    $( "#id_tenure" ).change(function() {
        var id_tenure = $('#id_tenure').val();
        id_tenure= $('#month_tenure').val(id_tenure);
    });
</script>

<script type="text/javascript">
$( "#sendapp" ).click(function() {
    var id_praapplication = $('#id_praapplication_term').val();
    var _token = $('#token_term').val();
    var remark = $('#remark_verification').val();
    var status = 77;
    if (remark=='') {
        alert("Please  Fill a Remark!");
    }
    else {
      var r = confirm("Are You Sure to Approve  this Documents");
      if (r == true) {
        $.ajax({
            type: "POST",
            url: '{{ url('/verifiedDoc/term/') }}',
            data: { status : status, id_praapplication: id_praapplication, _token : _token, remark : remark},
            cache: false,
            beforeSend: function () {
            },
            success: function () {
               alert("Send Success !");
               location.href='{{url('/')}}/admin';
           },
           error: function () {
           }
       });
    }
   }
});
</script>

<script type="text/javascript">
$( "#sendreject" ).click(function() {
    var id_praapplication = $('#id_praapplication_term').val();
    var _token = $('#token_term').val();
    var remark = $('#remark_verification').val();
    var status = 88;
    if (remark=='') {
        alert("Please  Fill a Remark!");

    }
    else {
        var r = confirm("Are You Sure to Reject this documents");

    if (r == true) {
        $.ajax({
            type: "POST",
            url: '{{ url('/verifiedDoc/term/') }}',
               data: { status : status, id_praapplication: id_praapplication, _token : _token, remark : remark},
                cache: false,
                beforeSend: function () {
                },
                success: function () {
                    alert("Rejected Success !");
                    location.href='{{url('/')}}/admin';
                },
               error: function () {
               }
           });
        }
    }
});
</script>

@if($view=='view')
<script type="text/javascript">
    $(document).ready(function(){
        $("#wizard-1 :input").prop("disabled", true);
    });
@endif
</script>

<script type="text/javascript">
    $( ".id_tenure" ).change(function() {
        var num = this.id.split('id_tenure')[1];
        var id_tenure = $('#id_tenure'+ num).val();
        $.ajax({
            url: "<?php  print url('/'); ?>/tenureyears/"+id_tenure,
            dataType: 'json',
            data: {
            },
        success: function (data, status) {
          jQuery.each(data, function (k) {
            var years = data[k].years;
            $("#c").val(years);
            $("#financing_tenure").val(years);
            var cur_age = $('#cur_age').val();
            var age_maturity = parseInt(cur_age)+years;
            $("#age_maturity").val(parseInt(age_maturity));

            var a = $('#a').val();
            var b = $('#b').val();
            var c = years;
            var f = $('#f').val();

            var d = (parseFloat(b)*(3.99/100))*parseFloat(c);
                $("#d").val(parseFloat(d).toFixed(2));

            var e = ((parseFloat(b)*(parseFloat(a)/100))*parseFloat(c)+parseFloat(b))/(parseFloat(c)*12);
                $("#e").val(parseFloat(e).toFixed(2));

            var g = parseFloat(e)/((100-parseFloat(f))/100);
                $("#g").val(parseFloat(g).toFixed(2));
            });
            }
        });
    });
</script>


<script type="text/javascript">
    $(document).ready(function() {
        var found = [];
        $("select[name='gender'] option").each(function() {
            if($.inArray(this.value, found) != -1) $(this).remove();
            found.push(this.value);
        });
        var found = [];
            $("select[name='category'] option").each(function() {
                if($.inArray(this.value, found) != -1) $(this).remove();
                found.push(this.value);
                });

        var found = [];
            $("select[name='existing_mbsb'] option").each(function() {
                if($.inArray(this.value, found) != -1) $(this).remove();
                found.push(this.value);
            });
    });
</script>

<script>
    function yesno(that) {
        if (that.value == "YES") {
         
             document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        }
    }
</script>
<script type="text/javascript">
    $("#existing_mbsb").change(function() {
  if ($(this).val() == "YES") {
    $('#otherFieldDiv').show();
    $('#otherField').attr('required', '');
    $('#otherField').attr('data-error', 'This field is required.');
  } else {
    $('#otherFieldDiv').hide();
    $('#otherField').removeAttr('required');
    $('#otherField').removeAttr('data-error');
  }
});
$("#existing_mbsb").trigger("change");

$("#seeAnotherFieldGroup").change(function() {
  if ($(this).val() == "YES") {
    $('#otherFieldGroupDiv').show();
    $('#otherField1').attr('required', '');
    $('#otherField1').attr('data-error', 'This field is required.');
    $('#otherField2').attr('required', '');
    $('#otherField2').attr('data-error', 'This field is required.');
  } else {
    $('#otherFieldGroupDiv').hide();
    $('#otherField1').removeAttr('required');
    $('#otherField1').removeAttr('data-error');
    $('#otherField2').removeAttr('required');
    $('#otherField2').removeAttr('data-error');
  }
});
$("#seeAnotherFieldGroup").trigger("change");
</script>