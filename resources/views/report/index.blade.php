<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Administrator";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   	@if (Session::has('error'))
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('error') }}</strong> 
        </div>
        @elseif (Session::has('success'))
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('success') }}</strong> 
        </div>
            
      @endif

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            
                         

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                                <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                    
                                    data-widget-colorbutton="false" 
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true" 
                                    data-widget-sortable="false"
                                    
                                -->
                                
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
                                <i> Report Filter Form</i>
									<!-- widget content -->
                                    <div class="row">
                                        <div class="col-md-4">
                                             {!! Form::open(['url' => 'admin/report_result/', 'method'=> 'GET' ,'class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                                        <fieldset>
                                        <section >
										    
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <label class="label"><b>Range Date </b></label>
                                               <label class="input"><i class="icon-append">Start</i>
  
                                                 <input type='text' name='startdate' class='form-control report' value="<?php if($startdate<>'0') echo $startdate; ?>" required id="startdate"> </label>
                                                  <b class="tooltip tooltip-bottom-right">Date Start</b>to
												  <label class="input"><i class="icon-append">End</i>
                                                  <input type='text' name='finishdate' value="<?php if($finishdate<>'0') echo $finishdate; ?>" id="finishdate" required class='form-control report'> 
                                                  <b class="tooltip tooltip-bottom-right">Date End</b>
                                              </label>
                                          </section>
                                            <section >
                                            <label class="label"><b>Status</b></label>
                                               <label class="input">
  
                                                  <select name="status" id="status" class="form-control select2 report">
													  @if($status_selected=='1')
														<option value="1" selected>Approved</option>
                                                        <option value="2">Rejected</option>
                                                        <option value="3">Pending Documentation</option>
                                                        <option value="4">Pending Disbursement</option>
                                                        <option value="5">Disbursed</option>
														<option value="99">All Status</option>
													  @elseif ($status_selected=='2')
														<option value="1">Approved</option>
                                                        <option value="2" selected>Rejected</option>
                                                        <option value="3">Pending Documentation</option>
                                                        <option value="4">Pending Disbursement</option>
                                                        <option value="5">Disbursed</option>
														<option value="99">All Status</option>
													  @elseif ($status_selected=='3')
														<option value="1">Approved</option>
                                                        <option value="2">Rejected</option>
                                                        <option value="3" selected>Pending Documentation</option>
                                                        <option value="4">Pending Disbursement</option>
                                                        <option value="5">Disbursed</option>
														<option value="99">All Status</option>
													  @elseif ($status_selected=='4')
														<option value="1">Approved</option>
                                                        <option value="2">Rejected</option>
                                                        <option value="3">Pending Documentation</option>
                                                        <option value="4" selected>Pending Disbursement</option>
                                                        <option value="5">Disbursed</option>
														<option value="99">All Status</option>
													  @elseif ($status_selected=='5')
														<option value="1">Approved</option>
                                                        <option value="2">Rejected</option>
                                                        <option value="3">Pending Documentation</option>
                                                        <option value="4">Pending Disbursement</option>
                                                        <option value="5" selected>Disbursed</option>
														<option value="99">All Status</option>
													  @else
														<option value="1">Approved</option>
                                                        <option value="2">Rejected</option>
                                                        <option value="3">Pending Documentation</option>
                                                        <option value="4">Pending Disbursement</option>
                                                        <option value="5">Disbursed</option>
														<option value="99">All Status</option>
													  
													  @endif
                                              
                                                  </select>
                                                  <b class="tooltip tooltip-bottom-right">Status</b>
                                              </label>
                                          </section>
                                         <section >
                                            <label class="label"><b>Branch</b></label>
                                               <label class="input">
  
                                                  <select name="branch" id="branch" class="form-control select2 report">
												  @foreach ($branch as $branch)
                                                         <option value='{{ $branch->id }}'>{{ $branch->branchname}}</option>
												  @endforeach	
														<option value="99">All Branch</option>
                                                      
                                                  </select>
                                                  <b class="tooltip tooltip-bottom-right">Branch</b>
                                              </label>
                                          </section>
										  <section><div align='left'>
												<a id='submitReport' class="btn btn-default btn-sm btn-primary">Submit</a>
										 
										  </div>
										 </section>
										  
                                        </fieldset>
										
                                    {!! Form::close() !!}  
                                        </div>
                                    </div>
                                    


									</div>
									<br><br><br><br>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 3400);
 
});
</script>
<script>
     $('#startdate').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });
	  $('#finishdate').datepicker({
				dateFormat : 'yy-mm-dd',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>',
				onSelect : function(selectedDate) {
					$('#startdate').datepicker('option', 'maxDate', selectedDate);
				}
			});
</script>
  
  <script type="text/javascript">

$( "#submitReport" ).click(function() {


      var startdate = $('#startdate').val();
      var finishdate = $('#finishdate').val();
      var status = $('#status').val();
      var branch = $('#branch').val();
	  
	if (startdate=='') {
	  alert('Please Insert Start Date !');
	  location.reload();
	
	}
	else if (finishdate=='') {
	  //code
	   alert('Please Insert Finish Date !');
	   location.href='{{url('/')}}/admin/report/index';
	}
	else {
         $.ajax({
               success: function () {      
                   location.href='{{url('/')}}/admin/report_result/'+startdate+'/'+finishdate+'/'+status+'/'+branch;
                
               },
            
           });
	}
     
   
});
</script>
   





          
