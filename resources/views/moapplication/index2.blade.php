<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Marketing Officier Page";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");



?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header.php");
?>
    <div id="main" role="main">
      <!-- MAIN CONTENT -->
      <div id="content" class="container">
                     @if (Session::has('message'))
    

    <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
     <i class="fa-fw fa-lg fa fa-exclamation"></i>
      <strong>{{ Session::get('message') }}</strong> 
    </div>
            
            @endif             
            @if (count($errors) > 0)
    <div class="alert alert-danger">
          <button class="close" data-dismiss="alert">
         ×
    </button>  
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">

<script>
var apiGeolocationSuccess = function(position) {
  showLocation(position);
};

var tryAPIGeolocation = function() {
  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyCz-U3AbNWcMJ8Ous00VgIR0RsQXypsOlY", function(success) {
    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng, location: success.location.location}});
  })
  .fail(function(err) {
    // alert("API Geolocation error! \n\n"+err);
    window.location.href = "/geolocation/error/"+err;
  });
};

var browserGeolocationSuccess = function(position) {
  showLocation(position);
};

var browserGeolocationFail = function(error) {
  switch (error.code) {
    case error.TIMEOUT:
      alert("Browser geolocation error !\n\nTimeout.");
      break;
    case error.PERMISSION_DENIED:
      if(error.message.indexOf("Only secure origins are allowed") == 0) {
        tryAPIGeolocation();
      }
         window.location.href = "{{url('/')}}/geolocation/error/"+error.code;
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Browser geolocation error !\n\nPosition unavailable.");
      break;
  }
};

var tryGeolocation = function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      browserGeolocationSuccess,
      browserGeolocationFail,
      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
  }
};

tryGeolocation();

function showLocation(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
   var location = position.coords.location;
  var _token = $('#_token').val();
  $.ajax({
    type:'POST',
    url: "{{url('/')}}/getLocation",
    data: { latitude: latitude, _token : _token, longitude : longitude, location:location },
    success:function(data){
            if(data){
              $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");
              $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");
               $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");
                 

            }else{
                $("#location").html('Not Available');
            }
    }
  });
}
</script>  
                <div class="row">
     
          
          <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
            <div class="well no-padding">

                              {!! Form::open(['url' => 'moapplication','class' => 'smart-form client-form', 'id' =>'smart-form-register' ]) !!}

                                    <span id="latitude"></span>
                                    <span id="longitude"></span>
                                    <span id="location"></span>

                <header>
                  <p class="txt-color-white"><b>  Mohon Sekarang  </b> </p>
                </header>

                <fieldset>
                            <div class="row">
                                     <div class="col-xs-6 col-12">
                                        <section class="col col-12">
                                          <label class="label"> <b> Nama Penuh </b></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" id="FullName" name="FullName" onkeyup="this.value = this.value.toUpperCase()" placeholder="Full Name"  maxlength="100"  @if (Session::has('fullname'))  value="{{ Session::get('fullname') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">Nama Penuh</b>
                                            </label>
                                        </section>
                                        </div>
                                        <div class="col-xs-6 col-12">
                                             <section class="col col-12">
                                            <label class="label"> <b> No Kad Pengenalan </b></label>
                                            <label class="input @if (Session::has('icnumber_error')) state-error  @endif">
                                                <i class="icon-append fa fa-user"></i>
                                                   <input type="text" id="ICNumber" name="ICNumber" minlength="12" maxlength="12" placeholder="IC Number"  @if (Session::has('icnumber'))  value="{{ Session::get('icnumber') }}" @endif  onKeyPress="return goodchars(event,'1234567890',this)">
                                                <b class="tooltip tooltip-bottom-right">No Kad Pengenalan</b>
                                            </label>
                                        </section>

                                      </div>
                                    </div>
                                        <div class="row">
                                         <div class="col-xs-6 col-12">
                                        <section class="col col-12">
                                            <label class="label"> <b> No. Tel Bimbit</b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-mobile-phone"></i>
                                                <input type="text" id="PhoneNumber" name="PhoneNumber" placeholder="Handphone Number" inputmode="numeric" pattern="[0-9]*" minlength="7" maxlength="12"  onkeypress="return isNumberKey(event)"  @if (Session::has('phone'))  value="{{ Session::get('phone') }}" @endif>
                                                <b class="tooltip tooltip-bottom-right">Nombor Telefon Bimbit </b>
                                            </label>
                                        </section>
                                    </div>
                                
                                    
                                  <div class="col-xs-6 col-12">
                                       <section class="col col-12">
                                            <label class="label"> <b> Jenis Pekerjaan </b></label>
                                                <label class="select">
                                                    <select name="Employment" id="Employment" class="form-control"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                                                
                                                       
                                                         @foreach ($employment as $employment)
                            <option value="{{ $employment->id }}">{{ $employment->name }}</option>
                              @endforeach
                                                    </select> <i></i>

                                                    <input type="hidden" name="Employment2" id="Employment2" value="" />
                                                </label>
                                            </section>
                                        </div>
                                  </div>
                                </fieldset>
                                <fieldset>
                                    <div class="row">
                                         <div class="col-xs-6 col-12">
                                             <section class="col col-12">
                                            <label class="label"> <b> Majikan</b></label>
                                                <label class="select" id="majikan">
                                                    <select name="Employer" id="Employer" class="form-control" onchange="document.getElementById('Employer2').value=this.options[this.selectedIndex].text"  >
                                                    @if (Session::has('employer')) 
                                                    <option  value="{{ Session::get('employer') }}">{{ Session::get('employer2') }}</option>
                                                     
                                                     
                                                    @endif

                                                   
                                                       
                                                    </select> <i></i>
                                                    <input type="hidden" name="Employer2" id="Employer2" value="" />
                                                </label>

                                                 <label id="majikan2" class="input">
                                                <i class="icon-append fa fa-briefcase"></i>
                                                <input type="text" id="majikantext" onkeyup="this.value = this.value.toUpperCase()" name="majikan"  placeholder="Majikan" maxlength="70"  @if (Session::has('majikan'))  value="{{ Session::get('majikan') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right"> Majikan</b>
                                            </label>

                                            </section>
                                            </div>
                                          <div class="col-xs-6 col-12">
                                                      <section class="col col-12">
                                            <label class="label"> <b> Gaji Asas (RM) </b>  </label>
                                            <label class="input @if (Session::has('hadpotongan')) state-error  @endif ">
                                                <i class="icon-append fa fa-credit-card"></i>
                                    <input id="BasicSalary" name="BasicSalary" placeholder="Basic Salary (RM)" type="number" required=""   class="form-control fn" step=".01" onkeypress="return isNumberKey(event)" @if (Session::has('basicsalary'))  value="{{ Session::get('basicsalary') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">Gaji Asas (RM) </b>
                                            </label>
                                        </section>
                                            </div>
                                        </div>
                                            <div class="row">
                                                    <div class="col-xs-6 col-12">
                                                       <section class="col col-12">
                                                    <label class="label"> <b> Elaun (RM) </b></label>
                                                    <label class="input @if (Session::has('hadpotongan')) state-error  @endif">
                                                        <i class="icon-append fa fa-credit-card"></i>
                                                           <input id="Allowance" name="Allowance" placeholder="Basic Salary (RM)" type="number" required=""   class="form-control fn" step=".01" onkeypress="return isNumberKey(event)" @if (Session::has('allowance'))  value="{{ Session::get('allowance') }}" @endif >

                                                      
                                                        <b class="tooltip tooltip-bottom-right">Elaun (RM)</b>
                                                    </label>
                                                </section>
                                      
                                        </div>
                                                 <div class="col-xs-6 col-12">
                                                          <section class="col col-12">
                                                    <label class="label"><b> Potongan Bulanan</b> </label>
                                                    <label class="input @if (Session::has('hadpotongan')) state-error  @endif">
                                                        <i class="icon-append fa fa-credit-card"></i>
                                                          <input id="Deduction" name="Deduction" placeholder="Basic Salary (RM)" type="number" required=""   class="form-control fn" step=".01" onkeypress="return isNumberKey(event)" @if (Session::has('deduction'))  value="{{ Session::get('deduction') }}" @endif >

                                                        
                                                        <b class="tooltip tooltip-bottom-right"> Jumlah Potongan Bulanan Semasa (RM)  </b>
                                                    </label>
                                                </section>
                                                </div>
                                              
                                                
                                            </div>
                                </fieldset>
                                <fieldset>
                                    <div class="row">
                                         <div class="col-xs-6 col-12">
                                        <section class="col col-12 col-lg-12 col-md-12">
                                            <label class="label"> <b> Pakej</b><div class="visible-xs"></div> </label>
                                                <label class="input">
                         
                                                    <input type="text" name="Package" id="Package" @if (Session::has('package_name'))  value="{{ Session::get('package_name') }}" @else value="Mumtaz-i" @endif class="form-control" readonly>

                                                </label>
                                           </section>
                                            </div>
                                        <div class="col-xs-6 col-12">
                                        <section class="col col-12">
                                            <label class="label"> <b> Jumlah Pembiayaan </b></label>
                                            <label class="input @if (Session::has('hadpotongan')) state-error  @endif"">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                  <input id="LoanAmount" name="LoanAmount" placeholder="Basic Salary (RM)" type="number" required=""   class="fn" step=".01" onkeypress="return isNumberKey(event)" @if (Session::has('loanAmount'))  value="{{ Session::get('loanAmount') }}" @endif >

                                            
                                                <b class="tooltip tooltip-bottom-right">Jumlah Pembiayaan (RM)</b>
                                            </label>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </section>
                                        </div>
                                        </div>

                                </fieldset>

                                <footer>
                                  
                                    <button type="submit" class="btn btn-primary">
                                     <b>    Kira Kelayakan  </b>
                                    </button>
                                    <div id="response"></div>
                                </footer>

                <div class="message">
                  <i class="fa fa-check"></i>
                  <p>
                    Thank you for your registration!
                  </p>
                </div>
              </form>
                                   
        </div>
      </div>

    </div>

        

  

    
        

<!-- ==========================CONTENT ENDS HERE ========================== -->
<script type="text/javascript">

    $(document).ready(function() {
        
          $("#smart-form-register2").hide();
        $("#smart-form-register").validate({

          // Rules for form validation
          rules : {
              FullName: {
              required : true,
              maxlength:100
            },
            ICNumber : {
              required : true,
              maxlength:12,
              minlength:12
            },
            
            PhoneNumber: {
                required: true,
                 maxlength:16,
                minlength:9

            },
            Deduction: {
                required: true
            },
            
            Allowance: {
                required: true
            },
            Package: {
                required: true
            },
            Employment: {
                required: true
            },
            Employer: {
                required: true,
              maxlength:60,
            },
            
            
            BasicSalary: {
                required: true
                            
            },
            LoanAmount: {
                required: true
            }
          },

          // Messages for form validation
          messages : {

              FullName: {
              required : 'Please enter your full name'
            },
            
            ICNumber: {
                required: 'Please enter your ic number'
            },
            PhoneNumber: {
                required: 'Please enter your phone number'
            },
            Allowance: {
                required: 'Please enter  yor allowance'
            },
            Deduction: {
                required: 'Please enter your total deduction'
            },
            Package: {
                required: 'Please select package'
            },
            Employment: {
                required: 'Please select employement type'
            },

            Employer: {
                required: 'Please select employer'
            },


            BasicSalary: {
                required: 'Please enter your basic salary'
            },
            LoanAmount: {
                required: 'Please select your loan amount'
            }
          }
        });

      });
    </script>

<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email2 : {
                    required : true,
                    email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                Password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                ICNumber : {
                    required : true,
                    minlength : 12,
                    maxlength : 13
                },
                PasswordConfirmation : {
                    required : true,
                    minlength : 3,
                    maxlength : 20,
                    equalTo : '#Password'
                }
            },

            // Messages for form validation
             messages : {

                    Email2 : {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
                },
                BasicSalary : {
                    required : 'Please enter your email address'
                },
                Password : {
                    required : 'Please enter your password'
                },
                PasswordConfirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>
<?php 
  //include required scripts
  include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
    

<script type="text/javascript">

 @if (Session::has('employer')) 
 
   $("#majikan2").hide();
    $("#majikan").show();
  @else
   $("#majikan").hide();
   $("#majikan2").show();
  @endif

$( "#Employment" ).change(function() {
    var Employment = $('#Employment').val();

    if( Employment == '1') {

      $("#Employer").html(" ");
      $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
       $("#Package").val("Mumtaz-i");
    }
      else if( Employment == '2') {
   
     $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
         $("#Package").val("Afdhal-i");
    }

    else if( Employment == '3') {
   
     $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
         $("#Package").val("Afdhal-i");
    }
      else if( Employment == '4') {
   
     $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
         $("#Package").val("Private Sector PF-i");
    }
    else if( Employment == '5') {
         
         $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();

          $("#Package").val("Afdhal-i");
            
        //  $("#majikan").simulate('click');
         


    }
  $.ajax({
                url: "<?php  print url('/'); ?>/employer/"+Employment,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#Employer").append("<option value='" + data[k].id + "'>" + data[k].name + "</option>");
                    });

                }
            });

   
});
</script>



<script type="text/javascript">
    
     $(document).ready(function() {

   var found = [];
$("#Employment option").each(function() {
  if($.inArray(this.value, found) != -1) $(this).remove();
  found.push(this.value);
});
     });



</script>
<script>
function isNumberKey(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          if (key.length == 0) return;
          var regex = /^[0-9.,\b]+$/;
          if (!regex.test(key)) {
              theEvent.returnValue = false;
              if (theEvent.preventDefault) theEvent.preventDefault();
          }


}

 
function minmax(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 2; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}
</script>

<script type="text/javascript">

function getSelectedText(elementId) {
    var elt = document.getElementById(elementId);

    if (elt.selectedIndex == -1)
        return null;

    return elt.options[elt.selectedIndex].text;
}


         var Employment = getSelectedText('Employment');
         var Employer = getSelectedText('Employer');
          $("#Employment2").val(Employment);
           $("#Employer2").val(Employer); 

</script>
<script type="text/javascript">
    document.getElementById("BasicSalary_x").onblur =function (){    
    this.value = parseFloat(this.value.replace(/,/g, ""))
                    .toFixed(2)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
    document.getElementById("display_BasicSalary_x").value = this.value.replace(/,/g, "");   
    
}
  
</script>

<script type="text/javascript">
    document.getElementById("LoanAmount_x").onblur =function (){    
      this.value = parseFloat(this.value.replace(/,/g, ""))
                      .toFixed(2)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ","); 

    document.getElementById("display_LoanAmount_x").value = this.value.replace(/,/g, "");  
  }
</script>

<script type="text/javascript">
    document.getElementById("Allowance_x").onblur =function (){    
      this.value = parseFloat(this.value.replace(/,/g, ""))
                      .toFixed(2)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");   
  
  document.getElementById("display_Allowance_x").value = this.value.replace(/,/g, ""); 

  }

</script>

<script type="text/javascript">
    document.getElementById("Deduction_x").onblur =function (){    
    this.value = parseFloat(this.value.replace(/,/g, ""))
                    .toFixed(2)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
    document.getElementById("display_Deduction_x").value = this.value.replace(/,/g, "");   
    
}
  
</script>
<script type="text/javascript">
  $( ".fn" ).keyup(function() {

  var BasicSalary = $('#BasicSalary').val().replace(/,/g,'');
  var Allowance = $('#Allowance').val().replace(/,/g,'');

  var Deduction = parseFloat(BasicSalary) + parseFloat(Allowance);
  $("#Deduction").val(parseFloat(Deduction).toFixed(2));



});
</script>
<!--Add the following script at the bottom of the web page (before </body></html>)-->
