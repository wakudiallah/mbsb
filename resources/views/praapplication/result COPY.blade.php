<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "MBSB Personal Financing-i";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>
    <div id="main" role="main">
<br><br><br><br>
      <!-- MAIN CONTENT -->
      <div id="content" class="container">
           @if (Session::has('message'))
    
                    <script>
                        function pesan() {
                            bootbox.alert("<b>{{Session::get('message')}}</b>");
                        }

                       
                           window.onload = pesan;
                     


                       

                    </script>
            @endif             
            @if (count($errors) > 0)
                <script>
                     function pesan() {
                          bootbox.alert("<b>@foreach ($errors->all() as $error) {{ $error }} <br> @endforeach</b>");
                      }
                      window.onload = pesan;

                      
              </script>

          @endif
           
          
        <div class="row">
          <div class="col-xs-11 col-sm-12 col-md-6 col-lg-6">


        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">

<script>
        jQuery(document).ready(function ($) {
            
            var jssor_1_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
                 ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
    
        
            //responsive code end
        });
    </script>



    <style>
        
        /* jssor slider bullet navigator skin 05 css */
        /*
        .jssorb05 div           (normal)
        .jssorb05 div:hover     (normal mouseover)
        .jssorb05 .av           (active)
        .jssorb05 .av:hover     (active mouseover)
        .jssorb05 .dn           (mousedown)
        */
        .jssorb05 {
            position: absolute;
        }
        .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
            position: absolute;
            /* size of bullet elment */
            width: 16px;
            height: 16px;
            background: url('{{url('/')}}/img/b05.png') no-repeat;
            overflow: hidden;
            cursor: pointer;
        }
        .jssorb05 div { background-position: -7px -7px; }
        .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
        .jssorb05 .av { background-position: -67px -7px; }
        .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

        /* jssor slider arrow navigator skin 22 css */
        /*
        .jssora22l                  (normal)
        .jssora22r                  (normal)
        .jssora22l:hover            (normal mouseover)
        .jssora22r:hover            (normal mouseover)
        .jssora22l.jssora22ldn      (mousedown)
        .jssora22r.jssora22rdn      (mousedown)
        */
        .jssora22l, .jssora22r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 40px;
            height: 58px;
            cursor: pointer;
            background: url('{{url('/')}}/img/a22.png') center center no-repeat;
            overflow: hidden;
        }
        .jssora22l { background-position: -10px -31px; }
        .jssora22r { background-position: -70px -31px; }
        .jssora22l:hover { background-position: -130px -31px; }
        .jssora22r:hover { background-position: -190px -31px; }
        .jssora22l.jssora22ldn { background-position: -250px -31px; }
        .jssora22r.jssora22rdn { background-position: -310px -31px; }
    </style>


    <div class="well" id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('{{url('/')}}/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden;">
          <div data-p="225.00" style="display: none;">
                <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/teguh.png">
          <img data-u="image" src="{{ url('/') }}/img/teguh.png" />
        </a>
            </div>

            <div data-p="225.00" style="display: none;">
            <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/slidembsb.png">    
          <img data-u="image" src="{{ url('/') }}/img/slidembsb.png" />
        </a>
            </div>

            <div data-p="225.00" style="display: none;">
                 <a data-toggle="modal"  data-toggle="modal" data-target="#payment">
          <img data-u="image" src="{{ url('/') }}/img/payment_method.png" />
        </a>
            </div>
           
            <div data-p="225.00" style="display: none;">
                <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/blue.jpg">
          <img data-u="image" src="{{ url('/') }}/img/blue.jpg" />
        </a>
            </div>
          
            
            
            <div data-p="225.00" style="display: none;">
        <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/WEB-05.jpg"> 
         <img data-u="image" src="{{ url('/') }}/img/WEB-05.jpg" />
        </a>
            </div>
              
            <div data-p="225.00" style="display: none;">
        <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/WEB-07.jpg"> 
          <img data-u="image" src="{{ url('/') }}/img/WEB-07.jpg" />
        </a>
            </div>        
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:1px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
    </div>

    <h1><font color='#005aac'>Personal Financing-i</font></h1>
<p align='justify'>Learn about our Islamic personal financing solutions through our host of attractive packages that best suit your needs.</p>

<ul>
  <li>Competitive profit rates as low as ECOF-i -0.20% p.a.*</li>
  <li>No guarantor required.</li>
  <li>No hidden charges.</li>
  <li>Quick approval</li>
  <li>Open to public servant and private sector employees.</li>
  <li>Monthly instalment via Biro Angkasa (Biro) or Accountant General (AG) or Employer Salary Deduction or Over-the-counter</li>
</ul>
<font size='2'>
<p><i>* Terms and conditions apply.<br>
ECOF-i = Islamic Effective Cost of Fund</i></p></font>

    <br>
                
            </div>
                        
                        
          </div>

          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="well no-padding">

							  @foreach($pra as $pra)

        <?php 
        $icnumber = $pra->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
         $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur =  $oDateIntervall->y;
        $durasix = 60 - $oDateIntervall->y;
        if( $durasix  > 10){ $durasi = 10 ;} 
        else { $durasi = $durasix ;}
 
        ?>
						
                                   <div class='smart-form client-form'id='smart-form-register2'>
                                <header> <p class="txt-color-white"><b>    Kelayakan Pembiayaan </b> </p> </header>
                               
                              
<form action='{{url('/')}}/praapplication/{{$id}}' method='post' enctype="multipart/form-data">
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label"> <b>  Pakej </b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <b><input type="text" id="Package2" value="{{$pra->package->name}}"  name="Package2" placeholder="Package" disabled="disabled"></b>
                                                <input name="id_praapplication" id="id_praapplication" type="hidden"  value="{{$pra->id}}" >
                                                 <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                
                                                <b class="tooltip tooltip-bottom-right">Pakej</b>
                                            </label>
                                        </section>
                                        
                                        <section class="col col-6">
                                            <label class="label"> <b> <font color="red" size="2.5" > KELAYAKAN  PEMBIAYAAN  MAKSIMA  </font>  </b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
@foreach($loan as $loan)
 <?php
 $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;


 $ndi = ($zbasicsalary - $zdeduction) -  1300;

$max  =  $salary_dsr * 12 * 10 ;
                               
function pembulatan($uang) {
              $puluhan = substr($uang, -3);
              if($puluhan<500) {
                $akhir = $uang - $puluhan; 
              } 
              else {
                $akhir = $uang - $puluhan;
              }
              return $akhir;
            }


if(!empty($loan->max_byammount))  {
  
      $ansuran = intval($salary_dsr)-1;
      if($pra->package->id=="1") {
          $bunga = 3.8/100;
      }
      elseif($pra->package->id=="2") {
          $bunga = 4.9/100;
      }

      else {
          $bunga = 5.92/100;
      }
   
      $pinjaman = 0;


      for ($i = 0; $i <= $loan->max_byammount; $i++) {
          $bungapinjaman = $i  * $bunga * $durasi ;
          $totalpinjaman = $i + $bungapinjaman ;
          $durasitahun = $durasi * 12;
          $ansuran2 = intval($totalpinjaman / ($durasi * 12))  ;
          //echo $ansuran2."<br>";
          if ($ansuran2 < $ndi)
          {
              $pinjaman = $i;
           
          }
        
      }   

      if($pinjaman > 1) {
           
                                                        
          $bulat = pembulatan($pinjaman);
          $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
          $loanz = $bulat;
      }
      else {
          $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
          $loanz = $loan->max_byammount;

      }

}
else { 

    $bulat = pembulatan($loan->max_bysalary * $total_salary);
    $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
    $loanz = $bulat;
    if ($loanz > 199000) {

          $loanz  = 250000;
          $loanx =  number_format($loanz, 0 , ',' , ',' ) ; 
    }


}

 ?>
@endforeach

                                                <b><input readonly type="text" id="MaxLoan"  
                                                value=" RM {{$loanx}}" name="MaxLoan" placeholder="Max Loan Eligibility (RM)"
                                                 class="merah" requierd> </b>
                                                    <input readonly type="hidden" id="maxloanz"  
                                                value="{{$loanz}}" name="maxloanz" placeholder="Max Loan Eligibility (RM)" requierd> 
                                                <b class="tooltip tooltip-bottom-right">Max Loan Eligibility (RM)</b>
                                            </label>


                                        </section>
                                        
                                       
                                    </div>
                                    <div class="row">
                                    <section class="col col-6">
                                            <label class="label"><b>  Jumlah Pembiayaan (RM) </b> </label>
                                            <label class="input state-<?php if( $pra->loanamount <= $loanz ) { print "success"; } else { print "error"; }?>">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" name="LoanAmount2"  id="LoanAmount2" onkeypress="return isNumberKey(event)" value="{{ $pra->loanamount }}"  placeholder="RM " onkeyup="this.value = minmax(this.value, 0, {{$loanz}})">
                                                <b class="tooltip tooltip-bottom-right">Jumlah Pembiayaan</b>
                                            </label>


                                        </section>
                                        
                                         <section class="col col-6">
                                            <label class="label"> <b>  Jumlah Pendapatan </b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" id="pendapatan" value="RM {{ number_format($total_salary, 0 , ',' , ',' )}}" name="pendapatan" placeholder="Loan Amount" disabled="disabled">
                                                <b class="tooltip tooltip-bottom-right">Loan Amount</b>
                                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            </label>
                                        </section>
                                         <section class="col col-6">
                                            <label class="label"> <b>  Ansuran Maksima </b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" id="ansuran_maksima" value="RM {{ number_format($ndi, 0 , ',' , ',' )  }}   / month" name="ansuran_maksima" placeholder="Ansuran Maksima" readonly>
                                                <b class="tooltip tooltip-bottom-right">Loan Amount</b>
                                            </label>
                                            
                                        </section>
                                         <section class="col col-6">
                                            <label class="label"> &nbsp; </label>
                                             <footer>
                                             <button class="btn btn-primary " type="submit">
                                    <b> Kira Semula </b>
                                </button>
                                            
                                        </footer>

                                        </section>


                                        
                                        </div>
                                       
                                </fieldset>
                                 
                              </form> 
                                <fieldset>

                                    <table class="table table-bordered table-hover" border='1'>
                                        <thead>
                                            <tr>
                                                <th valign="middle"> <b>  Tempoh </b> </th>
                                                <th class="hidden-xs"> <b>  Jumlah Pembiayaan  </b> </th>
                                                <th><b>  Ansuran Bulanan </b> </th>
                                                 <th width="40"> <b>  Kadar  Keuntungan </b> </th>
                                                   <th> <b>  Pilih </b> </th>
                                                

                                            </tr>
                                        </thead>
                                        <tbody>
                                   <?php if( $pra->loanamount <= $loanz ) { $ndi_limit=$loan->ndi_limit;?>
                                            @foreach($tenure as $tenure)
                                            <?php 

                                                   $bunga2 =  $pra->loanamount * $tenure->rate /100   ;
                                                   $bunga = $bunga2 * $tenure->years;
                                                   $total = $pra->loanamount + $bunga ;
                                                   $bulan = $tenure->years * 12 ;
                                                   $installment =  $total / $bulan ;
                                                   $ndi_state = ($total_salary - $zdeduction) - $installment; 
                                                   
                                                   

                                                   if($installment  <= $salary_dsr && $ndi_state>=$ndi_limit) {
                                                      
                                                  
                                                ?>
                                            <tr>
                                                <td>{{$tenure->years}} years</td>
                                                <td class="hidden-xs"> RM {{ number_format( $pra->loanamount, 0 , ',' , ',' )  }}  </td>
                                                
                                                <td>RM {{ number_format($installment, 0 , ',' , ',' )  }} /bln</td>
                                              
                                                <td  align="center" >{{$tenure->rate}} %</td>
                                                <td align="center">  <input type="radio" name="tenure" id="tenure" class="tenure" value="{{$tenure->id}}" required> </td>
                                            </tr>
                                            <?php }  ?>
                                            @endforeach
                                             <?php } ?>
                                        </tbody>
                                    </table>
                                </fieldset>
                                <fieldset>
                                    

                                <footer>
                                       
                                  <button type="submit" class="btn btn-primary btn-lg" id="submit_tenure">
                                    <b>  Teruskan </b>
                                  </button>
                                   
                                  
                                </footer>

                    <div>
                    </form>
                                      

						</div>
						
					
					</div>
					
					
				</div>
								
				
			</div>
		

		</div>
		

        </div>
    </div>
           <div class="page-footer">
            <div class="row">
             

                <div class="col-xs-12 col-sm-12 text-left ">
                    <div class="txt-color-black inline-block">
                        <span class="txt-color-black">NetXpert Sdn Bhd  © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>


		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Pendaftaran</h4>
                    </div>
                    <div class="modal-body">
        {!! Form::open(['url' => 'user','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                      <fieldset>

                      
                                      <section >
                                          <label class="label"><br>Emel</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-envelope"></i>
                                                <input type="hidden" name="idpra" value="{{$pra->id}}">
                                                <input type="Email" id="Email" name="Email" required placeholder="Email" >
                                                <b class="tooltip tooltip-bottom-right">Emel</b>
                                            </label>
                                        </section>
                                          <input type="hidden" id="FullName2" name="FullName2"  value="{{$pra->fullname}}" placeholder="Full Name" readonly >
                                   @endforeach     
                                        <section >
                                            <label class="label">Kata Laluan</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-key "></i>
                                                <input type="Password" required id="password" name="password" placeholder="Password" >
                                                <b class="tooltip tooltip-bottom-right">
Must be between 8 – 36 characters long - combination of numbers (0 to 9) and letters  (a to z) with at least one capital letter (A to Z)</b>
                                            </label>

                                        </section>
                                        <section >
                                            <label class="label">Pengesahan Kata Laluan</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-key "></i>
                                                <input type="Password" id="password_confirmation" name="password_confirmation" required placeholder="Password Confirmation" >
                                                <b class="tooltip tooltip-bottom-right">Pengesahan Kata Laluan</b>
                                            </label>
                                        </section>
                               
                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      
                                     
                                </fieldset>
                       
                       
        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Batal
                        </button>
                        <button type="submit" name="submit" class="btn btn-primary">
                                       Daftar
                                    </button>
                                   
                           {!! Form::close() !!}   
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        

          <!-- Modal -->
    <div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel"><b>Payment Methods</b></h4>
                    </div>
                    <div class="modal-body">
   
                 You can make payment through the channels stated below:
                 <ol type='1'>
                   <li>MBSB nationwide branches</li>
                   <li>Maybank2u.com (Online bill payment services)</li>
                   <li>Cimbclicks.com (Online bill payment services)</li>
                   <li>RHB.com.my (Online bill payment services)</li>
                   <li>Bank Simpanan Nasional (under Giro Services)</li>
                   <li>Standing Instruction from respective banks (where applicable)</li>
                  
                </ol>
                <p><i>*Please state your name and account number on all payments made to MBSB.</i></p>
                  <p>
                  Disclaimer: Payment via Online bill payment shall be credited into your account in the following business day subject to successful transaction.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                          Close
                        </button> 
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!-- ==========================CONTENT ENDS HERE ========================== -->
<script type="text/javascript">

		$(document).ready(function() {
				
				
				$("#smart-form-register").validate({

					// Rules for form validation
					rules : {
					    FullName: {
							required : true
						},
						ICNumber : {
							required : true
						},
						
						PhoneNumber: {
						    required: true
						},
						Deduction: {
						    required: true
						},
						
						Allowance: {
						    required: true
						},
						Package: {
						    required: true
						},
						Employment: {
						    required: true
						},
						Employer: {
						    required: true
						},
						
						
						BasicSalary: {
						    required: true,
                            min : 2999
						},
						LoanAmount: {
						    required: true
						}
					},

					// Messages for form validation
					messages : {

					    FullName: {
							required : 'Please enter your full name'
						},
						
						ICNumber: {
						    required: 'Please enter your ic number'
						},
						PhoneNumber: {
						    required: 'Please enter your phone number'
						},
						Allowance: {
						    required: 'Please enter yor allowance'
						},
						Deduction: {
						    required: 'Please enter your total deduction'
						},
						Package: {
						    required: 'Please select package'
						},
						Employment: {
						    required: 'Please select employement type'
						},

						Employer: {
						    required: 'Please select employer'
						},


						BasicSalary: {
						    required: 'Please enter your basic salary',
                            min: 'minimum salary is 3000 my'
						},
						LoanAmount: {
						    required: 'Please enter your loan amount'
						}
					}
				});

			});
    </script>

<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email2 : {
                    required : true,
                    email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                password : {
                    required : true,
                    minlength : 8,
                    maxlength : 36
                },
                password_confirmation : {
                    required : true,
                   
                    maxlength : 36,
                    equalTo : '#password'
                }
            },

            // Messages for form validation
             messages : {

                    email : {
                    required : 'Please enter your email address',
                    Email2 : 'Please enter a VALID email address'
                },
                BasicSalary : {
                    required : 'Please enter your email address'
                },
                password : {
                    required : 'Please enter your password'
                },
                password_confirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>

<script type="text/javascript">

$( ".tenure" ).change(function() {
    var tenure = $('input[name=tenure]:checked').val();
    var id_praapplication = $('#id_praapplication').val();
     var _token = $('#token').val();
       var maxloanz = $('#maxloanz').val();
        var package = $('#Package2').val();
      

  $.ajax({
                type: "PUT",
                url: '{{ url('/form/') }}'+'/99',
                data: { id_praapplication: id_praapplication, tenure: tenure, _token : _token, maxloan : maxloanz, package : package
                   
                },
                success: function (data, status) {

                

                }
            });

   
});
</script>

<script type="text/javascript">
    
     $(document).ready(function() {


    var id_praapplication = $('#id_praapplication').val();
     var _token = $('#token').val();
      var LoanAmount = $('#LoanAmount2').val();
       var maxloanz = $('#maxloanz').val();
      

  $.ajax({
                type: "PUT",
                url: '{{ url('/form/') }}'+'/9',
                data: { id_praapplication: id_praapplication,  _token : _token, loanammount : LoanAmount, maxloan : maxloanz
                   
                },
                success: function (data, status) {

                

                }
            });
     });

</script>

<?php 
	//include required scripts
	include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
		
<script>
$(document).ready(function(){
    $("#submit_tenure").click(function(){
  var tenure = $('input[name=tenure]:checked').val();
     
        if (tenure>0) { 
      $('#myModal').modal('show');
    }
    else{
      bootbox.alert('Sila Pilih Tempoh Pembiayaan');
      
    }
    });
});
</script>


<script type="text/javascript">
function minmax(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 0; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}
</script>


<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>


