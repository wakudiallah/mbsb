<?php
//initilize the page
require_once("asset/inc/init.php");
//require UI configuration (nav, ribbon, etc.)

require_once("asset/inc/config.ui.php");
/*---------------- PHP Custom Scripts ---------
YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */
$page_title = "Personal Financing-i ";
/* ---------------- END PHP Custom Scripts ------------- */

//include header

//you can add your custom css in $page_css array.

//Note: all css files are inside css/ folder

$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->

    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
    include ("asset/inc/header-home.php");
?>
<style type="text/css">
.content-box{
  background-color: #d1d2d4;
      height: auto;
      width: 100%;
      border: 2px solid #0155a2;
      box-sizing: content-box;
      margin-bottom: 10px;
      }
    .content-box>.box-heading {
        color: #fff;
        background-color: #0155a2;
        border-color: #0155a2;
    }
    .content-box .box-heading {
        font-size: 16px;
        text-align: center;
    }
    .box-heading {
        padding: 10px 15px;
    }
    .box-body {
        padding: 8px;
    }
    .box-body h5{
        text-align: center;
        font-size: 16px;
    }
    .box {
        width: 100%;
        margin: 5px auto;
        text-align: center;
    }

    .box .button {
        font-weight: bold;
        font-size: 16px;
        color: #0155a2;
        border: 3px solid grey;
        border-radius: 10px;
        text-decoration: none;
        cursor: pointer;
        transition: all 0.3s ease-out;
    }
    .button:hover {
        background: #808080;
        color: #fff;
        text-decoration: none;
    }

    .titlesmall {
        margin-left:0;
    }
    .retailSection a {
        color: #0055a2;
    }
    .modal-content{background-color: #d1d2d4;}
    a.button.nav-link.app {
        background-color: #0155a2;
        border: 3px solid #0155a2;
        color: #fff;
    }
    a.button.nav-link.app:hover {
        /* background-color: green; */
        color: #d1d2d4;
    }

    .detail{
        margin-bottom: 10px !important;
    }
</style>
<script>
    var apiGeolocationSuccess = function(position) {
      showLocation(position);
    };

    var tryAPIGeolocation = function() {
      jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyCz-U3AbNWcMJ8Ous00VgIR0RsQXypsOlY", function(success) {
        apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng, location: success.location.location}});
      })
      .fail(function(err) {
        // alert("API Geolocation error! \n\n"+err);
        window.location.href = "/geolocation/error/"+err;
      });
    };

    var browserGeolocationSuccess = function(position) {
      showLocation(position);
    };

    var browserGeolocationFail = function(error) {
      switch (error.code) {
        case error.TIMEOUT:
          alert("Browser geolocation error !\n\nTimeout.");
          break;
        case error.PERMISSION_DENIED:
          if(error.message.indexOf("Only secure origins are allowed") == 0) {
            tryAPIGeolocation();
          }
             window.location.href = "{{url('/')}}/geolocation/error/"+error.code;
          break;
        case error.POSITION_UNAVAILABLE:
          alert("Browser geolocation error !\n\nPosition unavailable.");
          break;
      }
    };

    var tryGeolocation = function() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          browserGeolocationSuccess,
          browserGeolocationFail,
          {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
      }
    };

    tryGeolocation();

    function showLocation(position) {
      var latitude = position.coords.latitude;
      var longitude = position.coords.longitude;
       var location = position.coords.location;
      var _token = $('#_token').val();
      $.ajax({
        type:'POST',
        url: "{{url('/')}}/getLocation",
        data: { latitude: latitude, _token : _token, longitude : longitude, location:location },
        success:function(data){
                if(data){
                  $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");
                  $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");
                   $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");
                     

                }else{
                    $("#location").html('Not Available');
                }
        }
      });
    }
</script>  

<div id="main" role="main">
    <br><br><br><br>
    <!-- MAIN CONTENT -->
    <div id="content" class="container" style=" max-width: 100% !important;margin: 0 auto;">
        @include('sweetalert::alert')  
        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">

        <div class="row" style="margin-bottom: 30px !important">
            <div class="col-sm-12 col-md-12 col-lg-12 ">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" >
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <a href="https://mbsbbank.com/en/consumer-banking/financing/personal-financing-i" target="_blank"><img data-u="image" class="d-block w-200" src="{{ url('/') }}/img/mbsb/1.jpg" width="1200px" height="1200px" alt="First slide"/></a>
                        </div>
                        <div class="item">
                           <a href="https://mbsbbank.com/en/consumer-banking/financing/personal-financing-i" target="_blank"> <img data-u="image" class="d-block w-200" src="{{ url('/') }}/img/mbsb/2a.jpg" width="1200px" height="1200px" alt="Second slide"/></a>
                        </div>
                        <div class="item">
                           <a href="https://mbsbbank.com/en/consumer-banking/financing/personal-financing-i" target="_blank"> <img data-u="image" class="d-block w-200" src="{{ url('/') }}/img/mbsb/3.jpg" width="1200px" height="1200px" alt="Third slide"/></a>
                        </div>
                        <div class="item">
                           <a href="https://mbsbbank.com/en/consumer-banking/financing/personal-financing-i" target="_blank"> <img data-u="image" class="d-block w-200" src="{{ url('/') }}/img/mbsb/5.png" width="1200px" height="1200px" alt="Forth slide"/></a>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="">
            @include('auth.login_modal') 
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6 hidden-xs">
               <h2>Personal Financing-<i>i</i></h2>
                    <p>Learn about our Shariah compliant personal financing solutions through our host of attractive packages that best suit your financing needs.</p>
                    <ul>
                        <li>Competitive financing rates</li>
                        <li>Financing amount of up to RM400,000</li>
                        <li>Financing tenure of up to 10 years</li>
                        <li>No guarantor required</li>
                        <li>No hidden charges</li>
                        <li>Fast approval</li>
                        <li>Applicable for public servant and private sector employees</li>
                    </ul>
                <div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="content-box">
                            <div class="box-heading">Mumtaz<i>-i</i></div>
                            <div class="box-body">
                                <h5 class="detail"><b>Financing Amount</b></h5>
                                    Financing amount of up to RM250,000
                                <h5 class="detail"><b>Features / Benefits</b></h5>
                                    <ul>
                                        <li>No guarantor required</li>
                                        <li>Takaful coverage is optional</li>
                                        <li>No early settlement charges</li>
                                        <li>Fast Approval</li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="content-box">
                            <div class="box-heading">Afdhal-<i>i</i></div>
                                <div class="box-body">
                                <h5 class="detail"><b>Financing Amount</b></h5>
                                    Financing amount of up to RM400,000
                                <h5 class="detail"><b>Features / Benefits</b></h5>
                                    <ul>
                                        <li>No guarantor required</li>
                                        <li>Takaful coverage is optional</li>
                                        <li>No early settlement charges</li>
                                        <li>Fast Approval</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="content-box">
                            <div class="box-heading">Private Sector<i>-i</i></div>
                            <div class="box-body">
                                <h5 class="detail"><b>Financing Amount</b></h5>
                                    Financing amount up to RM300,000
                                <h5 class="detail"><b>Features / Benefits</b></h5>
                                    <ul>
                                        <li>No guarantor required</li>
                                        <li>Takaful coverage is optional</li>
                                        <li>No early settlement charges</li>
                                        <li>Fast Approval</li>
                                    </ul>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php if (!(Auth::user())) {?>
                <div class="col-sm-12 col-md-12 col-lg-6 ">
                    <div class="well no-padding">
                        {!! Form::open(['url' => 'praapplication','class' => 'smart-form client-form', 'id' =>'smart-form-register' ]) !!}
                            <header>
                                <p class="txt-color-white"><b>Check Loan Eligibility</b> </p>
                            </header>

                            <fieldset>
                                <div class="row">
                                    <div class="col-xs-12 col-12 col-md-12 col-lg-12">
                                        <section class="col col-12 col-lg-12 col-md-12 col-xs-12">
                                            <label class="label"> <b> FullName </b></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" id="FullName"  onkeyup="this.value = this.value.toUpperCase()" name="FullName" placeholder="Full Name"    @if (Session::has('fullname'))  value="{{ Session::get('fullname') }}" @endif size="55">
                                                <b class="tooltip tooltip-bottom-right">Nama Penuh</b>
                                            </label>
                                        </section>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 col-12">
                                        <section class="col col-12 col-xs-12 col-md-12">
                                            <label class="label"> <b> Id Type </b></label>
                                            <label class="select">
                                                <select name="type" id="type" class="form-control" onchange="yesno(this);" required="">
                                                <option>--Select--</option>
                                                @foreach ($idtype as $idtype)
                                                    <option value="{{ $idtype->idtypecode }}">{{ $idtype->idtypedesc }}</option>
                                                @endforeach
                                              </select> <i></i>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-xs-6 col-12" id="ifNewIc" style="display: none">
                                         <section class="col col-12 col-xs-12 col-md-12">
                                        <label class="label"> <b> IC Number <sup>*</sup></b></label>
                                        <label class="input @if (Session::has('icnumber_error')) state-error  @endif">
                                            <i class="icon-append fa fa-user"></i>
                                            <input  type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==12) return false;" id="ICNumber" name="ICNumber" minlength="12" maxlength="12" placeholder="IC Number"  @if (Session::has("icnumber"))  value="{{ Session::get('icnumber') }}" @endif >
                                            <b class="tooltip tooltip-bottom-right">No Kad Pengenalan</b>
                                        </label>
                                    </section>
                                    </div>
                                    <div class="col-xs-6 col-12" id="ifOther" style="display: none">
                                        <section class="col col-12 col-xs-12 col-md-12">
                                            <label class="label"> <b>Other IC Number</b></label>
                                            <label class="input @if (Session::has('icnumber_error')) state-error  @endif">
                                              <i class="icon-append fa fa-user"></i>
                                                <input  type="text" id="others" onkeyup="this.value = this.value.toUpperCase()" name="other"  placeholder="Other"  @if (Session::has('other'))  value="{{ Session::get('other') }}" @endif minlength="6" maxlength="12" required="">
                                                <b class="tooltip tooltip-bottom-right">No Kad Pengenalan</b>
                                            </label>
                                        </section>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 col-12" id="ifDOB" style="display: none">
                                        <section class="col col-12 col-xs-12 col-md-12">
                                            <label class="label"> <b>DOB (dd/mm/yyyy)</b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-mobile-phone"></i>
                                                <input type="text" data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  maxlength="14" name="dob" value=""  class="form-control startdate" id="dob" required=""/>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-xs-6 col-12" id="ifgender" style="display: none">
                                         <section class="col col-12 col-xs-12 col-md-12">
                                            <label class="label"> <b> Gender </b></label>
                                            <label class="select">
                                                <select name="gender" id="gender" class="form-control" required="">
                                                    <option>--Select--</option>
                                                    <option value="11">Male</option>
                                                    <option value="22">Female</option>
                                                </select> <i></i>
                                            </label>
                                        </section>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 col-12">
                                        <section class="col col-12 col-xs-12 col-md-12">
                                            <label class="label"> <b> Phone Number</b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-mobile-phone"></i>
                                                <input type="number" id="PhoneNumber" name="PhoneNumber" placeholder="Handphone Number"  minlength="7" class="fn" step=".01" maxlength="12"  onkeypress="return isNumberKey(event)"  @if (Session::has('phone'))  value="{{ Session::get('phone') }}" @endif>
                                                <b class="tooltip tooltip-bottom-right">Nombor Telefon Bimbit </b>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-xs-6 col-12">
                                        <section class="col col-12 col-xs-12 col-md-12">
                                            <label class="label"> <b> Job Type </b></label>
                                            <label class="select">
                                                <select  name="Employment" id="Employment" class="form-control"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                                                    @foreach ($employment as $employment)
                                                    <option value="{{ $employment->id }}">{{ $employment->name }}</option>
                                                    @endforeach
                                                </select> <i></i>
                                                <input type="hidden" name="Employment2" id="Employment2" value="" />
                                             </label>
                                        </section>
                                     </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="row">
                                    <div class="col-xs-6 col-12">
                                        <section class="col col-12 col-xs-12 col-md-12">
                                            <label class="label"> <b> Employer</b></label>
                                            <label class="select" id="majikan">
                                                <select name="Employer" id="Employer" class="form-control" onchange="document.getElementById('Employer2').value=this.options[this.selectedIndex].text"  >
                                                    @if (Session::has('employer')) 
                                                        <option  value="{{ Session::get('employer') }}">{{ Session::get('employer2') }}</option>
                                                    @endif
                                                </select> <i></i>
                                                <input type="hidden" name="Employer2" id="Employer2" value="" />
                                            </label>
                                            <label id="majikan2" class="input @if (Session::has('basicsalary')) state-error @endif ">
                                                <i class="icon-append fa fa-briefcase"></i>
                                                <input type="text" id="majikantext" onkeyup="this.value = this.value.toUpperCase()" name="majikan"  placeholder="Majikan"  @if (Session::has('majikan'))  value="{{ Session::get('majikan') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right"> Majikan</b>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-xs-6 col-12">
                                        <section class="col col-12 col-xs-12 col-md-12">
                                            <label class="label"> <b> Basic Salary (RM)</b>  </label>
                                            <label class="input @if (Session::has('basicsalary')) state-error @endif ">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input id="BasicSalary" value="" name="BasicSalary" placeholder="Basic Salary (RM)" type="number" required=""   class="fn" step=".01" onkeypress="return isNumberKey(event)" @if (Session::has('basicsalary'))  value="{{ Session::get('basicsalary') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">Gaji Asas (RM) </b>
                                            </label>
                                        </section>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 col-12">
                                        <section class="col col-12 col-xs-12 col-md-12">
                                            <label class="label"> <b> Allowance (RM) </b></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                  <input id="Allowance" value="" name="Allowance" placeholder="Allowance (RM)" type="number" required=""   class="fn" step=".01" onkeypress="return isNumberKey(event)" @if (Session::has('allowance'))  value="{{ Session::get('allowance') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">Elaun (RM)</b>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-xs-6 col-12">
                                        <section class="col col-12 col-xs-12 col-md-12">
                                            <label class="label"><b> Deduction (RM) </b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input id="Deduction" value="" name="Deduction" placeholder="Existing Total Deduction (RM)" type="number" required=""   class="fn" step=".01" onkeypress="return isNumberKey(event)" @if (Session::has('deduction'))  value="{{ Session::get('deduction') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right"> Jumlah Potongan Bulanan Semasa (RM)  </b>
                                            </label>
                                        </section>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="row">
                                    <div class="col-xs-6 col-12">
                                        <section class="col col-12 col-xs-12 col-md-12">
                                            <label class="label"> <b> Package</b> </label>
                                            <label class="input">
                                                <input type="text" name="Package" id="Package" @if (Session::has('package_name'))  value="{{ Session::get('package_name') }}" @else value="Mumtaz-i" @endif class="form-control" readonly>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="col-xs-6 col-12">
                                        <section class="col col-12 col-xs-12 col-md-12">
                                            <label class="label"> <b> Loan Amount(RM) </b></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                    <input id="LoanAmount" name="LoanAmount" placeholder="Loan Amount (RM)" type="number" required=""   class="fn" step=".01" onkeypress="return isNumberKey(event)" @if (Session::has('loanAmount'))  value="{{ Session::get('loanAmount') }}" @endif >
                                                    <b class="tooltip tooltip-bottom-right">Loan Amount (RM)</b>
                                            </label>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </section>
                                    </div>
                                </div>
                            </fieldset>
                            <footer>
                                <button type="submit" class="btn btn-primary">
                                    <b> Calculate  </b>
                                </button>
                                <div id="response"></div>
                            </footer>
                        </div>
                    </form>
                <?php } ?>
                    </div>
                </div>
        </div>
    </div>


   @include('layouts.js')

