<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
    box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
    float: left;
    width: 50%;
    padding: 0px;
    height: 300px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}
 td.clas {
   border: 1px solid black;
    width: 50px;
    height:15px;
}
td.clas1 {
   border: 1px solid black;
    
}
body {
    font-size: 5px;

  font-family: "Arial", Arial, Sans-serif;

}
  
th {
    font-size: 14px;
  

}

br.border2  {
   display: block;
   margin: 20px 0;
}

ol{
  padding-left:1em;
}

li{
  padding-left:1em;
}

td {
    font-size: 7px;

  font-family: "Arial", Arial, Sans-serif;

}
p {
    
    line-height: 130%;

}
td.header {
 background-color:black;
 color:white;
 font-weight: bold;
 width: 320px;

}
td.border {
  border-collapse: collapse;
    border: 1px solid black;
    width: 10px;
}

.bor-table td {
    border: 1px solid black;
}

td.bor-noBorder {
    border: none;
   
}

.bor-table {
    border-collapse: collapse;
}

</style>
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:4px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:4px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-baqh{text-align:center;vertical-align:top}
.tg .tg-wxgh{text-decoration:underline;vertical-align:top}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-9hbo{font-weight:bold;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
</style>
</head>

<body>

	<table>
		<tr>
			<td colspan="2" class="border header title" style="width: 670px; background-color: blue">
   				<img src="<?php echo e(url('/asset/img/mbsb_form.png')); ?>" width="670px" height="50px">
			</td>
		</tr>
	</table>
    <!--<table>
        <tr>
            <td colspan="2" class="border header title" style="width: 670px; background-color: blue">
                <h2>Credit Application Form (Government Employee /GLC) /<br>
                <i>Borang Permohonan Kredit (Pekerja Kerajaan / GLC</i></h2>
            </td>
        </tr>
    </table>-->
	<table>
		<tr>
			<td style="width: 670px;">Please write in full block letters and tick where applicable. / <i> Sila tulis dengan menggunakan huruf besar dan tandakan di mana perlu</td>
		</tr>	
    </table>

<div class="row">
  	<div class="column">
  		<table>
    		<tr>
    			<td colspan="2" class="border header" style="width: 325px">
       				A) Details of Credit Requested / <i>Maklumat Permohonan Peribadi</i>
    			</td>
  			</tr>	
    	</table>
    	<table>
		  	<tr>
		    	<td style="width: 80px"> Amount of Credit / <br> Kredit Amaun</td>
		    	<td style="width: 10px">:</td>
		     	<td class="clas" style="width: 220px"> </td>
		 	</tr>
		    <tr>
		    	<td>&nbsp;</td>
		    	<td>&nbsp;</td>
		     	<td>&nbsp;</td>
		  	</tr>
		  	<tr>
		    	<td>Takaful Premium (if any) / <br> <i>Premium Takaful (jika ada)</i></td>
		    	<td>:</td>
		     	<td class="clas"></td>
		  	</tr>
		  	<tr>
		    	<td>&nbsp;</td>
		    	<td>&nbsp;</td>
		     	<td>&nbsp;</td>
		  	</tr>
   			<tr>
    			<td>Total Amount / <br><i>Jumlah Amaun</i></td>
    			<td>:</td>
     			<td class="clas">&nbsp;</td>
  			</tr>
  			<tr>
		    	<td>&nbsp;</td>
		    	<td>&nbsp;</td>
		     	<td>&nbsp;</td>
		  	</tr>
		</table>
		<table>
   			<tr>
    			<td style="width: 80px">Credit Tenure(month) / <br><i>Tempoh Pembiayaan (Bulan)</i></td>
    			<td style="width: 10px">:</td>
     			<td class="clas" style="width: 60px"><?php echo e($financial->l4_tempohbulan); ?></td>
     			<td style="width: 10px">&nbsp;</td>
     			<td style="width: 60px">Profit Rate / <br><i>Kadar Keuntungan</i></td>
    			<td style="width: 10px">:</td>
     			<td class="clas"  style="width: 55px"></td>
  			</tr>
		</table><br>
		<table>
			<tr>
				<td>Purpose of Application of Credit / <i> Tujuan untuk Permohonan Kredit</i></td>
				<td style="width: 135px"></td>
			</tr>
		</table>
		<table class="bor-table">
        	<tr>
	            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
             	<td class="bor-noBorder" style="width: 310px">Medical /<i>Perubatan</i></td>
        	</tr>
	       	<tr>
	            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
             	<td class="bor-noBorder">Travel /<i>Mengembara</i></td>
        	</tr>
        	<tr>
	            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
             	<td class="bor-noBorder">Education /<i>Pendidikan</i></td>
        	</tr>
        	<tr>
	            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
             	<td class="bor-noBorder">Purchase of Asset (please specify) / <i> Pembelian Harta (sila nyatakan) </i></td>
        	</tr>
        	<tr>
	            <td class="bor-noBorder"></td>
             	<td>&nbsp;<br><br></td>
        	</tr>
        	<tr>
	            <td>&nbsp; V &nbsp;</td>
             	<td class="bor-noBorder">Others (please specify)  /<i> Lain-lain (sila nyatakan) : </i></td>
        	</tr>
        	<tr>
	            <td class="bor-noBorder"></td>
             	
        	</tr>
    	</table>
    	<br><br>
    	<table>
			<tr>
				<td style="width: 310px">Full Name as per NRIC/Passport / <i>Nama Penuh seperti di dalam Kad Pengenalan/Pasport</i> :</td>
			</tr>
			<tr>
				<td class="clas">&nbsp;<?php echo e($term->Basic->fullname); ?></td>
			</tr>
		</table>
		<table>
			<tr>
				<td style="width: 155px">New NRIC No. / <i>No. Kad Pengenalan Baru </i> :</td>
				<td style="width: 5px">&nbsp;</td>
				<td style="width: 155px">Old NRIC No. / <i>No. Kad Pengenalan Lama </i> :</td>
			</tr>
			<tr>
				<td class="clas" ><?php echo e($term->Basic->ic_number); ?></td>
				<td >&nbsp;</td>
				<td class="clas" ></td>
			</tr>
		</table><br>
		<table>
			<tr>
				<td style="width: 310px">Other Identification (If without NRIC Information)/ <br><i>Pengenalan Lain (Jika Tiada Maklumat Kad Pengenalan)</i>:</td>
			</tr>
		</table>
		<table class="bor-table">
        	<tr>
	            <td style="width: 8px">&nbsp;&nbsp;&nbsp;&nbsp; </td>
             	<td class="bor-noBorder"  style="width: 80px">Army No. /</td>
             	<td class="bor-noBorder"  style="width: 40px">&nbsp;</td>
           		<td rowspan="2" style="width: 180px">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        	</tr>
        	<tr>
        		 <td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
             	<td class="bor-noBorder"><i>No. Tentera</i></td>
           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
        	</tr>
        	<tr>
	            <td>&nbsp;&nbsp;&nbsp;&nbsp; </td>
             	<td class="bor-noBorder">Police No. /</td>
           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
           		<td rowspan="2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        	</tr>
        	<tr>
        		 <td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
             	<td class="bor-noBorder"><i>No. Polis</i></td>
           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
        	</tr>
        	<tr>
	            <td>&nbsp;&nbsp;&nbsp;&nbsp; </td>
             	<td class="bor-noBorder">Others (please specify) /</td>
           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
           		<td  rowspan="2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        	</tr>
        	<tr>
        		 <td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
             	<td class="bor-noBorder"><i>Lain-lain (sila nyatakan)</i></td>
           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
        	</tr>
    	</table><br>
    	
		<?php 
            $regis =  date('d F Y ', strtotime($term->Basic->dob));
            $today= date('d F Y');
            $days = (strtotime($today) - strtotime($regis)) / (60 * 60 * 24)/365.242199; // tanggal hari ini dikurang tanggal selesai
            $age = substr($days, 0, 2);     
            $dob =  date('Y-m-d ', strtotime($term->Basic->dob));
        ?>
        <table>
   			<tr>
    			<td style="width: 50px">Date of Birth / : <br><i>Tarikh Lahir</i></td>
    			<td style="width: 5px"></td>
     			<td class="clas" style="width: 90px"><?php echo e($dob); ?></td>
     			<td style="width: 10px">&nbsp;</td>
     			<td style="width: 50px">(dd/mm/yyyy) /<br><i>(hh/bb/tttt)</i></td>
     			<td style="width: 40px">Age / : <br><i>Umur</i></td>
    			<td style="width: 5px"></td>
     			<td class="clas"  style="width: 40px"><?php echo e($term->Basic->age); ?></td>
  			</tr>
		</table>
		<table>
			<tr>
				<td style="width: 85px">Gender / <i>Jantina</i> :</td>
				<td style="width: 85px">Bumiputra / <i> Bumiputra</i></td>
				<td style="width: 100px">Nationality / <i>Warganegara </i> :</td>
			</tr>
		</table>
		<table class="bor-table">
        	<tr>
	            <td>
	            	<?php if($term->Basic->gender=="M"): ?> 
				        &nbsp;V&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
	            </td>
             	<td class="bor-noBorder">Male /<i> Lelaki </i></td>
           		<td class="bor-noBorder">&nbsp;</td>
            	<td><?php if($term->Basic->bumi_putera=="yes"): ?> 
			            &nbsp;V&nbsp;
			        <?php else: ?>
			            &nbsp;&nbsp;&nbsp;
			        <?php endif; ?></td>
             	<td class="bor-noBorder" style="width: 40px">Yes /<i> Ya </i></td>
             	<td class="bor-noBorder" style="width: 40px">&nbsp;</td>
             	<td rowspan="2" class="clas" style="width: 130px"><?php echo e($term->Basic->nationality); ?></td>
        	</tr>
	        <tr>
	            <td>
	            	<?php if($term->Basic->gender=="F"): ?> 
				        &nbsp;V&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
	            </td>
             	<td class="bor-noBorder">Female /<i> Perempuan </i></td>
           		<td class="bor-noBorder">&nbsp;</td>
            	<td><?php if($term->Basic->bumiputera=="N"): ?> 
			            &nbsp;V&nbsp;
			        <?php else: ?>
			            &nbsp;&nbsp;&nbsp;
			        <?php endif; ?></td>
             	<td class="bor-noBorder">No /<i> Tidak </i></td>
             	<td class="bor-noBorder">&nbsp;</td>
	        </tr>
    	</table><br>
    	<table>
			<tr>
				<td style="width: 135px">Race / <i>Bangsa</i> :</td>
				<td style="width: 85px"></td>
				<td style="width: 135px"></td>
				<td style="width: 100px">Religion / <i>Agama </i> :</td>
			</tr>
		</table>
		<table class="bor-table">
        	<tr>
	            <td>
	            	<?php if($term->Basic->race=="478"): ?> 
				        &nbsp;V&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
	            </td>
             	<td class="bor-noBorder" style="width: 130px">Malay /<i> Melayu </i></td>
             	<td class="bor-noBorder">&nbsp;</td>
             	<td>
	            	<?php if($term->Basic->religion=="489"): ?> 
				        &nbsp;V&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
	            </td>
             	<td class="bor-noBorder" style="width: 150px">Islam /<i> Islam </i></td>
        	</tr>
	       	<tr>
	            <td>
	            	<?php if($term->Basic->race=="475"): ?> 
				        &nbsp;V&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
	            </td>
             	<td class="bor-noBorder">Chinese /<i> Cina </i></td>
             	<td class="bor-noBorder">&nbsp;</td>
             	<td>
	            	<?php if($term->Basic->religion=="485"): ?> 
				        &nbsp;V&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
	            </td>
             	<td class="bor-noBorder">Buddist /<i> Budha </i></td>
        	</tr>
        	
        	<tr>
        		<td class="bor-noBorder"></td>
        		<td class="bor-noBorder"><i>Lain-lain (Sila nyatakan)</i></td>
        		<td class="bor-noBorder">&nbsp;</td>
             	<td>
	            	<?php if($term->Basic->Religion=="I"): ?> 
				        &nbsp;V&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
	            </td>
             	<td class="bor-noBorder">Others (please specify) /<br> </td>
        	</tr>
        	<tr>
	            <td class="bor-noBorder"></td>
             	<td rowspan="2">
             		<?php if(empty($term->Basic->race_others)): ?> 
             		&nbsp;&nbsp;&nbsp;
		  			<?php else: ?>
		           		<?php echo e($term->Basic->race_others); ?>

		            <?php endif; ?>
             	</td>
             	<td class="bor-noBorder"></td>
             	<td class="bor-noBorder"></td>
        		<td class="bor-noBorder"><i>Lain-lain (Sila nyatakan)</i></td>
        	</tr>
        	<tr>
        		<td class="bor-noBorder"></td>
        		<td class="bor-noBorder"></td>
        		<td class="bor-noBorder"></td>
        		<td rowspan="3">
             		<?php if(empty($term->Basic->religion_others)): ?> 
             		&nbsp;&nbsp;&nbsp;<br>
		  			<?php else: ?>
		           		<?php echo e($term->Basic->religion_others); ?>

		            <?php endif; ?>
             	</td>
        	</tr>
    	</table>
    	<table>
			<tr>
				<td style="width: 310px">Residency Status / <i>Taraf Mastautin</i> :</td>
			</tr>
		</table>
		<table class="bor-table">
        	<tr>
	            <td>
	            	<?php if($term->Basic->residency_status=="yes"): ?> 
				        &nbsp;V&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
	            </td>
             	<td class="bor-noBorder">Resident /<i> Pemastautin </i></td>
           		<td class="bor-noBorder">&nbsp;</td>
            	<td>
            		<?php if($term->Basic->residency_status=="no"): ?> 
				        &nbsp;V&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
				</td>
             	<td class="bor-noBorder" style="width: 40px">Non Resident /<i> Bukan Pemastautin </i></td>
        	</tr>
    	</table><br>
    	<table>
			<tr>
				<td style="width: 310px">Marital Status / <i>Taraf Perkahwinan</i> :</td>
			</tr>
		</table>
		<table class="bor-table">
        	<tr>
	            <td>
	            	<?php if($term->Basic->marital=="6"): ?> 
				        &nbsp;V&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
	            </td>
             	<td class="bor-noBorder">Single /</td>
           		<td class="bor-noBorder">&nbsp;</td>
            	<td>
	            	<?php if($term->Basic->marital=="7"): ?> 
				        &nbsp;V&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
	            </td>
             	<td class="bor-noBorder">Married /</td>
             	<td class="bor-noBorder">&nbsp;</td>
            	<td>
	            	<?php if(($term->Basic->marital=="8") OR ($term->Basic->marital=="9")): ?>
				        &nbsp;V&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
	            </td>
             	<td class="bor-noBorder">Others (please specify) /</td>
             	<td class="bor-noBorder">&nbsp;</td>
             	<td rowspan="2" style="width: 130px"><?php if(($term->Basic->marital=="8") OR ($term->Basic->marital=="9")): ?>
             		<?php echo e($term->Basic->Marital->label); ?>

             		 <?php else: ?>
				        &nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
             	</td>
        	</tr>
        	<tr>
        		<td class="bor-noBorder">&nbsp;</td>
        		<td class="bor-noBorder"><i>Bujang</i></td>
        		<td class="bor-noBorder">&nbsp;</td>
        		<td class="bor-noBorder">&nbsp;</td>
        		<td class="bor-noBorder"><i>Kahwin</i></td>
        		<td class="bor-noBorder">&nbsp;</td>
        		<td class="bor-noBorder">&nbsp;</td>
        		<td class="bor-noBorder"><i>Lain-lain (sila nyatakan)</i></td>
        		<td class="bor-noBorder">&nbsp;</td>
        	</tr>
    	</table><br>
    	 <table>
   			<tr>
    			<td style="width: 90px">No. of Dependants / : <br><i>Bilangan Tanggungan</i></td>
    			<td style="width: 5px"></td>
     			<td class="clas" style="width: 30px"><?php echo e($term->Basic->no_dependants); ?></td>
     			<td style="width: 10px">&nbsp;</td>
     			<td style="width: 50px">No. of Children /<br><i>Bilangan Anak</i></td>
     			<td style="width: 5px"></td>
     			<td class="clas" style="width: 30px"><?php echo e($term->Basic->no_children); ?></td>
    			<td style="width: 45px">18 Years Old / <i>Tahun</i><br> 18 Years Old / <i>Tahun</i></td>
  			</tr>
		</table><br>
    	<table>
			<tr>
				<td>Highest Education Level / <i>Taraf Pendidikan Tertinggi</i></td>
				<td style="width: 135px"></td>
			</tr>
		</table>
		<table class="bor-table">
        	<tr>
	            <td>
	            	<?php if($term->Basic->highest_education=="582"): ?> 
				        &nbsp;V&nbsp;&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
				</td>
             	<td class="bor-noBorder" style="width: 310px">Post Graduate /<i>Lepasan Ijazah</i></td>
        	</tr>
	       	<tr>
	            <td><?php if($term->Basic->highest_education=="583"): ?> 
				        &nbsp;V&nbsp;&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
				</td>
             	<td class="bor-noBorder">Professional Qualification /<i>Kelulusan Profesional</i></td>
        	</tr>
        	<tr>
	            <td>
	            	<?php if($term->Basic->highest_education=="584"): ?> 
				        &nbsp;V&nbsp;&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
				</td>
             	<td class="bor-noBorder">Degree /<i>Ijazah</i></td>
        	</tr>
        	<tr>
	            <td><?php if($term->Basic->highest_education=="585"): ?> 
				        &nbsp;V&nbsp;&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
				</td>
             	<td class="bor-noBorder">Diploma /<i>Diploma</i></td>
        	</tr>
        	<tr>
	            <td><?php if($term->Basic->highest_education=="586"): ?> 
				        &nbsp;V&nbsp;&nbsp;
				    <?php else: ?>
				        &nbsp;&nbsp;&nbsp;&nbsp;
				    <?php endif; ?>
				</td>
             	<td class="bor-noBorder">Secondary /<i>Sek. Menengah</i></td>
        	</tr>
        	<tr>
	            <td></td>
             	<td class="bor-noBorder">Others (please specify)  /<i> Lain-lain (sila nyatakan) : </i></td>
        	</tr>
        	<tr>
	            <td class="bor-noBorder"></td>
             	<td><?php if($term->Basic->highest_education=="591"): ?>
              			<?php echo e($term->Basic->highest_education_others); ?> <br><br>
              		<?php else: ?>
              			&nbsp;&nbsp;&nbsp;&nbsp;<br><br>
              		<?php endif; ?></td>
        	</tr>
    	</table><br>
  	</div>
  <div class="column">
    	<table>
    		<tr>
    			<td colspan="2" class="border header" style="width: 320px">
       				B) term->Basic's Information(Cont'd) / <i>Maklumat Peribadi Pemohon (Bersambung)</i>
    			</td>
  			</tr>	
    	</table>
    	<table>
  			<tr>
  				<td style="width: 300px">Residental Address / <i>Alamat Kediaman</i> :</td>
  				<td>&nbsp;</td>
  			</tr>
  			<tr>
  				<td class="clas1" style="width: 320px;height: 30px; text-transform: uppercase;" >&nbsp;<?php echo e($term->Basic->address); ?></td>
  			</tr>
  		</table>
       	<table>
       		<tr>
       			<td style="width: 100px">Postcode / <i>Poskod</i></td>
       			<td>&nbsp;</td>
       			<td style="width: 100px">State / <i>Negeri</i></td>
       			<td>&nbsp;</td>
       			<td style="width: 100px">Country / <i>Negara</i></td>
       		</tr>
       		<tr>
       			<td class="clas">&nbsp;<?php echo e($term->Basic->postcode_); ?></td>
       			<td>&nbsp;</td>
       			<td class="clas">&nbsp;<?php echo e($term->Basic->state); ?></td>
       			<td>&nbsp;</td>
       			<td class="clas">&nbsp;<?php echo e($term->Basic->country); ?></td>
       		</tr>
       </table>
       <table>
			<tr>
				<td>Ownership Status / <i> Taraf Pemilikan : </i></td>
			</tr>
			<tr>
                <td>
                  	<table style="border-spacing: 0;">
	                    <tr>
	                      	<td class="border">
                                <?php if($term->Basic->ownership=="LP"): ?> 
                                    &nbsp;V&nbsp;&nbsp;
                                <?php else: ?>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                <?php endif; ?>
                            </td>
	                      	<td style="width: 280px">&nbsp;Owned (free from encumbrances) /<i>Sendiri (bebas daripada bebanan)</i></td>
	                    </tr>
	                    <tr>
	                      	<td class="border">
                                <?php if($term->Basic->ownership=="588"): ?> 
                                    &nbsp;V&nbsp;&nbsp;
                                <?php else: ?>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                <?php endif; ?>    
                            </td>
	                      	<td>&nbsp;Rented  /<i>Sewa</i></td>
	                    </tr>
	                    <tr>
	                      	<td class="border">
                                <?php if($term->Basic->ownership=="589"): ?> 
                                    &nbsp;V&nbsp;&nbsp;
                                <?php else: ?>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                <?php endif; ?>    
                            </td>
	                      	<td>&nbsp;Parents  /<i>Ibubapa</i></td>
	                    </tr>
	                    <tr>
	                      	<td class="border">
                                <?php if($term->Basic->ownership=="590"): ?> 
                                    &nbsp;V&nbsp;&nbsp;
                                <?php else: ?>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                <?php endif; ?>     
                            </td>
	                      	<td>&nbsp;Encumbered   /<i>Bebanan</i></td>
	                    </tr>
	                     <tr>
	                      	<td class="border">
                                <?php if(($term->Basic->ownership_status!="587") OR 
                                ($term->Basic->ownership!="588") OR 
                                ($term->Basic->ownership!="589")  
                                OR ($term->Basic->ownership!="590")): ?> 
                                    &nbsp;&nbsp;&nbsp;
                                <?php else: ?>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                <?php endif; ?>     
                            </td>
	                      	<td>&nbsp;Others (please specify)  /<i> Lain-lain (sila nyatakan) : </i></td>
	                    </tr>
	                    <tr>
	                      	<td>&nbsp;<br><br></td>
	                      	<td class="clas" >
                                <?php if(($term->Basic->ownership!="587") 
                                OR ($term->Basic->ownership!="588") OR
                                 ($term->Basic->ownership!="589")  OR
                                  ($term->Basic->ownership!="590")): ?>
                                    <?php echo e($term->Basic->ownership_other); ?> 
                                <?php endif; ?>
	                      	</td>
	                    </tr>
                	</table><br>

                	<table>
			   			<tr>
			    			<td style="width: 250px">Duration in Current Residence / <i>Tempoh Menetap Di Kediaman Sekarang</i>: </td>
			     			<td class="clas" style="width: 60px"><?php echo e($term->Basic->duration_residence); ?> YEARS</td>
			  			</tr>
					</table>
					<table>
						<tr>
							<td style="width: 150px">Tel. House / <i>Rumah</i> :</td>
      						<td style="width: 5px">&nbsp;</td>
      						<td style="width: 150px">Tel. Mobile / <i>Bimbit </i> :</td>
      					</tr>
      					<tr>
      						<td class="clas"><?php echo e($term->Basic->corres_homephone); ?></td>
      						<td>&nbsp;</td>
      						<td class="clas"><?php echo e($term->Basic->corres_mobilephone); ?></td>
      					</tr>
      				</table>
      				<table>
			  			<tr>
			  				<td style="width: 300px">Mailing Address (if different from residential address)  /<br> <i>Alamat Surat-Menyurat (jika berbeza daripada alamat kediaman)</i> :</td>
			  				<td>&nbsp;</td>
			  			</tr>
			  			<tr>
			  				<td class="clas1" style="width: 320px;height: 30px; text-transform: uppercase;">&nbsp;<?php echo e($term->Basic->address); ?></td>
			  			</tr>
			  		</table>
			       	<table>
			       		<tr>
			       			<td style="width: 100px">Postcode / <i>Poskod</i></td>
			       			<td>&nbsp;</td>
			       			<td style="width: 100px">State / <i>Negeri</i></td>
			       			<td>&nbsp;</td>
			       			<td style="width: 100px">Country / <i>Negara</i></td>
			       		</tr>
			       		<tr>
			       			<td class="clas">&nbsp;<?php echo e($term->Basic->postcode); ?></td>
			       			<td>&nbsp;</td>
			       			<td class="clas">&nbsp;<?php echo e($term->Basic->state); ?></td>
			       			<td>&nbsp;</td>
			       			<td class="clas">&nbsp;<?php echo e($term->Basic->country); ?></td>
			       		</tr>
			       </table>

			       <table>
	                    <tr>
	                    	<td style="width: 200px">Any relationship to I DESTINASI SDN BHD employee(s)?/: <br><i>Sebarang hubungan dengan pekerja I DESTINASI SDN BHD ?</i></td>
							<td class="border" style="height: 3px">
	                      		<?php if($term->Basic->idsb_relation=="yes"): ?> 
						            &nbsp;v&nbsp;
						        <?php else: ?>
						            &nbsp;&nbsp;&nbsp;
						        <?php endif; ?></td>
	                      	<td style="width: 50px;height: 3px">Yes/<i> Ya</i></td>
	                      	<td class="border" style="height: 3px">
	                      		<?php if($term->Basic->idsb_relation=="no"): ?> 
						            &nbsp;v&nbsp;
						        <?php else: ?>
						            &nbsp;&nbsp;&nbsp;
						        <?php endif; ?></td>
	                      	<td style="width: 50px;">No /<i> Tidak </i></td>
	                    </tr>
                	</table><br>
			       	<table>
      					<tr>
      						<td style="width: 310px">If "Yes" please specify  / <i>/ Jika "Ya" sila nyatakan</i> :</td>
      					</tr><br>
      					<tr>
      						<td class="clas">&nbsp;<?php echo e($term->Basic->relationship_idsb); ?></td>
      					</tr>
      					<tr>
      						<td>(E.g.: Husband, Wife, Parents, Child/Children, etc. / <br> <i>Contoh: Suami, Isteri, Ibubapa, Anak/Anak-anak, lain-lain.)</i></td>
      					</tr>
      				</table><br>
      				<table>
			    		<tr>
			    			<td colspan="2" class="border header" style="width: 320px">
			       				C) term->Basic's Employment Information  / <i>/ Maklumat Pekerjaan Pemohon</i>
			    			</td>
			  			</tr>	
			    	</table>
			    	<table>
			   			<tr>
			    			<td style="width: 40px">Staff ID / <br><i>No. Pekerja</i></td>
			    			<td style="width: 10px">:</td>
			     			<td class="clas" style="width: 60px"><?php echo e($term->Basic->staff_id); ?></td>
			     			<td style="width: 10px">&nbsp;</td>
			     			<td style="width: 50px">Occupation / <br><i>Jawatan</i></td>
			    			<td style="width: 10px">:</td>
			     			<td class="clas"  style="width: 110px"><?php echo e($term->Basic->label); ?></td>
			  			</tr>
					</table>
					<table>
			   			<tr>
			    			<td style="width: 80px">Department / <br><i>Bahagian</i></td>
			    			<td style="width: 10px">:</td>
			     			<td class="clas" style="width: 220px"><?php echo e($term->Basic->department); ?></td>
			  			</tr>
					</table>
					<table>
			   			<tr>
			    			<td style="width: 80px">Employer's Name  / <br><i>Nama Majikan</i></td>
			    			<td style="width: 10px">:</td>
			     			<td class="clas" style="width: 220px"><?php echo e($term->Basic->employer_name); ?></td>
			  			</tr>
					</table><br>
					<table>
			  			<tr>
			  				<td style="width: 300px">Office Address /<i>Alamat Pejabat</i> :</td>
			  				<td>&nbsp;</td>
			  			</tr>
			  			<tr>
			  				<td class="clas1" style="width: 320px;height: 30px">&nbsp;<?php echo e($term->Basic->office_address); ?></td>
			  			</tr>
			  		</table><br>
			       	<table>
			       		<tr>
			       			<td style="width: 100px">Postcode / <i>Poskod</i></td>
			       			<td style="width: 5px">&nbsp;</td>
			       			<td style="width: 100px">State / <i>Negeri</i></td>
			       			<td style="width: 5px">&nbsp;</td>
			       			<td style="width: 100px">Country / <i>Negara</i></td>
			       		</tr>
			       		<tr>
			       			<td class="clas">&nbsp;<?php echo e($term->Basic->postcode); ?></td>
			       			<td>&nbsp;</td>
			       			<td class="clas">&nbsp;<?php echo e($term->Basic->state); ?></td>
			       			<td>&nbsp;</td>
			       			<td class="clas">&nbsp;<?php echo e($term->Basic->country); ?></td>
			       		</tr>
			       </table><br>
			       <table> 
      					<tr>
      						<td style="width: 320px">Nature of Business / <i> Bidang Perniagaan </i> :</td>
      					</tr>
      					<tr>
      						<td class="clas" style="text-transform: uppercase;">&nbsp;<?php echo e($term->Basic->label); ?></td>
      					</tr>
      				</table><br>
      				<table>
			   			<tr>
			    			<td style="width: 60px">Years of Service  / <br><i>Tempoh Berkhidmat</i></td>
			    			<td style="width: 10px">:</td>
			     			<td class="clas" style="width: 40px"><?php echo e($term->Basic->year_service); ?></td>
			     			<td style="width: 10px">&nbsp;</td>
			     			<td style="width: 100px">Total Years of Working Experience  / <br><i>Jumlah Tahun Pengalaman Bekerja</i></td>
			    			<td style="width: 10px">:</td>
			     			<td class="clas"  style="width: 45px"><?php echo e($term->Basic->total_year_working); ?></td>
			  			</tr>
					</table><br>
					<table>
						<tr>
							<td style="width: 120px">Tel. Office / <i>Pejabat</i> :</td>
      						<td style="width: 5px">&nbsp;</td>
      						<td style="width: 5px">&nbsp;</td>
      						<td style="width: 45px">&nbsp;</td>
      						<td style="width: 5px">&nbsp;</td>
      						<td style="width: 100px">Fax. No/ <i>No. Faks </i> :</td>
      					</tr>
      					<tr>
      						<td class="clas"><?php echo e($term->Basic->tel_office); ?></td>
      						<td>&nbsp;</td>
      						<td style="width: 5px">(Ext./ <br> Samb.)</td>
      						<td class="clas"></td>
      						<td>&nbsp;</td>
      						<td class="clas"><?php echo e($term->Basic->fax); ?></td>
      					</tr>
      				</table><br>
      				<table> 
      					<tr>
      						<td style="width: 320px">E-mail Address / <i> Alamat E-mel</i> :</td>
      					</tr>
      					<tr>
      						<td class="clas">&nbsp;<?php echo e($term->Basic->email); ?></td>
      					</tr>
      				</table><br>
					<table>
			   			<tr>
			    			<td style="width: 70px">Monthly Income  / <br><i>Pendapatan Bulanan</i></td>
			    			<td style="width: 10px">:</td>
			     			<td class="clas" style="width: 60px">RM <?php echo e($term->Basic->monthly_income); ?></td>
			     			<td style="width: 10px">&nbsp;</td>
			     			<td style="width: 70px">Other Income  / <br><i>Pendapatan Lain</i></td>
			    			<td style="width: 10px">:</td>
			     			<td class="clas"  style="width: 60px">RM <?php echo e($term->Basic->other_income); ?></td>
			  			</tr>
					</table>
                </td>
            </tr>
        </table><br>
  </div>
</div>

	<!--PAGE 2 START-->
	<table width="100%"  cellspacing="4" style="page-break-before: always" id="customers"> 
		<div class="row">
  			<div class="column">
		  		<table>
		    		<tr>
		    			<td colspan="2" class="border header" style="width: 320px">
		       				D) Application's Spouse Information  / <i>/ Maklumat Pasangan Pemohon</i>
		    			</td>
		  			</tr>	
		    	</table><br>
		    	<table class="bor-table">
		        	<tr>
		        		<td class="bor-noBorder" style="width: 40px">Salutation / :</td>
			            <td>
                            <?php if($term->Basic->marital==7): ?>
                                <?php if(empty($spouse->salutation)): ?>
                                    &nbsp;&nbsp;&nbsp;
                                <?php else: ?>
                                    <?php if($spouse->SalutationSpouse->id=="148"): ?> 
                                        &nbsp;V&nbsp;
                                    <?php else: ?>
                                        &nbsp;&nbsp;&nbsp;
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                 &nbsp;&nbsp;&nbsp;
                            <?php endif; ?>
			            	
			            </td>
		             	<td class="bor-noBorder">Mr. /</td>
		           		<td class="bor-noBorder">&nbsp;</td>
		            	<td>
                            <?php if($term->Basic->marital==7): ?>
                                <?php if(empty($spouse->salutation)): ?>
                                    &nbsp;&nbsp;&nbsp;
                                <?php else: ?>
                                    <?php if($spouse->SalutationSpouse->id=="159"): ?> 
                                        &nbsp;V&nbsp;
                                    <?php else: ?>
                                        &nbsp;&nbsp;&nbsp;
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                 &nbsp;&nbsp;&nbsp;
                            <?php endif; ?>
			            </td>
		             	<td class="bor-noBorder">Mdm. /</td>
		             	<td class="bor-noBorder">&nbsp;</td>
		            	<td>
                            <?php if($term->Basic->marital==7): ?>
                                <?php if(empty($spouse->salutation)): ?>
                                    &nbsp;&nbsp;&nbsp;
                                <?php else: ?>
        			            	<?php if($spouse->SalutationSpouse->id=="149"): ?> 
        						        &nbsp;V&nbsp;
        						    <?php else: ?>
        						        &nbsp;&nbsp;&nbsp;
        						    <?php endif; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                 &nbsp;&nbsp;&nbsp;
                            <?php endif; ?>
			            </td>
		             	<td class="bor-noBorder">Ms. /</td>
		             	<td class="bor-noBorder">&nbsp;</td>
		            	<td>
                            <?php if($term->Basic->marital==7): ?>
                                <?php if(empty($spouse->salutation)): ?>
                                    &nbsp;&nbsp;&nbsp;
                                <?php else: ?>
        			            	<?php if(($spouse->SalutationSpouse->id!="149") OR ($spouse->Spouse->SalutationSpouse->id!="148") OR ($spouse->SalutationSpouse->id!="159")): ?>
        						        &nbsp;V&nbsp;
        						    <?php else: ?>
        						        &nbsp;&nbsp;&nbsp;
        						    <?php endif; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                 &nbsp;&nbsp;&nbsp;
                            <?php endif; ?>

			            </td>
		             	<td class="bor-noBorder" style="width: 80px">Others (please specify) /</td>
		             	<td class="bor-noBorder">&nbsp;</td>
		             	<td rowspan="2" style="width: 73px">
                            <?php if($term->Basic->marital==7): ?>
                                 <?php if(empty($spouse->salutation)): ?>
                                &nbsp;&nbsp;&nbsp;
                                <?php else: ?>
                                    <?php if(($spouse->SalutationSpouse->id!="149") OR ($spouse->SalutationSpouse->id!="148") OR ($spouse->SalutationSpouse->id!="159")): ?>
                                    <?php echo e($spouse->SalutationSpouse->label); ?>

                                    <?php else: ?>
                                    &nbsp;&nbsp;&nbsp;
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                 &nbsp;&nbsp;&nbsp;
                            <?php endif; ?>
                           
                           
		             	</td>
		        	</tr>
		        	<tr>
		        		<td class="bor-noBorder"><i>Gelaran</i></td>
		        		<td class="bor-noBorder">&nbsp;</td>
		        		<td class="bor-noBorder"><i>Encik</i></td>
		        		<td class="bor-noBorder">&nbsp;</td>
		        		<td class="bor-noBorder">&nbsp;</td>
		        		<td class="bor-noBorder"><i>Puan</i></td>
		        		<td class="bor-noBorder">&nbsp;</td>
		        		<td class="bor-noBorder">&nbsp;</td>
		        		<td class="bor-noBorder"><i>Cik</i></td>
		        		<td class="bor-noBorder">&nbsp;</td>
		        		<td class="bor-noBorder">&nbsp;</td>
		        		<td class="bor-noBorder"><i>Lain-lain (sila nyatakan)</i></td>
		        		<td class="bor-noBorder">&nbsp;</td>
		        	</tr>
		    	</table><br>
		    	<table>
					<tr>
						<td style="width: 320px">Full Name as per NRIC/Passport / <i>Nama Penuh seperti di dalam Kad Pengenalan/Pasport</i> :</td>
					</tr>
					<tr>
						<td class="clas">&nbsp;<?php echo e($spouse->fullname); ?></td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="width: 155px">New NRIC No. / <i>No. Kad Pengenalan Baru </i> :</td>
						<td style="width: 5px">&nbsp;</td>
						<td style="width: 150px">Old NRIC No. / <i>No. Kad Pengenalan Lama </i> :</td>
					</tr>
					<tr>
						<td class="clas" ><?php if($term->Basic->marital==7): ?>
                                <?php echo e($spouse->ic); ?>

                            <?php else: ?>
                                 &nbsp;&nbsp;&nbsp;
                            <?php endif; ?></td>
						<td >&nbsp;</td>
						<td class="clas" ></td>
					</tr>
				</table><br>
				<table>
					<tr>
						<td style="width: 310px">Other Identification (If without NRIC Information)/ <br><i>Pengenalan Lain (Jika Tiada Maklumat Kad Pengenalan)</i>:</td>
					</tr>
				</table>
				<table class="bor-table">
		        	<tr>
			            <td style="width: 8px">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		             	<td class="bor-noBorder"  style="width: 80px">Army No. /</td>
		             	<td class="bor-noBorder"  style="width: 40px">&nbsp;</td>
		           		<td rowspan="2" style="width: 182px">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        	</tr>
		        	<tr>
		        		 <td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		             	<td class="bor-noBorder"><i>No. Tentera</i></td>
		           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		        	</tr>
		        	<tr>
			            <td>&nbsp;&nbsp;&nbsp;&nbsp; </td>
		             	<td class="bor-noBorder">Police No. /</td>
		           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		           		<td rowspan="2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        	</tr>
		        	<tr>
		        		 <td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		             	<td class="bor-noBorder"><i>No. Polis</i></td>
		           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		        	</tr>
		        	<tr>
			            <td>&nbsp;&nbsp;&nbsp;&nbsp; </td>
		             	<td class="bor-noBorder">Others (please specify) /</td>
		           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		           		<td  rowspan="2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        	</tr>
		        	<tr>
		        		 <td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		             	<td class="bor-noBorder"><i>Lain-lain (sila nyatakan)</i></td>
		           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		        	</tr>
		    	</table><br>

		        <table>
		   			<tr>
		    			<td style="width: 50px">Date of Birth / : <br><i>Tarikh Lahir</i></td>
		    			<td style="width: 5px"></td>
		     			<td class="clas" style="width: 90px">
                            <?php if($term->Basic->marital==7): ?>
                                <?php echo e($spouse->dob); ?>

                            <?php else: ?>
                                 &nbsp;&nbsp;&nbsp;
                            <?php endif; ?>
                        </td>
		     			<td style="width: 10px">&nbsp;</td>
		     			<td style="width: 50px">(dd/mm/yyyy) /<br><i>(hh/bb/tttt)</i></td>
		     			<td style="width: 40px">Age / : <br><i>Umur</i></td>
		    			<td style="width: 5px"></td>
		     			<td class="clas"  style="width: 46px">
                            <?php if($term->Basic->marital==7): ?>
                                <?php echo e($spouse->age); ?> years
                            <?php else: ?>
                                 &nbsp;&nbsp;&nbsp;
                            <?php endif; ?></td>
		  			</tr>
				</table>
				<table>
					<tr>
						<td style="width: 85px">Gender / <i>Jantina</i> :</td>
						<td style="width: 85px">Bumiputra / <i> Bumiputra</i></td>
						<td style="width: 100px">Nationality / <i>Warganegara </i> :</td>
					</tr>
				</table>
				<table class="bor-table">
		        	<tr>
			            <td>
                            <?php if(empty($spouse->gender)): ?>
                                &nbsp;&nbsp;&nbsp;
                            <?php else: ?>
    			            	<?php if($spouse->Gender->id=="146"): ?> 
    						        &nbsp;/&nbsp;
    						    <?php else: ?>
    						        &nbsp;&nbsp;&nbsp;
    						    <?php endif; ?>
                            <?php endif; ?>
			            </td>
		             	<td class="bor-noBorder">Male /<i> Lelaki </i></td>
		           		<td class="bor-noBorder">&nbsp;</td>
		            	<td><?php if($spouse->bumi_putera=="yes"): ?> 
					            &nbsp;/&nbsp;
					        <?php else: ?>
					            &nbsp;&nbsp;&nbsp;
					        <?php endif; ?></td>
		             	<td class="bor-noBorder" style="width: 40px">Yes /<i> Ya </i></td>
		             	<td class="bor-noBorder" style="width: 40px">&nbsp;</td>
		             	<td rowspan="2" class="clas" style="width: 135px">
                            <?php if(empty($spouse->nationality)): ?> 
                                 &nbsp;&nbsp;&nbsp;
                            <?php else: ?>
                                <?php echo e($spouse->Nationality->id); ?>

                            <?php endif; ?>
                        </td>
		        	</tr>
			        <tr>
			            <td>
                            <?php if(empty($spouse->gender)): ?>
                                &nbsp;&nbsp;&nbsp;
                            <?php else: ?>
    			            	<?php if($spouse->Gender->id=="147"): ?> 
    						        &nbsp;/&nbsp;
    						    <?php else: ?>
    						        &nbsp;&nbsp;&nbsp;
    						    <?php endif; ?>
                            <?php endif; ?>
			            </td>
		             	<td class="bor-noBorder">Female /<i> Perempuan </i></td>
		           		<td class="bor-noBorder">&nbsp;</td>
		            	<td><?php if($spouse->bumi_putera=="no"): ?> 
					            &nbsp;/&nbsp;
					        <?php else: ?>
					            &nbsp;&nbsp;&nbsp;
					        <?php endif; ?></td>
		             	<td class="bor-noBorder">No /<i> Tidak </i></td>
		             	<td class="bor-noBorder">&nbsp;</td>
			        </tr>
		    	</table><br>
		    	<table>
					<tr>
						<td style="width: 135px">Race / <i>Bangsa</i> :</td>
						<td style="width: 85px"></td>
						<td style="width: 135px"></td>
					</tr>
				</table>
				<table class="bor-table">
		        	<tr>
			            <td>
                            <?php if(empty($spouse->race)): ?>
                                &nbsp;&nbsp;&nbsp;
                            <?php else: ?>
                                <?php if($spouse->Races->id=="478"): ?> 
                                    &nbsp;/&nbsp;
                                <?php else: ?>
                                    &nbsp;&nbsp;&nbsp;
                                <?php endif; ?>
                            <?php endif; ?>
    			            	
			            </td>
		             	<td class="bor-noBorder" style="width: 70px">Malay /<i> Melayu </i></td>
		             	<td class="bor-noBorder" style="width: 230px">Others (please specify) / <i>Lain-lain (sila nyatakan) :</i></td>
		        	</tr>
			       	<tr>
			            <td>
                            <?php if(empty($spouse->race)): ?>
                                &nbsp;&nbsp;&nbsp;
                            <?php else: ?>
    			            	<?php if($spouse->Races->id=="475"): ?> 
    						        &nbsp;/&nbsp;
    						    <?php else: ?>
    						        &nbsp;&nbsp;&nbsp;
    						    <?php endif; ?>
                            <?php endif; ?>
			            </td>
		             	<td class="bor-noBorder">Chinese /<i> Cina </i></td>
		             	<td rowspan="2"><?php if(empty($term->Basic->race_others)): ?> 
		             		&nbsp;&nbsp;&nbsp;
				  			<?php else: ?>
				           		<?php echo e($term->Basic->race_others); ?>

				            <?php endif; ?></td>
		        	</tr>
		        	<tr>
			            <td>
                            <?php if(empty($spouse->race)): ?>
                                &nbsp;&nbsp;&nbsp;
                            <?php else: ?>
    			            	<?php if($spouse->Races->id=="477"): ?> 
    						        &nbsp;/&nbsp;
    						    <?php else: ?>
    						        &nbsp;&nbsp;&nbsp;
    						    <?php endif; ?>
                            <?php endif; ?>

			            </td>
		             	<td class="bor-noBorder">Indian /<i> India </i></td>
		        	</tr>
		    	</table>
		    	<table>
					<tr>
						<td style="width: 310px">Residency Status / <i>Taraf Mastautin</i> :</td>
					</tr>
				</table>
				<table class="bor-table">
		        	<tr>
			            <td>
			            	<?php if($spouse->residency_status=="yes"): ?> 
						        &nbsp;/&nbsp;
						    <?php else: ?>
						        &nbsp;&nbsp;&nbsp;
						    <?php endif; ?>
			            </td>
		             	<td class="bor-noBorder">Resident /<i> Pemastautin </i></td>
		           		<td class="bor-noBorder">&nbsp;</td>
		            	<td>
		            		<?php if($spouse->residency_status=="no"): ?> 
						        &nbsp;/&nbsp;
						    <?php else: ?>
						        &nbsp;&nbsp;&nbsp;
						    <?php endif; ?>
						</td>
		             	<td class="bor-noBorder" style="width: 40px">Non Resident /<i> Bukan Pemastautin </i></td>
		        	</tr>
		    	</table><br>
		  		<table>
		    		<tr>
		    			<td colspan="2" class="border header" style="width: 320px">
		       				E) Emergency Contact Person Information  / <i>/ Rujukan Semasa Kecemasan</i>
		    			</td>
		  			</tr>	
		    	</table>
		    	<table>
					<tr>
					    <td style="width: 80px">Relationship with term->Basic / <br><i>Hubungan Dengan Pemohon</i></td>
		    			<td style="width: 10px">:</td>
		     			<td class="clas" style="width: 210px;text-transform: uppercase;">
		     				<?php if(empty($term->Basic->relationship)): ?> 
				  			<?php else: ?>
				           		<?php echo e($term->Basic->relationship); ?>

				            <?php endif; ?>
				        </td>
		  			</tr>
				</table>
				<table>
					<tr>
						<td style="width: 317px">Full Name / <i>Nama Penuh</i> :</td>
					</tr>
					<tr>
						<td class="clas" style="text-transform: uppercase;">&nbsp;<?php echo e($term->Basic->fullname); ?></td>
					</tr>
				</table>
				<table>
		  			<tr>
		  				<td style="width: 310px">Residental Address /<i>Alamat Kediamann</i> :</td>
		  			</tr>
		  			<tr>
		  				<td class="clas1" style="width: 310px;height: 30px;text-transform: uppercase;">&nbsp;<?php echo e($term->Basic->residental_address); ?></td>
		  			</tr>
		  		</table><br>
		       	<table cellspacing="0">
		       		<tr>
		       			<td style="width: 105px">Postcode / <i>Poskod</i></td>
		       			<td style="width: 105px">State / <i>Negeri</i></td>
		       			<td style="width: 105px">Country / <i>Negara</i></td>
		       		</tr>
		       		<tr>
		       			<td class="clas">&nbsp;<?php echo e($term->Basic->postcode); ?></td>
		       			<td class="clas">&nbsp;<?php echo e($term->Basic->state); ?></td>
		       			<td class="clas">&nbsp;<?php echo e($term->Basic->country); ?></td>
		       		</tr>
		       </table>
		       <table cellspacing="0">
					<tr>
					    <td style="width: 80px">Tel.House / <br><i>Rumah</i></td>
		    			<td style="width: 10px">:</td>
		     			<td class="clas" style="width: 225px">
		     				<?php if(empty($term->Basic->tel_house)): ?> 
				  			<?php else: ?>
				           		<?php echo e($term->Basic->tel_house); ?>

				            <?php endif; ?>
		     			</td>
		  			</tr>
		  			<tr>
					    <td style="width: 80px">Tel.Office / <br><i>Pejabat</i></td>
		    			<td style="width: 10px">:</td>
		     			<td class="clas" style="width: 225px">
		     				<?php if(empty($term->Basic->tel_office)): ?> 
				  			<?php else: ?>
				           		<?php echo e($term->Basic->tel_office); ?>

				            <?php endif; ?>
				        </td>
		  			</tr>
		  			<tr>
					    <td style="width: 80px">Handphone No / <br><i>No. Telefon Bimbit</i></td>
		    			<td style="width: 10px">:</td>
		     			<td class="clas" style="width: 225px">
		     				<?php if(empty($term->Basic->no_handphone)): ?> 
				  			<?php else: ?>
				           		<?php echo e($term->Basic->no_handphone); ?>

				            <?php endif; ?>
		     			</td>
		  			</tr>
				</table><BR>
				<table style="font-size: 10px;" >
		    		<tr>
		    			<td colspan="2" class="border header" style="width: 320px">
		       				F) Commitments With I DESTINASI SDN BHD or any Financial Institutions  /<br> <i>Tanggungan Dengan I DESTINASI SDN BHD atau Institusi Kewangan Lain</i>
		    			</td>
		  			</tr>	
		    	</table>
				<table>
					<tr>
						<td>(a) term->Basic / <i>Pemohon</i></td>
					</tr>
				</table>
				<table class="bor-table">
					<tr>
			            <td class="bor-noBorder" style="width: 8px"></td>
		             	<td class="bor-noBorder" style="width: 100px">Financial Institution /<br><i>Institusi Kewangan</i></td>
		             	<td class="bor-noBorder" style="width: 90px">Facility Type /<br><i>Jenis Kemudahan</td>
		           		<td class="bor-noBorder" style="width: 120px">Outstanding Balance  /<br><i>Baki Semasa</i></td>
		        	</tr>
		        	<tr>
			            <td class="bor-noBorder">1<br></td>
		             	<td>&nbsp;<br><br></td>
		             	<td>&nbsp;<br><br></td>
		           		<td>&nbsp;<br><br></td>
		        	</tr>
		        	<tr>
			            <td class="bor-noBorder">2<br></td>
		             	<td>&nbsp;<br><br></td>
		             	<td>&nbsp;<br><br></td>
		           		<td>&nbsp;<br><br></td>
		        	</tr>
		        	<tr>
			            <td class="bor-noBorder">3<br></td>
		             	<td>&nbsp;<br><br></td>
		             	<td>&nbsp;<br><br></td>
		           		<td>&nbsp;<br><br></td>
		        	</tr>
		        	<tr>
			            <td class="bor-noBorder">4<br></td>
		             	<td>&nbsp;<br><br></td>
		             	<td>&nbsp;<br><br></td>
		           		<td>&nbsp;<br><br></td>
		        	</tr>
		    	</table><br>
		    	<table>
					<tr>
						<td>(b) Spouse / <i>Paasangan Pemohon</i></td>
					</tr>
				</table>
				<table class="bor-table">
					<tr>
			            <td class="bor-noBorder" style="width: 8px"></td>
		             	<td class="bor-noBorder" style="width: 100px">Financial Institution /<br><i>Institusi Kewangan</i></td>
		             	<td class="bor-noBorder" style="width: 90px">Facility Type /<br><i>Jenis Kemudahan</td>
		           		<td class="bor-noBorder" style="width: 120px">Outstanding Balance  /<br><i>Baki Semasa</i></td>
		        	</tr>
		        	<tr>
			            <td class="bor-noBorder">1<br></td>
		             	<td>&nbsp;<br><br></td>
		             	<td>&nbsp;<br><br></td>
		           		<td>&nbsp;<br><br></td>
		        	</tr>
		        	<tr>
			            <td class="bor-noBorder">2<br></td>
		             	<td>&nbsp;<br><br></td>
		             	<td>&nbsp;<br><br></td>
		           		<td>&nbsp;<br><br></td>
		        	</tr>
		        	<tr>
			            <td class="bor-noBorder">3<br></td>
		             	<td>&nbsp;<br><br></td>
		             	<td>&nbsp;<br><br></td>
		           		<td>&nbsp;<br><br></td>
		        	</tr>
		        	<tr>
			            <td class="bor-noBorder">4<br></td>
		             	<td>&nbsp;<br><br></td>
		             	<td>&nbsp;<br><br></td>
		           		<td>&nbsp;<br><br></td>
		        	</tr>
		    	</table>
  			</div>
  		<!--PAGE 2 COLUMN-->
  			<div class="column">
		    	<table style="font-size: 10px;">
		    		<tr>
		    			<td colspan="2" class="border header" style="width: 330px">
		       				G) Declaration / <i>Pengakuan</i>
		    			</td>
		  			</tr>	
		    	</table>
    			<b style="font-size: 11px;  " >By signing below, I hereby declare acknowledge and confirm as follows:</b>
    			<br><i style="font-size: 11px;" >Dengan menandatangi di bawah ini, saya dengan ini mengisytiharkan, mengakui, mempersetujui dan mengesahkan seperti berikut:</i>
                  			<ol style="list-style-type: none;"><li>
		    	<table cellpadding="4"  style="font-size: 9px;width: 310px;line-height: 140%;">
			        <tr>
			          <td valign='top'>
			                <b  style="font-size: 9px; ">1.</b>
			            </td>
			            <td><div align="justify"  style="font-size: 9px; ">
			              <b>I am desirous of purchasing the Products on deferred payment terms by way of deduction of my salary. </b><br><i> 

			              Saya ingin membeli Produk-produk dengan pembayaran tertunda melalui potongan gaji saya. </i><br></div>
			            </td>
			        </tr>
			        <tr>
			          <td valign='top'>
			                <b  style="font-size: 9px; ">2.</b>
			            </td>
			            <td><div align="justify"  style="font-size: 9px; ">
			              <b>I am not a bankrupt or facing any bankruptcy actions and the above information given by me is true and complete.</b><br><i> 

			              Saya tidak muflis atau tidak menghadapi sebarang tindakan kemuflisan dan semua maklumat yang telah saya berikan di atas adalah benar dan lengkap.</i><br></div>
			            </td>
			        </tr>
			        <tr>
			          <td valign='top'>
			                <b  style="font-size: 9px; ">3.</b>
			            </td>
			            <td><div align="justify"  style="font-size: 9px; ">
			              <b>Save as disclosed herein, that none of my spouse, parents and/or child/children is/are employees of I DESTINASI SDN BHD. </b><br><i> 

			              Selain daripada yang dinyatakan di sini, bahawa tiada di antara suami atau isteri, ibubapa dan/atau anak/anak-anak saya yang bekerja di I DESTINASI SDN BHD. </i><br></div>
			            </td>
			        </tr>
			        <tr>
			          <td valign='top'>
			                <b  style="font-size: 9px; ">4.</b>
			            </td>
			            <td><div align="justify"  style="font-size: 9px; ">
			              <b>I am fully aware that all the information disclosed by me herein is for the purpose of I DESTINASI SDN BHD making an evaluation in connection with my application to purchase the Products on deferred payment and it is obligatory on my part to disclose such information which includes Personal Data (as defined in the Personal Data Protection Act 2010). Without such disclosure I am fully aware that I DESTINASI SDN BHD may not be able to approve such aforementioned application.</b><br><i> 

			              Saya sedar bahawa segala maklumat yang didedahkan oleh saya di atas adalah bertujuan membolehkan I DESTINASI SDN BHD membuat penilaian berhubung dengan pembelian produk dengan pembayaran tertunda oleh saya dan adalah menjadi kewajipan untuk saya mendedahkan maklumat tersebut termasuk maklumat Data Peribadi (seperti yang ditakrif di bawah Akta Perlindungan Data Peribadi 2010). Tanpa pendedahan tersebut, saya sedar bahawa I DESTINASI SDN BHD mungkin tidak dapat meluluskan pembiayaan dan/atau menyediakan perkhidmatan-perkhidmatan lain kepada saya.</i><br></div>
			            </td>
			        </tr>
			        <tr>
			          <td valign='top'>
			                <b  style="font-size: 9px; ">5.</b>
			            </td>
			            <td><div align="justify"  style="font-size: 9px; ">
			              <b>Irrevocably authorize I DESTINASI SDN BHD to conduct all necessary background checks and/or investigation (including but not limited to CTOS/CCRIS checks) on my financial status and/or standing with any organization, companies, corporations and/or authorities at I DESTINASI SDN BHD’s absolute discretion. For the avoidance of doubt, this authorization shall be extended to any future transferee or assignee and such authorization is granted to conduct all necessary checks both prior and at any time after the said transfer or assignment.</b><br><i> 

			              Memberi kuasa muktamad kepada I DESTINASI SDN BHD untuk melaksanakan pemeriksaan dan/atau penyiasatan (termasuk tetapi tidak terhad kepada pemeriksaan CTOS/CCRIS) ke atas kedudukan kewangan saya dengan mana-mana organisasi, syarikat, perbadanan dan/atau pihak berkuasa mengikut budi bicara I DESTINASI SDN BHD yang muktamad. Secara jelasnya, pemberian kuasa ini adalah terpakai kepada mana-mana penerima serah hak atau pemegang serah hak akan datang dan pemberian kuasa ini merangkumi pemeriksaan sebelum atau selepas tarikh penyerahan hak tersebut berkuatkuasa.</i><br></div>
			            </td>
			        </tr>
		    	</table></li></ol>
  			</div>
		</div>
	</table>
	<!--PAGE 2 END-->

	<!--PAGE 3 START-->
	<table width="100%"  cellspacing="4" style="page-break-before: always" id="customers"> 
		<div class="row">
	  		<div class="column">
		  		<table cellpadding="4" style="font-size: 9px;width: 300px;line-height: 155%;" >
		    		<tr>
			        	<td valign='top' style="font-size: 9px; ">
			                <b>6.</b>
			            </td>
			          	<td><div align="justify" style="font-size: 9px; ">
			            	<b>Irrevocably authorize I DESTINASI SDN BHD to verify and exchange with any other person any information that I DESTINASI SDN BHD may have on me. </b><br><i>

			            	Memberi kuasa muktamad kepada I DESTINASI SDN BHD untuk menentusahkan dan berkongsi dengan mana-mana pihak
							maklumat berkenaan diri saya yang diperoleh dari mana-mana pihak.</p></i><br></div>
			          	</td>
			        </tr>
			        <tr>
			        	<td valign='top' style="font-size: 9px; ">
			                <b>7.</b>
			            </td>
			          	<td><div align="justify" style="font-size: 9px; ">
			            	<b>That I DESTINASI SDN BHD is irrevocably authorized to disclose any information relating to my account, and under all documents related thereto, including my affairs and particulars to any transferee or assignee or proposed transferee or assignee and any debt collection agents.</b><br><i> 

			            	Bahawasanya, I DESTINASI SDN BHD diberi kuasa muktamad untuk mendedahkan sebarang maklumat mengenai akaun saya dan sebarang maklumat yang terkandung di dalam dokumendokumen yang berkaitan, termasuk maklumat peribadi saya kepada mana-mana penerima serah hak atau pemegang serah hak atau penerima serah hak atau pemegang serah hak yang dicadangkan dan mana-mana ejen kutipan hutang.</i><br></div>
			          	</td>
			        </tr>
			        <tr>
			        	<td valign='top' style="font-size: 9px; ">
			                <b>8.</b>
			            </td>
			          	<td><div align="justify" style="font-size: 9px; ">
			            	<b>That the information given herein is true, correct and complete. I undertake to inform I DESTINASI SDN BHD if there are any changes. I am fully aware that I shall have the right to request access to and request correction of such information.</b><br><i> 

			            	Bahawa maklumat-maklumat yang dinyatakan di sini adalah benar, tepat dan lengkap. Saya berjanji untuk memaklumkan I DESTINASI SDN BHD sekiranya terdapat sebarang perubahan terhadap maklumat tersebut. Saya juga sedar bahawa saya mempunyai hak untuk memohon akses dan memohon pembetulan dibuat terhadap maklumat-maklumat tersebut.</i><br></div>
			          	</td>
			        </tr>
			        <tr>
			        	<td valign='top' style="font-size: 9px; ">
			                <b>9.</b>
			            </td>
			          	<td><div align="justify" style="font-size: 9px; ">
			            	<b>Irrevocably authorize I DESTINASI SDN BHD to disclose the information relating to me or Personal Data (as defined in the Personal Data Protection Act 2010) to any branch, member or any entities affiliated with I DESTINASI SDN BHD for the purposes of I DESTINASI SDN BHD informing or updating me with any product or services offered by I DESTINASI SDN BHD, and until I DESTINASI SDN BHD is notified by me in writing to cease this service, I DESTINASI SDN BHD shall continue to inform and update me accordingly of any product or services of I DESTINASI SDN BHD, notwithstanding that the customerfinancier relationship between me and I DESTINASI SDN BHD may have ceased.</b><br>

			            <i>
			            	Memberi kuasa muktamad kepada I DESTINASI SDN BHD untuk mendedahkan maklumat berkenaan diri saya atau Data Peribadi dan (seperti yang ditakrif di bawah Akta Perlindungan Data Peribadi 2010 kepada  cawangan,  ahli  atau  mana-mana  entiti  yang bernaung dengan I DESTINASI SDN BHD bagi tujuan memaklum dan mengemaskini tentang produk-produk atau perkhidmatan-perkhidmatan yang ditawarkan oleh I DESTINASI SDN BHD, sehingga I DESTINASI SDN BHD diberikan notis secara bertulis oleh saya untuk menghentikan perkhidmatan ini, I DESTINASI SDN BHD akan terus memaklum dan mengemaskini saya tentang produk-produk atau perkhidmatan-perkhidmatan yang ditawarkan I DESTINASI SDN BHD, tanpa mengira hubungan pelanggan-pembiaya di antara saya dan I DESTINASI SDN BHD telah tamat.</i><br></div>
			          	</td>
			        </tr>
			        <tr>
			         	 <td valign='top' style="font-size: 9px; ">
			                <b>10.</b>
			            </td>
			            <td><div align="justify" style="font-size: 9px; ">
			              <b>Irrevocably authorise I DESTINASI SDN BHD to store and use information given above for such purpose as I DESTINASI SDN BHD deems necessary.</b><br>

			              <i>
			              Memberi kuasa muktamad kepada I DESTINASI SDN BHD untuk menyimpan and menggunakan maklumat yang diberikan di atas bagi apa jua tujuan yang I DESTINASI SDN BHD anggap bersesuaian.</i><br></div>
			            </td>
			        </tr>
			        <tr>
			          <td valign='top' style="font-size: 9px; ">
			                <b>11.</b>
			            </td>
			            <td><div align="justify" style="font-size: 9px; ">
			              <b>That the Product purchased is Shariah compliant.</b><br>

			              <i>
			              Bahawa Produk yang dibeli adalah dibenarkan oleh Shariah.</i><br></div>
			            </td>
			        </tr>
			    </table>
		  	</div>

	  	<!--PAGE 3 COLUMN-->

	  		<div class="column">
		    	<table cellpadding="4" style="font-size: 9px; width: 300px; line-height: 155%;">
		    		<tr>
		         		<td valign='top' style="font-size: 9px;">
		                <b>12.</b>
		            	</td>
		            <td><div align="justify" style="font-size: 9px;">
		              <b>That I DESTINASI SDN BHD has the absolute right to approve or reject my application without having to disclose any reject whatsoever.</b><br>
			 			<i>Bahawa I DESTINASI SDN BHD mempunyai hak muktamad untuk meluluskan atau menolak permohonan saya tanpa perlu apa-apa alasan.</i><br></div>
		            </td>
		        </tr>
		        <tr>
		          	<td valign='top' style="font-size: 9px;">
		                <b>13.</b>
		            </td>
		            <td><div align="justify" style="font-size: 9px">
		              <b>Agree to be bound by the terms and conditions that  may be stipulated by I DESTINASI SDN BHD in in the Terms and Forms attached hereto and any other relevant or related document(s) as may be issued by I DESTINASI SDN BHD at any time and from time to time.</b><br><i>
		              Bersetuju untuk terikat dengan mana-mana terma dan syarat yang mungkin ditetapkan oleh I DESTINASI SDN BHD dalam Terma-terma dan Syarat-syarat dan mana-mana perjanjian atau dokumen yang berkaitan pada bila-bila masa dan dari masa ke semasa.</i><br></div>
		            </td>
		        </tr>
		        <tr>
		          	<td valign='top' style="font-size: 9px;">
		                <b>14.</b>
		            </td>
		            <td><div align="justify" style="font-size: 9px;">
		              <b>Irrevocably authorize I DESTINASI SDN BHD to register my name under the Group Takaful Scheme with I DESTINASI SDN BHD’s panel Takaful operators for the whole tenure of the deferred payment scheme for the Products for the purposes of securing payment upon event of death permanent disability.</b><br><i>
		              Memberi kuasa muktamad kepada I DESTINASI SDN BHD untuk mendaftarkan nama saya di bawah  skim Takaful ortotal Berkumpulan  dengan pengusaha Takaful yang menjadi panel I DESTINASI SDN  untuk  tempoh  keseluruhan  skim  pembayaran  tertunda  untuk Produk-produk untuk tujuan menjamin pembayaran jika berlaku kematian atau hilang upaya kekal menyeluruh.</i><br></div>
		            </td>
		        </tr>
		        <tr>
		          <td valign='top' style="font-size: 9px;">
		                <b>15.</b>
		            </td>
		            <td><div align="justify" style="font-size: 9px">
		              <b>In the event of my/either or our death(s), I DESTINASI SDN BHD is authorized to pay any credit balance of account(s) to my/our survivor(s) subject to compliance (if required) with the relevant statutory legislation.</b><br><i>
		              Jika berlaku kematian ke atas saya/kami, I DESTINASI SDN BHD diberi kuasa membayar baki akaun ini kepada pewaris atau pewaris-pewaris saya/kami, tertakluk kepada penetapan perundangan yang relevan.</i><br></div>
		            </td>
		        </tr>
		        <tr>
		          <td valign='top' style="font-size: 9px; ">
		                <b>16.</b>
		            </td>
		            <td><div align="justify" style="font-size: 9px; ">
		              <b>I DESTINASI SDN BHD may without my prior consent assign all or any part of I DESTINASI SDN BHD’s rights and benefits under the contract between us to any bank or financial institution, and I agree that such transferee or assignee shall be entitled to the full benefit of the contract to the same extent as if it were an original party in respect of the rights assigned to it.</b><br>

		              <i>
		              Bahawasanya I DESTINASI SDN BHD berhak tanpa memperoleh kebenaran daripada saya terlebih dahulu, melakukan serah hak kesemua atau sebahagian daripada hak-hak dan faedah-faedah di bawah kontrak yang dimeterai di antara saya dan I DESTINASI SDN BHD kepada mana-mana bank atau institusi kewangan, dan saya bersetuju bahawa penerima serah hak atau pemegang serah hak tersebut adalah berhak menerima segala faedah yang termaktub di dalam kontrak seperti mana hak-hak yang diberikan kepada pihak pemegang kontrak yang asal.</i><br></div>
		            </td>
		        </tr>
		    </table>
		    <table>
				<tr>
				    <td style="width: 300px"><b>Signature of term->Basic /</b> <i>Tandatangan Pemohon :</i></td>
	  			</tr>
	  			<tr>
	    			<td class="clas" style="width: 300px;height: 70px">&nbsp;</td>
	  			</tr>
		  	</table>
	  		<table>
	  			<tr>
				    <td style="width: 13px"><b>Name / </b> <br><i>Nama :</i></td>
	    			<td  class="clas" style="width: 287px;height: 40px">&nbsp;</td>
	  			</tr>
	  		</table>
	  		<table>
	  			<tr>
				    <td style="width: 70px"><b>Date (dd/mm/yyyy) / </b> <br><i>Tarikh (hh/bb/tttt)</i></td>
	    			<td  class="clas" style="width: 245px;height: 30px">&nbsp;</td>
	  			</tr>
			</table>
		  </div>
		</div>
	</table>
	<!--PAGE 4 END-->

	<!--PAGE 5 START-->
	<table width="100%"  cellspacing="4" style="page-break-before: always" id="customers"> 
		<div class="row">
  			<div class="column">
		  		<table>
		    		<tr>
		    			<td colspan="2" class="border header" style="width: 320px">
		       				H) For Sale Agency Use Only  / <i>Untuk Kegunaan Agen Jualan Sahaja</i>
		    			</td>
		  			</tr>	
		    	</table><br>
		    	<table>
				  	<tr>
				    	<td style="width: 80px"> Application Date (dd/mm/yyyy)/ <br><i> Tarikh Permohonan (hh/bb/yyyy)</i></td>
				    	<td style="width: 10px">:</td>
				     	<td class="clas" style="width: 200px"> </td>
				 	</tr>
				 </table><br>
				<table style="background-color:grey;">
					<tr>
						<td style="width: 323px;">Supporting Documents Submitted (Please Tick) / <br><i>Dokumen Sokongan Yang Diserahkan (Sila Tanda)</i>:</td>
					</tr>
				</table>
				<table class="bor-table">
		        	<tr>
			            <td class="bor-noBorder"   style="width: 8px">1)</td>
		             	<td class="bor-noBorder"  style="width: 200px">Document Checklist /</td>
		             	<td class="bor-noBorder"  style="width: 40px">&nbsp;</td>
		           		<td rowspan="2" style="width: 70px">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        	</tr>
		        	<tr>
		        		 <td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		             	<td class="bor-noBorder"><i>Dokumen Semakan</i></td>
		           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		        	</tr>
		        	<tr>
			            <td class="bor-noBorder"   style="width: 8px">2)</td>
		             	<td class="bor-noBorder"  style="width: 200px">Photocopy of NRIC (both sides) /</td>
		             	<td class="bor-noBorder"  style="width: 40px">&nbsp;</td>
		           		<td rowspan="2" style="width: 70px">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        	</tr>
		        	<tr>
		        		 <td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		             	<td class="bor-noBorder"><i>Salinan Kad Pengenalan (depan dan belakang)</i></td>
		           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		        	</tr>
		        	<tr>
			            <td class="bor-noBorder"   style="width: 8px">3)</td>
		             	<td class="bor-noBorder"  style="width: 200px">Latest 3 months salary slip /</td>
		             	<td class="bor-noBorder"  style="width: 40px">&nbsp;</td>
		           		<td rowspan="2" style="width: 70px">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        	</tr>
		        	<tr>
		        		 <td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		             	<td class="bor-noBorder"><i>Slip gaji untuk 3 bulan terkini</i></td>
		           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		        	</tr>
		        	<tr>
			            <td class="bor-noBorder"   style="width: 8px">4)</td>
		             	<td class="bor-noBorder"  style="width: 200px">Others (please specify) /</td>
		             	<td class="bor-noBorder"  style="width: 40px">&nbsp;</td>
		           		<td rowspan="2" style="width: 70px">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        	</tr>
		        	<tr>
		        		 <td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		             	<td class="bor-noBorder"><i>Lain-lain (sila nyatakan)</i></td>
		           		<td class="bor-noBorder">&nbsp;&nbsp;&nbsp;&nbsp; </td>
		        	</tr>
		        </table>
        <table class="bor-table">
        	<tr>
           		<td style="width: 326px;height: 500px">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        	</tr>
    	</table><br><br >
    	<p style="font-size: 8px">I hereby confirm that signature has been verified and all documents ticked above have<br>
    	been submitted to us at the following address. /<br>
    	<i>
    		Saya mengesahkan bahawa tandatangan telah disemak dan semua dokumen yang<br>
    		ditandakan di atas telah diserahkan kepada kami di alamat bawah.
    	</i></p><br><br>
    	<table class="bor-table">
    	  	<tr>
    	  		<td class="bor-noBorder">Signature & Company's Stamp / <i>Tandatangan & Cop Koperasi</i></td>
    	  	</tr>
    	</table>
    	<table class="bor-table">
        	<tr>
           		<td style="width: 325px;height: 50px">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        	</tr>
    	</table><br>
    	<table>
		  	<tr>
		    	<td style="width: 80px"> Agent Code / <br> Kod Agen</td>
		    	<td style="width: 10px">:</td>
		     	<td class="clas" style="width: 225px"> </td>
		 	</tr>
		    <tr>
		    	<td>&nbsp;</td>
		    	<td>&nbsp;</td>
		     	<td>&nbsp;</td>
		  	</tr>
		  	<tr>
		    	<td>Staff ID / <br> <i>No. Pekerja</i></td>
		    	<td>:</td>
		     	<td class="clas"></td>
		  	</tr>
		  	<tr>
		    	<td>&nbsp;</td>
		    	<td>&nbsp;</td>
		     	<td>&nbsp;</td>
		  	</tr>
   			<tr>
    			<td>Name / <br><i>Nama</i></td>
    			<td>:</td>
     			<td class="clas"></td>
  			</tr>
  			<tr>
		    	<td>&nbsp;</td>
		    	<td>&nbsp;</td>
		     	<td>&nbsp;</td>
		  	</tr>
		  		<tr>
    			<td>NRIC No. / <br><i>No. K/F</i></td>
    			<td>:</td>
     			<td class="clas"></td>
  			</tr>
  			<tr>
		    	<td>&nbsp;</td>
		    	<td>&nbsp;</td>
		     	<td>&nbsp;</td>
		  	</tr>
		  		<tr>
    			<td>Date (dd/mm/yyyy) / <br><i>Tarikh (hh/bb/tttt)</i></td>
    			<td>:</td>
     			<td class="clas"></td>
  			</tr>
  			<tr>
		    	<td>&nbsp;</td>
		    	<td>&nbsp;</td>
		     	<td>&nbsp;</td>
		  	</tr>
		</table>
  	</div>
  	<!--PAGE 4 COLUMN-->
  	<div class="column">
  		<table>
    		<tr>
    			<td colspan="2" class="border header" style="width: 335px">
       				I) For I DESTINASI SDN BHD Use Only / <i>Untuk Kegunaan I DESTINASI SDN BHD Sahaja</i>
    			</td>
  			</tr>	
    	</table>
    	<table>
            <tr>
            	<td style="width: 100px">New Customer / <i> Pelanggan Baru : </i></td>
				<td class="border">&nbsp;&nbsp;&nbsp;</td>
              	<td style="width: 50px">Yes/<i> Ya</i></td>
              	<td class="border">&nbsp;&nbsp;&nbsp;
              	<td style="width: 50px">No /<i> Tidak </i></td>
            </tr>
        </table><br>
    	<table>
		  	<tr>
		    	<td style="width: 80px"> Date Received (dd/mm/yyyy) / <br>Tarikh Menerima (hh/bb/tttt)</td>
		    	<td style="width: 10px">:</td>
		     	<td class="clas" style="width: 220px"> </td>
		 	</tr>
		    <tr>
		    	<td>&nbsp;</td>
		    	<td>&nbsp;</td>
		     	<td>&nbsp;</td>
		  	</tr>
		  	<tr>
		    	<td>Referral's Name  / <br> <i>Referral's Name </i></td>
		    	<td>:</td>
		     	<td class="clas"></td>
		  	</tr>
		  	<tr>
		    	<td>&nbsp;</td>
		    	<td>&nbsp;</td>
		     	<td>&nbsp;</td>
		  	</tr>
   			<tr>
    			<td>Takaful Company  / <br><i>Takaful Company </i></td>
    			<td>:</td>
     			<td class="clas">&nbsp;</td>
  			</tr>
  			<tr>
		    	<td>&nbsp;</td>
		    	<td>&nbsp;</td>
		     	<td>&nbsp;</td>
		  	</tr>
		</table>
		 <table>
       		<tr>
       			<td style="width: 105px">Product Code  / <br><i>Kod Produk</i></td>
       			<td>&nbsp;</td>
       			<td style="width: 105px">Source Code  / <br><i>Kod Sumber</i></td>
       			<td>&nbsp;</td>
       			<td style="width: 105px">Campaign Code  /<br> <i>Kod Kempen</i></td>
       		</tr>
       		<tr>
       			<td class="clas">&nbsp;</td>
       			<td>&nbsp;</td>
       			<td class="clas">&nbsp;</td>
       			<td>&nbsp;</td>
       			<td class="clas">&nbsp;</td>
       		</tr>
       	</table><br>
       	<table>
   			<tr>
    			<td style="width: 130px">Objective of Opening  / <i>Pembukaan</i> : </td>
     			<td class="clas" style="width: 200px;height: 2px">&nbsp;</td>
  			</tr>
		</table>
       	<table>
   			<tr>
    			<td style="width: 80px">Account Category  / <br><i>Kategori Akaun</i></td>
    			<td style="width: 10px">:</td>
     			<td class="clas" style="width: 70px">Individual</td>
     			<td style="width: 10px">&nbsp;</td>
     			<td style="width: 60px">RIM No. / <i>No. RIM: </i></td>
    			<td style="width: 10px">:</td>
     			<td class="clas"  style="width: 67px">&nbsp;</td>
  			</tr>
		</table><br>
       	<table>
       		<tr>
       			<td style="width: 105px">Business Unit / <br><i>Unit Perniagaan </i></td>
       			<td>&nbsp;</td>
       			<td style="width: 105px">Agent Code / <br><i>Kod Ejen</i></td>
       			<td>&nbsp;</td>
       			<td style="width: 105px">SSC Code /<br> <i>Kod SSC</i></td>
       		</tr>
       		<tr>
       			<td class="clas">&nbsp;</td>
       			<td>&nbsp;</td>
       			<td class="clas">&nbsp;</td>
       			<td>&nbsp;</td>
       			<td class="clas">&nbsp;</td>
       		</tr>
       </table>
        <table>
			<tr>
				<td style="width: 200px">Class Code (CCC) / <i> Class Code (CCC) : </i></td>
				<td style="width: 150px">Category / <i> Kategori: </i></td>
			</tr>
		</table>
                  	<table style="border-spacing: 0;">
	                    <tr>
	                      	<td class="border">&nbsp; <b></b>&nbsp;</td>
	                      	<td style="width: 220px">&nbsp;Bumiputra  /<i>Bumiputera  </i></td>
	                      	<td class="border">&nbsp; <b></b>&nbsp;</td>
	                      	<td style="width: 100px">&nbsp;High /<i>Tinggi  </i></td>
	                    </tr>
	                    <tr>
	                      	<td class="border">&nbsp; <b></b>&nbsp;</td>
	                      	<td>&nbsp;Non Bumiputra  /<i>Bukan Bumiputera  </i></td>
	                      	<td class="border">&nbsp; <b></b>&nbsp;</td>
	                      	<td>&nbsp;Low /<i>Rendah  </i></td>
	                    </tr>
	                    <tr>
	                      	<td class="border">&nbsp; <b></b>&nbsp;</td>
	                      	<td>&nbsp;Foreigner Individuals   /<i>Orang Asing Individual  </i></td>
	                      	<td class="border">&nbsp; <b></b>&nbsp;</td>
	                      	<td>&nbsp;PF /<i>PF </i></td>
	                    </tr>
	                    <tr>
	                      	<td class="border">&nbsp; <b></b>&nbsp;</td>
	                      	<td>&nbsp;Foreigner - Employed/Studying in Malaysia   </td>
	                    </tr>
	                     <tr>
	                      	<td>&nbsp; <b></b>&nbsp;</td>
	                      	<td>&nbsp;<i> Orang Asing - Bekerja/Belajar di Malaysia</i></td>
	                    </tr>
                	</table><br>
           </td>
       </tr>
        <table>
			<tr>
				<td style="font-size: 5px">Customer's Relationship to Koperasi Sejati / <i> Hubungan Pelanggan kepada Koperasi Sejati : </i></td>
			</tr>
			<tr>
                <td>
                  	<table style="border-spacing: 0;">
	                    <tr>
	                      	<td class="border">&nbsp; <b></b>&nbsp;</td>
	                      	<td style="width: 280px">&nbsp;Executive Officer  /<i>Executive Officer </i></td>
	                    </tr>
	                    <tr>
	                      	<td class="border">&nbsp; <b></b>&nbsp;</td>
	                      	<td>&nbsp;Director   /<i>Pengarah</i></td>
	                    </tr>
	                    <tr>
	                      	<td class="border">&nbsp; <b></b>&nbsp;</td>
	                      	<td>&nbsp;Principal Shareholder   /<i>Pemegang Saham Utama</i></td>
	                    </tr>
	                    <tr>
	                      	<td class="border">&nbsp; <b></b>&nbsp;</td>
	                      	<td>&nbsp;Employee    /<i>Pekerja</i></td>
	                    </tr>
	                     <tr>
	                      	<td class="border">&nbsp; <b></b>&nbsp;</td>
	                      	<td>&nbsp;Related Profit /<i> Keuntungan</i></td>
	                    </tr>
	                     <tr>
	                      	<td class="border"> &nbsp;&nbsp;</td>
	                      	<td>&nbsp;NA  /<i> Tidak Berkaitan </i></td>
	                    </tr>
	                    <tr>
	                      	<td class="border"> &nbsp;&nbsp;</td>
	                      	<td>&nbsp;</td>
	                    </tr>
                	</table><br>
           </td>
       </tr>
   	</table>
   	<table>
   		<tr>
   			<td style="width: 330px">Comment / <i>Komen</i></td>
   			
   		</tr>
   		<tr>
   			<td class="clas">&nbsp;</td>
   			<td style="width: 320px;height: 480px">&nbsp;</td>
   		</tr>
       </table>
        <!--<table class="bor-table">
        	<tr>
        		<td>Comment / <i>Komen</i></td>
        	<tr>
           		<td class="border" style="width: 325px;height: 500px">&nbsp;&nbsp;&nbsp;&nbsp;</td>
        	</tr>
    	</table>-->

		
    	
  	</div>
  </div>
	</table>
	<!--PAGE 4 END-->

     <!--PAGE 5 START-->
    <table width="100%"  cellspacing="4" style="page-break-before: always" id="customers"> 
		<div class="row">
  			<div class="column" style="font-size: 7px; line-height: 105%;">
  				<table>
		    		<tr>
		    			<td colspan="2" class="border header" style="width: 320px">
		       				TERMA DAN SYARAT SERTA BORANG</i>
		    			</td>
		  			</tr>	
		    	</table><br>
		    	<p style="font-size: 7px">Sila maklum bahawa kelulusan I DESTINASI SDN BHD bagi permohonan kredit untuk pembelian Komoditi tertakluk kepada Terma dan Syarat berikut:</p>

				<ol type="1" style="font-size: 7px; width: 300px" >
					<li>&nbsp;&nbsp; <b> KEMUDAHAN</b></li>
						<div align="justify">
							<ol style="list-style-type: none;"><li>
		                  		<ol style="list-style-type: lower-alpha;">
				                    <li>Kemudahan kredit untuk pembelian Komoditi (“Kemudahan”) yang diberikan kepada anda adalah dalam jumlah yang dinyatakan dalam Pemberitahuan Tawaran yang akan dikeluarkan oleh I DESTINASI SDN BHD kepada anda sebelum pembayaran Kemudahan itu dibuat.</li>
				                    <li>Tertakluk kepada Terma dan Syarat berikut dan selaras dengan prinsip Syariah, anda dan I DESTINASI SDN BHD akan mengadakan urusan jualbeli Komoditi secara pembayaran tertunda menurut Fasal 2 dokumen ini.I DESTINASI SDN BHD berhak membatalkan Kemudahan jika Kemudahan itu tidak digunakan untuk tujuan yang dibenarkan oleh Syariah.</li>
		                  		</ol><br></li>
	                  		</ol>
	                  	</div>

                  	<li>&nbsp;&nbsp; <b>CARA PEMBIAYAAN</b></li>
                  		<div align="justify">
                  			<ol style="list-style-type: none;"><li>
				                <ol style="list-style-type: lower-alpha;">
				                  	<li>Tertakluk kepada terma dan syarat yang terkandung dalam dokumen ini, I DESTINASI SDN BHD bersetuju menyediakan Kemudahan ini untuk anda sebanyak amaun yang sama dengan Amaun Kemudahan semasa menerima Pemberitahuan Permohonan, Pemberitahuan Tawaran dan Penerimaan serta Surat Agensi yang ditandatangani sewajarnya dan dilampirkan dalam dokumen ini yang baginya:</li>
				                    	<ol type="i">
				                      		<li>I DESTINASI SDN BHD akan, atas permintaan anda dan apabila anda mengaku janji untuk membeli Komoditi daripada I DESTINASI SDN BHD pada Harga Jualan Tertunda, membeli Komoditi itu daripada pembekal (“Pembelian Komoditi”) pada Harga Belian yang sama dengan Amaun Kemudahan; dan </li>
				                      		<li>I DESTINASI SDN BHD kemudiannya akan menjual Komoditi tersebut kepada anda (“Penjualan Komoditi”) pada Harga Jualan Tertunda tertakluk kepada terma dan syarat bagi Terma dan Syarat ini, Pemberitahuan Permohonan dan Pemberitahuan Tawaran. </li>
				                    	</ol>
				                 	 <li>Melainkan I DESTINASI SDN BHD bersetuju sebaliknya, anda tidak boleh menggunakan Kemudahan tersebut, melainkan I DESTINASI SDN BHD memberikan pengesahan kepada anda bahawa anda boleh menggunakan Kemudahan itu.</li>
				                </ol><br></li>
				            </ol>
			            </div>

			        <li>&nbsp;&nbsp; <b>PEMBELIAN KOMODITI DAN PENJUALAN KOMODITI </b></li>
			        	<div align="justify">
			        		<ol style="list-style-type: none;"><li>
	               				<ol style="list-style-type: lower-alpha;">
	                  				<li>Tertakluk kepada pematuhan Syarat Terdahulu, apabila anda ingin menggunakan Kemudahan tersebut, anda hendaklah melengkapkan Pemberitahuan Permohonan dan menyerahkannya kepada I DESTINASI SDN BHD dengan menetapkan amaun yang hendak anda bayar (yang tidak boleh melebihi Amaun Kemudahan). Kemudian daripada itu:</li>
					                  	<ol type="i">
						                    <li>sebaik sahaja diserahkan, Pemberitahuan Permohonan tidak boleh ditarik balik; dan</li>
						                    <li>dengan penyerahan Pemberitahuan Permohonan tersebut, anda mengaku janji secara muktamad untuk melaksanakan Penjualan Komoditi (tertakluk hanya kepada penerimaan I DESTINASI SDN BHD terhadap Pemberitahuan Permohonanitu) menurut Terma dan Syarat ini.</li>
						                </ol>
					                <li>Apabila menerima Pemberitahuan Permohonan daripada anda, I DESTINASI SDN BHD akan mengadakan urusan pembelian dengan pembekal untuk membeli Komoditi pada Harga Belian, iaitu amaun yang sama dengan amaun yang dipohon untuk dibayar oleh anda di bawah Pemberitahuan Permohonan yang dikeluarkan menurut Fasal 3(a) di atas.</li>
					                <li>I DESTINASI SDN BHD akan menerima terma dan syarat Pemberitahuan Permohonan dengan melengkapkan dan menyerahkan Pemberitahuan Tawaran yang dilaksanakan kepada anda selepas Tarikh Pembelian dan mengkreditkan Harga Belian ke dalam akaun pembekal Komoditi pada Tarikh Pembelian jika I DESTINASI SDN BHD telah menerima Pemberitahuan Permohonan anda bersama-sama Borang Penerimaan yang telah anda laksanakan sewajarnya.</li>
					            </ol><br></li>
					        </ol>
				        </div>

              		<li>&nbsp;&nbsp; <b>TENOR KEMUDAHAN  </b></li>
              			<div align="justify">
              				<ol style="list-style-type: none;">
              		 			<li>Tertakluk kepada penamatan awal, tenor Kemudahan ialah tempoh yang dinyatakan dalam Bahagian 2 bagi Rumusan (“Tenor”). </li>
              		 		</ol>
              		 	</div><br>
               
              		<li>&nbsp;&nbsp; <b>HARGA JUALAN TERTUNDA  </b></li>
	              		<div align="justify">
	              			<ol style="list-style-type: none;">
	              		 			<li>Harga Jualan Tertunda yang perlu anda bayar kepada I DESTINASI SDN BHD untuk Penjualan Komoditi ialah amaun yang dinyatakan dalam Bahagian 3 bagi Rumusan.<br>Harga Jualan Tertunda terdiri daripada:  </li>
	              		 	</ol>
	              		 	<ol style="list-style-type: none;"><li>
				                <ol style="list-style-type: lower-alpha;">
				                  	<li>Harga Belian; dan </li>
				                  	<li>Keuntungan; dan</li> 
			                 		<br>Keuntungan ialah amaun seperti yang dinyatakan dalam Bahagian 5 bagi Rumusan. Keuntungan boleh dikira dengan memasukkan Kadar Keuntungan ke dalam formula yang dinyatakan dalam Bahagian 6 bagi Rumusan.
				                </ol></li>
				            </ol>
			            </div><br>

             	 	<li>&nbsp;&nbsp; <b>PEMBAYARAN </b></li>
             	 		<div align="justify">
             	 			<ol style="list-style-type: none;"><li>
				                <ol style="list-style-type: lower-alpha;">
				                  	<li>Pembayaran Harga Jualan Tertunda oleh anda kepada I DESTINASI SDN BHD akan diuruskan oleh Perbadanan dan mesti dibayar kepada I DESTINASI SDN BHD secara Ansuran bulanan dengan Ansuran pertama hendaklah dibayar pada atau sebelum hari berkenaan bagi bulan selepas tempoh tangguh seperti yang dinyatakan dalam Bahagian 7 bagi Rumusan, dan kemudian daripada itu, Ansuran bulanan yang seterusnya dan berikutnya hendaklah dibayar pada atau sebelum hari berkenaan bagi setiap bulan berikutnya sehingga Harga Jualan Tertunda dan semua wang yang perlu dibayar di bawah Terma dan Syarat ini dibayar dan dijelaskan sepenuhnya. </li>
				                  	<li>Anda berhak membuat penyelesaian awal Kemudahan ini dengan memberi notis bertulis selama tiga puluh (30) hari kepada I DESTINASI SDN BHD dan pihak penerima serah hak (jika berkenaan) . Notis penyelesaian awal yang diberikan adalah muktamad dan tidak boleh ditarik-balik. Tertakluk kepada prinsip-prinsip, resolusi-resolusi dan garis panduan Majlis Penasihat Syariah, Bank Negara Malaysia (BNM), I DESTINASI SDN BHD akan memberikan rebat (ibra’) ke atas Harga Jualan Tertunda untuk penyelesaian awal tersebut. Pengiraan, mekanisme dan amaun rebat (ibra’) akan ditentukan berdasarkan budi bicara I DESTINASI SDN BHD atau pihak penerima serah hak atau formula rebat lain yang mungkin akan ditentukan oleh BNM dan rebat tersebut adalah muktamad dan mengikat. Bayaran penyelesaian awal tersebut hendaklah dibuat terus ke dalam akaun I DESTINASI SDN BHD atau pihak penerima serah hak yang akan dimaklumkan kepada anda oleh I DESTINASI SDN BHD sebelum penyelesaian tersebut dibuat. </li>
				                  	<li>Semua bayaran daripada anda yang berhubung dengan kos, kerugian, perbelanjaan atau cukai hendaklah dibuat dalam mata wang ia ditanggung. Apa-apa jumlah lain, termasuk Harga Jualan Tertunda, mesti dibayar dalam Ringgit Malaysia.</li>
				                  	<li>Semua bayaran daripada anda kepada I DESTINASI SDN BHD (termasuk Harga Jualan Tertunda) di bawah Kemudahan ini, sama ada berhubung dengan jumlah pokok, keuntungan, fi atau apa-apa amaun lain, hendaklah dibayar penuh tanpa apa-apa pemotongan atau penangguhan (sama ada berhubung dengan penolakan, tuntutan balas, duti, caj atau selainnya) dan bebas daripada apa-apa cukai semasa atau akan datang melainkan jika anda dipaksa oleh undang-undang untuk membuat mana-mana penolakan atau penangguhan tersebut. Dalam keadaan tersebut, anda hendaklah membayar kepada pihak berkuasa yang berkenaan, dalam tempoh bayaran yang dibenarkan oleh undang-undang yang terpakai, amaun yang perlu dipotong atau ditahan dan hendaklah membayar kepada I DESTINASI SDN BHD , amaun selanjutnya itu, seperti yang diperlukan, supaya amaun bersih yang diterima dan dipegang oleh I DESTINASI SDN BHD selepas pemotongan atau penahanan itu sama dengan amaun yang sepatutnya diterima dan dipegang oleh I DESTINASI SDN BHD jika tidak ada pemotongan atau penahanan tersebut. </li>
				                </ol><br></li>
				            </ol>
			            </div>

              		<li>&nbsp;&nbsp; <b>TERMA DAN SYARAT PENJUALAN KOMODITI</b></li>
              			<div align="justify">
              				<ol style="list-style-type: none;"><li>
	                			<ol style="list-style-type: lower-alpha;">
				                  	<li>Penjualan Komoditi akan dikawal oleh terma dan syarat yang terkandung dalam dokumen ini dan peruntukan yang terpakai bagi Pemberitahuan Permohonan dan Pemberitahuan Tawaran.</li>
				                  	<li>Anda hendaklah mendapatkan hak milik tersebut bagi Komoditi sebagaimana I DESTINASI SDN BHD menerimanya daripada pembekalnya di bawah Pembelian Komoditi yang bebas daripada semua bebanan. I DESTINASI SDN BHD tidak boleh dianggap sebagai memberikan apa-apa jaminan atau pernyataan (yang tersurat atau tersirat), sama ada yang timbul daripada undang-undang, statut atau selainnya, dan tanpa menjejaskan kenyataan umum sebelum ini, apa-apa jaminan atau pernyataan tersebut oleh I DESTINASI SDN BHD </li>
				                </ol></li>
				            </ol>
			            </div>
             	</ol>
            </div>
              		
			<!-- PAGE 5 COLUMN-->
  			<div class="column" style="font-size: 7px;line-height: 105%;">
  				<div align="justify">
  					<ol style="list-style-type: none;"><li>
	  					<ol style="list-style-type: none; width: 300px" ><li>
	      					dengan ini dikecualikan sepenuhnya secara nyata mengikut yang dibenarkan oleh undang-undang yang terpakai.</li>
	    				</ol>
	    				<ol style="list-style-type: lower-alpha;width: 300px">
		                  	<li>Semua risiko dalam Komoditi akan diberikan kepada anda dengan segera pada masa Penjualan Komoditi dibuat, iaitu apabila anda menerima Pemberitahuan Tawaran. </li>
		                  	<li>Komoditi dijual berasaskan “seadanya, dan dengan semua kelemahan” dan bebas daripada semua bebanan.</li>
		                  	<li>Anda mengakui bahawa:</li>
		                  		 <ol type="i">
				                    <li>anda akan dianggap sebagai telah menerima Komoditi itu tanpa syarat dan tanpa keraguan dan tidak mempunyai remedi terhadap I DESTINASI SDN BHD berhubung dengan kualiti, syarat, kuantiti, jenis, hak milik atau selainnya; dan</li>
				                    <li>tanpa menjejaskan Fasal 7 (e) ini, anda dengan ini mengetepikan apa-apa tuntutan yang mungkin ada terhadap I DESTINASI SDN BHD berhubung dengan apa-apa kerugian atau kerosakan yang mungkin anda alami yang disebabkan oleh, atau yang timbul daripada atau berhubung dengan Terma dan Syarat ini, mana-mana Dokumen Kemudahan lain atau selainnya (walau bagaimana ia timbul sekalipun) berhubung dengan atau timbul daripada Pembelian Komoditi dan/atau Penjualan Komoditi melainkan jika disebabkan oleh pelanggaran yang disengajakan oleh I DESTINASI SDN BHD terhadap mana-mana dokumen tersebut atau disebabkan oleh kecuaian melampaunya. </li>
	                  			</ol>
	                	</ol> </li>
	                </ol>
	        	</div>
  				<ol start="8" type="1" style="font-size: 7px; width: 300px" >
  					<li>&nbsp;&nbsp; <b>TAKAFUL </b></li>
  						<div align="justify">
  							<ol style="list-style-type: none;"><li>
				                <ol style="list-style-type: lower-alpha;">
				                  	<li>Jika dikehendaki oleh I DESTINASI SDN BHD, anda boleh mengambil kumpulan takaful kredit dengan syarikat takaful yang diluluskan oleh I DESTINASI SDN BHD yang sumbangan untuk kumpulan takaful kredit itu hendaklah dibayar sepenuhnya oleh anda dan bahawa I DESTINASI SDN BHD hendaklah diendorskan sebagai benefisiari dalam polisi kumpulan takaful kredit itu. Jika anda dikehendaki mengambil kumpulan takaful kredit tersebut, nama dan butiran syarikat takaful itu serta jumlah sumbangan hendaklah seperti yang dinyatakan dalam Bahagian 8 bagi Rumusan.</li>
				                  	<li>Sesuai dengan dasar polisi kumpulan takaful kredit tersebut, dengan ini adalah dipersetujui bahawa terdapat masa menunggu selama 30 hari untuk semua bil penyakit, di mana anda tidak akan menikmati mana-mana manfaat berdasarkan dasar-dasar tersebut di dalam tempoh menunggu ini. Walau bagaimanapun, tidak terdapat masa menunggu untuk tuntutan kematian atau kecacatan kekal yang timbul dari sebab kebetulan.</li>
				                </ol><br></li>
				             </ol>
			            </div>

			        <li>&nbsp;&nbsp; <b>PAMPASAN BAGI PEMBAYARAN LEWAT DAN PEMBAYARAN INGKAR </b></li>
			        	<div align="justify">
	             			<ol style="list-style-type: none; width: 300px" >
	             				<li>I DESTINASI SDN BHD berhak diberikan pampasan bagi Ansuran lewat dan pembayaran ingkar berdasarkan kaedah berikut: </li>
	    					</ol>
	    					<ol style="list-style-type: none;"><li>
	              				<ol style="list-style-type: lower-alpha;">
					                <li>bagi kegagalan membayar mana-mana Ansuran di bawah Kemudahan dalam tempoh dari tarikh pembayaran pertama bagi Kemudahan sehingga tarikh matangnya (dengan pembayaran dibuat secara Ansuran), kadar pampasan adalah sebanyak satu peratus (1%) setahun bagi Ansuran lewat dibayar atau amaun lain seperti yang akan diputuskan oleh Majlis Penasihat Syariah Bank Negara Malaysia dari semasa ke semasa.</li>
					                <li>bagi kegagalan membayar mana-mana keterhutangan di bawah Kemudahan dan kegagalan itu berterusan melepasi tarikh matang Kemudahan, kadar pampasan adalah mengikut kadar Pasaran Wang Islam semasa (12 bulan keuntungan kasar atau R-Kadar) ke atas jumlah keseluruhan belum jelas yang perlu dibayar kepada I DESTINASI SDN BHD di bawah Kemudahan atau amaun lain seperti yang akan diputuskan oleh Majlis Penasihat SyariahBank Negara Malaysia dari semasa ke semasa. Amaun pampasan tidak boleh dikumpulkan ke atas jumlah keseluruhan belum jelas yang perlu dibayar kepada I DESTINASI SDN BHD ;</li>
					            </ol></li>
					        </ol>

				            <ol style="list-style-type: none; width: 300px" >
	             				<li>Dengan syarat bahawa apa-apa pampasan yang dimaksudkan di atas yang dibayar kepada I DESTINASI SDN BHD hendaklah didermakan kepada mana-mana pertubuhan amal berdaftar dan/atau digunakan untuk tujuan amal seperti yang ditentukan oleh I DESTINASI SDN BHD mengikut budi bicara mutlaknya.</li>
	    					</ol>
				        </div><br>

              		<li>&nbsp;&nbsp; <b>PERNYATAAN DAN JAMINAN</b></li>
              			<div align="justify">
              				<ol style="list-style-type: none; width: 300px" >
	             				<li>Anda dengan ini menyatakan dan menjamin kepada I DESTINASI SDN BHD bahawa:<br></li>
	    					</ol>
	    					<ol style="list-style-type: none;"><li>
				            	<ol style="list-style-type: lower-alpha;">
					                <li><b>Obligasi yang mengikat:<br></b>
					                    Obligasi yang dinyatakan akan ditanggung oleh anda, yang merupakan satu pihak, adalah mematuhi undang-undang, sah, mengikat dan boleh dikuatkuasakan tetapi tertakluk kepada mana-mana prinsip am undang-undang yang mengehadkan obligasinya berhubung dengan remedi ekuiti, ketakmampuan, pembubaran atau hak pemiutang secara amnya.
					                </li>
					                <li><b>Tidak bercanggah dengan obligasi lain:<br></b>
					                  	Urus niaga yang anda lakukan dan laksanakan melalui Kemudahan ini dan/atau Dokumen Kemudahan tidak dan tidak akan bercanggah dengan: 
					                </li>
					                	<ol type="i">
					                  		<li>apa-apa undang-undang atau peraturan yang terpakai ke atas anda; atau </li>
					                  		<li>apa-apa perjanjian atau surat cara yang mengikat anda atau mana-mana aset anda.</li>
					                	</ol>
					                <li><b>Tiada keingkaran: </b></li>
						                <ol type="i">
						                  	<li>Tiada Kejadian Keingkaran yang berterusan atau berkemungkinan akan berlaku disebabkan anda melaksanakan apa-apa urus niaga yang dipertimbangkan oleh Kemudahan ini; dan</li>
						                  	<li>Tiada kejadian atau keadaan lain yang belum selesai yang membentuk keingkaran di bawah mana-mana perjanjian atau surat cara lain yang mengikat anda.</li>
					                	</ol>
				                	<li><b>Persetujuan:<br></b> Semua persetujuan, kelulusan atau kebenaran daripada Biro Perkhidmatan Angkasa dan mana-mana pihak berkuasa yang berkaitan yang diperlukan untuk atau berhubung dengan pelaksanaan, penyerahan, prestasi, kesahan dan penguatkuasaan Dokumen Kemudahan telah diperoleh dan dikuatkuasakan sepenuhnya dan apa-apa syarat yang terkandung di dalamnya atau yang dikenakan di dalamnya telah dipatuhi. </li>
				                	<li><b>Tiada maklumat yang mengelirukan: </b></li>
					                  	<ol type="i">
					                    	<li>Apa-apa maklumat berdasarkan fakta yang anda berikan secara bertulis berhubung dengan Kemudahan ini adalah benar dan tepat bagi semua perkara yang penting seperti pada tarikh ia diberikan atau seperti pada tarikh (jika ada) ia dinyatakan; dan </li>
					                    	<li>Tiada apa-apa yang telah berlaku atau yang tidak dimasukkan ke dalam maklumat berdasarkan fakta yang dirujuk dalam perenggan (i) di atas dan tiada maklumat yang diberikan atau tidak diberikan yang menyebabkan maklumat itu menjadi tidak benar atau mengelirukan dalam mana-mana perkara yang penting.</li>
					                  	</ol>
				               		<li><b>Kebankrapan:</b><br>Anda tidak pernah dihukum bankrap dan tidak terdapat apa-apa petisyen kebankrapan ke atas anda yang belum diputuskan.</li>
              					</ol></li>
              				</ol>
              			</div><br>

             	 	<li>&nbsp;&nbsp; <b>WAAD/AKU JANJI </b></li>
              			<div align="justify">
              				<ol style="list-style-type: none; width: 300px" >
	             				<li>Anda dengan ini mengaku janji bahawa selagi Kemudahan ini masih belum dijelaskan, melainkan jika I DESTINASI SDN BHD bersetuju sebaliknya:<br></li>
	    					</ol>
	    						<ol style="list-style-type: none;"><li>
	              					<ol style="list-style-type: lower-alpha;">
	                					<li><b>Pematuhan undang-undang:<br> </b>
	               			 				Anda akan mematuhi semua perkara berhubung dengan semua undang-undang dan peraturan yang anda tertakluk kepadanya; </li>
	               	 					<li><b>Tidak Lagi Berkhidmat dengan Kerajaan: <br></b>
						                  	Anda hendaklah, dengan segera atau dalam apa-apa kejadian dalam tempoh tujuh (7) hari dari tarikh anda meletakkan jawatan, bersara atau diberhentikan daripada perkhidmatan dengan Kerajaan memaklumkan I.DESTINASI SDN BHD bahawa anda tidak lagi menjadi kakitangan awam dan dalam keadaan tersebut, anda hendaklah dengan segera membayar semua atau mana-mana jumlah yang perlu dibayar oleh anda (termasuk tetapi tidak terhad kepada Harga Jualan tertunda) kepada I.DESTINASI SDN BHD dibawah Terma dan Syarat ini, maka dengan itu ia perlu dibayar dan kena dibayar dengan segera. 
						                </li>
						                <li><b>Jaminan Selanjutnya:<br></b>
						                  	Anda hendaklah dari semasa ke semasa apabila diminta oleh I DESTINASI SDN BHD supaya melakukan atau memastikan berlakunya semua tindakan seumpamanya dan akan melaksanakan atau memastikan pelaksanaan semua dokumen seumpamanya seperti yang dianggap semunasabahnya perlu oleh I DESTINASI SDN BHD untuk memberikan kuat kuasa penuh kepada Kemudahan ini atau memberikan kepada I DESTINASI SDN BHD manfaat penuh bagi semua hak, kuasa dan remedi yang diberikan kepada I DESTINASI SDN BHD di bawah Kemudahan ini dan/atau Dokumen Kemudahan. 
						                </li>
						            </ol></li>
						        </ol>
					    </div>
					</ol>
				</div>
            </div>
        </table>
    <!--PAGE 5 END-->


    <!--PAGE 6 START-->
    <table width="100%"  cellspacing="4" style="page-break-before: always" id="customers"> 
		<div class="row">
  			<div class="column" style="font-size: 7px; line-height: 105%;">
				<ol start="12" type="1" style="font-size: 7px; width: 300px" >
              		<li>&nbsp;&nbsp; <b> KEJADIAN KEINGKARAN</b></li>
						<div align="justify">
              		 		<ol style="list-style-type: none;">
              		 			<li>Anda  dianggap  telah  melakukan  keingkaran  jika  anda  melakukan  atau  cuba  melakukan pelanggaran terhadap apa-apa waad, aku janji, ketetapan, terma, syarat atau peruntukan yang ditetapkan dalam dokumen ini dan tanpa menjejaskan kenyataan umum sebelum ini, apabila berlaku mana-mana satu kejadian berikut atau lebih:</li>
              		 		</ol>
	                  		<ol style="list-style-type: lower-alpha;">
				                <li>jika Ansuran pada bila-bila masa tidak semasa, atau jika anda gagal atau ingkar membayar mana-mana amaun yang perlu dibayar dan belum dijelaskan kepada I DESTINASI SDN BHD sama ada diminta secara rasmi atau tidak; atau</li>
				                <li>jika anda gagal mematuhi mana-mana pemberitahuan yang diberikan di bawah ini dan di bawah ini dan di bawah Dokumen Sekuriti lain yang menghendaki anda meremedi apa-apa pelanggaran syarat bagi Kemudahan ini atau Dokumen Sekuriti lain dalam tempoh yang ditetapkan dalam dokumen ini; atau</li>
				                <li>jika apa-apa keterhutangan anda diisytiharkan, atau boleh diisytiharkan menurut syarat yang berkaitan dengannya, sebagai perlu dibayar sebelum sampai masanya disebabkan oleh keingkaran anda dalam obligasi anda berhubung dengan perkara tersebut, atau anda gagal membuat apa-apa pembayaran berhubung dengannya pada tarikh cukup tempoh bagi pembayaran, atau apabila sekuriti bagi mana-mana keterhutangan tersebut menjadi berkuat kuasa; atau</li>
				                <li>jika anda meninggal dunia, diisytiharkan sebagai hilang akal, menjalani hukuman tahanan, melakukan apa-apa tindakan bankrap atau dihukum bankrap; atau </li>
				                <li>jika proses distres atau pelaksanaan atau proses lain oleh Mahkamah dengan bidang kuasa yang kompeten dikenakan atau dikeluarkan ke atas mana-mana harta anda dan proses distres, pelaksanaan atau proses lain tersebut, mengikut mana-mana yang berkenaan, tidak anda penuhi dalam tempoh empat belas (14) hari dari tarikhnya; atau</li>
				                <li>jika  anda  melakukan  atau  mengugut  untuk  melakukan  apa-apa  tindakan  atau  tidak melakukan apa-apa tindakan yang akan mengakibatkan anda melanggar mana-mana syarat, ketetapan, waad atau aku janji yang terkandung dalam dokumen ini, atau</li>
				                <li>jika anda melakukan keingkaran bagi mana-mana peruntukan dalam mana-mana perjanjian, atau dokumen sekuriti, atau kedua-duanya (mengikut mana-mana yang berkenaan), yang berkaitan dengan akaun atau kemudahan lain yang diberikan oleh I DESTINASI SDN BHD kepada anda atau pihak lain yang anda menjadi penjamin, atau penggadai atau penyerah haknya; atau</li>
				                <li>jika Dokumen Kemudahan atau mana-mana satu daripadanya dipertikaikan kesahannya oleh mana-mana orang; atau</li>
				                <li>jika satu kejadian atau beberapa kejadian telah berlaku, atau wujud keadaan yang boleh atau mungkin, mengikut pendapat I DESTINASI SDN BHD , menjejaskan kemampuan anda untuk melaksanakan obligasi anda di bawah Dokumen Kemudahan menurut syaratnya masing-masing; atau</li>
				                <li>jika mana-mana pernyataan atau jaminan yang dibuat atau yang dilibatkan menurut mana-mana peruntukan Terma dan Syarat ini atau mana-mana Dokumen Kemudahan atau menurut mana-mana notis, sijil, surat atau dokumen lain yang diserahkan menurut syarat dalam Terma dan Syarat ini atau mana-mana Dokumen Kemudahan didapati salah atau mengelirukan bagi butiran yang penting seperti pada tarikh ia dibuat atau dianggap telah dibuat; atau</li>
				                <li>jika  anda  hendak  membuat  penyerahan  hak  untuk  manfaat  pemiutang  anda  atau mengadakan urusan bagi setuju selesai untuk manfaat pemiutang anda atau membiarkan apa-apa penghakiman terhadap anda tidak dipenuhi bagi tempoh selama empat belas (14) hari atau lebih; atau</li>
				                <li>jika prosiding undang-undang, guaman atau apa-apa jenis tindakan (sama ada jenayah atau sivil) dimulakan terhadap anda; atau</li>
				                <li>jika berlaku apa-apa kejadian atau beberapa kejadian lain atau timbul keadaan yang pada pendapat mutlak I DESTINASI SDN BHD  memberikan alasan yang munasabah untuk mempercayai bahawa (i) anda tidak mungkin dapat melaksanakan apa-apa obligasi anda di bawah Terma dan Syarat ini atau Dokumen Kemudahan yang berkaitan atau anda mungkin tidak (atau mungkin tidak mampu) melaksanakan atau mematuhi obligasinya, dengan sewajarnya dan tepat pada waktunya, di bawah Terma dan Syarat ini atau Dokumen Kemudahan yang berkaitan; atau (ii) Kemudahan dalam keadaan terancam atau berbahaya atas apa-apa alasan sekalipun; atau</li>
				                <li>jika ia menjadi kesalahan di sisi undang-undang untuk I DESTINASI SDN BHD memberikan dan/atau terus memberikan Kemudahan; atau</li>
				                <li>Jika Kemudahan digunakan untuk tujuan bukan Islam, menyalahi undang-undang, tidak beretika, tidak bermoral atau tidak munasabah, atau dihentikan daripada digunakan untuk tujuan yang berlandaskan Syariah.</li>

				            </ol><br>
				        </div>
				       	<li>&nbsp;&nbsp; <b>HAK PENCEPATAN </b></li>
              				<div align="justify">
              					<ol style="list-style-type: none;">
              						<li>Jika berlaku satu Kejadian Keingkaran atau lebih, I DESTINASI SDN BHD  boleh, dengan menyerahkan pemberitahuan kepada anda:
	              				<ol style="list-style-type: lower-alpha;">
		                			<li>membatalkan Kemudahan ini; dan/atau</li>
					                <li>meminta anda membayar dengan segera semua atau mana-mana jumlah yang perlu anda bayar (termasuk tetapi tidak terhad kepada Harga Jualan Tertunda) kepada I DESTINASI SDN BHD  di bawah Terma dan Syarat ini, maka dengan itu ia perlu dibayar dan kena dibayar dengan segera; dan/atau</li>
					                <li style="  word-spacing: 1px;">menguatkuasakan semua atau apa-apa hak di bawah mana-mana Dokumen Kemudahan.</li>
				              	</ol><br></li>
              					</ol>
				            </div>
			            <li>&nbsp;&nbsp; <b>REMEDI SERENTAK </b></li>
			            	<div align="justify">
			            		<ol style="list-style-type: none;">
                  					<li>
              							Tanpa menyentuh apa-apa peruntukan yang terkandung dalam dokumen ini atau dalam Dokumen Kemudahan lain, apabila anda mengingkari atau melanggar mana-mana syarat, waad, ketetapan atau aku janji yang terkandung dalam dokumen ini dan/atau dalam mana-mana Dokumen Sekuriti lain yang perlu anda patuhi dan laksanakan, kemudian daripada itu I DESTINASI SDN BHD berhak melaksanakan semua atau mana-mana hak atau remedi yang ada sama ada di bawah Terma dan Syarat ini atau dalam Dokumen Sekuriti atau melalui statut atau selainnya dan berhak melaksanakan hak atau remedi tersebut serentak, termasuk meneruskan semua hak untuk pembayaran ganti rugi, mengumpulkan hasil daripada semua penyerahan hak dan dengan itu memberikan pelepasan yang sah, dan berhak memulakan prosiding undang-undang melalui guaman sivil atau prosiding penutupan atau jika tidak, ke atas anda untuk mendapatkan kembali semua wang yang perlu dibayar dan yang terhutang kepada I DESTINASI SDN BHD .
              						</li>
              					</ol>
              				</div>
              			<br>
              			<li>&nbsp;&nbsp; <b>SIJIL </b></li>
              				<div align="justify">
              					<ol style="list-style-type: none;">
                  					<li>
              							Apa-apa kebenaran atau pengakuan secara bertulis bagi pihak anda atau oleh mana-mana orang yang diberi kuasa untuk melaksanakan kuasa bagi pihak anda atau penghakiman (sama ada ingkar atau yang diperoleh terhadap anda) atau sijil atau penyata akaun I DESTINASI SDN BHD  mengikut jumlah yang perlu dibayar oleh anda kepada I DESTINASI SDN BHD  di bawah Terma dan Syarat ini dan Dokumen Kemudahan lain yang disahkan sewajarnya oleh pegawai I DESTINASI SDN BHD  yang diberi kuasa adalah, melainkan bagi apa-apa kesilapan nyata, muktamad dan mengikat anda untuk semua tujuan termasuk sebagai bukti muktamad keterhutangan di mahkamah.
              						</li>
              					</ol>
              				</div>
              			<br>
              			<li>&nbsp;&nbsp; <b>TANGGUNG RUGI </b></li>
              				<div align="justify">
              					<ol style="list-style-type: none;">
                  					<li>
              							Anda hendaklah menanggung rugi dan sentiasa menanggung rugi I DESTINASI SDN BHD sepenuhnya dan melindunginya pada setiap masa daripada dan terhadap apa-apa dakwaan, untutan, ganti rugi, kerugian, prosiding, kos dan/atau perbelanjaan yang mungkin ditanggung atau dialami oleh I DESTINASI SDN BHD disebabkan ia memberikan Kemudahan tersebut kepada anda, atau pelanggaran mana-mana pernyataan, jaminan, waad dan aku janji di bawah ini.
              						</li>
              					</ol>
              				</div>
              			<br>
              			<li>&nbsp;&nbsp; <b>PENOLAKAN </b></li>
              				<div align="justify">
              					<ol style="list-style-type: none;"><li>
                				<ol style="list-style-type: lower-alpha;">
                  					<li style="  word-spacing: 1px;">I DESTINASI SDN BHD boleh, pada bila-bila masa dan tanpa memberitahu anda, menggabungkan, menyatukan, mencantumkan semua atau mana-mana akaun anda dengan dan liabiliti kepada I DESTINASI SDN BHD , sama ada yang sedang terhutang atau yang di luar jangka termasuk fi dan keuntungan dan I DESTINASI SDN BHD boleh menolak atau memindahkan apa-apa jumlahyang ada dalam kredit bagi mana-mana akaun tersebut dalam atau untuk memenuhi mana-mana liabiliti anda kepada I DESTINASI SDN BHD , dan I DESTINASI SDN BHD boleh berbuat demikian tanpa mengambil kira baki akaun tersebut dan liabiliti itu mungkin tidak dinyatakan dalam mata wang yang sama dan I DESTINASI SDN BHD dengan ini diberi kuasa untuk menguatkuasakan apa-apa pertukaran yang perlu pada kadar pertukarannya sendiri berbanding kadar pertukaran yang biasa digunakan.</li>
                  					<li>Anda dengan ini, tanpa boleh membatalkannya dan tanpa syarat, memberikan kuasa </li>
                				</ol><br></li>
                			</ol>
                			</div>
				</ol>
			</div>

			<!-- PAGE 6 COLUMN-->
  			<div class="column" style="font-size: 7px;line-height: 105%;">
  				<div align="justify">
  					<ol style="list-style-type: none; width: 300px" ><li>
      					kepada I DESTINASI SDN BHD pada bila-bila masa mengikut budi bicara mutlaknya untuk mendebitkan mana-mana akaun anda dengan I DESTINASI SDN BHD (atau mana-mana cawangannya) dengan jumlah tertentu yang sama dengan apa-apa fi, keuntungan, bayaran pokok atau apa-apa jumlah lain yang perlu dibayar di bawah Kemudahan ini dan Dokumen Kemudahan dengan syarat pendebitan itu tidak boleh dianggap sebagai pembayaran bagi jumlah yang perlu dibayar atau penepian bagi mana-mana Kejadian Keingkaran. I DESTINASI SDN BHD tidak berkewajiban untuk melaksanakan hak yang diberikan di sini.
    				</ol></li>
  				<ol start="18" type="1" style="font-size: 7px; width: 300px" >
  					<li>&nbsp;&nbsp; <b>KOS DAN CAJ </b></li>
  						<div align="justify">
	  						<ol style="list-style-type: none;"><li>
			                	<ol style="list-style-type: lower-alpha;">
				                  	<li>Anda hendaklah, apabila diminta, membayar semua kos dan perbelanjaan yang ditanggung semunasabahnya (dan disokong dengan bukti dokumen) oleh I DESTINASI SDN BHD dan/atau Perbadanan (termasuk fi perundangan berdasarkan klien-peguam cara), dan apa-apa cukai nilai ditambah ke atas perbelanjaan tersebut: </li>
				                    	<ol type="i">
					                      	<li>berhubung dengan penyediaan, perundingan, pencetakan dan pelaksanaan Terma dan Syarat ini dan Dokumen Kemudahan termasuk semua fi notari dan penterjemahan;</li>
					                      	<li>berhubung dengan pemberian bagi apa-apa pelepasan, penepian atau persetujuan atau berhubung dengan apa-apa pindaan atau perubahan Terma dan Syarat ini atau mana-mana Dokumen Sekuriti lain; dan </li>
					                      	<li>dalam menguatkuasakan, menyempurnakan, melindungi atau memelihara (atau cubaan untuk berbuat demikian) mana-mana haknya, atau dalam mendakwa atau mendapatkan kembali apa-apa jumlah yang perlu dibayar oleh anda atau oleh mana-mana pihak di bawah mana-mana Dokumen Kemudahan, atau dalam menyiasat apa-apa Kejadian Keingkaran yang mungkin. </li>
				                    	</ol>
				                 	<li>Semua fi dan caj lain yang perlu dibayar oleh anda dalam dokumen ini, hendaklah dibayar sepenuhnya dan tidak termasuk apa-apa cukai nilai ditambah, cukai perkhidmatan atau cukai atau duti lain. Apa-apa cukai nilai ditambah, cukai perkhidmatan atau cukai atau duti lain yang perlu dibayar berhubung dengan fi tersebut hendaklah dibayar oleh anda selain membayar jumlah tersebut.</li>
				                  	<li>Berhubung dengan kos dan perbelanjaan tersebut yang ditanggung semunasabahnya oleh Perbadanan di bawah Kemudahan yang mesti dibayar oleh anda kepada Perbadanan, anda dengan ini memberikan kuasa kepada I DESTINASI SDN BHD untuk membayar kos dan perbelanjaan tersebut terus kepada Perbadanan.</li>
				                </ol> <br></li>
				            </ol>
				        </div>
				    <li>&nbsp;&nbsp; <b>PENEPIAN, KUMULATIF REMEDI  </b></li>
  						<div align="justify">
	  						<ol style="list-style-type: none;"><li>
			                	<ol style="list-style-type: lower-alpha;">
				                  	<li>Hak I DESTINASI SDN BHD di bawah Dokumen Kemudahan:</li>
				                    	<ol type="i">
					                      	<li>boleh dilaksanakan sekerap yang perlu; </li>
					                      	<li>bersifat kumulatif dan tidak eksklusif dalam haknya di bawah undang-undang am; dan  </li>
					                      	<li>boleh diketepikan hanya secara bertulis dan secara khusus. Kelewatan melaksanakan atau tidak melaksanakan mana-mana hak tersebut tidak menjadi satu penepian bagi hak itu.</li>
				                    	</ol>
				                 	<li> Kegagalan atau kelewatan pihak I DESTINASI SDN BHD dalam melaksanakan dan juga apa-apa peninggalan untuk melaksanakan apa-apa hak, kuasa, keistimewaan atau remedi yang terakru kepada I DESTINASI SDN BHD di bawah Terma dan Syarat ini dan/atau Dokumen Kemudahan lain apabila berlaku apa-apa keingkaran di pihak anda, tidak boleh menjejaskan apa-apa hak, kuasa, keistimewaan atau remedi tersebut atau tidak boleh dianggap sebagai penepian baginya atau apa-apa persetujuan dalam mana-mana keingkaran tersebut dan apa-apa tindakan I DESTINASI SDN BHD berhubung dengan mana-mana keingkaran atau apa-apa persetujuan dalam mana-mana keingkaran tersebut, tidak boleh mempengaruhi atau menjejaskan apa-apa hak, kuasa, keistimewaan atau remedi bagi I DESTINASI SDN BHD berhubung dengan apa-apa keingkaran lain atau keingkaran berikutnya.</li>
				                  	
				                </ol> <br></li>
				            </ol>
				        </div>
				        <li>&nbsp;&nbsp; <b>PEMUTUSAN </b></li>
              				<div align="justify">
              					<ol style="list-style-type: none;">
              						<li>Jika apa-apa peruntukan bagi mana-mana Dokumen Kemudahan merupakan atau menjadi kesalahan di sisi undang-undang, tidak sah atau tidak boleh dikuatkuasakan dalam mana-mana bidang kuasa, ia tidak menjejaskan:
	              					<ol style="list-style-type: lower-alpha;">
			                			<li>keundangan, kesahan atau kebolehkuatkuasaan dalam bidang kuasa itu bagi mana-mana peruntukan lain dalam Dokumen Kemudahan; atau</li>
						                <li>keundangan, kesahan atau kebolehkuatkuasaan dalam mana-mana bidang kuasa lain bagi peruntukan itu atau mana-mana peruntukan lain dalam Dokumen Kemudahan.</li>
				              		</ol><br></li>
              					</ol>
				            </div>
				        <li>&nbsp;&nbsp; <b>PEMBERITAHUAN </b></li>
  						<div align="justify">
	  						<ol style="list-style-type: none;"><li>
			                	<ol style="list-style-type: lower-alpha;">
			                		<li>Setiap pemberitahuan atau komunikasi lain yang akan diberikan di bawah Terma dan Syarat ini hendaklah diberikan secara bertulis dalam bahasa Melayu  atau bahasa Inggeris  dan, melainkan jika diberikan dengan cara selainnya, boleh diberikan melalui faks atau surat.</li>
				                  	<li>Apa-apa tuntutan, pemberitahuan atau komunikasi lain yang hendak diberikan oleh satu pihak kepada pihak yang satu lagi di bawah Terma dan Syarat ini hendaklah (melainkan jika satu pihak memberikan notis 15 hari kepada pihak yang satu lagi tentang perubahan alamat) diberikan kepada pihak yang satu lagi di alamat dan nombor yang diberikan dalam Bahagian 9 bagi Rumusan.</li>
				                  	<li>Apa-apa tuntutan, komunikasi atau dokumen yang dibuat atau yang diserahkan oleh I DESTINASI SDN BHD kepada anda di bawah atau berhubung dengan Terma dan Syarat ini hendaklah dianggap sebagai telah diserahkan sewajarnya.</li>
				                    	<ol type="i">
					                      	<li>jika dihantar melalui faks, laporan penghantaran yang dikeluarkan melalui rekod terminal penghantaran bahawa faks tersebut telah dihantar sewajarnya;</li>
					                      	<li>jika dihantar melalui surat, apabila ia telah ditinggalkan di alamat yang berkaitan atau setelah lima Hari Perniagaan dari tarikh ia diposkan di pejabat pos dengan bayaran pos prabayar di dalam sampul surat yang dialamatkan kepadanya di alamat tersebut; atau</li>
					                      	<li>jika melalui teleks, apabila dihantar, tetapi hanya jika jawapan balas yang betul muncul pada bahagian awal dan akhir salinan pemberitahuan penghantar semasa penghantaran; dan</li>
					                      	<li>jika jabatan atau pegawai tertentu ditetapkan sebagai sebahagian daripada butiran alamatnya yang diberikan di bawah Bahagian 9 bagi Rumusan, jika dialamatkan kepada jabatan atau pegawai tersebut. </li>
				                    	</ol>
				                  	<li>Apa-apa  komunikasi  atau  dokumen  yang  hendak  dibuat  atau  diserahkan  kepada  I DESTINASI SDN BHD  akan berkuat kuasa hanya apabila I DESTINASI SDN BHD  telah benar-benar menerimannya. </li>
				                </ol> <br></li>
				            </ol>
				        </div>
				        <li>&nbsp;&nbsp; <b>PENYAMPAIAN PROSES UNDANG-UNDANG </b></li>
              				<div align="justify">
              					<ol style="list-style-type: none;">
                  					<li>
              							Tanpa menjejaskan apa-apa bentuk penyampaian lain yang dibenarkan dalam undang-undang, penyampaian apa-apa writ, saman, pernyataan tuntutan atau apa-apa proses undang-undang berhubung dengan atau yang timbul daripada Terma dan Syarat ini boleh dikuatkuasakan ke atas anda dengan menghantar salinan writ, saman, pernyataan tuntutan atau proses undang-undang yang lain melalui pos berdaftar prabayar ke alamat anda yang dinyatakan dalam dokumen ini atau ke alamat berdaftar atau ke alamat anda yang terakhir yang diketahui.
              						</li>
              					</ol>
              				</div>
              			<br>
              			<li>&nbsp;&nbsp; <b>PERUBAHAN ALAMAT </b></li>
              				<div align="justify">
              					<ol style="list-style-type: none;">
                  					<li>
              							Jika berlaku perubahan dalam alamat anda, anda hendaklah seberapa segera yang boleh tetapi dalam tempoh tujuh (7) hari sebelum perubahan tersebut memberitahu I DESTINASI SDN BHD secara bertulis tentang perubahan itu dan semua dokumen, tuntutan, pemberitahuan atau permintaan yang tersebut akan dihantar ke alamat baru anda itu.
              						</li>
              					</ol>
              				</div>
              			<br>
              			<li>&nbsp;&nbsp; <b>PEMBERITAHUAN CETAKAN KOMPUTER</b></li>
              				<div align="justify">
              					<ol style="list-style-type: none;">
                  					<li>
              							Tanpa menyentuh apa-apa yang bertentangan yang diberikan atau yang terkandung di sini, apaapa surat, tuntutan, pernyataan, peringatan, sijil atau apa-apa pemberitahuan lain (selepas ini dirujuk sebagai “Pemberitahuan tersebut”) yang perlu diberikan oleh I DESTINASI SDN BHD di bawah ini tidak memerlukan tandatangan atau tidak perlu ditandatangani oleh mana-mana pegawai atau mana-mana orang lain yang dirujuk sebelum ini, dengan mencatatkan pada manamana Pemberitahuan tersebut bahawa Pemberitahuan tersebut adalah cetakan komputer dan tandatangan tidak diperlukan.
              						</li>
              					</ol>
              				</div>
              			<br>
              			 <li>&nbsp;&nbsp; <b>PENYERAHAN DAN PEMINDAHAN</b></li>
  						<div align="justify">
	  						<ol style="list-style-type: none;"><li>
			                	<ol style="list-style-type: lower-alpha;">
				                  	<li>Terma dan Syarat ini mengikat dan memastikan ia memanfaatkan setiap pihak dan pengganti serta pemegang serah haknya yang dibenarkan.</li>
				                 	<li>Anda tidak berhak menyerahkan atau memindahkan apa-apa hak atau obligasi anda atau</li>
				                </ol> <br></li>
				            </ol>
				        </div>
			    </ol>
          	</div>
        </div>
    </table>
    <!--PAGE 6 END-->

    <!--PAGE 7 START-->
    <table width="100%"  cellspacing="4" style="page-break-before: always" id="customers"> 
		<div class="row">
  			<div class="column" style="font-size: 7px; line-height: 105%;">
  				<div align="justify">
  					<ol style="list-style-type: none; width: 300px" ><li>
      					mewujudkan apa-apa urusan benefisial atau amanah, di bawah mana-mana Dokumen Kemudahan.</li>
    				</ol>
    				<ol style="list-style-type: lower-alpha;width: 300px">
	                  	<li>I DESTINASI SDN BHD  berhak pada bila-bila masa selepas ini untuk menyerahkan atau memindahkan apa-apa hak, manfaat atau obligasinya di bawah mana-mana Dokumen Kemudahan kepada mana-mana orang lain tanpa persetujuan anda dan tanpa perlu memaklumkan anda.</li>
	                 	<li>Jika berlaku pemindahan atau penyerahan oleh I DESTINASI SDN BHD  seperti yang dinyatakan sebelumnya, anda dengan ini mengaku janji atas kos dan perbelanjaan anda sendiri untuk melaksanakan semua dokumen tersebut seperti yang dikehendaki oleh I DESTINASI SDN BHD bagi memudahkan pemindahan atau penyerahan tersebut.</li>
	                </ol> 
	        	</div>
				<ol start="26" type="1" style="font-size: 7px; width: 300px" >
              		<li>&nbsp;&nbsp; <b>PENDEDAHAN MAKLUMAT</b></li>
						<div align="justify">
	                  		<ol type="1.1">
				                <li style="counter-increment: item;">Anda sedar bahawa segala maklumat yang diberikan oleh anda adalah bertujuan membolehkan I DESTINASI SDN BHD membuat penilaian berhubung dengan permohonan Kemudahan anda dan adalah menjadi kewajipan anda untuk mendedahkan maklumat tersebut termasuk Data Peribadi (seperti yang ditakrif di bawah Akta Perlindungan Data Peribadi 2010). Tanpa pendedahan maklumat tersebut, anda sedar bahawa I DESTINASI SDN BHD mungkin tidak dapat meluluskan Kemudahan dan/atau menyediakan perkhidmatan-perkhidmatan lain kepada anda. </li>
				                <li>Anda memberi kuasa kepada I DESTINASI SDN BHD untuk melaksanakan pemeriksaan dan/atau penyiasatan (termasuk tetapi tidak terhad kepada pemeriksaan CTOS/CCRIS) ke atas kedudukan kewangan saya dengan mana-mana organisasi, syarikat, perbadanan dan/atau pihak berkuasa mengikut budi bicara I DESTINASI SDN BHD yang mutlak. Secara jelasnya, pemberian kuasa ini adalah terpakai kepada mana-mana penerima serah hak atau pemegang serah hak akan datang dan pemberian kuasa ini merangkumi pemeriksaan sebelum atau selepas tarikh penyerahan hak tersebut berkuatkuasa. </li>
				                <li>Anda memberi kuasa kepada I DESTINASI SDN BHD untuk menentusahkan dan berkongsi dengan mana-mana pihak maklumat berkenaan diri anda yang diperoleh dari mana-mana pihak. </li>
				                <li>Anda mengesahkan bahawa segala maklumat yang diberikan oleh anda adalah benar, tepat dan lengkap. Anda bersetuju untuk memaklumkan I DESTINASI SDN BHD sekiranya terdapat perubahan terhadap maklumat tersebut. Anda juga sedar bahawa anda mempunyai hak untuk memohon akses dan memohon pembetulan dibuat terhadap maklumat-maklumat tersebut. </li>
				                <li>Anda memberi kuasa kepada I DESTINASI SDN BHD untuk mendedahkan maklumat berkenaan diri anda atau Data Peribadi (seperti yang ditakrif di bawah Akta Perlindungan Data Peribadi 2010) kepada cawangan, ahli atau mana-mana entiti yang bernaung dengan I DESTINASI SDN BHD bagi tujuan memaklum dan mengemas-kini tentang produk-produk atau perkhidmatan-perkhidmatan yang ditawarkan oleh I DESTINASI SDN BHD, dan sehingga I DESTINASI SDN BHD diberikan notis secara bertulis oleh anda untuk menghentikan perkhidmatan tersebut, I DESTINASI SDN BHD akan terus memaklum dan mengemas-kini anda tentang produk-produk dan perkhidmatan-perkhidmatan yang ditawarkan I DESTINASI SDN BHD, tanpa mengira hubungan pelanggan-pembiaya di antara saya dan I DESTINASI SDN BHD telah tamat atau saya tidak lagi mempunyai sebarang akaun atau kemudahan dengan I DESTINASI SDN BHD. </li>
				                <li>Anda juga memberi kuasa kepa I DESTINASI SDN BHD untuk menyimpan dan menggunakan maklumat yang diberikan bagi apa jua tujuan yang I DESTINASI SDN BHD anggap bersesuaian. </li>
				                <li>Anda dengan ini bersetuju dan memberikan persetujuan anda secara nyata bahawa I DESTINASI SDN BHD berhak, tanpa notis dan tanpa liabiliti dan tanpa persetujuan anda untuk mendedahkan apa-apa maklumat yang berkaitan dengan anda, Data Peribadi (seperti yang ditakrif di dalam Akta Perlindungan Data Peribadi 2010) (termasuk tetapi tidak terhad kepada butiran, perniagaan dan operasi anda (kewangan atau selainnya) dan pengendalian akaun anda (semasa dan pada masa depan), Kemudahan (termasuk Terma dan Syarat ini, baki yang belum dijelaskan dan sekuriti (jika ada) yang diberikan berhubung dengannya) kepada mana-mana pihak berikut:</li> 
				            </ol>
				            <ol style="list-style-type: lower-alpha;">
			                  	<li>Unit Kredit Pusat (termasuk untuk tujuan penyimpanan di Sistem Maklumat Rujukan Kredit Pusat, dengan maklumatnya boleh didapati oleh semua institusi kewangan yang menyertainya), Bank Negara Malaysia dan mana-mana agensi kerajaan yang lain atau pihak berkuasa kawal selia yang mempunyai kuasa ke atas I DESTINASI SDN BHD ; </li>
			                 	<li>mana-mana pihak sekuriti lain; </li>
			                 	<li>(c)	ibu pejabat I DESTINASI SDN BHD ; </li>
			                 	<li>(d)	mana-mana cawangan I DESTINASI SDN BHD , pejabat perwakilan, Ahli gabungan (semasa dan juga pada masa depan); </li>
			                 	<li>(e)	mana-mana orang yang menyediakan perkhidmatan kepada I DESTINASI SDN BHD (termasuk tetapi tidak terhad kepada juruaudit I DESTINASI SDN BHD , penasihat undang-undang dan penasihat profesional); </li>
			                 	<li>(f)	mana-mana orang yang kepadanya (atau melaluinya) I DESTINASI SDN BHD menyerahkan atau memindahkan (atau yang berpotensi menyerahkan atau memindahkan) semua atau mana-mana hak dan obligasinya di bawah Terma dan Syarat ini; </li>
			                 	<li>(g)	mana-mana orang yang dengannya (atau melaluinya) I DESTINASI SDN BHD mengadakan (atau mungkin akan mengadakan) apa-apa subpenyertaan berhubung dengan, atau apa-apa urus niaga lain yang di bawahnya pembayaran akan dibuat dengan merujuk, Terma dan Syarat ini atau anda; </li>
			                 	<li>(h)	mana-mana orang yang kepadanya dan selagi maklumat perlu didedahkan oleh mana-mana undang-undang atau peraturan yang terpakai; </li>
			                 	<li>(i)	mana-mana orang yang kepadanya I DESTINASI SDN BHD mempunyai kewajipan untuk mendedahkannya; </li>
			                 	<li>(j)	mana-mana pengendali Takaful I DESTINASI SDN BHD atau anda;</li>
			                 	<li>(k)	mana-mana penyedia perkhidmatan (termasuk agensi pemungut hutang dan penyedia khidmat pesanan ringkas); dan </li>
			                 	<li>(l)	mana-mana orang atau pihak untuk tujuan tersebut yang dianggap perlu atau wajar oleh I DESTINASI SDN BHD , mengikut budi bicara mutlaknya. </li>
			                </ol> <br></li>
			                <ol style="list-style-type: none;">
              					<li>Dalam keadaan tersebut, anda mengaku dan bersetuju bahawa I DESTINASI SDN BHD tidak bertanggungan ke atas anda terhadap peruntukan, keselamatan, ketepatan atau penggunaan maklumat tersebut. Fasal ini menggantikan mana-mana perjanjian terdahulu yang berhubung dengan kerahsiaan maklumat ini.<br><br></li>
              				</ol>
				        </div>
				         <li>&nbsp;&nbsp; <b>BIDANG KUASA </b></li>
  						<div align="justify">
	  						<ol style="list-style-type: none;"><li>
			                	<ol style="list-style-type: lower-alpha;">
			                		<li>Untuk manfaat I DESTINASI SDN BHD , anda bersetuju bahawa mahkamah Malaysia mempunyai bidang kuasa untuk menyelesaikan apa-apa pertikaian berhubung dengan Terma dan Syarat ini dan oleh itu menyerahkannya kepada bidang kuasa mahkamah Malaysia. </li>
				                  	<li>Tiada satu pun dalam Fasal  27 yang mengehadkan hak I DESTINASI SDN BHD  untuk mengenakan prosiding terhadap anda berhubung dengan Terma dan Syarat ini. </li>
				                    	<ol type="i">
					                      	<li>dalam mana-mana mahkamah lain yang mempunyai bidang kuasa kompeten; atau</li>
					                      	<li>serentak dalam lebih daripada satu bidang kuasa.</li>
				                    	</ol>
				                </ol> <br></li>
				            </ol>
				        </div>
				       	<li>&nbsp;&nbsp; <b>PENGUBAHSUAIAN DAN KELONGGARAN </b></li>
              				<div align="justify">
              					<ol style="list-style-type: none;">
              						<li>I DESTINASI SDN BHD boleh pada bila-bila masa tanpa dalam apa cara sekalipun menjejaskan
	              				<ol style="list-style-type: lower-alpha;">
		                			<li>membatalkan Kemudahan ini; dan/atau</li>
					                <li>meminta anda membayar dengan segera semua atau mana-mana jumlah yang perlu anda bayar (termasuk tetapi tidak terhad kepada Harga Jualan Tertunda) kepada I DESTINASI SDN BHD  di bawah Terma dan Syarat ini, maka dengan itu ia perlu dibayar dan kena dibayar dengan segera; dan/atau</li>
					                <li style="  word-spacing: 1px;">menguatkuasakan semua atau apa-apa hak di bawah mana-mana Dokumen Kemudahan.</li>
				              	</ol><br></li>
              					</ol>
				            </div>
			            <li>&nbsp;&nbsp; <b>REMEDI SERENTAK </b></li>
			            	<div align="justify">
			            		<ol style="list-style-type: none;">
                  					<li>
              							Tanpa menyentuh apa-apa peruntukan yang terkandung dalam dokumen ini atau dalam Dokumen Kemudahan lain, apabila anda mengingkari atau melanggar mana-mana syarat, waad, ketetapan atau aku janji yang terkandung dalam dokumen ini dan/atau dalam mana-mana Dokumen Sekuriti lain yang perlu anda patuhi dan laksanakan, kemudian daripada itu I DESTINASI SDN BHD berhak melaksanakan semua atau mana-mana hak atau remedi yang ada sama ada di bawah Terma dan Syarat ini atau dalam Dokumen Sekuriti atau melalui statut atau selainnya dan berhak melaksanakan hak atau remedi tersebut serentak, termasuk meneruskan semua hak untuk pembayaran ganti rugi, mengumpulkan hasil daripada semua penyerahan hak dan dengan itu memberikan pelepasan yang sah, dan berhak memulakan prosiding undang-undang melalui guaman sivil atau prosiding penutupan atau jika tidak, ke atas anda untuk mendapatkan kembali semua wang yang perlu dibayar dan yang terhutang kepada I DESTINASI SDN BHD .
              						</li>
              					</ol>
              				</div>
				</ol>
			</div>

			<!-- PAGE 7 COLUMN-->
  			<div class="column" style="font-size: 7px;line-height: 105%;">
  				<div align="justify">
  					<ol style="list-style-type: none; width: 300px" ><li>
      					dan arahan Suruhanjaya Koperasi Malaysia , Bank Negara Malaysia dan/atau mana-mana badan atau pihak berkuasa lain yang mempunyai kuasa ke atas I DESTINASI SDN BHD sama ada perkara tersebut dibuat sebelum atau selepas Terma dan Syarat ini dilaksanakan; dan/atau</li>
    				</ol>
    				<ol style="list-style-type: lower-alpha;width: 300px">
	                  	<li>mengubah atau menyimpang daripada terma dan syarat yang mengawal Kemudahan atau mana-mana bahagiannya dan/atau peruntukan Dokumen Kemudahan dan anda dengan ini memberikan persetujuan secara nyata bagi semua perubahan dan penyimpangan itu, walau bagaimana penting sekalipun; dan/atau;</li>
	                 	<li>menggunakan semua atau apa-apa remedi atau cara untuk mendapatkan semula amaun yang dicagar di bawah Dokumen Kemudahan yang mungkin ada untuk tujuan tertentu, pada masa tertentu dan mengikut aturan dan cara tertentu seperti yang difikirkan sesuai oleh I DESTINASI SDN BHD.</li>
	                </ol> 

  				<ol start="29" type="1" style="font-size: 7px; width: 300px" >
  					<li>&nbsp;&nbsp; <b>TARIKH KUAT KUASA </b></li>
		            	<div align="justify">
		            		<ol style="list-style-type: none;">
              					<li>
          							Pihak dalam dokumen ini bersetuju bahawa Terma dan Syarat ini akan berkuat kuasa pada tarikh anda bersetuju dengan Terma dan Syarat ini.
          						</li>
          					</ol>
          				</div><br>
          			<li>&nbsp;&nbsp; <b>PERJANJIAN SELURUH </b></li>
		            	<div align="justify">
		            		<ol style="list-style-type: none;">
              					<li>
          							Melainkan seperti yang disyaratkan sebaliknya, Dokumen Kemudahan menetapkan perjanjian dan persefahaman seluruh antara pihak dalam dokumen ini sebagai perkara dalam perjanjian dalam dokumen ini dan menggantikan semua perjanjian terdahulu sama ada yang tersurat atau tersirat.
          						</li>
          					</ol>
          				</div><br>
          			<li>&nbsp;&nbsp; <b>AKAUN TERGANTUNG </b></li>
		            	<div align="justify">
		            		<ol style="list-style-type: none;">
              					<li>
          							Apa-apa wang yang diterima di bawah Dokumen Kemudahan boleh diletakkan atau disimpan dalam akaun tergantung bukan pendapatan selagi I DESTINASI SDN BHD  mendapati sesuai tanpa apa-apa obligasi bagi tempoh sementara untuk memohon perkara yang sama atau mana-mana bahagiannya dalam atau untuk menjelaskan apa-apa wang atau liabiliti yang perlu dibayar atau yang ditanggung oleh anda kepada I DESTINASI SDN BHD di bawah atau menurut Dokumen Kemudahan. Tanpa menyentuh mana-mana bayaran tersebut jika berlaku apa-apa prosiding dalam atau seakan-akan penyelesaian, komposisi atau perkiraan, I DESTINASI SDN BHD	boleh membuktikan dan bersetuju untuk menerima apa-apa dividen atau setuju selesai berhubung dengan seluruh atau mana-mana bahagian wang dan liabiliti tersebut mengikut cara yang sama seolah-olah sekuriti di bawah Terma dan Syarat ini tidak pernah diwujudkan.
          						</li>
          					</ol>
          				</div><br>
          			<li>&nbsp;&nbsp; <b>MENDEBITKAN AKAUN PELANGGAN </b></li>
		            	<div align="justify">
		            		<ol style="list-style-type: none;">
              					<li>
          							Anda dengan ini bersetuju bahawa tanpa menjejaskan apa-apa hak dan remedi lain bagi I DESTINASI SDN BHD , bahawa mana-mana wang yang perlu dibayar dan mesti dibayar kepada I DESTINASI SDN BHD , atau didahulukan oleh I DESTINASI SDN BHD bagi pihak anda, boleh didebitkan daripada akaun anda yang dibuka dan diselenggara oleh I DESTINASI SDN BHD atau akaun lain seumpamanya seperti yang dianggap sesuai oleh I DESTINASI SDN BHD .
          						</li>
          					</ol>
          				</div><br>
  					<li>&nbsp;&nbsp; <b>PEMATUHAN SYARIAH </b></li>
  						<div align="justify">
	  						<ol style="list-style-type: none;"><li>
			                	<ol style="list-style-type: lower-alpha;">
				                  	<li>Anda dan I DESTINASI SDN BHD dengan ini bersetuju dan mengakui secara muktamad bahawa seperti pada tarikh dokumen ini, Terma dan Syarat ini (termasuk setiap syarat yang terkandung dalam dokumen ini dan struktur yang menjadi asas dokumen ini) dan Dokumen Kemudahan lain Mematuhi Syariah. Sewajarnya, setiap pihak dalam dokumen ini bersetuju bahawa mulai sekarang ia tidak boleh menimbulkan apa-apa pertikaian tentang sama ada Terma dan Syarat ini dan Dokumen Kemudahan lain mematuhi Syariah atau tidak. </li>
				                    	
				                 	<li>Tertakluk kepada Fasal 33(a) di atas, anda dan I DESTINASI SDN BHD dengan ini bersetuju dan mengaku bahawa jika, selepas tarikh pada dokumen ini, apa-apa soalan timbul berhubung dengan perkara Syariah dalam mana-mana prosiding di hadapan mana-mana mahkamah atau penimbang tara berhubung dengan Kemudahan, Terma dan Syarat ini dan/atau Dokumen Kemudahan lain, maka soalan tersebut hendaklah dirujuk kepada Majlis Penasihat Syariah yang ditubuhkan di bawah subbahagian 16B(1) Akta Bank Pusat Malaysia 2009 untuk keputusannya dan keputusan tersebut adalah muktamad dan mengikat pihak dalam Terma dan Syarat ini dan Dokumen Kemudahan lain dan pihak itu hendaklah mengambil semua langkah yang perlu untuk menyebabkan/mendapakan mahkamah atau penimbang tara merujuk soalan tersebut kepada Majlis Penasihat Syariah. </li>
				                </ol> <br></li>
				            </ol>
				        </div>
				    <li>&nbsp;&nbsp; <b>PERUBAHAN SYARAT  </b></li>
		            	<div align="justify">
		            		<ol style="list-style-type: none;">
              					<li>
          							Tanpa menyentuh apa-apa yang bertentangan yang terkandung dalam dokumen ini, I DESTINASI SDN BHD boleh, pada bila-bila masa dan mengikut budi bicara mutlaknya tanpa melepaskan dalam apa cara sekalipun liabliti anda di bawah ini dan di bawah Kemudahan, mengubah dan/ atau menambah syarat dalam dokumen ini dan perubahan dan/atau tambahan itu hendaklah berkuat kuasa apabila pemberitahuan diberikan oleh I DESTINASI SDN BHD kepada anda dengan sentiasa tertakluk kepada prinsip Syariah. Perubahan dan/atau tambahan itu hendaklah terus dianggap sebagai telah diubah dan/atau ditambah sewajarnya, dan hendaklah dibaca dan ditafsirkan seolah-olah perubahan dan/atau tambahan itu telah dimasukkan ke dalam dan telah membentuk sebahagian daripada Terma dan Syarat ini pada masa dokumen ini dilaksanakan. 
          						</li>
          					</ol>
          				</div><br>
          			 <li>&nbsp;&nbsp; <b>TAFSIRAN</b></li>
		            	<div align="justify">
		            		<ol style="list-style-type: none;">
              					<li>
          							Terma dan syarat yang diyatakan dalam borang dan surat yang dilampirkan dalam dokumen ini membentuk bahagian penting Terma dan Syarat ini dan jika berlaku apa-apa konflik atau percanggahan antara Terma dan Syarat ini dengan terma dan syarat dalam borang dan surat dalam Terma dan Syarat ini, maka Terma dan Syarat ini hendaklah digunakan. 
          						</li>
          					</ol>
          				</div><br>
          			 <li>&nbsp;&nbsp; <b>UNDANG-UNDANG YANG MENGAWAL </b></li>
		            	<div align="justify">
		            		<ol style="list-style-type: none;">
              					<li>
          							Terma dan Syarat ini dikawal oleh dan ditafsirkan menurut undang-undang Malaysia. 
          						</li>
          					</ol>
          				</div><br>
          			 <li>&nbsp;&nbsp; <b>MASA </b></li>
		            	<div align="justify">
		            		<ol style="list-style-type: none;">
              					<li>
          							Masa, di mana sahaja disebut dalam dokumen ini, hendaklah menjadi intipati bagi Terma dan Syarat ini. 
          						</li>
          					</ol>
          				</div><br>
          			<li>&nbsp;&nbsp; <b>PENYELESAIAN AWAL PEMBIAYAAN </b></li>
		            	<div align="justify">
		            		<ol style="list-style-type: none;">
              					<li>
          							Anda berhak membuat penyelesaian awal Kemudahan ini dengan memberi notis bertulis selama tiga puluh (30) hari kepada I DESTINASI SDN BHD. Notis penyelesaian awal yang diberikan adalah muktamad dan tidak boleh ditarik balik. Pengiraan, mekanisme dan amaun rebat (Ibra’) akan ditentukan berdasarkan budi bicara I DESTINASI SDN BHD atau formula rebat lain yang mungkin akan ditentukan oleh I DESTINASI SDN BHD dan rebat tersebut adalah muktamad dan mengikat. 
          						</li>
          					</ol>
          				</div><br>
          			<li>&nbsp;&nbsp; <b>TAKRIF </b></li>
		            	<div align="justify">
		            		<ol style="list-style-type: none;">
              					<li>
          							Dalam Terma dan Syarat ini, kecuali konteks menghendaki sebaliknya, perkataan dan ungkapan berikut hendaklah mempunyai makna berikut: 
          						</li>
          					</ol>
          				</div><br>
			    </ol>
			    <table cellpadding="3" style="font-size: 11px; ">
  					 <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Harga Jualan Tertunda"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud, berhubung dengan Penjualan Komoditi, harga jualan Komoditi oleh I DESTINASI SDN BHD kepada anda seperti yang ditetapkan dalam Pemberitahuan Tawaran;
								<br>
							</div>
			            </td>
			        </tr>
			    </table>
          	</div>
        </div>
    <!--PAGE 7 END-->

    <!--PAGE 8 START-->
  	<table width="100%"  cellspacing="4" style="page-break-before: always" id="customers"> 
		<div class="row">
  			<div class="column">
  				<table cellpadding="3" style="font-size: 11px; ">
  					 <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Harga Jualan Tertunda"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud, berhubung dengan Penjualan Komoditi, harga jualan Komoditi oleh I DESTINASI SDN BHD kepada anda seperti yang ditetapkan dalam Pemberitahuan Tawaran;
								<br>
							</div>
			            </td>
			        </tr>
			         <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Kejadian Keingkaran"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud apa-apa kejadian seperti yang diterangkan dalam Fasal 12 dalam dokumen ini;
								<br>
							</div>
			            </td>
			        </tr>
			         <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Kemudahan"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud kemudahan Kemudahan yang diterangkan dalam Fasal 1 dalam dokumen ini;
								<br>
							</div>
			            </td>
			        </tr>
			        <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Amaun Kemudahan"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud  amaun  Kemudahan  seperti  yang dinyatakan dalam Fasal 1 dalam dokumen ini;
								<br>
							</div>
			            </td>
			        </tr>
			         <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Dokumen<br>
			                Kemudahan"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud:<br>
			              		<b>(a)</b> Terma dan Syarat;<br>
			              		<b>(b)</b> Pemberitahuan Permohonan;<br>
			              		<b>(c)</b> Pemberitahuan Tawaran;<br>
			              		<b>(d)</b> apa-apa dokumen lain seperti yang ditentukan oleh I DESTINASI SDN BHD;<br><br>
			              		dan “Dokumen Kemudahan” bermaksud salah satu daripadanya; <br>
							</div>
			            </td>
			        </tr>
			        <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Kerajaan"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud Kerajaan Malaysia
								<br>
							</div>
			            </td>
			        </tr>
			         <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Ansuran"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud ansuran bagi Harga Jualan Tertunda yang perlu dibayar seperti yang dinyatakan dalam Pemberitahuan Tawaran dan “Ansuran” bermaksud salah satu daripadanya;	
								<br>
							</div>
			            </td>
			        </tr>
			         <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "I DESTINASI SDN BHD"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud I DESTINASI SDN BHD  (No. Syarikat: 66784-V) beralamat di 4 & 6 Pekeliling Business Centre, Persiaran 65C, Jalan Pahang Barat, Off Jalan Pahang, 53000 Kuala Lumpur dan termasuk penerima  pindahan,  penerima  serah  hak  dan pengganti dalam hak milik;
								<br>
							</div>
			            </td>
			        </tr>
			         <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "bulan"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud tempoh yang bermula pada satu hari dalam bulan kalendar dan berakhir pada hari yang sepadan dari segi bilangan dalam bulan kalendar berikutnya; melainkan:<br><br>
			              		<b>(m)</b>jika hari yang sepadan dari segi bilangan itu bukan  Hari  Perniagaan,  maka  tempoh  itu berakhir  pada  Hari  Perniagaan  berikutnya dalam  bulan  kalendar  itu  yang  akan  tamat tempohnya jika ada, atau jika tiada, pada Hari Perniagaan sebelumnya;<br><br>
			              		<b>(n)</b> jika tiada hari yang sepadan dari segi bilangan dalam  bulan  kalendar  itu  yang  akan  tamat tempohnya, maka tempoh itu berakhir pada Hari Perniagaan terakhir dalam bulan kalendar itu; dan<br><br>
			              		<b>(o)</b>jika tempoh itu bermula pada Hari Perniagaan terakhir  dalam  suatu  bulan  kalendar,  maka tempoh  itu  berakhir  pada  Hari  Perniagaan terakhir dalam bulan kalendar berikutnya;<br>
							</div>
			            </td>
			        </tr>
			         <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Keuntungan"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud jumlah yang akan dikira oleh I DESTINASI SDN BHD menurut Fasal 6 dalam dokumen ini;

								<br>
							</div>
			            </td>
			        </tr>
			         <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Pemberitahuan Permohonan"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud arahan yang	anda keluarkan	kepada I DESTINASI SDN BHD	untuk membeli Komoditi dalam borang yang dilampirkan bersama-sama ini;

								<br>
							</div>
			            </td>
			        </tr>
			         <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Pemberitahuan Tawaran"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud pemberitahuan untuk menjual Komoditi kepada anda oleh I DESTINASI SDN BHD borang yang dilampirkan bersama-sama ini;

								<br>
							</div>
			            </td>
			        </tr>
			         <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Pihak"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud pihak dalam Terma dan Syarat ini;
								<br>
							</div>
			            </td>
			        </tr> <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Tarikh Pembelian"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud, berhubung dengan Penjualan Komoditi, tarikh (mesti merupakan Hari Perniagaan) apabila I  DESTINASI	SDN  BHD daripada pembekal seperti yang ditetapkan dalam Pemberitahuan Tawaran;	

								<br>
							</div>
			            </td>
			        </tr>
			        <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Harga Belian"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud, berhubung dengan Pembelian Komoditi, amaun  yang  sama  dengan  Amaun  Kemudahanbseperti  yang  ditetapkan  dalam  Pemberitahuan Permohonan;				

								<br>
							</div>
			            </td>
			        </tr> 
			        <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Kadar Keuntungan"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud kadar seperti yang dinyatakan dalam Bahagian 4 bagi Rumusan;			

								<br>
							</div>
			            </td>
			        </tr> 
			        <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Ringgit Malaysia" atau "RM"

			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud mata wang yang sah pada masa ini di Malaysia; dan

								<br>
							</div>
			            </td>
			        </tr> 
			        <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Terma dan Syarat"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud  terma  dan  syarat  ini  dan  yang ungkapannya  hendaklah  merangkumi  apa-apa pindaan, pengubahsuaian dan perubahan padanya yang dibuat oleh I DESTINASI SDN BHD pada bila- bila masa dan dari semasa ke semasa.<br>
							</div>
			            </td>
			        </tr> 
			        <tr>
			          	<td valign='top'  style="font-size: 7px; width: 110px ">
			                "Tenor"
			            </td>
			            <td>
			            	<div align="justify"  style="font-size: 7px; width: 170px ">
			              		bermaksud tempoh bagi anda membuat pembayaran Harga   Jualan	tertunda	berhubung	dengan Kemudahan  ini  seperti  yang  dinyatakan  dalam Bahagian 2 bagi Rumusan<br>
							</div>
			            </td>
			        </tr> 
			        

			    </table>
			    <table>
			    	<tr>
			    		<td><div align="justify"  style="font-size: 7px; width: 310px "> Terma dan Syarat ini diberikan kepada anda untuk kegunaan khusus dan disediakan dengan persefahaman yang nyata bahawa maklumat yang terkandung di dalamnya dianggap sebagai sulit dan tidak boleh diterbitkan dan/atau didedahkan dalam apa cara sekalipun, melainkan untuk penasihat, ejen dan perunding anda dan hanya untuk tujuan urus niaga yang dibincangkan di sini, melainkan jika dikehendaki sebaliknya oleh undang-undang atau dengan kebenaran bertulis yang diperoleh terlebih dahulu daripada I DESTINASI SDN BHD.<br><br></div></td>
			    	</tr>
			    	<tr>
			    		<td><div align="justify"  style="font-size: 7px; width: 310px ">  Tanpa menyentuh pengeluaran Terma dan Syarat ini oleh I DESTINASI SDN BHD dan/atau persetujuan anda terhadap Terma dan Syarat tersebut, I DESTINASI SDN BHD berhak menarik balik Kemudahan ini tanpa memberikan apa-apa alasan.<br><br></div></td>
			    	</tr>
			    </table>
			</div>
  	<div class="column">
  		<table>
    		<tr>
    			<td colspan="2" class="border header" style="width: 330px">
       				RUMUSAN
    			</td>
  			</tr>	
    	</table>
    	<br>
        <table border="1px" style="border-spacing: 0; border: none;">
			<tr>
			    <td style="border-color:inherit;text-align:center;vertical-align:top; font-size: 8px; width: 60px">Bahagian</td>
			    <td  style="border-color:inherit;text-align:left;vertical-align:top; font-size: 8px; width: 80px"><b>Perkara</b></td>
			    <td style="border-color:inherit;text-align:left;vertical-align:top; font-size: 8px; width: 180px"><b>Butiran</b></td>
			</tr>
			 <tr>
			    <td style="border-color:inherit;text-align:center;vertical-align:top;">1</td>
			    <td style="border-color:inherit;text-align:left;vertical-align:top;">Tujuan</td>
			    <td style="border-color:inherit;text-align:left;vertical-align:top;">Untuk pembelian Komoditi</td>
			</tr>
			 <tr>
			    <td style="border-color:inherit;text-align:center;vertical-align:top;">2</td>
			    <td style="border-color:inherit;text-align:left;vertical-align:top;">Tenor</td>
			    <td style="border-color:inherit;text-align:left;vertical-align:top;">
			     	<span style="text-decoration:underline"> 
			     		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			     	</span>tahun<br><br>
			     	(<span style="text-decoration:underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>bulan)<br><br></td>
			 </tr>
			<tr>
			    <td style="border-color:inherit;text-align:center;vertical-align:top;">3</td>
			    <td style="border-color:inherit;text-align:left;vertical-align:top;">Harga Jualan Tertunda</td>
			    <td style="border-color:inherit;text-align:left;vertical-align:top;">RM <span style="text-decoration:underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br></td>
			</tr>
			<tr>
			    <td style="border-color:inherit;text-align:center;vertical-align:top;">4</td>
			    <td class="tg-yw4l">Kadar Keuntungan</td>
			    <td class="tg-wxgh"><span style="text-decoration:underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>% setahun<br></td>
			</tr>
			<tr>
			    <td style="border-color:inherit;text-align:center;vertical-align:top;">5</td>
			    <td class="tg-yw4l">Keuntungan</td>
			    <td class="tg-yw4l"><span style="text-decoration:underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br></td>
			</tr>
			<tr>
			    <td style="border-color:inherit;text-align:center;vertical-align:top;">6</td>
			    <td class="tg-yw4l">Formula untuk<br>pengiraan Keuntungan<br></td>
			    <td class="tg-yw4l">Kadar Keuntungan didarab dengan Amaun Kemudahan dan<br>didarab dengan tenor</td>
			</tr>
			<tr>
			    <td style="border-color:inherit;text-align:center;vertical-align:top;">7</td>
			    <td class="tg-yw4l">Tempoh Tangguh</td>
			    <td class="tg-yw4l"><span style="text-decoration:underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>bulan.<br></td>
			</tr>
			<tr>
			    <td style="border-color:inherit;text-align:center;vertical-align:top;">8</td>
			    <td class="tg-yw4l">Nama dan butiran<br>syarikat takaful dan<br>jumlah bayaran (jika<br>berkenaan)<br><br><br><br><br>&nbsp;&nbsp;&nbsp;<br> <br></td>
			    <td class="tg-yw4l">Nama:<br><br><span style="text-decoration:underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    </span><br><br><br>Bayaran:<br><br><span style="text-decoration:underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br></td>
			</tr>
			<tr>
			    <td style="border-color:inherit;text-align:center;vertical-align:top;">9</td>
			    <td class="tg-yw4l">Alamat Pihak yang<br>berkenaan<br><br><br><br><br>&nbsp;&nbsp;&nbsp;<br> <br></td>
			    <td class="tg-yw4l"><span style="text-decoration:underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br>I DESTINASI SDN BHD :<br><br><span style="text-decoration:underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br><br><span style="text-decoration:underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br><br>Pelanggan<br><span style="text-decoration:underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br><br><span style="text-decoration:underline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br><br></td>
		</tr>
</table>
  	</div>
  </div>
</table>
</body>
</html>
