<?php
//initilize the page
require_once("asset/inc/init.php");
//require UI configuration (nav, ribbon, etc.)

require_once("asset/inc/config.ui.php");
/*---------------- PHP Custom Scripts ---------
YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */
$page_title = "Pinjaman Peribadi MBSB ";
/* ---------------- END PHP Custom Scripts ------------- */

//include header

//you can add your custom css in $page_css array.

//Note: all css files are inside css/ folder

$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");
?>
<!-- ==========================CONTENT STARTS HERE ========================== -->

    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
    include ("asset/inc/header-home.php");
?>

<div id="main" role="main">
    <br><br><br><br>
      <!-- MAIN CONTENT -->
    <div id="content" class="container">
        <?php if(Session::has('message')): ?>
            <div class="alert adjusted alert-info fade in">
                <button class="close" data-dismiss="alert">
                    ×
                </button>
                <i class="fa-fw fa-lg fa fa-exclamation"></i>
                <strong><?php echo e(Session::get('message')); ?></strong> 
            </div>
        <?php endif; ?>
        <?php if(count($errors) > 0): ?>
            <div class="alert alert-danger">
                <button class="close" data-dismiss="alert">
                    ×
                </button>  
                <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
        <?php endif; ?>

        <div class="row" style="margin-bottom: 30px !important">
            <div class="col-sm-12 col-md-12 col-lg-12 ">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">

                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>

                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>

                </ol>

                <div class="carousel-inner">

                  <div class="item active">

                    <img data-u="image" class="d-block w-200" src="<?php echo e(url('/')); ?>/img/mbsb/3.jpg" width="1200px" height="1200px" alt="First slide"/>

                  </div>

                  <div class="item">

                    <img data-u="image" class="d-block w-200" src="<?php echo e(url('/')); ?>/img/mbsb/1.jpg" width="1200px" height="1200px" alt="Second slide"/>

                  </div>

                  

                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">

                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                  <span class="sr-only">Previous</span>

                </a>

                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">

                  <span class="carousel-control-next-icon" aria-hidden="true"></span>

                  <span class="sr-only">Next</span>

                </a>

              </div>

            </div>

          </div>

        



        <div class="row">

            <div class="col-sm-6 col-md-6 col-lg-6 hidden-xs">

              <h1>Personal Financing-i</h1>

              <p>Learn about our Shariah compliant personal financing solutions through our host of attractive packages that best suit your financing needs.</p>



              <ul>

                <li>Competitive financing rates</li>

                <li>Financing amount of up to RM400,000</li>

                <li>Financing tenure of up to 10 years</li>

                <li>No guarantor required</li>

                <li>No hidden charges</li>

                <li>Fast approval</li>

                <li>Applicable for public servant and private sector employees</li>

              </ul>

            </div>

  <?php if (!(Auth::user())) {?>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

            <div class="well no-padding">



              <!-- <?php echo Form::open(['url' => 'praapplication','class' => 'smart-form client-form', 'id' =>'smart-form-register' ]); ?>


                <header>

                  <p class="txt-color-white"><b>   Semak Kelayakan Pembiayaan / Pinjaman Anda  </b> </p>

                </header>-->



              <!--  <fieldset>
                    <div class="row">
                        <div class="col-xs-12 col-12">
                            <section class="col col-12">
                                <label class="label"> <b> Nama Penuh </b></label>
                                <label class="input">
                                    <i class="icon-append fa fa-user"></i>
                                    <input type="text" id="FullName"  onkeyup="this.value = this.value.toUpperCase()" name="FullName" placeholder="Full Name"    <?php if(Session::has('fullname')): ?>  value="<?php echo e(Session::get('fullname')); ?>" <?php endif; ?> size="60">
                                    <b class="tooltip tooltip-bottom-right">Nama Penuh</b>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-12">
                            <section class="col col-12">
                                <label class="label"> <b> Jenis Pengenalan </b></label>
                                <label class="select">
                                    <select name="type" id="type" class="form-control" onchange="yesno(this);" required="">
                                    <option>--Select--</option>
                                    <?php $__currentLoopData = $idtype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $idtype): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($idtype->idtypecode); ?>"><?php echo e($idtype->idtypedesc); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  </select> <i></i>
                                </label>
                            </section>
                        </div>
                      <div class="col-xs-6 col-12" id="ifNewIc" style="display: none">
                            <section class="col col-12">
                                <label class="label"> <b> No Kad Pengenalan </b></label>
                                <label class="input <?php if(Session::has('icnumber_error')): ?> state-error  <?php endif; ?>">
                                  <i class="icon-append fa fa-user"></i>
                                    <input  type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==12) return false;" id="ICNumber" name="ICNumber" minlength="12" maxlength="12" placeholder="IC Number"  <?php if(Session::has('icnumber')): ?>  value="<?php echo e(Session::get('icnumber')); ?>" <?php endif; ?> >
                                    <b class="tooltip tooltip-bottom-right">No Kad Pengenalan</b>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12" id="ifOther" style="display: none">
                            <section class="col col-12">
                                <label class="label"> <b> No Kad Pengenalan Lain</b></label>
                                <label class="input <?php if(Session::has('icnumber_error')): ?> state-error  <?php endif; ?>">
                                  <i class="icon-append fa fa-user"></i>
                                    <input  type="text" id="others" onkeyup="this.value = this.value.toUpperCase()" name="other"  placeholder="Other"  <?php if(Session::has('other')): ?>  value="<?php echo e(Session::get('other')); ?>" <?php endif; ?> minlength="6" maxlength="12" required="">
                                    <b class="tooltip tooltip-bottom-right">No Kad Pengenalan</b>
                                </label>
                            </section>
                        </div>
                    </div>
                     <div class="row">
                      <div class="col-xs-6 col-12" id="ifDOB" style="display: none">
                        <section class="col col-12">
                                <label class="label"> <b>DOB (dd/mm/yyyy)</b> </label>
                                <label class="input">
                                    <i class="icon-append fa fa-mobile-phone"></i>
                                    <input type="text" data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  maxlength="14" name="dob" value=""  class="form-control startdate" id="dob" required=""  />
                                
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12" id="ifgender" style="display: none">
                            <section class="col col-12">
                                <label class="label"> <b> Jantina </b></label>
                                <label class="select">
                                    <select name="gender" id="gender" class="form-control" required="">
                                        <option>--Select--</option>
                                        <option value="11">Male</option>
                                        <option value="22">Female</option>
                                    </select> <i></i>
                                </label>
                            </section>
                        </div>
                    </div>

                                        <div class="row">

                                         <div class="col-xs-12 col-12">

                                        <section class="col col-12">

                                            <label class="label"> <b> Nombor Telefon Bimbit</b> </label>

                                            <label class="input">

                                                <i class="icon-append fa fa-mobile-phone"></i>

                                                <input type="number" id="PhoneNumber" name="PhoneNumber" placeholder="Handphone Number"  minlength="7" class="fn" step=".01" maxlength="12"  onkeypress="return isNumberKey(event)"  <?php if(Session::has('phone')): ?>  value="<?php echo e(Session::get('phone')); ?>" <?php endif; ?>>

                                                <b class="tooltip tooltip-bottom-right">Nombor Telefon Bimbit </b>

                                            </label>

                                        </section>

                                        </div>
                                      </div>

                                          <div class="row">
                      <div class="col-xs-12 col-12">
                        <section class="col col-12">
                                <label class="label"> <b> Jenis Pekerjaan </b></label>
                                <label class="select">
                                  <select  name="Employment" id="Employment" class="form-control"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                                            <?php $__currentLoopData = $employment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($employment->id); ?>"><?php echo e($employment->name); ?></option>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select> <i></i>
                                    <input type="hidden" name="Employment2" id="Employment2" value="" />
                                 </label>
                             </section>
                         </div>
                    </div></fieldset>
                                <fieldset>

                                    <div class="row">

                                         <div class="col-xs-6 col-12">

                                             <section class="col col-12 col-lg-10">

                                            <label class="label"> <b> Majikan</b></label>

                                                <label class="select" id="majikan">

                                                    <select name="Employer" id="Employer" class="form-control" onchange="document.getElementById('Employer2').value=this.options[this.selectedIndex].text"  >

                                                    <?php if(Session::has('employer')): ?> 

                                                    <option  value="<?php echo e(Session::get('employer')); ?>"><?php echo e(Session::get('employer2')); ?></option>

                                                   
                                                    <?php endif; ?>


                                                    </select> <i></i>

                                                    <input type="hidden" name="Employer2" id="Employer2" value="" />

                                                </label>



                                                 <label id="majikan2" class="input <?php if(Session::has('basicsalary')): ?> state-error <?php endif; ?> ">

                                                <i class="icon-append fa fa-briefcase"></i>

                                                <input type="text" id="majikantext" onkeyup="this.value = this.value.toUpperCase()" name="majikan"  placeholder="Majikan"  <?php if(Session::has('majikan')): ?>  value="<?php echo e(Session::get('majikan')); ?>" <?php endif; ?> >

                                                <b class="tooltip tooltip-bottom-right"> Majikan</b>

                                            </label>



                                            </section>

                                            </div>

                                          <div class="col-xs-6 col-12">

                                                      <section class="col col-12">

                                            <label class="label"> <b> Gaji Asas (RM)</b>  </label>

                                            <label class="input <?php if(Session::has('basicsalary')): ?> state-error <?php endif; ?> ">

                                                <i class="icon-append fa fa-credit-card"></i>

                                                  <input id="BasicSalary" value="0.00" name="BasicSalary" placeholder="Basic Salary (RM)" type="number" required=""   class="fn" step=".01" onkeypress="return isNumberKey(event)" <?php if(Session::has('basicsalary')): ?>  value="<?php echo e(Session::get('basicsalary')); ?>" <?php endif; ?> >

                                                <!--class="hidden" <input type="text" name="BasicSalary" id="BasicSalary" placeholder="Basic Salary (RM)" onkeypress="return isNumberKey(event)" maxlength="7"  <?php if(Session::has('basicsalary')): ?>  value="<?php echo e(Session::get('basicsalary')); ?>" <?php endif; ?> > -->

                                                 <!-- <b class="tooltip tooltip-bottom-right">Gaji Asas (RM) </b>

                                            </label>

                                        </section>

                                            </div>

                                        </div>

                                            <div class="row">

                                                    <div class="col-xs-6 col-12">

                                                       <section class="col col-12">

                                                    <label class="label"> <b> Elaun (RM) </b></label>

                                                    <label class="input">

                                                        <i class="icon-append fa fa-credit-card"></i>

                                                          <input id="Allowance" value="0.00" name="Allowance" placeholder="Allowance (RM)" type="number" required=""   class="fn" step=".01" onkeypress="return isNumberKey(event)" <?php if(Session::has('allowance')): ?>  value="<?php echo e(Session::get('allowance')); ?>" <?php endif; ?> >

                                                        <b class="tooltip tooltip-bottom-right">Elaun (RM)</b>

                                                    </label>

                                                </section>

                                      

                                        </div>

                                                 <div class="col-xs-6 col-12">

                                                          <section class="col col-12">

                                                    <label class="label"><b> Potongan Bulanan (RM) </b> </label>

                                                    <label class="input">

                                                        <i class="icon-append fa fa-credit-card"></i>

                                                        <input id="Deduction" value="0.00" name="Deduction" placeholder="Existing Total Deduction (RM)" type="number" required=""   class="fn" step=".01" onkeypress="return isNumberKey(event)" <?php if(Session::has('deduction')): ?>  value="<?php echo e(Session::get('deduction')); ?>" <?php endif; ?> >

                                                        <b class="tooltip tooltip-bottom-right"> Jumlah Potongan Bulanan Semasa (RM)  </b>

                                                    </label>

                                                </section>

                                                </div>

                                              

                                                

                                            </div>

                                </fieldset>

                                <fieldset>

                                    <div class="row">

                                         <div class="col-xs-6 col-12">

                                        <section class="col col-12 col-lg-10 col-md-10">

                                            <label class="label"> <b> Pakej</b><div class="visible-xs"><br></div> </label>

                                               <label class="select" style="width: 206px !important">

                                                   <input type="text" name="Package" id="Package" <?php if(Session::has('package_name')): ?>  value="<?php echo e(Session::get('package_name')); ?>" <?php else: ?> value="Mumtaz-i" <?php endif; ?> class="form-control" readonly>



                                                </label>

                                           </section>

                                            </div>

                                        <div class="col-xs-6 col-12">

                                        <section class="col col-12">

                                            <label class="label"> <b> Jumlah Pembiayaan(RM) </b></label>

                                            <label class="input">

                                                <i class="icon-append fa fa-credit-card"></i>

                                              <input id="LoanAmount" name="LoanAmount" placeholder="Loan Amount (RM)" type="number" required=""   class="fn" step=".01" onkeypress="return isNumberKey(event)" <?php if(Session::has('loanAmount')): ?>  value="<?php echo e(Session::get('loanAmount')); ?>" <?php endif; ?> >


                                                <b class="tooltip tooltip-bottom-right">Jumlah Pembiayaan (RM)</b>

                                            </label>

                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                                        </section>

                                        </div>

                                        </div>





                                </fieldset>



                                <footer>

                                    <button type="submit" class="btn btn-primary">

                                     <b>    Kira Kelayakan  </b>

                                    </button>

                                    <div id="response"></div>

                                </footer>-->



               <!--   <div class="message">

                  <i class="fa fa-check"></i>

                  <p>

                    Thank you for your registration!

                  </p>

                </div>

              </form>-->
<?php } ?>
            

                                   

        </div>

      </div>



    </div></div>



            

        

       <div class="page-footer">

            <div class="row">

             



                <div class="col-xs-12 col-sm-12 text-left ">

                    <div class="txt-color-black inline-block">

                        <span class="txt-color-black">NetXpert Sdn Bhd  © All rights reserved   </span>

                        

                    </div>

                </div>

            </div>

        </div>

  



    

        



<!-- ==========================CONTENT ENDS HERE ========================== -->

<script type="text/javascript">



    $(document).ready(function() {

        

          $("#smart-form-register2").hide();

        $("#smart-form-register").validate({



          // Rules for form validation

          rules : {

               FullName: {
              required : true,
              maxlength:100
            },
            ICNumber : {
              required : true,
              maxlength:12,
              minlength:12
            },
            
            PhoneNumber: {
                required: true,
                 maxlength:16,
                minlength:9

            },

            Deduction: {

                required: true

            },

            

            Allowance: {

                required: true

            },

            Package: {

                required: true

            },

            Employment: {

                required: true

            },

            Employer: {

                required: true,
                 maxlength:20

            },

            majikan: {

              maxlength: 60

            },

            BasicSalary: {

                required: true

                            

            },

            LoanAmount: {

                required: true

            }

          },



          // Messages for form validation

          messages : {



              FullName: {

              required : 'Please enter your full name'

            },

            

            ICNumber: {

                required: 'Please enter your ic number'

            },

            PhoneNumber: {

                required: 'Please enter your phone number'

            },

            Allowance: {

                required: 'Please enter  yor allowance'

            },

            Deduction: {

                required: 'Please enter your total deduction'

            },

            Package: {

                required: 'Please select package'

            },

            Employment: {

                required: 'Please select employement type'

            },



            Employer: {

                required: 'Please select employer'

            },





            BasicSalary: {

                required: 'Please enter your basic salary'

            },

            LoanAmount: {

                required: 'Please select your loan amount'

            }

          }

        });



      });

    </script>

<script type="text/javascript">
    
    $("#FullNames").keypress(function(e) {
       if (e.which === 32 && !this.value.length) {
           e.preventDefault();
       }
       var inputValue = event.charCode;
       if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0) && (inputValue != 191)){
           event.preventDefault();
       }          
   });
     $("#name_card").keypress(function(e) {
       if (e.which === 32 && !this.value.length) {
           e.preventDefault();
       }
       var inputValue = event.charCode;
       if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0) && (inputValue != 191)){
           event.preventDefault();
       }          
   });
     $("#mother_name").keypress(function(e) {
       if (e.which === 32 && !this.value.length) {
           e.preventDefault();
       }
       var inputValue = event.charCode;
       if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0) && (inputValue != 191)){
           event.preventDefault();
       }          
   });
</script>
<!--only 1 space-->
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
  
  var input = document.getElementById('FullNamse');
  input.addEventListener('keydown', function(e){      
       var input = e.target;
       var val = input.value;
       var end = input.selectionEnd;
       if(e.keyCode == 32 && (val[end - 1] == " " || val[end] == " ")) {
         e.preventDefault();
         return false;
      }      
    });
   var input = document.getElementById('customer_name');
  input.addEventListener('keydown', function(e){      
       var input = e.target;
       var val = input.value;
       var end = input.selectionEnd;
       if(e.keyCode == 32 && (val[end - 1] == " " || val[end] == " ")) {
         e.preventDefault();
         return false;
      }      
    });
   var input = document.getElementById('mother_name');
  input.addEventListener('keydown', function(e){      
       var input = e.target;
       var val = input.value;
       var end = input.selectionEnd;
       if(e.keyCode == 32 && (val[end - 1] == " " || val[end] == " ")) {
         e.preventDefault();
         return false;
      }      
    });
});
</script>

<script type="text/javascript">

   

    // Validation

    $(function() {

         runAllForms();

        $("#smart-form-register3").validate({



            // Rules for form validation

           rules : {

                

                Email2 : {

                    required : true,

                    email : true

                },

                BasicSalary : {

                    required : true,

                    min :2000



                    

                },

                Password : {

                    required : true,

                    minlength : 3,

                    maxlength : 20

                },

                ICNumber : {

                    required : true,

                    minlength : 12,

                    maxlength : 13

                },

                PasswordConfirmation : {

                    required : true,

                    minlength : 3,

                    maxlength : 20,

                    equalTo : '#Password'

                }

            },



            // Messages for form validation

             messages : {



                    Email2 : {

                    required : 'Please enter your email address',

                    email : 'Please enter a VALID email address'

                },

                BasicSalary : {

                    required : 'Please enter your email address'

                },

                Password : {

                    required : 'Please enter your password'

                },

                PasswordConfirmation : {

                    required : 'Please enter your password one more time',

                    equalTo : 'Please enter the same password as above'

                }

                    }

        });



    });

</script>

<?php 

  //include required scripts

  include("asset/inc/scripts.php"); 

?>



<!-- PAGE RELATED PLUGIN(S) 

<script src="..."></script>-->

 <!-- 
<script type="text/javascript">
    document.getElementById("BasicSalary_x").onblur =function (){    
    this.value = parseFloat(this.value.replace(/,/g, ""))
                    .toFixed(2)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
    document.getElementById("display_BasicSalary_x").value = this.value.replace(/,/g, "");   
    
}
  
</script>

<script type="text/javascript">
    document.getElementById("LoanAmount_x").onblur =function (){    
      this.value = parseFloat(this.value.replace(/,/g, ""))
                      .toFixed(2)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ","); 

    document.getElementById("display_LoanAmount_x").value = this.value.replace(/,/g, "");  
  }
</script>

<script type="text/javascript">
    document.getElementById("Allowance_x").onblur =function (){    
      this.value = parseFloat(this.value.replace(/,/g, ""))
                      .toFixed(2)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");   
  
  document.getElementById("display_Allowance_x").value = this.value.replace(/,/g, ""); 

  }

</script>

<script type="text/javascript">
    document.getElementById("Deduction_x").onblur =function (){    
    this.value = parseFloat(this.value.replace(/,/g, ""))
                    .toFixed(2)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    majikantext
    document.getElementById("display_Deduction_x").value = this.value.replace(/,/g, "");   
    
}
  
</script> -->

<!-- <script>
  form.onSubmit = function(){
     form.action = form.action.replace(",", "");
     return true;
  }
</script>
-->




<script type="text/javascript">



 <?php if(Session::has('employer')): ?> 

 

   $("#majikan2").hide();

    $("#majikan").show();

  <?php else: ?>

   $("#majikan").hide();

   $("#majikan2").show();

  <?php endif; ?>



$( "#Employment" ).change(function() {

    var Employment = $('#Employment').val();



    if( Employment == '1') {



      $("#Employer").html(" ");

      $("#Employer2").val(" ");

        $("#majikan2").show();

        $("#majikantext").focus();

         $("#majikan").hide();

       $("#Package").val("Mumtaz-i");

    }

      else if( Employment == '2') {

   

     $("#Employer").html(" ");

     $("#Employer2").val(" ");

        $("#majikan2").show();

        $("#majikantext").focus();

         $("#majikan").hide();

         $("#Package").val("Afdhal-i");

    }



    else if( Employment == '3') {

   

     $("#Employer").html(" ");

     $("#Employer2").val(" ");

        $("#majikan2").show();

        $("#majikantext").focus();

         $("#majikan").hide();

         $("#Package").val("Afdhal-i");

    }

      else if( Employment == '4') {

   

     $("#Employer").html(" ");

     $("#Employer2").val(" ");

        $("#majikan2").show();

        $("#majikantext").focus();

         $("#majikan").hide();

         $("#Package").val("Private Sector PF-i");

    }

    else if( Employment == '5') {

         

         $("#Employer").html(" ");

     $("#Employer2").val(" ");

        $("#majikan2").show();

        $("#majikantext").focus();

         $("#majikan").hide();



          $("#Package").val("Afdhal-i");

            

        //  $("#majikan").simulate('click');

         





    }

  $.ajax({

                url: "<?php  print url('/'); ?>/employer/"+Employment,

                dataType: 'json',

                data: {

                   

                },

                success: function (data, status) {



                    jQuery.each(data, function (k) {



                        $("#Employer").append("<option value='" + data[k].id + "'>" + data[k].name + "</option>");

                    });



                }

            });



   

});

</script>


<script type="text/javascript">
  $( "#BasicSalary" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(2);
});
</script>
<script type="text/javascript">
  $( "#Allowance" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(2);
});
</script>
<script type="text/javascript">
  $( "#Deduction" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(2);
});
</script>
<script type="text/javascript">
  $( "#LoanAmount" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(2);
});
</script>
<script>
function isNumberKey(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          if (key.length == 0) return;
          var regex = /^[0-9.,\b]+$/;
          if (!regex.test(key)) {
              theEvent.returnValue = false;
              if (theEvent.preventDefault) theEvent.preventDefault();
          }


}

 
function minmax(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 2; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}
</script>



<script type="text/javascript">

    

     $(document).ready(function() {



   var found = [];

$("#Employment option").each(function() {

  if($.inArray(this.value, found) != -1) $(this).remove();

  found.push(this.value);

});

     });







</script>



<script type="text/javascript">



function getSelectedText(elementId) {

    var elt = document.getElementById(elementId);



    if (elt.selectedIndex == -1)

        return null;



    return elt.options[elt.selectedIndex].text;

}





         var Employment = getSelectedText('Employment');

         var Employer = getSelectedText('Employer');

          $("#Employment2").val(Employment);

           $("#Employer2").val(Employer); 



</script>

<!--Add the following script at the bottom of the web page (before </body></html>)

<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=21343764"></script>
-->


<script language="javascript">

    function getkey(e)

    {

        if (window.event)

            return window.event.keyCode;

        else if (e)

            return e.which;

        else

            return null;

    }

    function goodchars(e, goods, field)

    {

        var key, keychar;

        key = getkey(e);

        if (key == null) return true;

 

        keychar = String.fromCharCode(key);

        keychar = keychar.toLowerCase();

        goods = goods.toLowerCase();

 

        // check goodkeys

        if (goods.indexOf(keychar) != -1)

            return true;

        // control keys

        if ( key==null || key==0 || key==8 || key==9 || key==27 )

            return true;

    

        if (key == 13) 

            {

                var i;

                for (i = 0; i < field.form.elements.length; i++)

                if (field == field.form.elements[i])

                break;

                i = (i + 1) % field.form.elements.length;

                field.form.elements[i].focus();

                return false;

            };

        // else return false

            return false;

    }

</script>



<script type="text/javascript">

    $(document).ready(function(){

    $("#inputTextBox").keypress(function(event){

        var inputValue = event.charCode;

        if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0 ) && (inputValue != 44 && inputValue != 47 )){

            event.preventDefault();

        }

    });

});

</script>

<script type="text/javascript">
    $(document).ready(function(){
    $("#FullName").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46 ) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });
});
</script>
<script>
    function yesno(that) {
        if (that.value == "IP") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        }
        else if (that.value == "IO") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        } 
        else if (that.value == "PN") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        } 
        else if (that.value == "AN") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        } 
        else if (that.value == "PP") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        } 
        else if (that.value == "IN"){
          document.getElementById("ifOther").style.display = "none";
            $('.other').hide().find(':input').attr('required', false);

            document.getElementById("ifDOB").style.display = "none";
            $('.dob').hide().find(':input').attr('required', false);

             document.getElementById("ifgender").style.display = "none";
            $('.gender').hide().find(':input').attr('required', false);

            document.getElementById("ifNewIc").style.display = "block";
            $('.ICNumber').hide().find(':input').attr('required', true);
        }
        else{
          document.getElementById("ifOther").style.display = "none";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "none";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "none";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        }
    }
</script>
<script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
            
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();


                $('.startdate').datepicker({
                dateFormat : 'dd/mm/yy',
                 changeYear: true,
                 changeMonth: true,
                 yearRange: '1950:2017',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });

                 $('.date').datepicker({
                dateFormat : 'dd/mm/yy',
                 changeYear: true,
                 changeMonth: true,
                 yearRange: '1950:2017',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });


        </script>