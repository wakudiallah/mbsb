<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Submitted Report";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  	<?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   	<?php if(Session::has('error')): ?>
	   		<div class="alert adjusted alert-danger fade in">
		        <button class="close" data-dismiss="alert">
		            ×
		        </button>
        		<i class="fa-fw fa-lg fa fa-exclamation"></i>
          		<strong><?php echo e(Session::get('error')); ?></strong> 
        	</div>
        <?php elseif(Session::has('success')): ?>
           <div class="alert adjusted alert-success fade in">
		        <button class="close" data-dismiss="alert">
		            ×
		        </button>
         		<i class="fa-fw fa-lg fa fa-exclamation"></i>
          		<strong><?php echo e(Session::get('success')); ?></strong> 
        	</div>
      	<?php endif; ?>
      	
      	<article class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
      		<div class="jarviswidget well" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>Widget Title </h2>  
                </header>

            <div>
            	<div class="jarviswidget-editbox"></div>
            		<div class="widget-body no-padding">
            			 <?php echo Form::open(['url' => 'clrt/new_loan_offer_view','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]); ?>

                        <fieldset>
    						<section>
                                <label class="label">Select Task</label>
                                <label class="select">
                                    <i class="icon-append"></i>
                                    <select name='task'>
										<option value="-99"> All </option>
                                        <?php $__currentLoopData = $jobrun; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jobrun): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($jobrun->id); ?>"><?php echo e($jobrun->JobDesc); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</select>
                                    <b class="tooltip tooltip-bottom-right">Select Task</b>
                                </label>
                            </section>
                            <section>
                                <label class="label">Job Sector</label>
                                <label class="select">
                                    <i class="icon-append"></i>
                                    <select name='jobsector'>
                                        <option value="-99"> All </option>
                                        <option value="P"> Private Sector </option>
                                        <option value="G"> Government </option>
                                    </select>
                                    <b class="tooltip tooltip-bottom-right">Job Sector</b>
                                </label>
                            </section>
                             <section>
                                <label class="label">Select State</label>
                                <label class="select">
                                    <i class="icon-append"></i>
                                    <select name='state'>
                                        <option value="-99"> Select </option>
                                        <?php $__currentLoopData = $state; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($state->clrt_name); ?>"><?php echo e($state->clrt_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <b class="tooltip tooltip-bottom-right">Select Task</b>
                                </label>
                            </section>
                            <section>
                                <label class="label">NettProceed</label>
                                <label class="input">
                                    <i class="icon-append"></i>
                                    <input type="text" name="net1" placeholder="From"  class="form-control"  required>
                                    <b class="tooltip tooltip-bottom-right">NettProceed</b>
                                </label>
                            </section>
                            <section>
                                <label class="label">To</label>
                                <label class="input">
                                    <i class="icon-append"></i>
                                    <input type="text" name="net2" placeholder="To"  class="form-control"  required>
                                    <b class="tooltip tooltip-bottom-right">To</b>
                                </label>
                            </section>
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        </fieldset>
                        <div class="modal-footer">
                         	<button type="submit" name="submit" class="btn btn-primary">
                         		&nbsp; Generate &nbsp;
                         	</button>
                         <?php echo Form::close(); ?><br><br><br>
                     	</div>
                 	</div>
             	</div>
         	</div>
     	</article>

     	<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
     		<div align='center'><b>List of Anti Attrition - State : <?php echo e($name_state); ?>

     			<!--<button type="submit" name="submit" class="btn btn-primary">
     				&nbsp; &nbsp; Export Excel &nbsp; &nbsp;
     			</button>-->
     			<br>
     			<div class="jarviswidget well" id="wid-id-0">
     				<header>
     				 	<span class="widget-icon"> <i class="fa fa-comments"></i> </span>
     				 	<h2>Widget Title </h2>   
     				 </header>
     				<div>
     				 	<div class="jarviswidget-editbox"></div>
     				 	<div class="widget-body no-padding">
     				 		<table id="example" class="table table-striped table-bordered table-hover" width="100%">
								<thead>			                
									<tr>
										<th>No.</th>
										<th>Name</th>
                                        <th>Max Loan</th>
                                        <th>Net Proceed</th>
										<th>Installment</th>
										<th>Tenor</th>
                                        <th>DSR %</th>
										<th>Increment</th>
                                        <th>Sallary</th>
                                        <th>Max Deduction</th>
                                        <th>Interest</th>
                                        <th>Variance Rate</th>
                                        <th>Profit Earn</th>
                                        <th>Payout Percent</th>
                                        <th>Detail</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1;?>
									<?php $__currentLoopData = $newloanoffer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>		  
									<tr>
										<td><?php echo e($i); ?></td>
										<td><?php echo e($new->CustName); ?></td>
										<td><?php echo e($new->NewLoanOffer); ?></td>
										<td><?php echo e($new->NetProceed); ?></td>
										<td><?php echo e($new->MonthlyInst); ?></td>
										<td><?php echo e($new->Tenor); ?></td>
										<td><?php echo e($new->NewDSR); ?></td>
										<td><?php echo e($new->NewLoanOffer); ?></td>
										<td><?php echo e($new->NewSalary); ?></td>
										<td><?php echo e($new->NewLoanOffer); ?></td>
										<td><?php echo e($new->IntRate); ?></td>
										<td><?php echo e($new->VarianceRate); ?></td>
										<td><?php echo e($new->ProfitToEarn); ?></td>
										<td><?php echo e($new->NewLoanOffer); ?></td>
										<td><a href="<?php echo e(url('/clrt/loan-offer-detail/'.$new->ACID)); ?>" class='btn btn-sm btn-success'>Detail</a></td>
									</tr>
									<?php $i++; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</tbody>
							</table>
                                       
													
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		pageSetUp();
	
		/* BASIC ;*/
			var responsiveHelper_dt_basic = undefined;
			var responsiveHelper_datatable_fixed_column = undefined;
			var responsiveHelper_datatable_col_reorder = undefined;
			var responsiveHelper_datatable_tabletools = undefined;
			
			var breakpointDefinition = {
				tablet : 1024,
				phone : 480
			};
	})
</script>
							</div><br><br><br><br>
						</div>
					</div>
				</article>
			</div>
		</div>
<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>
<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
	var responsiveHelper_dt_basic = undefined;
	var responsiveHelper_datatable_fixed_column = undefined;
	var responsiveHelper_datatable_col_reorder = undefined;
	var responsiveHelper_datatable_tabletools = undefined;
	
	var breakpointDefinition = {
		tablet : 1024,
		phone : 480
	};
	$('#dt_basic').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
			"t"+
			"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_dt_basic) {
				responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_dt_basic.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_dt_basic.respond();
		}
	});
	
	$(document).ready(function() {
		var t = $('#example').DataTable( {
			"columnDefs": [ {
				"searchable": false,
				"orderable": false,
				"targets": 0
			} ],
			"order": [[ 1, 'asc' ]]
		} );
	 
		t.on( 'order.dt search.dt', function () {
			t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				cell.innerHTML = i+1;
			} );
		} ).draw();
	} );		
</script>
<script>
	  $(document).ready(function() {
	    // show the alert
	    window.setTimeout(function() {
	    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
	        $(this).remove(); 
	    });
	}, 2800);
	});
</script>
<script type="text/javascript">
    var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
        _gaq.push(['_trackPageview']);
    
    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();


        $('.startdate').datepicker({
        dateFormat : 'yy-mm-dd',
        prevText : '<i class="fa fa-chevron-left"></i>',
        nextText : '<i class="fa fa-chevron-right"></i>',
        onSelect : function(selectedDate) {
            $('#finishdate').datepicker('option', 'minDate', selectedDate);
        }
    });
</script>




          
