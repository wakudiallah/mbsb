<p>Dear <?php echo e($branchname); ?> Branch, </p>
<p>A New customer has submit a loan application and have been approved by Ezlestari Administrator.
The application is now ready to start the verification process.</p>
<p>Short detail of customer :</p>
<p> Name : <?php echo e($cus_name); ?></p>
<p> IC No. : <?php echo e($cus_ic); ?> </p>
<p>Please process this application request as soon as possible.</p>
<br>
<p>Thank You,</p>
<br>
<p>Ezlestari.com.my</p>
