<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Administrator [Second Level Verification]";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   	<?php if(Session::has('error')): ?>
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('error')); ?></strong> 
        </div>
        <?php elseif(Session::has('success')): ?>
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('success')); ?></strong> 
        </div>
            
      <?php endif; ?>

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            
                         

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                                <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                    
                                    data-widget-colorbutton="false" 
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true" 
                                    data-widget-sortable="false"
                                    
                                -->
                                
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
										<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
											<thead>			                
												<tr>
													<th>No.</th>
                                                    <th>IC Number</th>
                                                    <th>Name</th>
													<th>Phone</th>                                                
                                                    <th>Submit Date</th>
                                                    <th>Status</th>                                           
                                                    <th>Branch Status</th>
                                                    <th>Last Activity</th>
                                                    <th>Action</th>
                                                     <th>Download</th>
                                                      

												</tr>
											</thead>
											<tbody>
												<?php $i=1; ?>
                                                 <?php $__currentLoopData = $terma; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $term): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                  
                                              <tr>
                                              
                                                <td>
                                                   <?php echo e($i); ?>                              
                                                </td>
                                                  
                                                  <td>
                                              <?php echo e($term->Basic->new_ic); ?>

                                              <input type='hidden' id='ic99<?php echo e($i); ?>' name='ic' value='<?php echo e($term->Basic->new_ic); ?>'/>
                                                <div id='block<?php echo e($i); ?>'></div>
                                                <script type="text/javascript">

                                                    $(document).ready(function() {
                                                    
                                                    var old_icx = $('#ic99<?php echo e($i); ?>').val();
                                                    
                                                       
                                                    if(old_icx == '661013075335' | old_icx =='690719085131'  | old_icx =='720609085274' | old_icx== '55060207511'  | old_icx== '560703085625' | old_icx== '711004085963' | old_icx== '761123086729'| old_icx== '820508105756'| old_icx== '590717105964'| old_icx== '760127086668'| old_icx== '781007055405'| old_icx== '590102055532' | old_icx== '800514035960' | old_icx== '790929085867')
                                                    {
                                                    $('#block<?php echo e($i); ?>').html("<span class='label label-danger'>IC Blocked </span>");
                                                    
                                                    }
                                                    });
                                                  </script>

                                                  
                                                  
                                                  </td>
                                                <td><?php echo e($term->Basic->name); ?></td>
												<td><?php echo e($term->PraApplication->phone); ?></td>
                                                <td><?php echo e($term->file_created); ?></td>
                                                <td><div align='center'>
                                            <?php if($term->referral_id==0): ?>
                                                <?php if($term->verification_result ==0): ?>
                                                       
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-warning'>Pending Verification</span>
                                                      
                                                    <?php elseif($term->verification_result ==1): ?>                                                
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-success'>Ready for 2nd Verification</span>
                                                    <?php elseif($term->verification_result ==2): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-primary'>Routed to Branch</span>
                                                   <br>
                                                         <?php if($term->id_branch=='0'): ?>
                                                         <div align='center'> - </div> 
                                                    <?php else: ?>                                            
                                                        <i><?php echo e($term->Branch->branchname); ?></i>
                                                    <?php endif; ?> 
                                                
                                                    <?php elseif($term->verification_result ==3): ?>
                                                      <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-danger'>Rejected</span>
                                                    <?php elseif($term->verification_result ==4): ?>
                                                      <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-default'>Waiting Document</span>
                                               
                                                    <?php endif; ?>

                                              <?php else: ?>
                                              <?php if($term->status==0): ?>
                                                       <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-default'>Check DSR</span>
                                                <?php elseif($term->status==88): ?>
                                                <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-danger'>Rejected</span>
                                                 <?php elseif($term->status==1): ?>
                                                <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-danger'>Complete the form</span>
                                                <?php else: ?> 

                          <?php if($term->verification_result ==0): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-primary'>Submitted to Processor / WAPS</span>
                                                    <?php elseif($term->verification_result ==1): ?>                                                
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-success'>Submitted to WAPS</span>
                                                    <?php elseif($term->verification_result ==2): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-primary'>Routed to Branch / Submitted to WAPS</span>
                            <br>
                            <?php if($term->id_branch=='0'): ?>
                                                         <div align='center'> - </div> 
                            <?php else: ?>                                            
                                                        <i><?php echo e($term->Branch->branchname); ?></i>
                            <?php endif; ?> 
                                                
                                                    <?php elseif($term->verification_result ==3): ?>
                                                      <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-danger'>Rejected</span>
                                                 
                          <?php elseif($term->verification_result ==4): ?>
                                                      <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-default'>Route Back to User</span>
                                                 
                                                    <?php endif; ?>
                                                <?php endif; ?>
                          
            <?php endif; ?>

                                                       </div>
                                                </td>
                                         
                                                <td><div align='center'>
                                                  <?php if($term->verification_result_by_bank ==0 AND $term->verification_result ==2): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-default'>In Process</span>
                                                  <?php elseif($term->verification_result_by_bank ==0 AND $term->verification_result ==0): ?> 
                                                    -
                                                  <?php elseif($term->verification_result_by_bank ==0 AND $term->verification_result ==1): ?>
                                                     -
                                                  <?php elseif($term->verification_result_by_bank ==0 AND $term->verification_result ==3): ?>
                                                     -
                                                  <?php elseif($term->verification_result_by_bank ==0 AND $term->verification_result ==4): ?>
                                                     -
                                                  <?php endif; ?>
                                                  
                                                    <?php if($term->verification_result_by_bank ==1): ?>
                                                       <span data-toggle="tooltip" title="<?php echo e($term->remark_bank); ?>"  class='label label-success'>Approved</span>
                                                    <?php elseif($term->verification_result_by_bank ==2): ?>
                                                      <span data-toggle="tooltip" title="<?php echo e($term->remark_bank); ?>"  class='label label-danger'>Rejected</span>
                                                     <?php elseif($term->verification_result_by_bank ==3): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->remark_bank); ?>"  class='label label-warning'>Pending Approval</span>
													<?php endif; ?>
                                                    
                                                    </div>  
                                                </td>
                                               
                                                  <td>
                                                  <?php if(!empty($term->Log_download->first()->id)) {
                                                  $max = 0;
                                                  $k=0;
												
                                                      foreach ($term->Log_download as $log) {
                                                      
                                                              
                                                                  if($max < $log->id ) {
                                                                  $max = $log->id ;
                                                                  $tanggal = $log->downloaded_at ;
                                                                  $user_download =$log->user->name;
                                                                // echo $user_download; //echo "<br>";
                                                                  }
                                                            $k++;
                                                          
                                                    
                        
                                                       }
                                                       if ($k<>'0') {
                                                        echo "<a href='#' data-toggle='modal' data-target='#myModal".$i."'>".$tanggal." by ".$user_download."</a>";
                                                       
                                                       }
                                                       else {
                                                         echo "-";
                                                       }
													
													}
													else {
													  echo "-";
													}
													
													?>
													
                                                  
                                                  
                                                  
                                                  </td>
                                                <td align='center'>
													 <a href="<?php echo e(url('/admin/user_detail/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default'>View Application</a>
                                                    <?php if(($term->verification_status=='0') AND ($term->verification_result=='0')): ?> 
                                                        <a href="<?php echo e(url('/admin/user_detail/'.$term->id_praapplication.'/verify')); ?>" class='btn btn-sm btn-success'>Verified Now</a>
                                                    <?php elseif(($term->verification_status=='1') AND ($term->verification_result=='1')): ?>                                               
                                                        <a href="<?php echo e(url('/admin/user_detail/'.$term->id_praapplication.'/verify')); ?>" class='btn btn-sm btn-success'>Verified Now</a>
                           
                                                    <?php endif; ?> 


                                                      
                                                      <?php if($term->edit=='1'): ?>

                                                  <a href="<?php echo e(url('/')); ?>/form/approveedit/<?php echo e($term->id_praapplication); ?>" 
                                                  onclick="return confirm('Are you sure ?')"  class='btn btn-sm btn-warning'>Approve RB</a>
                                                         
                                                        <?php endif; ?> 

                                                         <?php if($term->status=='1'): ?> 
															<?php if($term->verification_result !=3): ?> <!--selain '3' atau selain status 'rejected' dari admin ezlestari-->
																<?php if($term->verification_result_by_bank ==0): ?> <!--belum diverifikasi oleh bank-->
                                                        <a href='#' data-toggle='modal' class='btn btn-primary' data-target='#amyModal<?php echo e($i); ?>'>Route Back</a>
												  <!-- Modal -->
													<div class="modal fade" id="amyModal<?php echo e($i); ?>" tabindex="-1" role="dialog" aria-labelledby="amyModal<?php echo e($i); ?>" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		&times;
																	</button>
																	<h4 class="modal-title" id="addBranch">Route Back Application to User</h4>
																</div>
																<div class="modal-body">
																 <?php echo Form::open(['url' => 'form/routeback','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]); ?>

																  <fieldset>
																			<section >
																				  <label class="label">Customer Name</label>
																					<label class="input">
																					<input type='text' value='<?php echo e($term->Basic->name); ?>' readonly disabled/>
																					<b class="tooltip tooltip-bottom-right">Customer Name</b>
																					</label><br>
																			</section>
																			<section >
																				  <label class="label">IC Number</label>
																					<label class="input">
																						<input type='text' value='<?php echo e($term->Basic->new_ic); ?>' readonly disabled/>
																					<b class="tooltip tooltip-bottom-right">IC Number</b>
																					</label><br>
																			</section>
																			<section >
																				  <label class="label">Reason</label>
																					<label class="input">
																						<textarea id='reason' name='reason' class='form-control' rows="4" cols="73"></textarea>
																					<b class="tooltip tooltip-bottom-right">Reason</b>
																					</label><br>
																			</section>
																
																		 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
																		  <input type="hidden" name="id_praapplication" value="<?php echo e($term->id_praapplication); ?>">
																		  <input type="hidden" name="cus_ic" value="<?php echo e($term->Basic->new_ic); ?>">
																		  <input type="hidden" name="cus_name" value="<?php echo e($term->Basic->name); ?>">
																		  <input type="hidden" name="cus_email" value="<?php echo e($term->PraApplication->email); ?>">
																	 </fieldset>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">
																		Cancel
																	</button>
																	<button type="submit" name="submit" class="btn btn-lg txt-color-darken">
																				   Submit
																				</button>
																			   
																	   <?php echo Form::close(); ?>   
																</div>
															</div><!-- /.modal-content -->
														</div><!-- /.modal-dialog -->
													</div><!-- /.modal -->
															
																<?php endif; ?> 
															<?php endif; ?> 
                                                        <?php endif; ?>   
                                                </td>
                                              
                                                <td> <a href="<?php echo e(url('/')); ?>/admin/downloadzip/<?php echo e($term->id_praapplication); ?>"
                                                ><img width='24' height='24' src="<?php echo e(url('/')); ?>/asset/img/zip.png"></img></a>&nbsp;&nbsp; 
                                                <?php if(($term->status=='77') OR (($term->status=='1') AND ($term->verification_status=='1'))): ?>

                                                  <a href="<?php echo e(url('/')); ?>/admin/downloadpdf/<?php echo e($term->id_praapplication); ?>"
                                                ><img width='24' height='24' src="<?php echo e(url('/')); ?>/asset/img/pdf.png"></img></a> 
                                              <?php endif; ?></td>
                                            </tr>
                                              <?php
                                              $i++; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</tbody>
										</table>
                                          
                                          <br>
                                            <?php $j=1; foreach($terma as $termxz) { 

                                                  if(!empty($termxz->Log_download->first()->id)) { ?>
                                                    <!-- Modal -->
													<div class="modal fade" id="myModal<?php echo e($j); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		&times;
																	</button>
																	<h4 class="modal-title" id="myModalLabel"><b>Log Download : <?php echo e($termxz->Basic->name); ?> - <?php echo e($termxz->Basic->new_ic); ?> of <?php if($termxz->id_branch=='0'): ?>
                                                         - 
                                                    <?php else: ?>                                            
                                                        <b><?php echo e($termxz->Branch->branchname); ?></b>
                                                    <?php endif; ?>  Branch</b></h4>
																
                                                                
                                                                </div>
																<div class="modal-body">												
																	  			<table id="datatable_col_reorder<?php echo e($j); ?>" class='table table-striped table-bordered table-hover'>
																		<thead>
																		  <tr>
																			<th>Date</th>
																		  <th>User</th>
                                                                            <th>Activity</th>
                                                                              <th>Remark</th>
																			  <th>Reason</th>
																		  </tr>
																		
																		</thead>
																		  	<tbody>
																		  <?php if(!empty($termxz->Log_download->first()->id)): ?>
												
																			<?php $__currentLoopData = $termxz->Log_download; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $logfull): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		
											
																					
																						  <tr>
 																							<td> <?php echo e($logfull->downloaded_at); ?></td>
																							<td> <?php echo e($logfull->user->name); ?></td>
                                                                                              <td> <?php echo e($logfull->activity); ?></td>
                                                                                                <td> <?php echo e($logfull->log_remark); ?></td>
																						  	<?php if($logfull->log_reason==""): ?>
																								<td> &nbsp; </td>
																								<?php else: ?>
																								<td> <?php echo e($logfull->log_reason); ?></td>
																								<?php endif; ?>
																						  </tr>
																						
																						
																			
																			 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																		</tbody>
																			
																		  <?php else: ?> 
																			<td colspan='2'>No Data</td>
																		  <?php endif; ?>
																		  
																							  
																	</table>							   																   													
																  </div>
																  <div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">
																		Close
																	</button>
																																	   
																</div>
															</div><!-- /.modal-content -->
														</div><!-- /.modal-dialog -->
													</div><!-- /.modal -->
													<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



		<script type="text/javascript">
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
		
			
			/* // DOM Position key index //
		
			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing 
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class
			
			Also see: http://legacy.datatables.net/usage/features
			*/	
	
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				
	
			/* END BASIC */
			
				/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder<?php echo e($j); ?>').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder<?php echo e($j); ?>'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */
		})
		</script>
          
													
													<?php
													}
													$j++;
													}
													?>

									</div>
									<br><br><br><br>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(2000, 0).slideUp(2000, function(){
        $(this).remove(); 
    });
}, 3800);
 
});
</script>




          
