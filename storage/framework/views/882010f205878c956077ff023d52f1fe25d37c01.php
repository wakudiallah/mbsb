<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "New Applicant";


$page_css[] = "your_style.css";
include("asset/inc/header.php");





?>

<style>
.not-active {
   pointer-events: none;
   cursor: default;
}
table.border {
    border-collapse: separate;
    border-spacing: 10px; /* cellspacing */
    *border-collapse: expression('separate', cellSpacing = '10px');
}

td.border {
    padding: 10px; /* cellpadding */
}

</style>

<!--<script>
var apiGeolocationSuccess = function(position) {
  showLocation(position);
};

var tryAPIGeolocation = function() {
  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDCa1LUe1vOczX1hO_iGYgyo8p_jYuGOPU", function(success) {
    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
  })
  .fail(function(err) {
    // alert("API Geolocation error! \n\n"+err);
    window.location.href = "/geolocation/error/"+err;
  });
};

var browserGeolocationSuccess = function(position) {
  showLocation(position);
};

var browserGeolocationFail = function(error) {
  switch (error.code) {
    case error.TIMEOUT:
      alert("Browser geolocation error !\n\nTimeout.");
      break;
    case error.PERMISSION_DENIED:
      if(error.message.indexOf("Only secure origins are allowed") == 0) {
        tryAPIGeolocation();
      }
         window.location.href = "<?php echo e(url('/')); ?>/geolocation/error/"+error.code;
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Browser geolocation error !\n\nPosition unavailable.");
      break;
  }
};

var tryGeolocation = function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      browserGeolocationSuccess,
      browserGeolocationFail,
      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
  }
};

tryGeolocation();

function showLocation(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
  var _token = $('#_token').val();
  $.ajax({
    type:'POST',
    url: "<?php echo e(url('/')); ?>/getLocation",
    data: { latitude: latitude, _token : _token, longitude : longitude },
    success:function(data){
            if(data){
              $("#latitude").val(data.latitude);
              $("#longitude").val(data.longitude);
               $("#location").val(data.location);
                 

            }else{
                $("#location").html('Not Available');
            }
    },
        error: function () {  
        alert("Something Wrong!");                                                             
        }
  });
}
</script>-->




    <?php
  include("asset/inc/nav.php");
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>

<div id="main" role="main">
    <div id="content">
<section id="widget-grid" class="">

                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
                
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                
                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"
                
                                -->
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-check"></i> </span>
                                    <h2>New Applicant - <?php echo e($data->first()->name); ?></h2>
                
                                </header>
                
                                <!-- widget div-->
                                <div>
                
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                
                                    </div>
                                    <!-- end widget edit box -->
                
                                    <!-- widget content -->
                                    <div class="widget-body">
                          
                                        <div class="row">
                                            <form id="wizard-1" novalidate="novalidate">
                                                <div id="bootstrap-wizard-1" class="col-sm-12">
                                                    <div class="form-bootstrapWizard">
                                                        <ul class="bootstrapWizard form-wizard">
                                                             <li data-target="#step1">
                                                                <a href="#tab1" data-toggle="tab"> <span class="step">1</span> <span class="title">Personal Particulars (Main Applicant)</span> </a>
                                                            </li>
                                                            <li data-target="#step2" class="not-active">
                                                                <a href="#tab2" data-toggle="tab"> <span class="step">2</span> <span class="title">Employment Details (Main Applicant)</span> </a>
                                                            </li>
                                                            <li data-target="#step3" class="not-active">
                                                                <a href="#tab3" data-toggle="tab"> <span class="step">3</span> <span class="title">Particular of Spouse & Emergency Contact**</span> </a>
                                                            </li>
                                                            <li data-target="#step4" class="not-active">
                                                                <a href="#tab4" data-toggle="tab"> <span class="step">4</span> <span class="title">Income Information (Main Applicant)</span> </a>
                                                            </li>
                                                            <li data-target="#step5" class="not-active">
                                                                <a href="#tab5" data-toggle="tab"> <span class="step">5</span> <span class="title">Your Commitments with Other Credit Providers (Non-Banks Only)**</span> </a>
                                                            </li>
                                                            <li data-target="#step6" class="not-active">
                                                                <a href="#tab6" data-toggle="tab"> <span class="step">6</span> <span class="title">Financing Details</span> </a>
                                                            </li>
                                                            <li data-target="#step7" class="not-active">
                                                                <a href="#tab7" data-toggle="tab"> <span class="step">7</span> <span class="title">Applicant For Personal Financing-i Facility</span> </a>
                                                            </li>
                                                            <li data-target="#step8" class="not-active">
                                                                <a href="#tab8" data-toggle="tab"> <span class="step">8</span> <span class="title"> Upload Document </span> </a>
                                                            </li>
                                                             <li data-target="#step9" class="not-active" >
                                                                <a href="#tab9" data-toggle="tab"> <span class="step">9</span> <span class="title">Declaration/Disclosure By Applicant</span> </a>
                                                            </li>
                                                       </ul>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                   
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <ul class="pager wizard no-margin">
                                                                        <!--<li class="previous first ">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                                                        </li>-->
                                                                    <li class="next">
                                                                        <a href="javascript:void(0);" class="btn btn-lg txt-color-blue"> Seterusnya / <i> Next </i> </a>
                                                                    </li>
                                                                    <li class="previous ">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> Sebelum / <i> Previous </i> </a>
                                                                    </li>
                                                                        <!--<li class="next last">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                                                        </li>-->
                                                                        
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab1">
                                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <br>
                                                <h3><strong>Step 1 </strong> - Personal Particulars (Main Applicant)/ <i>Butir-Butir Peribadi (Pemohon Utama)</i> </h3>
                                                <div class="col-md-4"> <br>
                                                    <div class="form-group">
                                                        <label>1. Salutation / <i>Gelaran</i> :</label>
                                                        <?php echo e(csrf_field()); ?>

                                                            <input type="hidden" name="id_praapplication" value="<?php echo e($pra->id); ?>"/>
                                                            
                                                                <select class=" select2" disabled="" name="title" id="salutation" required="required" disabled="">
                                                             <option selected=""></option>
                                                            <?php $__currentLoopData = $title; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $title): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if($data->title !=NULL): ?>
                                                                    <?php 
                                                                        if($data->title==$title->title_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option <?php echo e($selected); ?> value="<?php echo e($title->title_code); ?>"><?php echo e($title->title_desc); ?></option>
                                                                <?php else: ?>
                                                                    <option value="<?php echo e($title->title_code); ?>"><?php echo e($title->title_desc); ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>

                                                                <input type="text" disabled="" placeholder="Specify / Sila Nyatakan" name="title_others" id="salutation_others" class="form-control" disabled="" required <?php if($data->title=="OTH"): ?> value="<?php echo e($data->title_others); ?>" <?php endif; ?>>
                                                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                                <input name="id_praapplication" type="hidden"  value="<?php echo e($data->id_praapplication); ?>">
                                                      
                                                    </div>

                                                    <div class="form-group">
                                                        <label>2. Full Name (As per ID Document) / <i>Nama Penuh (Seperti dalam  Dokumen Pengenalan Diri)</i> :</label>
                                                            <input type="text" disabled="" maxlength="66" id="name" name="name" value="<?php echo e($data->name); ?>" class="form-control" disabled="">
                                                      
                                                    </div>
                                                    <div class="form-group">
                                                        <label>3. My Kad No. / <i>No MyKad</i> :</label>
                                                            <input name="new_ic"  id="new_ic"  type="text" disabled="" maxlength="12" value="<?php echo e($data->new_ic); ?>"class="form-control" onkeypress="return isNumberKey(event)" >
                                                       
                                                    </div>
                                                    <div class="form-group">
                                                        <label>4. Date Of Birth / <i>Tarikh Lahir</i> :  </label>
                                                            <?php 
                                                                $lahir =  date('d/m/Y', strtotime($data->dob));
                                                            ?>
                                                            <input type="text" disabled="" data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  maxlength="14" name="dob" value="<?php echo e($lahir); ?>"  class="form-control startdate" id="dob"  />
                                                       
                                                    </div>
                                                  
                               
                                                    <div class="form-group">
                                                        <label>5. Country of Birth/ <i>Tempat Lahir</i> :</label>
                                                          
                                                            <select name="country_dob" id="country_dob" class=" select2" disabled="" disabled="">
                                                             <option selected=""></option>
                                                            <?php $__currentLoopData = $country; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if($data->country_dob !=NULL): ?>
                                                                    <?php 
                                                                        if($data->country_dob==$country->country_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option <?php echo e($selected); ?> value="<?php echo e($country->country_code); ?>"><?php echo e($country->country_desc); ?></option>
                                                                <?php else: ?>
                                                                    <option value="<?php echo e($country->country_code); ?>"><?php echo e($country->country_desc); ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                       
                                                    </div>
                                                    <div class="form-group">
                                                        <label>6. Police  / Military No./<i>No. Polis / Tentera </i> :</label>
                                                            <input type="text" disabled="" maxlength="15" id="police_number" name="police_number" value="<?php echo e($data->police_number); ?>" class="form-control" onkeypress="return isNumberKey(event)">
                                                      
                                                    </div>
                                                    <div class="form-group">
                                                        <label>7. Gender / <i>Jantina</i> :</label>
                                                            <select class=" select2" disabled="" name="gender" id="gender" required="requeired" disabled="">
                                                                <?php if(!empty($data->gender)): ?>
                                                                    <?php if($data->gender=='M'): ?>
                                                                    <option value="M">Male / <i>Lelaki </i></option>
                                                                    <?php elseif($data->gender=='F'): ?>
                                                                    <option value="F">Female / <i>Perempuan </i></option>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                                    <option value="M">Male / <i>Lelaki </i></option>
                                                                    <option value="F">Female / <i>Perempuan </i></option>
                                                            </select>
                                                       
                                                    </div>
                                                    <div class="form-group">
                                                        <label>8. Home Address / <i>Alamat Rumah</i> </label>
                                                        <br>
                                                            <input type="text" disabled="" class="form-control"  name="address" required="requeired" value="<?php echo e($data->address); ?>" placeholder="Address Line 1">
                                                         <br>
                                                            <input type="text" disabled="" class="form-control"  name="address2"  placeholder="Address Line 2" value="<?php echo e($data->address2); ?>" >

                                                         <br>
                                                            <input type="text" disabled="" class="form-control"  name="address3"  placeholder="Address Line 3" value="<?php echo e($data->address3); ?>" >
                                                         <label> Postcode/ <i>Poskod :</i></label>
                                                            <br><input type="text" disabled="" maxlength="5" name="postcode" id="postcode" class="form-control" onkeypress="return isNumberKey(event)" value="<?php echo e($data->postcode); ?>" />
                                                        <label>State / <i>Negeri :</i></label><br>
                                                            <input type="text" disabled=""    maxlength="50"  id="state" name="state" class="form-control" value="<?php echo e($data->state); ?>">
                                                            <input type="hidden"    maxlength="50"  id="state_code" name="state_code" class="form-control" value="<?php echo e($data->state_code); ?>">
                                                       
                                                    </div>

                                                        <div class="form-group">
                                                        <label>9. Ownership Status / <i>Taraf Pemilikan</i>:</label>
                                                            <select class=" select2" disabled="" name="ownership" id="ownership" required="requeired" type="text" >
                                                              

                                                              <?php if(!empty($data->ownership)): ?>
                                                                    <?php if($data->ownership=='OW'): ?>
                                                                    <option value="OW">Owned /<i> Milik Sendiri</i> </option>
                                                                    <?php elseif($data->ownership=='LP'): ?>
                                                                   <option value="LP">Living with Parents/Relatives/<i> Tinggal bersama keluarga/saudara </i></option>
                                                                    <?php elseif($data->ownership=='RT'): ?>
                                                                    <option value="RT">Rented /<i> Sewa</i></option>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                                    <option value="OW">Owned /<i> Milik Sendiri</i> </option>
                                                                    <option value="LP">Living with Parents/Relatives/<i> Tinggal bersama keluarga/saudara </i></option>
                                                                    <option value="RT">Rented /<i> Sewa</i></option>
                                                                </select>
                                                    </div> 
                                                </div>
                                                <div class="col-md-4"><br>
                                                    <div class="form-group">
                                                        <label>10. Correspondence Address/<i> Alamat Surat Menyurat </i>: </label>
                                                            <input class="form-control" disabled="" maxlength="200"  name="corres_address1" placeholder="Corresspondence Address 1" value="<?php echo e($data->corres_address1); ?>"><br>
                                                            <input class="form-control"  disabled="" value="<?php echo e($data->corres_address2); ?>"  name="corres_address2" placeholder="Correspondence Address 2"><br>
                                                            <input class="form-control" disabled="" value="<?php echo e($data->corres_address3); ?>"  name="corres_address3" placeholder="Correspondence Address 3">
                                                        <label>Postcode / <i>Poskod :</i></label>
                                                            <br><input type="text" disabled="" id="postcode2" maxlength="5"   name="corres_postcode" class="form-control" onkeypress="return isNumberKey(event)" value="<?php echo e($data->corres_postcode); ?>"/>
                                                        <label>State / <i>Negeri :</i></label><br>
                                                            <input type="text" disabled=""    maxlength="50"  id="state2" name="corres_state" class="form-control"  value="<?php echo e($data->corres_state); ?>">
                                                            <input type="hidden"    maxlength="50"  id="state_code2" name="corres_state2" class="form-control"  value="<?php echo e($data->corres_state1); ?>">
                                                        <label>Home Telephone / <i>Telefon Rumah :</i></label><br>
                                                            <input type="text" disabled="" id="homephone" maxlength="15"   name="corres_homephone" class="form-control" onkeypress="return isNumberKey(event)" value="<?php echo e($data->corres_homephone); ?>"/>
                                                        <label>Mobile Phone / <i>Telefon Bimbit :</i></label><br>
                                                            <input type="text" disabled="" id="mobilephone" maxlength="15"   name="corres_mobilephone" class="form-control" onkeypress="return isNumberKey(event)" value="<?php echo e($data->corres_mobilephone); ?>"/>
                                                        <label>Email Address / <i>Alamat E-mel :</i></label><br>

                                                            <?php if(empty($data->corres_email)): ?>
                                                                <?php $corres_email = $pra->acus_email; ?>
                                                            <?php else: ?>
                                                                 <?php $corres_email = $data->corres_email; ?>
                                                            <?php endif; ?>
                                                            <input type="text" disabled=""    maxlength="50"  id="email" name="corres_email" class="form-control" value="<?php echo e($corres_email); ?>" required >
                                                       
                                                    </div>
                                                     
                                                    <div class="form-group">
                                                        <label>11. Nationality / <i>Kewarganegaraan</i> :</label>
                                                            <select class=" select2" disabled="" id="country" name="country" required="requeired">
                                                                <?php if(!empty($data->country)): ?>
                                                                    <option><?php echo e($data->country); ?> </option>
                                                                <?php endif; ?>
                                                                    <option>Malaysian /<i>Malaysia</i> </option>
                                                                    <option>Permanent Resident / Penduduk Tetap</option>
                                                                     <option>Non-Citizen (Specify)/ Bukan Warganegara (Nyatakan)</option>
                                                            </select>
                                                              <input type="text" disabled="" class="form-control" placeholder="Specify / Sila Nyatakan" name="country_others" id="country_others" required <?php if($data->country=="Non-Citizen (Specify)/ Bukan Warganegara (Nyatakan)"): ?> value="<?php echo e($data->country_others); ?>" <?php endif; ?> >
                                                             <div id="country_origin">
                                                                Country Origin :
                                                                 <input type="text" disabled="" class="form-control" required placeholder="" name="country_origin" id="" required <?php if($data->country=="Permanent Resident / Penduduk Tetap"): ?> value="<?php echo e($data->country_origin); ?>" <?php endif; ?> >
                                                            </div>
                                                      
                                                    </div>
                                                        <div class="form-group">
                                                            <label>12. Race / <i>Bangsa </i> :</label>
                                                                <select class=" select2" disabled="" id="race" name="race" required="requeired">
                                                                    <?php if(!empty($data->race)): ?>
                                                                        <?php if($data->race=='MAL'): ?>
                                                                         <option value="MAL">Malay / <i>Melayu</i></option>
                                                                        <?php elseif($data->race=='IND'): ?>
                                                                        <option value="IND">Indian / <i>India</i></option>
                                                                         <?php elseif($data->race=='CHN'): ?>
                                                                        <option value="CHN" >Chinese / <i>China</i></option>
                                                                         <?php elseif($data->race=='ASK'): ?>
                                                                        <option value="ASK" >Other Bumiputera (Sabah & Sarawak) / <i>Lain-Lain Bumiputera (Sabah & Sarawak)</i></option>
                                                                         <?php elseif($data->race=='ASM'): ?>
                                                                        <option value="ASM" >Orang Asli (Sem. Malaysia)</option>
                                                                         <?php elseif($data->race=='EUR'): ?>
                                                                        <option value="EUR" >
                                                                         <?php elseif($data->race=='IBN'): ?>Serani </option>
                                                                         <option value="IBN" >Iban </option>
                                                                          <?php elseif($data->race=='KDZ'): ?>
                                                                          <option value="KDZ" >Kadazan </option>
                                                                           <?php elseif($data->race=='PUN'): ?>
                                                                          <option value="PUN" >Punjabi </option>
                                                                           <?php elseif($data->race=='OTL'): ?>
                                                                        <option value="OTL">Others Local (Specify)/ Lain-lain Lokal (Nyatakan)</option>
                                                                         <?php elseif($data->race=='OTH'): ?>
                                                                          <option value="OTH">Others Foreign (Specify)/ Lain-lain Orang Asing (Nyatakan)</option>
                                                                          <?php endif; ?>
                                                                    <?php endif; ?>
                                                                        <option value="MAL">Malay / <i>Melayu</i></option>
                                                                        <option value="IND">Indian / <i>India</i></option>
                                                                        <option value="CHN" >Chinese / <i>China</i></option>
                                                                        <option value="ASK" >Other Bumiputera (Sabah & Sarawak) / <i>Lain-Lain Bumiputera (Sabah & Sarawak)</i></option>
                                                                        <option value="ASM" >Orang Asli (Sem. Malaysia)</option>
                                                                        <option value="EUR" >Serani </option>
                                                                         <option value="IBN" >Iban </option>
                                                                          <option value="KDZ" >Kadazan </option>
                                                                          <option value="PUN" >Punjabi </option>
                                                                        <option value="OTL">Others Local (Specify)/ Lain-lain Lokal (Nyatakan)</option>
                                                                          <option value="OTH">Others Foreign (Specify)/ Lain-lain Orang Asing (Nyatakan)</option>
                                                                </select>

                                                                 <input type="text" disabled="" class="form-control" placeholder="Specify / Sila Nyatakan" name="race_others" id="race_others" required <?php if($data->race=="OTH"): ?> value="<?php echo e($data->race_others); ?>" <?php endif; ?> >
                                        
                                                        </div>

                                                         <div class="form-group">
                                                            <label>13. Bumiputera Status / <i>Status Bumiputera </i> :</label>
                                                                <select class=" select2" disabled="" id="bumiputera" name="bumiputera" required>
                                                                   
                                                                 <?php if(!empty($data->bumiputera)): ?>
                                                                        <?php if($data->bumiputera=='Y'): ?>
                                                                         <option value="Y">Yes / <i>Ya</i></option>
                                                                        <?php elseif($data->bumiputera=='N'): ?>
                                                                      <option value="N">No / <i>Tidak</i></option>
                                                                        <?php endif; ?>

                                                                    <?php endif; ?>
                                                                     <option value="Y">Yes / <i>Ya</i></option>
                                                                        <option value="N">No / <i>Tidak</i></option>
                                                                </select>

                                                           
                                                        </div>


                                                        <div class="form-group">
                                                            <label>14. Religion / <i>Agama</i> :</label>
                                                                <select class=" select2" disabled="" id="religion" name="religion" required>
                                                                    <?php if(!empty($data->religion)): ?>
                                                                        <?php if($data->religion=='I'): ?>
                                                                        <option value="I">Islam / <i>Muslim</i></option>
                                                                        <?php elseif($data->religion=='H'): ?>
                                                                       <option value="H">Hindu /<i> Hindu</i></option>
                                                                        <?php elseif($data->religion=='C'): ?>
                                                                        <option value="C">Christian /<i> Kristian<i> </option>
                                                                        <?php elseif($data->religion=='B'): ?>
                                                                        <option value="B">Buddhist /<i>Buddha </i></option>
                                                                        <?php elseif($data->religion=='S'): ?>
                                                                        <option value="S">Sikh</option>
                                                                        <?php elseif($data->religion=='O'): ?>
                                                                       <option value="O">Others/ Lain-lain</i></option>
                                                                        <?php endif; ?>

                                                                    <?php endif; ?>
                                                                        <option value="I">Islam / <i>Muslim</i></option>
                                                                        <option value="H">Hindu /<i> Hindu</i></option>
                                                                        <option value="C">Christian /<i> Kristian<i> </option>
                                                                        <option value="B">Buddhist /<i>Buddha </i></option>
                                                                         <option value="S">Sikh</option>
                                                                        <option value="O">Others/ Lain-lain</i></option>
                                                                </select>

                                                                  <input type="text" disabled="" class="form-control" placeholder="Specify / Sila Nyatakan" name="religion_others" id="religion_others" required <?php if($data->religion=="O"): ?> value="<?php echo e($data->religion_others); ?>" <?php endif; ?> >
                                                          
                                                        </div>

                                                        <div class="form-group">
                                                            <label>15. Marital Status / <i>Taraf Perkahwinan </i> :</label>
                                                                <select class=" select2" disabled="" name="marital" id="marital" required="required">
                                                                <?php if(!empty($data->marital)): ?>
                                                                    <?php if($data->marital=='D'): ?>
                                                                    <option value="D">Divorced / <i>Balu</i></option>
                                                                    <?php elseif($data->marital=='M'): ?>
                                                                   <option value="M">Married / <i>Berkahwin</i></option>
                                                                    <?php elseif($data->marital=='W'): ?>
                                                                    <option value="W">Widower / <i>Balu</i></option>
                                                                    <?php elseif($data->marital=='S'): ?>
                                                                    <option value="S"> Single/ <i>Bujang</i></option>
                                                                    <?php elseif($data->marital=='U'): ?>
                                                                     <option value="U">Unknown / <i>Tidak diketahui</i></option>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                                    <option value="S"> Single/ <i>Bujang</i></option>
                                                                    <option value="M">Married / <i>Berkahwin</i></option>
                                                                    <option value="D">Divorced / <i>Balu</i></option>
                                                                    <option value="W">Widower / <i>Balu</i></option>
                                                                     <option value="U">Unknown / <i>Tidak diketahui</i></option>
                                                                </select>
                                                          
                                                        </div>

                                                        <div class="form-group">
                                                            <label>16. No. of Dependants / <i>Jumlah Tanggungan</i></label>
                                                                <input type="text" disabled="" class="form-control" maxlength="2" value="<?php echo e($data->dependents); ?>" name="dependents" onkeypress="return isNumberKey(event)" required="requeired">
                                                          
                                                        </div>
                                                         <div class="form-group">
                                                            <label>17. Education Level / <i>Taraf Pendidikan</i> :</label>
                                                                <select class=" select2" disabled="" id="education" name="education" required="requeired" disabled="">
                                                                   <?php if(!empty($data->education)): ?>
                                                                        <?php if($data->education=='DG'): ?>
                                                                            <option value="DG">Degree /<i>Ijazah</i></option>
                                                                            <?php elseif($data->education=='DI'): ?>
                                                                            <option value="DI">Diploma /<i>Diploma</i></option>
                                                                            <?php elseif($data->education=='PH'): ?>
                                                                            <option value="PH">PhD /<i>Kedoktoran</i></option>
                                                                            <?php elseif($data->education=='MA'): ?>
                                                                            <option value="MA">Masters /<i>Ijazah Sarjana</i></option>
                                                                            <?php elseif($data->education=='PS'): ?>
                                                                             <option value="PS">Primary /<i>Rendah</i></option>
                                                                            <?php elseif($data->education=='SS'): ?>
                                                                            <option value="SS">Secondary /<i>Menengah</i></option>
                                                                             <?php elseif($data->education=='Z'): ?>
                                                                             <option value="Z">Not Specified /<i>Tidak ditentukan</i></option>
                                                                        <?php endif; ?>
                                                                     <?php endif; ?>
                                                                        <option value="PS">Primary /<i>Rendah</i></option>
                                                                        <option value="SS">Secondary /<i>Menengah</i></option>
                                                                        <option value="DI">Diploma /<i>Diploma</i></option>
                                                                        <option value="DG">Degree /<i>Ijazah</i></option>
                                                                        <option value="MA">Masters /<i>Ijazah Sarjana</i></option>
                                                                        <option value="PH">PhD /<i>Kedoktoran</i></option>
                                                                         <option value="Z">Not Specified /<i>Tidak ditentukan</i></option>
                                                                 </select>
                                                          
                                                        </div>
                                                    </div>
                                                  
                                                </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                    <div class="tab-pane" id="tab2">
                                                  
                                                    <br>
                                                    <h3><strong>Step 2</strong> - Employment Details (Main Applicant)/ <i>Butir-Butir Pekerjaan (Pemohon Utama)</i> </h3>
                                                    <?php echo e(csrf_field()); ?>

                                                     <input type="hidden" name="id_praapplication" value="<?php echo e($pra->id); ?>"/>
                                                            <?php $__currentLoopData = $empinfo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $empinfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <div class="col-md-4">
                                                                <br>
                                                    <div class="form-group">
                                                        <label>1. Employment Type/ <i>Jenis Pekerjaan</i> :</label>
                                                            <select class=" select2" disabled="" id="emptype" name="emptype" required="requeired">
                                                              
                                                              <?php if(!empty($empinfo->emptype)): ?>
                                                                    <?php if($empinfo->emptype=='115'): ?>
                                                                    <option value="115">Self Employed /<i>Bekerja Sendiri</i></option>
                                                                    <?php elseif($empinfo->emptype=='113'): ?>
                                                                   <option value="113">Salaried - Private Sector /<i>Bergaji - Sektor Swasta</option>
                                                                    <?php elseif($empinfo->emptype=='112'): ?>
                                                                    <option value="112">Salaried - Government /<i>Bergaji - Kerajaan</i></option>
                                                                    <?php elseif($empinfo->emptype=='PPP'): ?>
                                                                    <option value="PPP">Professional /<i>Profesional</i></option>
                                                                    <?php elseif($empinfo->emptype=='ZZZ'): ?>
                                                                    <option value="ZZZ">Others (Specify) / Lain-lain (Nyatakan)</option>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                                    <option  value="115">Self Employed /<i>Bekerja Sendiri</i></option>
                                                                    <option value="113">>Salaried - Private Sector /<i>Bergaji - Sektor Swasta</i></option>
                                                                    <option value="112">Salaried - Government /<i>Bergaji - Kerajaan</i></option>
                                                                    <option value="PPP">Professional /<i>Profesional</i></option>
                                                                    <option value="ZZZ">Others (Specify) / Lain-lain (Nyatakan)</option>
                                                                </select>
                                                        <input type="text" disabled="" class=" select2" disabled="" placeholder="Sila Nyatakan" id="emptype_others" name="emptype_others" required <?php if($empinfo->emptype=="Others (Specify) / Lain-lain (Nyatakan)"): ?> value="<?php echo e($empinfo->emptype_others); ?>"  <?php endif; ?>>
                                                       
                                                    </div>
                                                    <div class="form-group">
                                                    <label>2. Employment Status / <i>Status Pekerjaan </i> :</label>
                                                        <select class=" select2" disabled=""  name="empstatus" id="empstatus" required="requeired">
                                                            <?php if(!empty($empinfo->empstatus)): ?>
                                                                <option  ><?php echo e($empinfo->empstatus); ?> </option>
                                                            <?php endif; ?>
                                                                <option>Permanent / <i>Tetap</i></option>
                                                                <option>Contract / <i>Kontrak</i></option>
                                                        </select>
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                    <label>3. Occupation / <i> Pekerjaan </i> :</label>
                                                      

                                                         <select name="occupation" id="occupation"class=" select2" disabled="" required="">
                                                             <option selected=""></option>
                                                            <?php $__currentLoopData = $occupation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $occupation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if($empinfo->occupation !=NULL): ?>
                                                                    <?php 
                                                                        if($empinfo->occupation==$occupation->occupation_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option <?php echo e($selected); ?> value="<?php echo e($occupation->occupation_code); ?>"><?php echo e($occupation->occupation_desc); ?></option>
                                                                <?php else: ?>
                                                                    <option value="<?php echo e($occupation->occupation_code); ?>"><?php echo e($occupation->occupation_desc); ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                       
                                                        
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label>4. Name of Employer/Business / <i>Nama Majikan/Perniagaan </i> :</label>
                                                            <?php if(!empty($empinfo->empname)): ?>
                                                                <input type="text" disabled="" name="empname" maxlength="66"  value="<?php echo e($empinfo->empname); ?>" class="form-control" required="requeired">
                                                            <?php else: ?>
                                                                <input type="text" disabled="" name="empname" maxlength="66"  
                                                                value="<?php echo e($pra->employer->name); ?>" class="form-control" required="requeired">  
                                                            <?php endif; ?>
                                                       
                                                    </div>

                                                    <div class="form-group">
                                                        <label>5. Department Name / <i>Nama Jabatan</i> :</label>
                                                            <input type="text" disabled=""  maxlength="50" value="<?php echo e($empinfo->dept_name); ?>" id="dept_name" name="dept_name" class="form-control" />
                                                     
                                                    </div>

                                                    <div class="form-group">
                                                        <label>6. Division & Unit / <i>Bahagian & Unit</i> :</label>
                                                            <input type="text" disabled=""  maxlength="50" value="<?php echo e($empinfo->division); ?>" id="division" name="division" class="form-control"/>
                                                       
                                                    </div>
                                                   
                                                    <div class="form-group">
                                                        <label>7. Address of Employera/Business / <i>Alamat Majikan/Perniagaan</i> :</label>
                                                            <input type="text" disabled="" class="form-control" name="address" value="<?php echo e($empinfo->address); ?>"  placeholder="Address Line 1" required="" ><br>

                                                            <input type="text" disabled="" class="form-control" name="address2" value="<?php echo e($empinfo->address2); ?>" placeholder="Address Line 2"><br>

                                                            <input type="text" disabled="" class="form-control" name="address3" value="<?php echo e($empinfo->address3); ?>" placeholder="Address Line 3"><br>

                                                        <label> Postcode/ <i>Poskod :</i></label>
                                                                <br><input  required="" type="text" disabled=""  value="<?php echo e($empinfo->postcode); ?>" maxlength="5" name="postcode" id="postcode3" class="form-control" onkeypress="return isNumberKey(event)" />
                                                            <label>State / <i>Negeri :</i></label><br>
                                                                <input type="text" disabled=""   value="<?php echo e($empinfo->state); ?>"   maxlength="50"  id="state3" name="state" class="form-control">

                                                                 <input type="hidden"   value="<?php echo e($empinfo->state_code); ?>"   maxlength="50"  id="state_code3" name="state_code3" class="form-control">
                                                           
                                                        </div> 
                                                    </div>
                                                     <div class="col-md-4">
                                                    <br>
                                                    <div class="form-group">
                                                        <label>8. Nature of Business / <i>Jenis Perniagaan</i> :</label>
                                                            <input type="text" disabled=""  maxlength="50" value="<?php echo e($empinfo->nature_business); ?>" id="nature_business" name="nature_business" class="form-control"/>
                                                      
                                                    </div>

                                                    <div class="form-group">
                                                        <label>9. Start Date of Work / <i>Tarikh Mula Bekerja</i> :</label>
                                                            <?php 
                                                                $joined =  date('d/m/Y', strtotime($empinfo->joined));
                                                                 $today=date('d/m/Y');
                                                            ?>
                                                            <?php if(empty($empinfo->joined)): ?>
                                                            <input type="text" disabled=""  name="joined"  data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  class="form-control startdate" name="joined" value=""/>
                                                            <?php else: ?>
                                                            <input type="text" disabled=""  name="joined"  data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  class="form-control startdate" name="joined" value="<?php echo e($joined); ?>"/>
                                                            <?php endif; ?>
                                                       
                                                    </div>

                                                    <div class="form-group">
                                                        <label>10. Position / <i>Jawatan</i> :</label>
                                                            
                                                            <select name="position" id="position" class=" select2" disabled="" required="">
                                                             <option selected=""></option>
                                                            <?php $__currentLoopData = $position; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $position): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if($empinfo->position !=NULL): ?>
                                                                    <?php 
                                                                        if($empinfo->position==$position->position_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option <?php echo e($selected); ?> value="<?php echo e($position->position_code); ?>"><?php echo e($position->position_desc); ?></option>
                                                                <?php else: ?>
                                                                    <option value="<?php echo e($position->position_code); ?>"><?php echo e($position->position_desc); ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                      
                                                    </div>

                                                    <div class="form-group">
                                                        <label>11. Office Telephone / <i>Telefon Pejabat :</i></label><br>
                                                            <input type="text" disabled="" required="" id="office_phone" maxlength="15"   name="office_phone" class="form-control" onkeypress="return isNumberKey(event)" value="<?php echo e($empinfo->office_phone); ?>"/>
                                                    </div>
                                                    <div class="form-group">

                                                        <label>11. Office Fax / <i>Faks Pejabat :</i></label><br>
                                                            <input type="text" disabled="" id="office_fax" value="<?php echo e($empinfo->office_fax); ?>" maxlength="15"   name="office_fax" class="form-control" onkeypress="return isNumberKey(event)"/>
                                                      
                                                    </div>
                                                </div>
                                                 
                                                    </div>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 

                                                <div class="tab-pane" id="tab3">
                                                    <br>
                                                    <h3><strong>Step 3</strong> - Particular of Spouse & Emergency Contact/ <i> Maklumat Suami-Isteri & Rujukan Kecemasan**</i></h3> 
                                                    <input type="hidden" name="id_praapplication" value="<?php echo e($pra->id); ?>">
                                                    <?php echo e(csrf_field()); ?>

                                                    
                                                    <div class="col-md-6"  id="tab99">
                                                        <br>
                                                        <h4><strong>PARTICULAR OF SPOUSE / <i>MAKLUMAT SUAMI-ISTERI</i></strong></h4>
                                                    <div id="spouse-group">
                                                    <div class="form-group" id="spouse_name" >
                                                        <?php $__currentLoopData = $spouse; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spouse): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <label>1. Full Name   / <i>  Nama Penuh   :  </i>  </label>
                                                            <input type="text" disabled="" maxlength="66" class="form-control" value="<?php echo e($spouse->name); ?>" name="name" id="" required="required">
                                                      
                                                    </div>

                                                    <div class="form-group">
                                                        <label>2. Home/Office Telephone / <i>Telefon Rumah/Pejabat :</i></label><br>
                                                            <input type="text" disabled="" id="home_phone" maxlength="15"   name="homephone" class="form-control" value="<?php echo e($spouse->homephone); ?>" onkeypress="return isNumberKey(event)"/>
                                                        <label>Mobile Phone / <i>Telefon Bimbit :</i></label><br>
                                                            <input type="text" disabled="" id="handphone" maxlength="15"   name="mobilephone" class="form-control" value="<?php echo e($spouse->mobilephone); ?>" onkeypress="return isNumberKey(event)"/>
                                                       
                                                    </div>

                                                    <div class="form-group" id="spouse_occupation">
                                                        <label>3. Employment Type/<i>Jenis Pekerjaan </i> :</label>
                                                            <select class="select2"  name="emptype" id="spouse_emptype" required="required">

                                                                     <?php if(!empty($spouse->emptype)): ?>
                                                                    <?php if($spouse->emptype=='115'): ?>
                                                                    <option value="115.0">Self Employed /<i>Bekerja Sendiri</i></option>
                                                                    <?php elseif($spouse->emptype=='113'): ?>
                                                                   <option value="113.0">Salaried - Private Sector /<i>Bergaji - Sektor Swasta</option>
                                                                    <?php elseif($spouse->emptype=='112'): ?>
                                                                    <option value="112.0">Salaried - Government /<i>Bergaji - Kerajaan</i></option>
                                                                    <?php elseif($spouse->emptype=='PPP'): ?>
                                                                    <option value="PPP.0">Professional /<i>Profesional</i></option>
                                                                    <?php elseif($spouse->emptype=='ZZZ'): ?>
                                                                    <option value="ZZZ.0">Others (Specify) / Lain-lain (Nyatakan)</option>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                                    <option  value="115.0">Self Employed /<i>Bekerja Sendiri</i></option>
                                                                    <option value="113.0">Salaried - Private Sector /<i>Bergaji - Sektor Swasta</i></option>
                                                                    <option value="112.0">Salaried - Government /<i>Bergaji - Kerajaan</i></option>
                                                                    <option value="PPP.0">Professional /<i>Profesional</i></option>
                                                                    <option value="ZZZ.0">Others (Specify) / Lain-lain (Nyatakan)</option>
                                                            </select>

                                                            <input type="text" disabled="" class="form-control" placeholder="Sila Nyatakan" id="spouse_emptype_others" name="emptype_others" required <?php if($spouse->emptype=="Others (Specify)/ Lain-lain (Nyatakan)"): ?> value="<?php echo e($spouse->emptype_others); ?>"  <?php endif; ?> >

                                                             <input type="text" disabled="" class="form-control" id="spouse_emptype_kosong">
                                                      
                                                    </div>
                                                    </div>
                                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                    <?php $sumref=1; ?>
                              
                                                    <br>
                                                    <h4><strong>EMERGENCY CONTACT / RUJUKAN KECEMASAN </strong></h4>

                                                    ** (Family Members/Relatives not staying with you)/ (Ahli Keluarga/Saudara terdekat yang tidak tinggal bersama anda)<br> <br>

                                                    <b>Contact 1 / <i> Rujukan 1 </i></b>
                                                    <div class="form-group">
                                                        <label>1. Full Name / <i> Nama Penuh </i> </label>
                                                            <input type="text" disabled="" maxlength="66" id="name1" name="name1" value="<?php echo e($reference->name1); ?>" class="form-control" required="required">
                                                             <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                                     <input name="id_praapplication" type="hidden"  value="<?php echo e($data->id_praapplication); ?>">
                                                     
                                                    </div>

                                                    <div class="form-group">
                                                        <label>2. Home Telephone / <i>Telefon Rumah :</i></label><br>
                                                            <input type="text" disabled="" id="home_phone1" maxlength="15"   name="home_phone1" class="form-control" value="<?php echo e($reference->home_phone1); ?>" onkeypress="return isNumberKey(event)"/>

                                                        <label>Mobile Phone / <i>Telefon Bimbit :</i></label><br>
                                                            <input type="text" disabled="" id="mobilephone1" maxlength="15"   name="mobilephone1" class="form-control" value="<?php echo e($reference->mobilephone1); ?>" onkeypress="return isNumberKey(event)"/ required="">
                                                       
                                                    </div>

                                                    <div class="form-group">
                                                        <label>3. Relationship / <i> Hubungan</i> </label>
                                                       
                                                              <select name="relationship1" id="relationship1" class=" select2" disabled="" required="">
                                                             <option selected=""></option>
                                                            <?php $__currentLoopData = $relationship; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $relationship): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if($reference->relationship1 !=NULL): ?>
                                                                    <?php 
                                                                        if($reference->relationship1==$relationship->relationship_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option <?php echo e($selected); ?> value="<?php echo e($relationship->relationship_code); ?>"><?php echo e($relationship->relationship_desc); ?></option>
                                                                <?php else: ?>
                                                                    <option value="<?php echo e($relationship->relationship_code); ?>"><?php echo e($relationship->relationship_desc); ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                     
                                                    </div>

                                                    <b>Contact 2 / <i> Rujukan 2 </i></b>
                                                    <div class="form-group">
                                                        <label>1. Full Name / <i> Nama Penuh </i> </label>
                                                            <input type="text" disabled="" maxlength="66" id="name2" name="name2" value="<?php echo e($reference->name2); ?>" class="form-control" required="required">
                                                      
                                                    </div>

                                                    <div class="form-group">
                                                        <label>2. Home Telephone / <i>Telefon Rumah :</i></label><br>
                                                            <input type="text" disabled="" id="home_phone2" maxlength="15"   name="home_phone2" class="form-control" value="<?php echo e($reference->home_phone2); ?>" onkeypress="return isNumberKey(event)"/>

                                                        <label>Mobile Phone / <i>Telefon Bimbit :</i></label><br>
                                                            <input type="text" disabled="" id="mobilephone2" maxlength="15"   name="mobilephone2" class="form-control" value="<?php echo e($reference->mobilephone2); ?>" onkeypress="return isNumberKey(event)"/ required="">
                                                       
                                                    </div>

                                                    <div class="form-group">
                                                        <label>3. Relationship / <i> Hubungan</i> </label>
                                                            <select name="relationship2" id="relationship2" class=" select2" disabled="" required="">
                                                             <option selected=""></option>
                                                            <?php $__currentLoopData = $relationship2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $relationship2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if($reference->relationship2 !=NULL): ?>
                                                                    <?php 
                                                                        if($reference->relationship2==$relationship2->relationship_code) {
                                                                          $selected = "selected";
                                                                        }
                                                                        else {
                                                                           $selected = "";
                                                                        } 
                                                                    ?>
                                                                        <option <?php echo e($selected); ?> value="<?php echo e($relationship2->relationship_code); ?>"><?php echo e($relationship2->relationship_desc); ?></option>
                                                                <?php else: ?>
                                                                     <option value="<?php echo e($relationship2->relationship_code); ?>"><?php echo e($relationship2->relationship_desc); ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </select>
                                                     
                                                    </div>
                                                </div>
                                       
                                            </div>
                                             <div class="tab-pane" id="tab4">
                                                    <br>
                                                        <h3><strong>Step 4</strong> - Income Information (Main Applicant)/  <i>Maklumat Pendapatan (Pemohon Utama)</i> </h3>
                                                         
                                                    <div class="col-md-6">
                                                           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                            <input name="id_praapplication" type="hidden"  value="<?php echo e($data->id_praapplication); ?>">   <br>
                                                  
                                                    <div class="form-group">
                                                        <label>1. Monthly Income  / <i> Pendapatan Bulanan </i>  </label>
                                                            <input type="text" disabled="" maxlength="10" name="monthly_income"  id="monthly_income" class="form-control income" placeholder="RM" onchange="toFloat('monthly_income')" value="<?php echo e(number_format((float)$financial->monthly_income, 2, '.', '')); ?>" onkeypress="return isNumberKey(event)">
                                                     
                                                    </div>
                                                    <div class="form-group">
                                                        <label>2.   Other Income  /<i> Pendapatan Lain</i> : </label>
                                                             <input type="text" disabled="" maxlength="10" required="required" class="form-control income" name="other_income" placeholder="RM" value="<?php echo e(number_format((float)$financial->other_income, 2, '.', '')); ?>" id="other_income" onchange="toFloat('other_income')"   onkeypress="return isNumberKey(event)">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>3. Total Income / <i>Jumlah Pendapatan </i> : </label>
                                                            <input   type="text" disabled="" maxlength="10" required="required" class="form-control" placeholder="RM" name="total_income" value="<?php echo e(number_format((float)$financial->monthly_income + $financial->other_income, 2, '.', '')); ?>" id="total_income" readonly >
                                                    </div>
                                                </div> 
                                      
                                                  </div>
                                                <div class="tab-pane" id="tab5">
                                                    <br>
                                                    <h3><strong>Step 5</strong> - Your Commitments with Other Credit Providers (Non-Banks Only)**/ <i>Komitmen Dengan Pembiaya Kredit Lain (Bukan Bank Sahaja)**</i></h3> 

                                                    <div class="col-lg-12"> <br>
                                                              ** (e.g. AEON Credit, PTPTN, MARA, etc.)/ (cth. AEON Credit, PTPTN, MARA, dll.)
                                                    </div> 

                                                    <input type="hidden" name="id_praapplication" value="<?php echo e($pra->id); ?>">
                                                    <?php echo e(csrf_field()); ?>

                                                    
                                                    <div class="col-md-4"  id="tab99">
                                                      
                                                        <br>
                                                        <h3><strong>Commitment 1<i>/ Komitmen 1</i></strong></h3>
                                                         <div class="form-group" >
                                                            <label>1. Name of Entity  / <i>  Nama Entiti   :  </i>  </label>
                                                                <input type="text" disabled="" maxlength="66" class="form-control" value="<?php if(!(empty($commitments->name1))): ?><?php echo e($commitments->name1); ?> <?php endif; ?>" name="name1" id="" ">
                                                        </div>
                                                         <div class="form-group" >
                                                            <label>2. Type of Financing  / <i>  Jenis Pembiayaan  :  </i>  </label>
                                                                <input type="text" disabled="" maxlength="66" class="form-control" value="<?php if(!(empty($commitments->financing1))): ?><?php echo e($commitments->financing1); ?><?php endif; ?>" name="financing1" id="">
                                                        </div>
                                                         <div class="form-group" >
                                                            <label>3. Monthly Payment  / <i>  Bayaran Bulanan  :  </i>  </label>
                                                              <div class="input-group">
                                                                    <span class="input-group-addon">RM</span>
                                                                    <input type="text" disabled="" maxlength="8" class="form-control" value="<?php if(!(empty($commitments->monthly_payment1))): ?><?php echo e($commitments->monthly_payment1); ?><?php endif; ?>" name="monthly_payment1" id="" onkeypress="return isNumberKey(event)" >
                                                            </div>
                                                        </div>
                                                         <div class="form-group" >
                                                            <label>4. Remaining Financing Term  / <i>  Baki Tempoh Pembiayaan  :  </i>  </label>
                                                                <input type="text" disabled="" maxlength="2" class="" size="3" value="<?php if(!(empty($commitments->remaining1))): ?><?php echo e($commitments->remaining1); ?><?php endif; ?>" name="remaining1" id="" onkeypress="return isNumberKey(event)" > Years/<i>Tahun</i>
                                                        </div>
                                                        
                                                    </div>
                                                     <div class="col-md-4"  id="tab99">
                                                            <br>
                                                            <h3><strong>Commitment 2<i>/ Komitmen 2</i></strong></h3>
                                                        <div class="form-group" >
                                                            <label>1. Name of Entity  / <i>  Nama Entiti   :  </i>  </label>
                                                                <input type="text" disabled="" maxlength="66" class="form-control" value="<?php if(!(empty($commitments->name2))): ?><?php echo e($commitments->name2); ?><?php endif; ?>" name="name2" id="" >
                                                        </div>
                                                         <div class="form-group" >
                                                            <label>2. Type of Financing  / <i>  Jenis Pembiayaan  :  </i>  </label>
                                                                <input type="text" disabled="" maxlength="66" class="form-control" value="<?php if(!(empty($commitments->financing2))): ?><?php echo e($commitments->financing2); ?><?php endif; ?>" name="financing2" id="" >
                                                        </div>
                                                       
                                                         <div class="form-group" >
                                                            <label>3. Monthly Payment  / <i>  Bayaran Bulanan  :  </i>  </label>
                                                                <div class="input-group">
                                                                  <span class="input-group-addon">RM</span>
                                                                <input type="text" disabled="" maxlength="8" class="form-control" value="<?php if(!(empty($commitments->monthly_payment2))): ?><?php echo e($commitments->monthly_payment2); ?><?php endif; ?>" name="monthly_payment2" id="" onkeypress="return isNumberKey(event)">
                                                            
                                                            </div>
                                                        </div>
                                                          <div class="form-group" >
                                                            <label>4. Remaining Financing Term  / <i>  Baki Tempoh Pembiayaan  :  </i>  </label>
                                                                <input type="text" disabled="" maxlength="2" size="3" class="" value="<?php if(!(empty($commitments->remaining2))): ?><?php echo e($commitments->remaining2); ?><?php endif; ?>" name="remaining2" id="" onkeypress="return isNumberKey(event)"> Years/<i>Tahun</i>
                                                        </div>
                                                 
                                                       
                                                    </div>
                                           
                                                </div>

                                            <div class="tab-pane" id="tab6">
                                                <br>
                                                <h3><strong>Step 6</strong> -  Financing Details/ <i> Maklumat Pembiayaan</i></h3>  
                                                <input type="hidden" name="id_praapplication" value="<?php echo e($pra->id); ?>">
                                                <?php echo e(csrf_field()); ?>

                                                <div class="col-md-7"  id="tab99">
                                                        <br>
                                                        <h3><strong>Facility Applied/ <i>Kemudahan Yang Dipohon</i></strong></h3>
                                                    <div class="form-group" >
                                                        <label>1. Package Applied  / <i>  Pakej Dipohon  :  </i>  </label>
                                                            <input type="text" disabled="" maxlength="66" class="form-control" value="<?php echo e($loanammount->package); ?>" name="package" id="package" required="required">
                                                       
                                                       
                                                    </div>
                                                     <div class="form-group" id="financing_detaila" >
                                                        <label>2. Product Bundling  / <i> Gabungan Produk  :  </i>  </label><br>
                                                        <td>&nbsp;</td>
                                                            <td>
                                                                <input type="radio" disabled="" <?php if($financial->product_bundling=="1"): ?> checked <?php endif; ?>  name="product_bundling" id="pb_yes" value="1" class="product_bundling">
                                                            </td>
                                                            <td><b>Yes (Specify)</b>/Ya (Nyatakan)</td>
                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                            <td>
                                                                 <input type="radio" disabled="" <?php if($financial->product_bundling=="0"): ?> checked <?php endif; ?>   name="product_bundling" id="pb_no" value="0" class="product_bundling">
                                                            </td>
                                                            <td><b>No</b>/Tidak</td><br>
                                                                <input type="text" disabled="" required="" id="product_bundling_specify" name="product_bundling_specify" class="form-control" placeholder="Specify/ Nyatakan" value="<?php echo e($financial->product_bundling_specify); ?>" />
                                                    

                                                            </div>
                                                             <div class="form-group" id="financing_detail" >
                                                        <label>3. Cross Selling  / <i> Jualan Silang  :  </i>  </label><br>
                                                            <td>&nbsp;</td>
                                                            <td>
                                                                <input type="radio" disabled="" required="" <?php if($financial->cross_selling=="1"): ?> checked <?php endif; ?> name="cross_selling" value="1" id="cs_yes">
                                                            </td>
                                                            <td><b>Yes (Specify)</b>/Ya (Nyatakan)</td>
                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                            <td>
                                                                 <input type="radio" disabled="" required="" <?php if($financial->cross_selling=="0"): ?> checked <?php endif; ?>  name="cross_selling" value="0" id="cs_no">
                                                            </td>
                                                            <td><b>No</b>/Tidak</td><br>
                                                    
                                                              <input type="text" disabled="" required="" id="cross_selling_specify" name="cross_selling_specify" class="form-control" placeholder="Specify/ Nyatakan" value="<?php echo e($financial->cross_selling_specify); ?>"/>
                                                     
                                                    

                                                            </div>
                                                      
                                                    <div class="form-group" ><br>
                                                     <h3><strong>Information on Takaful Coverage/<i> Maklumat untuk Perlindungan Takaful</i></strong></h3>

                                                        <label>4. Takaful Coverage  / <i> Perlindungan Takaful  :  </i>  </label><br>
                                                        &nbsp; &nbsp;Group Credit Term Takaful (GCTT)/ Perlindungan Takaful Berkelompok Bertempoh (GCTT)<br>
                                                            <td>&nbsp;</td>
                                                            <td>
                                                                <input type="radio" disabled="" required="" name="takaful_coverage" <?php if($financial->takaful_coverage=="1"): ?> checked <?php endif; ?> id="tf_yes" value="1" >
                                                            </td>
                                                            <td><b>Yes</b>/Ya</td>
                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                            <td>
                                                                <input type="radio" disabled="" name="takaful_coverage" <?php if($financial->takaful_coverage=="0"): ?> checked <?php endif; ?> id="tf_no" value="0" >
                                                            </td>
                                                            <td><b>No</b>/Tidak</td><br>

                                                            
                                             
                                                    </div>
                                                    
                                                    <div class="form-group" id="spouse_name" >
                                                        <h3><strong>Details of Financing /<i> Butir-butir Pembiayaan</i></strong></h3>
                                                        <label>5. Financing Amount  /<i> Jumlah Pembiayaan </i> : </label>
                                                          <div class="input-group">
                                                                <span class="input-group-addon">RM</span>
                                                            <input type="text" disabled="" maxlength="66" class="form-control" value="<?php echo e($dsr_b->financing_amount); ?>" name="financing_amount" id="" required="required" readonly="">
                                                        </div>
                                                 
                                                    </div>
                                                     <div class="form-group" id="spouse_name" >
                                                        <label>6. Tenure  (Years)/<i> Tempoh (Tahun)</i> : </label>
                                                        <?php if($loanammount->new_tenure==0): ?>
                                                           <?php  $tenure = $loanammount->tenure->years; ?>
                                                        <?php else: ?>
                                                           <?php $tenure = $loanammount->new_tenure; ?>
                                                        <?php endif; ?>
                                                         <input type="text" disabled="" maxlength="2" class="form-control" value="<?php echo e($dsr_b->tenure); ?>" name="tenure" id="" required="required" readonly="">
                                                   
                                                    </div>

                                                    </div>
                                                 
                                         
                                            </div>
                                            <div class="tab-pane" id="tab7">
                                                  
                                                    <br>
                                                    <h3><strong>Step 7</strong> Application for Personal Financing-<i>i</i> Facility/ <i> Permohonan Untuk Kemudahan Pembiayaan Peribadi-i</i> </h3>
                                                    <?php echo e(csrf_field()); ?>

                                                     <input type="hidden" name="id_praapplication" value="<?php echo e($pra->id); ?>">

                                                    <div class="col-md-7">
                                                                <br>
                                                    <div class="form-group"> 
                                                        <label>1. Purpose of Facility/ <i>Tujuan Pembiayaan</i> :</label>
                                                            <select class="form-control" id="purpose_facility" name="purpose_facility" required="required" disabled="">
                                                              <?php if(!empty($financial->purpose_facility)): ?>
                                                                      <?php if($financial->purpose_facility=='115'): ?>
                                                                    <option value="PF01">Personal Use/<i> Kegunaan Peribadi</i></option>
                                                                    <?php elseif($financial->purpose_facility=='113'): ?>
                                                                    <option value="PF02">Education/<i> Pendidikan</i></i></option>
                                                                    <?php elseif($financial->purpose_facility=='112'): ?>
                                                                    <option value="PF03">Business/<i> Perniagaan</i></option>
                                                                    <?php elseif($financial->purpose_facility=='PPP'): ?>
                                                                    <option value="PF04">Others/<i> Lain-lain</i></option>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                                    <option value="PF01">Personal Use/<i> Kegunaan Peribadi</i></option>
                                                                    <option value="PF02">Education/<i> Pendidikan</i></option>
                                                                    <option value="PF03">Business/<i> Perniagaan</i></option>
                                                                    <option value="PF04">Others/<i> Lain-lain</i></option>
                                                                </select>
                                                       
                                                    
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <label>2. Type of Customer/ <i>Jenis Pelanggan</i> :</label>
                                                            <select class="form-control" id="type_customer" name="type_customer" required="requeired" disabled="">
                                                            <?php if(!empty($financial->type_customer)): ?>
                                                                    <?php if($financial->type_customer=='115'): ?>
                                                                    <option value="Y">Biro/ <i>Biro</i></option>
                                                                    <?php elseif($financial->type_customer=='113'): ?>
                                                                   <option value="N">Non-Biro/ <i>Bukan Biro</i></option>
                                                                    <?php elseif($financial->type_customer=='112'): ?>
                                                                    <option value="P">Private Sector/ <i>Swasta</i></option>
                                                                    <?php elseif($financial->type_customer=='PPP'): ?>
                                                                    <option value="G">Federal/State AG/<i>Akauntan Negara/Negeri</i></option>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                                    <option value="Y">Biro/ <i>Biro</i></option>
                                                                    <option value="N">Non-Biro/ <i>Bukan Biro</i></option>
                                                                    <option value="P">Private Sector/ <i>Swasta</i></option>
                                                                    <option value="G">Federal/State AG/<i>Akauntan Negara/Negeri</i></option>
                                                                </select>
                                             
                                                    </div>
                                                    <div class="form-group">
                                                        <label>3. Payment Mode/ <i>Cara Bayaran</i> :</label>
                                                            <select class="form-control" id="payment_mode" name="payment_mode" required="requeired" disabled="">
                                                               
                                                              <?php if(!empty($financial->payment_mode)): ?>
                                                                      <?php if($financial->payment_mode=='115'): ?>
                                                                    <option value="BA">Biro Angkasa Salary Deduction/<i>Potongan Gaji melalui Biro Angkasa</i></option>
                                                                    <?php elseif($financial->payment_mode=='113'): ?>
                                                                   <option value="PG">Employer Salary Deduction /<i>Potongan Gaji Majikan</i></option>
                                                                    <?php elseif($financial->payment_mode=='112'): ?>
                                                                    <option value="FG">Federal/State AG Salary Deduction /<i>Potongan Gaji Melalui Akauntan Negara/Negeri</i></option>
                                                                    <?php elseif($financial->payment_mode=='PPP'): ?>
                                                                    <option value="OC">Over the counter /<i>Melalui Kaunter</i></option>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                                    <option value="BA">Biro Angkasa Salary Deduction/<i>Potongan Gaji melalui Biro Angkasa</i></option>
                                                                    <option value="P">Employer Salary Deduction /<i>Potongan Gaji Majikan</i></option>
                                                                    <option value="FG">Federal/State AG Salary Deduction /<i>Potongan Gaji Melalui Akauntan Negara/Negeri</i></option>
                                                                    <option value="OC">Over the counter /<i>Melalui Kaunter</i></option>
                                                                </select>
                                                    </div>
                                                    <br>
                                                    <h3><b>Application for Settlement (If Any)/ <i> Permohonan Penyelesaian Pembiayaan Lain (Jika Berkenaan)</i> </b></h3>
                                                    <div class="form-group">
                                                        <label>Name of Banks/Non Banks <i>Nama Bank/Bukan Bank**</i> :</label><br>
                                                        ** (e.g. AEON Credit, PTPTN, MARA, etc.)/ (cth. AEON Credit, PTPTN, MARA, dll.)<br>
                                                        <input type="text" disabled="" maxlength="66" class="form-control" value="<?php echo e($financial->bank1); ?>" name="bank1" id="" placeholder=" Name of Bank/Non Banks 1"><br>
                                                        <input type="text" disabled="" maxlength="66" class="form-control" value="<?php echo e($financial->bank2); ?>" name="bank2" id="" placeholder=" Name of Bank/Non Banks 2"><br>
                                                        <input type="text" disabled="" maxlength="66" class="form-control" value="<?php echo e($financial->bank3); ?>" name="bank3" id="" placeholder=" Name of Bank/Non Banks 3"><br>
                                                        <input type="text" disabled="" maxlength="66" class="form-control" value="<?php echo e($financial->bank4); ?>" name="bank4" id="" placeholder=" Name of Bank/Non Banks 4"><br>
                                                        <input type="text" disabled="" maxlength="66" class="form-control" value="<?php echo e($financial->bank5); ?>" name="bank5" id="" placeholder=" Name of Bank/Non Banks 5"><br>
                                                        <input type="text" disabled="" maxlength="66" class="form-control" value="<?php echo e($financial->bank6); ?>" name="bank6" id="" placeholder=" Name of Bank/Non Banks 6"><br>
                                                    </div>
                                                     <div class="form-group">
                                                        <label>Account to Credit Financing Amount /<i>Akaun di mana Amaun Pembiayaan Dikreditkan</i> :</label>
                                                        <label>Bank's Name/ <i>Nama Bank</i> :</label>
                                                        <input type="text" disabled="" maxlength="66" class="form-control" value="<?php echo e($financial->bank_name); ?>" name="bank_name" id="bank_name" placeholder="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Account No./ <i>No. Akaun</i> :</label>
                                                        <input type="text" disabled="" maxlength="66" class="form-control" value="<?php echo e($financial->account_no); ?>" name="account_no" id="account_no" placeholder="">
                                                    </div>
                                                </div>
                                      
                                            </div>
                                            <div class="tab-pane" id="tab8">
                                                <br>
                                                <h3><strong>Step 8</strong> - Upload Document/<i> Muat Naik dokumen</i> (<i>Document in PDF/JPG/PNG / Dokumen dalam format PDF/JPG/PNG</i> </h3>
                                                <div class="container" style="">
                                                     <?php echo e(csrf_field()); ?>

                                                     <input name="id_praapplication" type="hidden"  value="<?php echo e($pra->id); ?>" >

                                                            <div class="form-group">
                                                    <label class="col-md-4 control-label">Salinan Kad Pengenalan <sup>*</sup></label>
                                                    <div class="col-md-8">
                            <input id="fileupload6" disabled="" <?php if(empty($document6->name)): ?> required <?php endif; ?>  class="btn btn-default" type="file" name="file6"  >
                            <input type="hidden" name="document6"   id="documentx6"  value="Salinan Kad Pengenalan">
                            &nbsp; <span id="document6"> </span> 
                            <input type='hidden' value='<?php if(!empty($document6->name)): ?><?php echo e($document6->upload); ?> <?php endif; ?>' id='a6' name='a6'/>
                            <?php if(!empty($document6->name)): ?>
                              <span id="document6a"><a target='_blank' class="hidden-xs hidden-sm" href="<?php echo e(url('/')); ?>/admin/downloaddocpdf/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document6->upload); ?>"> <?php echo e($document6->name); ?></a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 

                               <a target='_blank' class="hidden-md hidden-lg" href="<?php echo e(url('/')); ?>/admin/get_document/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document6->upload); ?>"> <?php echo e($document6->name); ?> </a>
                            <?php endif; ?>
                                  
                                                    </div>
                                                </div> &nbsp; <hr><br>


                                         <div class="form-group">
                            <label class="col-md-4 control-label">Salinan Penyata Gaji Untuk 3 Bulan Terkini <sup>*</sup></label>
                            <div class="col-md-8">
                           <input id="fileupload7"  <?php if(empty($document7->name)): ?> required <?php endif; ?>   class="btn btn-default" type="file" name="file7" >
                            <input type="hidden" name="document7"  id="documentx7"  value="Salinan Penyata Gaji Untuk 3 Bulan Terkini">
                            &nbsp; <span id="document7"> </span> 

                            <input type='hidden' value='<?php if(!empty($document7->name)): ?><?php echo e($document7->upload); ?> <?php endif; ?>' id='a7' name='a7'/>
                            <?php if(!empty($document7->name)): ?>
                              <span id="document7a"><a target='_blank' class="hidden-xs hidden-sm" href="<?php echo e(url('/')); ?>/admin/downloaddocpdf/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document7->upload); ?>"> <?php echo e($document7->name); ?></a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                               <a target='_blank' class="hidden-md hidden-lg" href="<?php echo e(url('/')); ?>/admin/get_document/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document7->upload); ?>"> <?php echo e($document7->name); ?></a>
                            <?php endif; ?>
                            <br>
                             <i> Jika dokumen penyata gaji lebih daripada satu, sila muat naik dibawah </i>
                                                    
                             <input id="fileupload10"  class="btn btn-default" type="file" name="file10" >
                            <input type="hidden" name="document10"  id="documentx10"  value="Salinan Penyata Gaji Untuk 3 Bulan Terkini (optional 1))">
                            &nbsp; <span id="document10"> </span> 

                            <?php if(!empty($document10->name)): ?>
                              <span id="document10a"><a target='_blank' class="hidden-xs hidden-sm" href="<?php echo e(url('/')); ?>/admin/downloaddocpdf/<?php echo e(str_replace('/', '', $pra->fullname)); ?>/<?php echo e($document10->upload); ?>"> <?php echo e($document10->name); ?></a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 

                               <a target='_blank' class="hidden-md hidden-lg" href="<?php echo e(url('/')); ?>/admin/get_document/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document10->upload); ?>"> <?php echo e($document10->name); ?></a>
                            <?php endif; ?>

                            <input id="fileupload11"  class="btn btn-default" type="file" name="file11" >
                            <input type="hidden" name="document11"  id="documentx11"  value="Salinan Penyata Gaji Untuk 3 Bulan Terkini (optional 2)">
                            &nbsp; <span id="document11"> </span> 
                            <?php if(!empty($document11->name)): ?>
                              <span id="document11a"><a target='_blank' class="hidden-xs hidden-sm" href="<?php echo e(url('/')); ?>/admin/downloaddocpdf/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document11->upload); ?>"> <?php echo e($document11->name); ?></a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                                <a target='_blank' class="hidden-md hidden-lg" href="<?php echo e(url('/')); ?>/admin/get_document/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document11->upload); ?>"> <?php echo e($document11->name); ?></a>
                            <?php endif; ?>
            
             
                                                    </div>
                                                </div>
                         
                                               
                                        
                         
                                               
                                                      &nbsp; <hr><br>
                                                      
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Settlement Letter/ <i>Penyata Penyelesaian Awal</i></label>
                                                    <div class="col-md-8">
                                                        <input id="fileupload1"  <?php if(empty($document1->name)): ?> <?php endif; ?>   class="btn btn-default" type="file" name="file1" >
                            <input type="hidden" name="document1"  id="documentx1"  value="Salinan Penyata Penyelesaian Awal">
                            &nbsp; <span id="document1"> </span> 
                            <?php if(!empty($document1->name)): ?>
                              <span id="document1a"><a target='_blank' class="hidden-xs hidden-sm" href="<?php echo e(url('/')); ?>/admin/downloaddocpdf/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document1->upload); ?>"> <?php echo e($document1->name); ?></a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                               <a target='_blank' class="hidden-md hidden-lg" href="<?php echo e(url('/')); ?>/admin/get_document/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document1->upload); ?>"> <?php echo e($document1->name); ?></a>
                            <?php endif; ?>
                            <br><br>
                           If more than one document, please upload below/ <br><i>Jika dokumen penyata penyelesaian awal lebih daripada satu, sila muat naik dibawah </i>
                                                    
                             <input id="fileupload2"  class="btn btn-default" type="file" name="file2" >
                            <input type="hidden" name="document2"  id="documentx2"  value="Salinan Penyata Penyelesaian Awal (optional 1)">
                            &nbsp; <span id="document2"> </span> 
                            <?php if(!empty($document2->name)): ?>
                              <span id="document2a"><a target='_blank' class="hidden-xs hidden-sm" href="<?php echo e(url('/')); ?>/admin/downloaddocpdf/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document2->upload); ?>"> <?php echo e($document2->name); ?></a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                              <a target='_blank' class="hidden-md hidden-lg" href="<?php echo e(url('/')); ?>/admin/get_document/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document2->upload); ?>"> <?php echo e($document2->name); ?></a>
                            <?php endif; ?>

                            <input id="fileupload3"  class="btn btn-default" type="file" name="file3" >
                            <input type="hidden" name="document3"  id="documentx3"  value="Salinan Penyata Penyelesaian Awal  (optional 2)">
                            &nbsp; <span id="document3"> </span> 
                            <?php if(!empty($document3->name)): ?>
                              <span id="document3a"><a target='_blank' class="hidden-xs hidden-sm" href="<?php echo e(url('/')); ?>/admin/downloaddocpdf/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document3->upload); ?>"> <?php echo e($document3->name); ?></a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                               <a target='_blank' class="hidden-md hidden-lg" href="<?php echo e(url('/')); ?>/admin/get_document/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document3->upload); ?>"> <?php echo e($document3->name); ?></a>
                            <?php endif; ?>
            
             
                                                    </div>
                                                </div>
                         
                                               
                                                      <br> &nbsp; </br>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Surat Pengesahan Majikan<sup>*</sup></label>
                                                    <div class="col-md-8">
                            <input id="fileupload9"  <?php if(empty($document9->name)): ?> required <?php endif; ?>  class="btn btn-default" type="file" name="file9"  >
                            <input type="hidden" name="document9"   id="documentx9"  value="Surat Pengesahan Majikan">
                            &nbsp; <span id="document9"> </span> 
                            <?php if(!empty($document9->name)): ?>
                              <span id="document9a"><a target='_blank' class="hidden-xs hidden-sm" href="<?php echo e(url('/')); ?>/admin/downloaddocpdf/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document9->upload); ?>"> <?php echo e($document9->name); ?></a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                               <a target='_blank' class="hidden-md hidden-lg" href="<?php echo e(url('/')); ?>/admin/get_document/<?php echo e(str_replace('/', '', $pra->id)); ?>/<?php echo e($document9->upload); ?>"> <?php echo e($document9->name); ?></a>
                            <?php endif; ?>
                                  
                                                    </div>
                                                </div>
                                            </div>
                                               
                                            </fieldset>
                                              
                                            </div>
                                             
                                            <div class="tab-pane" id="tab9">

                                                <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                                                <input name="id_praapplication" type="hidden" id="id_praapplication" value="<?php echo e($data->id_praapplication); ?>" >

                                                <input type="hidden" name="password" id="password_copy">
                                                <input type="hidden" autocomplete="false" name="email" value="<?php echo e($corres_email); ?>" id="email_copy">

                                                <input type='hidden' name='latitude' id='latitude' >
                                                <input type='hidden' name='longitude' id='longitude'>
                                                <input type='hidden' name='location' id='location' >




                                                <br>
                                                <h3><strong>Step 9</strong> -ROUTE TO MBSB/ <i>HANTAR KEPADA MBSB </i>  </h3>
                                                           
                                                   <div class="row">
                                                        <div class="col-lg-1">
                                                        </div>
                                                        <div align="justify" class="col-lg-10">
                                                                  <?php if($user->role=='5'): ?> 
                                                    <?php if($view=='verify'): ?>
                                                    
                                                       <div class="row">
                                                         <div class="col-md-5"><br>
                                                           <div class="form-group">
                                                               <label><b>Route to :</b></label>
                                                               <input type="hidden" name="_token" id="token_term" value="<?php echo e(csrf_token()); ?>">
                                                                 <input type="hidden" name="id_praapplication_term" id="id_praapplication_term" value="<?php echo e($pra->id); ?>">
                                                               <select placeholder="--Select Branch--" name="branch" id='branch' class=" select2" disabled="">
                                                               <option></option>
                                                                  <?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                     <?php if($branch->branch_code==$term->id_branch): ?> 
                                                                     <?php $selected ="selected"; ?>
                                                                  <?php else: ?>
                                                                      <?php $selected =" "; ?>
                                                                     <?php endif; ?>
                                                                     <option value='<?php echo e($branch->branch_code); ?>' <?php echo e($selected); ?> ><?php echo e($branch->branchname); ?></option>
                                                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                       
                                                               </select>
                                                            </div>
                                                            <div class="form-group">
                                                               <label><b>Remark / Reason :</b></label>
                                                                <textarea disabled="" id='remark_verification' class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 183px;" name='verification_remark'><?php echo e($term->verification_remark); ?></textarea>       
                                                           </div>
                                                            
                                                            
                                                        </div>
                                                      </div>
                                                            <div class="row">
                                                               <div class="col-sm-3">
                                                                   <a href='<?php echo e(url('/admin')); ?>' class="btn btn-primary" >
                                                                  &nbsp; << Back to Dashboard &nbsp; &nbsp;
                                                                  </a> 
                                                               </div>
                                                              
                                                               <!--<div class="col-sm-2">
                                                                  <a id="waiting1" class="btn btn-warning" >
                                                                  Waiting User Response
                                                                  </a> 
                                                               </div>
                                                                  <div class="col-sm-2">
                                                                  <a id="waiting2" class="btn btn-warning" >
                                                                  Waiting Document
                                                                  </a> 
                                                               </div>-->
                                                            </div>
                                                         
                                                          <?php else: ?>

                                                  
                                                      <div class="row">
                                                         <div class="col-md-4"><br>
                                                             <?php if($term->id_branch=='0'): ?>  
                                                              <p><b>This Application has not been routed</b></p>
                                                             <?php else: ?>
                                                              <div class="form-group">
                                                               <label><b>Branch Route</b></label>
                                                               <select disabled name="branch" id='branch' class=" select2">
                                                                  <option value=''>--Select Branch--</option>
                                                                  <?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                     <?php if($branch->id==$term->id_branch): ?> 
                                                                      <?php $selected ="selected"; ?>
                                                                      <?php else: ?>
                                                                      <?php $selected =" "; ?>
                                                                     <?php endif; ?>
                                                                     <option value='<?php echo e($branch->id); ?>' <?php echo e($selected); ?> ><?php echo e($branch->branchname); ?></option>
                                                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                       
                                                               </select>
                                                            </div>
                                                                <?php endif; ?>
                                                              <div class="form-group">
                                                               <label><b>Remark / Reason :</b></label>
                                                                <textarea disabled id='remark_verification' class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 183px;" name='verification_remark'><?php echo e($term->verification_remark); ?></textarea>       
                                                           </div>
                                                            
                                                            
                                                        </div>
                                                      </div>
                                                            <div class="row">
                                                               <div class="col-sm-2">
                                                                  <a href='<?php echo e(url('/admin')); ?>' class="btn btn-primary" >
                                                                  &nbsp; << Back to Dashboard &nbsp; &nbsp;
                                                                  </a> 
                                                               </div>
                                                            
                                                            </div>
                                                         <?php endif; ?>
                                                         <?php endif; ?>
                                                            <!--<table border="1">
                                                                <tr>
                                                                    <td class="border" align="left" bgcolor="#0055a5">  
                                                                        <font color="white"> <b>7.1) PURCHASE APPLICATION AND PROMISE TO BUY</b>/ <i>PERMOHONAN BELIAN DAN AKUJANJI UNTUK MEMBELI </i></font>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="border">
                                                                        <b><br>Upon approval of financing and my acceptance to the Terms and Conditions, I hereby order and request MBSB to purchase commodity with the purchase price that will be approved by MBSB as per SMS 'Aqad and promise to buy the same commodity at cost plus profit, depending on the financing facility and I will be held responsible for any violation to the agreement.</b><br> 
                                                                        <i>Setelah mendapat kelulusan pembiayaan dan penerimaan saya kepada Terma dan Syarat, saya dengan ini membuat pesanan dan memohon MBSB untuk membeli komoditi dengan harga belian yang akan diluluskan oleh MBSB seperti tertera di dalam 'Aqad SMS dan berjanji untuk membeli komoditi tersebut pada kos tambah keuntungan, bergantung kepada kelulusan pembiayaan, di mana saya bertanggungjawab sepenuhnya akibat mengingkari perjanjian ini.</i><br><br>
                                                                    <table width="90%" border="0">
                                                                         <tr>
                                                                             <td valign="top" width="40%">
                                                                                 <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Name</b><br>
                                                                                            <i>Nama</i>
                                                                                        </td>
                                                                                         <td>
                                                                                            <input type="text" disabled="" class="form-control" value="<?php echo e($data->name); ?>" name="" disabled/>
                                                                                        </td>

                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td>
                                                                                             <b>MyKad No.</b><br>
                                                                                            <i>No. MyKad</i>
                                                                                         </td>
                                                                                         <td>
                                                                                            <input type="text" disabled="" class="form-control" value="<?php echo e($data->new_ic); ?>" name="" disabled/>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td>
                                                                                             <b>Date.</b><br>
                                                                                            <i>Tarikh</i>
                                                                                         </td>
                                                                                          <td>

                                                                                            <input type="text" disabled="" class="form-control" value="<?php echo e($today); ?>" name="" disabled/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                             </td>
                                                                             <td valign="top">
                                                                                    <table border="1" width="500" height="100">
                                                                                        <tr>
                                                                                            <td align="center"><h2>
                                                                                                <input type="checkbox" value="1" name="purchase_application" <?php if($term->purchase_application>0): ?> checked <?php endif; ?>    ><b> I Agree</b> / Saya Bersetuju</h2>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                             </td>
                                                                         </tr>
                                                                    </table>
                                                                       <br>


                                                                       
                                                                    
                                                                                
                                                                    </td>
                                                                </tr>
                                                            </table>-->

                                                          <!-- APPOINTMENT --><br>

                                                           <!-- <table border="1">
                                                          <tr>
                                                          <td class="border" align="left" bgcolor="#0055a5">  <font color="white"> <b>7.2) APPOINTMENT OF MBSB AS A SALES AGENT</b> / <i>PERLANTIKAN MBSB SEBAGAI EJEN JUALAN</i></font></td>
                                                          </tr>
                                                          <tr>
                                                        
                                                          <td class="border">

                                                          <b><br>Subject to MBSB’s acceptance, I hereby irrevocably and unconditionally appoint MBSB as my agent under the shariah contract of Wakalah to sell the commodities to any third party purchaser/commodity trader as MBSB may deem fit and in accordance with such terms acceptable to MBSB. I shall be bound by any contract or agreement that MBSB may enter into with the said third party purchaser/trader for the purpose of the sale of the commodities on my behalf. I hereby agree to pay MBSB a sum of RM36.04 (inclusive of Goods and Services Tax) as Wakalah Fee. I hereby undertake to indemnify MBSB to make good in full all losses, costs and expenses resulting from any claims, proceedings, actions, requests or any form of damages that MBSB may suffer or incur as a result of fulfilling MBSB’s agency function as set out above. </b><br> 
                                                          <i>Tertakluk kepada penerimaan MBSB , saya dengan ini secara tidak boleh batal dan tanpa syarat melantik MBSB sebagai ejen saya di bawah kontrak syariah Wakalah untuk menjual komoditi kepada mana-mana pembeli / peniaga komoditi pihak ketiga sebagaimana yang difikirkan patut oleh MBSB dan mengikut apa-apa terma yang diterima oleh MBSB. Saya akan terikat dengan mana-mana kontrak atau perjanjian yang dibuat oleh MBSB dengan pembeli / peniaga komoditi pihak ketiga tersebut bagi tujuan penjualan komoditi bagi pihak saya. Saya dengan ini bersetuju untuk membayar MBSB sebanyak <b>RM36.04</b>  (termasuk Cukai Barangan dan Perkhidmatan) sebagai fi Wakalah. Saya dengan ini mengaku janji untuk menanggung segala kerugian MBSB dengan membayar sepenuhnya semua kerugian, kos dan perbelanjaan yang timbul daripada apa-apa tuntutan, prosiding, tindakan, permintaan atau apa-apa bentuk kerosakan yang mungkin dialami atau ditanggung oleh MBSB akibat memenuhi fungsi agensi seperti yang dinyatakan di atas.</i><br><br>

                                                        <table width="90%" border="0">
                                                             <tr>
                                                                 <td valign="top" width="40%">

                                                                   <table>
                                                                        <tr>
                                                                            <td>
                                                                                <b>Name</b><br>
                                                                                <i>Nama</i>
                                                                            </td>
                                                                             <td>
                                                                                <input type="text" disabled="" class="form-control" value="<?php echo e($data->name); ?>" name="" disabled/>
                                                                            </td>

                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>MyKad No.</b><br>
                                                                                <i>No. MyKad</i>
                                                                             </td>
                                                                             <td>
                                                                                <input type="text" disabled="" class="form-control" value="<?php echo e($data->new_ic); ?>" name="" disabled/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>Date.</b><br>
                                                                                <i>Tarikh</i>
                                                                             </td>
                                                                              <td>

                                                                                <input type="text" disabled="" class="form-control" value="<?php echo e($today); ?>" name="" disabled/>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <table border="1" width="500" height="100">
                                                                            <tr>
                                                                                <td align="center"><h2><input type="checkbox" name="appointment_mbsb" value="1" <?php if($term->appointment_mbsb>0): ?> checked <?php endif; ?>  ><b> I Agree</b> / Saya Bersetuju</h2>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                  </td>
                                                              </tr>
                                                          </table>

                                                            </td>

                                                        
                                                          </tr>
                                                          </table>

                                                        <br>
                                                          
                                                      <table border="1">
                                                          <tr>
                                                          <td class="border" align="left" bgcolor="#0055a5">  <font color="white"> <b>7.3) DECLARATION/DISCLOSURE BY APPLICANT/CO-APPLICANT/GUARANTOR </b> / <i>PERAKUAN/PENDEDAHAN OLEH PEMOHON/PEMOHON BERSAMA/PENJAMIN</i></font></td>
                                                          </tr>
                                                          <tr>
                                                        
                                                          <td class="border">
                                                        <ol type='1'>
                                                          <li> <b>I/We hereby declare that I/we am/are NOT a bankrupt.<br></b>
                                                                <i> Saya/Kami mengesahkan bahawa saya/kami TIDAK muflis. 
                                                                <br></i><br>
                                                          </li>
                                                          <li>
                                                                 <b> I/We declare that the information furnished in this form are completely true and accurate and I/we have not withheld any information which may prejudice my/our financing application or have a bearing on your financing decision.</b> <br><i>Saya/Kami mengesahkan bahawa maklumat yang disediakan di dalam borang ini adalah benar, tepat, dan lengkap dan saya/kami tidak menyembunyikan sebarang maklumat yang mungkin prejudis terhadap 
                                                                 permohonan pembiayaan saya/kami atau mempunyai kesan ke atas keputusan 
                                                                 pembiayaan anda.<br></i><br></li>
                                                          <li> <b> I/We hereby authorize you/your representative to obtain the relevant information relating to this application from any relevant source as deemed suitable by MBSB but not limited to any bureaus or agencies established by Bank Negara Malaysia (”BNM”) or other parties. The authorization to obtain the relevant information is also extended to prospective guarantors, security providers, authorized depository agent and other party relating to this application as deemed necessary by MBSB.</b><br><i>Saya/Kami memberi kebenaran kepada MBSB untuk mendapatkan maklumat yang relevan terhadap permohonan ini dari sumber-sumber yang relevan dan yang dianggap sesuai oleh MBSB termasuk dan tidak terhad kepada mana-mana biro atau agensi yang ditubuhkan oleh Bank Negara Malaysia (”BNM”) atau pihak yang lain. Kebenaran untuk mendapatkan maklumat yang relevan juga merangkumi bakal penjamin dan/atau pemberi sekuriti dan mana-mana wakil penyimpan yang dibenarkan dan pihak yang berkenaan dengan permohonan ini yang dianggap sesuai oleh MBSB. <br></i><br> </li>

                                                          <li> <b>I/We understand that MBSB reserves the absolute right to approve or decline this application as MBSB deems fit without assigning any reason.</b><br>
                                                          <i>Saya/Kami faham bahawa pihak MBSB mempunyai hak mutlak untuk meluluskan atau menolak permohonan tanpa menyatakan sebarang alasan. <br></i><br></li>

                                                          <li> <b>I/We expressly irrevocably conselt and authorize MBSB to funish all relevant information relating to or arising from or in connection with financing facilities to any subsidiary companies of MBSB, its agents and/or such person or BNM, Cagamas Berhad and debt collection agents or such other authority or body established by BNM, or such other authority having jurisdiction over MBSB as MBSB may absolutely deem fit and such other authority as may be authorized by law. </b><br> <i>Saya/Kami bersetuju tanpa hak menarik balik dan tanpa syarat memberi kebenaran kepada MBSB untuk mendedahkan sebarang maklumat yang diperlukan berkaitan dengan atau berbangkit daripada apa-apa hubungan dengan kemudahan pembiayaan kepada mana-mana anak syarikat MBSB, ejennya, dan/atau mana-mana individu atau BNM, Cagamas Berhad dan ejen pemungut hutang atau mana-mana pihak berkuasa lain yang mempunyai bidang kuasa ke atas MBSB di mana MBSB secara mutlak berhak memberi kata putus dan mana-mana pihak berkuasa yang dibenarkan selaras dengan undang-undang. <br></i><br></li>

                                                          <li><b>I/We hereby further expressly irrevocably consent and authorise MBSB to seek any information concerning me/us with or from any credit reference/reporting agencies, including but not limited to CCRIS, CTOS, RAMCI, FIS and/or Inland Revenue Authorities or any authority as MBSB may from time to time deem fit.</b> <br> <i>Saya/Kami bersetuju tanpa hak menarik balik dan tanpa syarat memberi kebenaran kepada MBSB untuk mendapatkan sebarang maklumat berkaitan Saya/Kami daripada mana-mana agensi rujukan kredit, termasuk dan tidak terhad kepada CCRIS, CTOS, RAMCI, FIS dan/atau Lembaga Hasil Dalam Negeri atau mana-mana pihak berkuasa yang  diputuskan oleh MBSB dari masa ke semasa.<br></i><br></li>

                                                          <li><b>I/We also acknowledge that it is a requirement that all information relating to this application must be transmitted and/or updated to the Central Credit Reference Information System ("CCRIS"), a database maintained by BNM as and when necessary.</b> <br> <i>Saya/Kami juga mengesahkan bahawa semua maklumat berkaitan permohonan ini mestilah dimaklumkan dan/atau dikemaskini kepada Sistem Maklumat Rujukan Kredit Pusat ("CCRIS"), pengkalan data yang diuruskan oleh BNM apabila perlu.<br></i><br></li>

                                                          <li><b>I/We shall comply with MBSB's requirements in respect of my/our application and I/we understand that MBSB's offer of the financing shall be subject to MBSB performing the necessary verification.</b> <br> <i>Saya/Kami akan mematuhi segala keperluan MBSB untuk permohonan saya/kami dan saya/kami memahami bahawa tawaran pembiayaan oleh MBSB adalah tertakluk kepada pengesahan yang diperlukan oleh MBSB<br></i><br></li>

                                                          <li><b>I/We hereby undertake to notify and/or inform the emergency contact person and my/our spouse that their personal data has been provided to MBSB and undertake to indemnify and hold MBSB harmless in the event of any legal repercussions arising from my/our failure and/or to notify the said emergency contact person/spouse. </b><br> <i>Saya/Kami dengan ini bersetuju untuk memberitahu dan/atau menghubungi oenama rujukan kecemasan dan suami isteri bahawa data peribadi mereka telah diberi kepada MBSB dan berjanji tudak akan mengambil sebarang tindakan ke atas MBSB sekiranya timbul sebarang tindakan undang-undang daripada kegagalan saya/kami untuk memberitahu dan/atau menghubungi penama rujukan kecemasan/suami isteri.<br></i><br></li>

                                                          <li><b>This application form and all supporting documents that were submitted to MBSB shall be the sole property of MBSB and MBSB is entitled to retain the same irrespective of whether my/our application is approved or rejected by MBSB. </b><br> <i>Borang permohonan ini dan semua dokumen sokongan yang telah diserahkan kepada MBSB adalah hak milik mutlak MBSB dan MBSB berhak untuk mengekalkan semua dokumen tanpa mengira samaada permohonan saya/kami diluluskan atau ditolak oleh MBSB.<br></i><br></li>

                                                          <li><b>I/We hereby irrevocably agree to waive my/our rights to a refund where the amount is not exceeding RM5.00 arising from but not limitted  to early settlement or closure of my/our financing account. I/We further consent and authorize MBSB to donate the said amount to charitable organisations deemed appropriate by MBSB.</b><br> <i>Saya/Kami bersetuju tanpa hak menarik balik bagi bayaran balik di mana jumlahnya tidak melebihi RM5.00, hasil daripada tetapi tidak terhad kepada penyelesaian awal atau penutupan akaun pembiayaan saya/kami. Saya/Kami seterusnya membenar dan memberi kuasa kepada MBSB untuk menderma jumlah tersebut kepada badan-badan kebajikan yang difikirkan wajar oleh pihak MBSB.<br></i><br></li>

                                                        </ol>
                                                         
                                                         
                                                         
                                                         
                                                         
                                                          
                                                          
                                                          
                                                          
                                                         
                                                          </td>
                                                            </tr>
                                                          </table>-->


                                                          <!-- PERMISSION TO DEDUCT --><br>

                                                          <!-- <table border="1">
                                                          <tr>
                                                            <td class="border" align="left" bgcolor="#0055a5">  <font color="white"><b>7.4) PERMISSION TO DEDUCT FROM PERSONAL FINANCING-i FACILITY</b> / <i>KEBENARAN PEMOTONGAN DARI KEMUDAHAN PEMBIAYAAN PERIBADI-i</i></font></td>
                                                          </tr>
                                                          <tr>
                                                        
                                                          <td>
                                                            <ol type="1">

                                                                <li>
                                                                     <b>I hereby authorize and allow MBSB to deduct and pay directly from the financing amount approved by MBSB, the following :</b><br>
                                                                     <i>Saya dengan ini memberi kuasa dan membenarkan MBSB melakukan pemotongan / pembayaran secara terus daripada jumlah pembiayaan yang diluluskan oleh MBSB, seperti berikut:</i></li>
                                                                      <ol type="i">
                                                                        <li><b>Security Deposit</b>/<i> Deposit Sekuriti</i>.</li>
                                                                        <li><b> GCTT Takaful Contribution (if applicable)</b>/<i> Sumbangan Takaful GCTT (sekiranya berkenaan)</i>.</li>
                                                                        <li><b>Wakalah Fee</b>/<i> Fi Wakalah</i>.</li>
                                                                        <li><b>Redemption for other financing (if applicable)</b>/<i> Penyelesaian pembiayaan lain (sekiranya berkenaan)</i>.</li>
                                                                        <li><b>Banca Takaful Product (if applicable)</b>/<i> Produk Banca Takaful (sekiranya berkenaan)</i>.</li>
                                                                        <li><b>Interbank Giro (IBG) Fee</b>/<i> Fi Giro Antara Bank (IBG)</i>.</li>
                                                                        <li><b>Other charges (if applicable)</b>/<i> Lain-lain caj (sekiranya berkenaan)</i>.</li>
                                                                    </ol><br>
                                                                </li>

                                                                <li> 
                                                                    <b>The deductions to be made are subject to the package taken by me and based on the terms imprinted in the SMS 'aqad.</b>
                                                                    <br><i>Potongan yang akan dibuat adalah tertakluk kepada pakej yang diambil oleh saya dan sebagaimana yang tertera didalam `aqad SMS.</i><br><br>
                                                                </li>

                                                                <li>
                                                                    <b>I also agree that the net financing amount (after deductions) will be paid by MBSB to me by crediting my account details of which I have provided or by any other mode deemed suitable by MBSB.</b>
                                                                    <br> <i>Saya juga bersetuju bahawa baki amaun pembiayaan di atas (selepas potongan) akan dibayar oleh MBSB kepada saya dengan mengkreditkan akaun saya mengikut butiran yang telah disertakan atau melalui apa-apa cara yang difikirkan sesuai oleh MBSB.</i><br><br>
                                                                </li>

                                                                <li>
                                                                    <b>I also confirm and undertake that this authorization is irrevocable in the event the amount to settle other debts has been paid to any third on my behalf by MBSB  and I further agree and undertake to pay all indebtedness owing to MBSB in the event I cancel the facility.</b>
                                                                    <br> <i>Saya juga dengan ini akujanji bahawa kebenaran ini adalah kebenaran tidak boleh batal sekiranya kesemua amaun tebus hutang telah pun dibayar kepada mana-mana pihak ketiga bagi pihak saya oleh MBSB dan saya juga bersetuju serta berakujanji akan membayar kesemua amaun tebus hutang tersebut kepada MBSB jika saya membatalkan kemudahan tersebut.  </i><br><br>
                                                                </li>

                                                                <li>
                                                                    <b>I shall be responsible to pay in full any cost incurred. I hereby further undertake to keep MBSB fully indemnified and make good in full all losses, costs and expenses resulting from any claims, proceedings, actions, requests or any form of damages that MBSB may suffer or incur as a result of fulfilling its function as set out above.</b>
                                                                    <br><i>Saya akan bertanggungjawab untuk membayar sepenuhnya semua kos yang ditanggung. Saya juga mengaku janji akan menanggung segala kerugian MBSB dengan membayar sepenuhnya  semua kerugian, kos dan perbelanjaan yang timbul daripada apa-apa tuntutan, prosiding, tindakan, permintaan atau apa-apa bentuk kerosakan yang mungkin dialami atau ditanggung oleh MBSB akibat memenuhi fungsi seperti yang dinyatakan di atas.</i>
                                                                </li>

                                                            </ol>
                                                        
                                                         </td>
                                                        </tr>
                                                      </table>-->

                                                       <!-- Credit Transactions --><br>

                                                        <!--<table border="1" >
                                                          <tr>
                                                                <td class="border" align="left" bgcolor="#0055a5">  <font color="white"><b>7.5) CREDIT TRANSACTIONS AND EXPOSURES WITH CONNECTED PARTIES/ </b>/ <i>TRANSAKSI KREDIT DAN PENDEDAHAN DENGAN PIHAK BERKAITAN </i></font> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="border">
                                                                    <b><br> Do you have any immediate family or close relatives (including parents, brother/sister and their spouses, dependent's spouse and own/step/adopted child) that are employees of MBSB?</b><br> 
                                                                    <i>Adakah anda mempunyai ahli keluarga atau saudara terdekat (termasuk ibubapa, abang/kakak/adik dan pasangan, pasangan dibawah tanggungan, anak dan saudara tiri/angkat) yang sedang bekerja dengan MBSB? </i><br><br>
                                                                     <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" id="credit_yes" required name="credit_transactions" value="1" <?php if($term->credit_transactions>0): ?> checked <?php endif; ?> >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" id="credit_no" required name="credit_transactions" value="0" <?php if($term->credit_transactions==0): ?> checked <?php endif; ?> >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                    <br>
                                                                    <b>If Yes, please complete the information below:/</b><i> Jika Ya, sila lengkapkan maklumat dibawah:</i><br><br>
                                                                    <table width="40%">
                                                                        <tr>
                                                                            <td>
                                                                                <b>Full Name</b><br>
                                                                                <i>Nama Penuh</i>
                                                                            </td>
                                                                             <td>
                                                                                <input type="text" disabled="" class="form-control" id="fullname" name="fullname" <?php if(!empty($credit->fullname)): ?>  value="<?php echo e($credit->fullname); ?>" <?php endif; ?> />
                                                                            </td>

                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>MyKad No.</b><br>
                                                                                <i>No. MyKad</i>
                                                                             </td>
                                                                             <td>
                                                                                <input type="text" disabled="" id="mykad" class="form-control" <?php if(!empty($credit->mykad)): ?>   value="<?php echo e($credit->mykad); ?>" <?php endif; ?>  name="mykad" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>Passport No.</b><br>
                                                                                <i>No. Passport</i>
                                                                             </td>
                                                                              <td>

                                                                                <input type="text" disabled="" class="form-control" id="passport" <?php if(!empty($credit->passport)): ?>  value="<?php echo e($credit->passport); ?>" <?php endif; ?> name="passport" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>Relationship</b><br>
                                                                                <i>Hubungan</i>
                                                                             </td>
                                                                              <td>

                                                                                <input type="text" disabled="" id="relationship" class="form-control" <?php if(!empty($credit->relationship)): ?>   value="<?php echo e($credit->relationship); ?>" <?php endif; ?>  name="relationship" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                              
                                                            </tr>
                                                        </table>-->


                                                          <!-- Consent --><br>

                                                       <!-- <table border="1" >
                                                          <tr>
                                                                <td align="left" class="border" bgcolor="#0055a5">  <font color="white"><b>7.6) CONSENT FOR CROSS-SELLING, MARKETING, PROMOTIONS, ETC</b>/ <i>PERSETUJUAN UNTUK JUALAN SILANG, PEMASARAN, PROMOSI DAN LAIN-LAIN </i></font> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="border">
                                                                    <b><br> I/ee expressly consent and authorize MBSB to process any information that I/we have provided to MBSB for the purposes of cross-selling, marketing and promotions including disclosure to its strategic partners or such persons or third parties as MBSB deem fit.</b><br> 
                                                                    <i>Saya/Kami mengesahkan bahawa saya/kami memberi kebenaran dan kuasa kepada MBSB yang tidak boleh dibatal tanpa kebenaran untuk mendedahkan sebarang maklumat yang telah saya/kami kemukakan kepada MBSB bagi tujuan jualan silang, pemasaran dan promosi termasuk pendedahan kepada rakan strategik atau mana-mana individu atau pihak ketiga yang difikirkan wajar oleh MBSB.</i><br><br>
                                                                     <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" name="consent_for" id="consent_for_yes" value="1" <?php if($term->consent_for>0): ?> checked <?php endif; ?> >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" name="consent_for" id="consent_for_no" value="0" <?php if($term->consent_for==0): ?> checked <?php endif; ?> >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                              
                                                            </tr>
                                                        </table>-->

                                                        <!-- High Networth --><br>

                                                       <!-- <table border="1"> 
                                                            <tr>
                                                                 <td align="left" class="border" bgcolor="#0055a5">  <font color="white">  <b>7.7) HIGH NETWORTH INDIVIDUAL CUSTOMER ("HNWI")</b>/ <i>INDIVIDU YANG BERPENDAPATAN TINGGI ("HNWI") </i></font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border">
                                                                    <br> <b> HNWI means an individual whose total net personal assets, or total net joint assets with his or her spouse, exceeds RM3 million or its equivalent in foreign currencies, excluding the value of the individual's primary residence. Calculation of HNWI is total asset less total liabilities.</b><br> 
                                                                    <i>HNWI bermaksud seseorang individu di mana jumlah bersih aset-aset peribadi, atau jumlah bersih aset-aset bersama dengan pasangan, melebihi RM3 juta atau yang setaraf dengannya dalam mata awang asing, tidak termasuk nilai kediaman utama individu tersebut. Pengiraan HNWI adalah berdasarkan jumlah keseluruhan aset tolak jumlah keseluruhan liabiliti.</i><br>

                                                                    <br> <b>Does your total net personal assets or total net joint assets with your spouse exceeds RM3 million?</b></br>
                                                                    <i>Adakah jumlah bersih aset-aset peribadi anda atau jumlah bersih aset bersama dengan pasangan anda melebihi RM3 juta?. </i><br><br>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" name="high_networth" id="high_networth_yes" value="1" <?php if($term->high_networth>0): ?> checked <?php endif; ?> >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" name="high_networth" id="high_networth_no" value="0" <?php if($term->high_networth==0): ?> checked <?php endif; ?> >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                           
                                                          </table>-->

                                                          <!-- POLITICALY EXPOSED PERSON --><br>

                                                       <!-- <table border="1"> 
                                                            <tr>
                                                                 <td align="left" class="border" bgcolor="#0055a5">  <font color="white">  <b>7.8) POLITICALLY EXPOSED PERSON ("PEP")</b>/ <i> INDIVIDU BERKAITAN POLITIK ("PEP") </i></font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border">
                                                                    <br> <b> PEP - Individuals who are or who have been entrusted with prominent public functions domestically or internationally. Family members of PEPs are defined as those who may be expected to influence or be influenced by that PEP, as well as dependents of the PEP. This includes the PEP’s:</b><br> 
                                                                    <i>PEP – seseorang Individu yang diamanahkan dengan “Fungsi Awam Yang Penting” samaada domestik atau antarabangsa. Ahli keluarga PEP adalah ditakrifkan sebagai mereka yang dijangka boleh mempengaruhi atau dipengaruhi oleh PEP tersebut dan juga tanggungan PEP. Ianya termasuk:</i><br>
                                                                    <ol type="i">
                                                                        <li>
                                                                            <b>Spouse and dependents of the spouse;</b><br><i>Pasangan suami atau isteri berserta tanggungannya;</i>
                                                                        </li>
                                                                        <li>
                                                                            <b>Child (including step children and adopted children) and spouse of the child;</b><br>
                                                                            <i>Anak (termasuk anak tiri atau anak angkat yang sah) berserta pasangan suami atau isteri kepada anak-anak tersebut;</i>
                                                                        </li>
                                                                        <li>
                                                                            <b>Parent; and</b><br>
                                                                            <i>Ibu bapa; dan</i>
                                                                        </li>
                                                                        <li>
                                                                            <b>Brother or sister and their spouses.</b><br>
                                                                            <i>Adik beradik berserta pasangan suami atau isteri mereka. </i>
                                                                        </li>
                                                                    </ol>

                                                                    <b>Definition of Related Closed Associates of PEPs:/</b> Definisi Kenalan-Kenalan yang Berkait Rapat dengan PEPs:
                                                                    <ul>
                                                                        <li>
                                                                            <b>Related close associate to PEP is defined as individual who is closely connected to a PEP, either socially or professionally.</b><br>
                                                                            <i>Kenalan-kenalan yang berkait rapat dengan PEP ditakrifkan sebagai individu yang saling berhubung dan berkait rapat dengan PEP, samada secara sosial atau profesional. </i>
                                                                        </li>
                                                                    </ul><br>
                                                                     <b>For Individual</b> / <i>Untuk Individu:</i>
                                                                     <ol type="1">
                                                                        <li>
                                                                            <b>Are you a PEP or a Family Member(s) of PEP or a Related Close Associate(s) of PEP?</b>
                                                                            <br>
                                                                            <i>Adakah anda seorang PEP atau ahli keluarga PEP atau kenalan-kenalan berkait rapat dengan PEP?</i>
                                                                             <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="radio" name="politically" value="1" id="politically_yes" <?php if($term->politically>0): ?> checked <?php endif; ?> >
                                                                                    </td>
                                                                                    <td><b>Yes</b>/Ya</td>
                                                                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                                                                    <td>
                                                                                        <input type="radio" name="politically" id="politically_no" value="0" <?php if($term->politically==0): ?> checked <?php endif; ?> >
                                                                                    </td>
                                                                                    <td><b>No</b>/Tidak</td>
                                                                                </tr>
                                                                            </table><br>
                                                                            <b>If Yes, please complete the information below:/</b><i> Jika Ya, sila lengkapkan maklumat di bawah:</i><br>
                                                                            <table border="1" width="100%">
                                                                                <tr align="center">
                                                                                    <td width="5%">No.</td>
                                                                                    <td><b>Name</b><br><i>Nama</i></td>
                                                                                    <td><b>Relationship with customer</b><br>
                                                                                    <i>Hubungan dengan Pelanggan</i></td>
                                                                                    <td><b>Status**</b><br><i>Status**</i></td>
                                                                                    <td><b>Prominent Public Position</b><br><i>Kedudukan Awam yang Penting</i></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="no1" value="1" readonly>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="name1" id="pep_name1" <?php if(!empty($pep->name1)): ?>  value="<?php echo e($pep->name1); ?>" <?php endif; ?>>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="relationship1" id="pep_relationship1" <?php if(!empty($pep->relationship1)): ?>  value="<?php echo e($pep->relationship1); ?>" <?php endif; ?>>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="status1" id="pep_status1" <?php if(!empty($pep->status1)): ?>  value="<?php echo e($pep->status1); ?>" <?php endif; ?>>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="prominent1" id="pep_prominent1" <?php if(!empty($pep->prominent1)): ?>  value="<?php echo e($pep->prominent1); ?>" <?php endif; ?>>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="no2" value="2" readonly>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="name2" id="pep_name2"  <?php if(!empty($pep->name2)): ?>  value="<?php echo e($pep->name2); ?>" <?php endif; ?>>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="relationship2" id="pep_relationship2"<?php if(!empty($pep->relationship2)): ?>  value="<?php echo e($pep->relationship2); ?>" <?php endif; ?>>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="status2"  id="pep_status2"<?php if(!empty($pep->status2)): ?>  value="<?php echo e($pep->status2); ?>" <?php endif; ?>>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="prominent2"  id="pep_prominent2"<?php if(!empty($pep->prominent2)): ?>  value="<?php echo e($pep->prominent2); ?>" <?php endif; ?>>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="no3" value="3" readonly>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="name3"  id="pep_name3" <?php if(!empty($pep->name3)): ?>  value="<?php echo e($pep->name3); ?>" <?php endif; ?>>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="relationship3"  id="pep_relationship3" <?php if(!empty($pep->relationship3)): ?>  value="<?php echo e($pep->relationship3); ?>" <?php endif; ?>>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="pep_status3"   id="status3" <?php if(!empty($pep->status3)): ?>  value="<?php echo e($pep->status3); ?>" <?php endif; ?>>
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" disabled="" class="form-control" name="prominent3"   id="pep_prominent3" <?php if(!empty($pep->prominent3)): ?>  value="<?php echo e($pep->prominent3); ?>" <?php endif; ?>>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <b>** Currently holding/ is actively seeking/ is being considered/ Previously holding</b><br><i>** Memegang jawatan buat masa ini/ masih aktif mencari/ dalam pertimbangan/ memegang jawatan sebelum ini</i>

                                                                        </li>
                                                                     </ol>
                                                                </td>
                                                            </tr>

                                                           
                                                          </table>-->

                                                          <!-- FOR GOODS AND SERVICES--><br>

                                                        <!--<table border="1"> 
                                                            <tr>
                                                                 <td align="left"  class="border" bgcolor="#0055a5">  <font color="white">  <b>7.9) FOR GOODS AND SERVICES TAX ("GST") EFFECTIVE 1 APRIL 2015</b>/ <i>UNTUK CUKAI BARANGAN DAN PERKHIDMATAN (“CBP”) BERKUATKUASA 1 APRIL 2015 </i></font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border">
                                                                    <br> <b>I/We hereby agree that I/We shall be liable for all Goods and Services Tax (GST) payable in connection with this application or any account or any service in connection therein and MBSB shall be authorized to debit my/our account for the same.</b><br> 
                                                                    <i>Saya/Kami bersetuju bahawa saya/kami akan bertanggungjawab ke atas Cukai Barangan dan Perkhidmatan (“CBP”) yang berkaitan dengan permohonan ini atau mana-mana akaun atau apa-apa perkhidmatan yang berkaitan dan MBSB dibenarkan untuk mendebit akaun saya/kami bagi tujuan yang sama.</i><br>
                                                                   
                                                                </td>
                                                            </tr>

                                                           
                                                          </table>-->


                                                          <!-- FOR GOODS AND SERVICES--><br>

                                                        <!--<table border="1"> 
                                                            <tr>
                                                                 <td class="border" align="left" bgcolor="#0055a5">  <font color="white">  <b>7.10) PRODUCT DISCLOSURE SHEET</b>/ <i>LEMBARAN PENJELASAN PRODUK</i></font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border">
                                                                    <br> <b>I/We hereby declare that I/ we have been briefed on the information contained in the Product Disclosure Sheet that has been given to me/us for the product applied herein</b><br> 
                                                                    <i>Saya/Kami mengesahkan bahawa saya/kami telah diberitahu mengenai maklumat yang terkandung di dalam Lembaran Penjelasan Produk yang telah diberikan kepada saya/kami berkaitan dengan produk yang dipohon di sini.</i><br><br>
                                                                    <table width="90%" border="0">
                                                                         <tr>
                                                                             <td valign="top" width="40%">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Name</b><br>
                                                                                            <i>Nama</i>
                                                                                        </td>
                                                                                         <td>
                                                                                            <input type="text" disabled="" class="form-control" value="<?php echo e($data->name); ?>" name="" disabled/>
                                                                                        </td>

                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td>
                                                                                             <b>MyKad No.</b><br>
                                                                                            <i>No. MyKad</i>
                                                                                         </td>
                                                                                         <td>
                                                                                            <input type="text" disabled="" class="form-control" value="<?php echo e($data->new_ic); ?>" name="" disabled/>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td>
                                                                                             <b>Date.</b><br>
                                                                                            <i>Tarikh</i>
                                                                                         </td>
                                                                                          <td>

                                                                                            <input type="text" disabled="" class="form-control" value="<?php echo e($today); ?>" name="" disabled/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                             <td valign="top">
                                                                                    <table border="1" width="500" height="100">
                                                                                        <tr>
                                                                                            <td align="center"><h2>
                                                                                                 <input type="checkbox" value="1" name="product_disclosure" <?php if($term->product_disclosure>0): ?> checked <?php endif; ?> > <b>I Agree</b> / Saya Bersetuju</h2>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                   

                                                                   
                                                                </td>
                                                            </tr>

                                                           
                                                          </table>-->


                                                           


                                                          </div>
                                                            <div class="col-lg-1"></div>

                                                          </div>

                                                          <div class="row">
                                                          <div class="col-lg-1">
                                                          </div>
                                                            <div align="justify" class="col-lg-10">

                                                   
                                                      

                                                          </div>
                                                          <div class="col-lg-1">
                                                          </div>
                                                           </div>


                                                     
                                                </div>


                                                      

                                                     
                
                                                       <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <ul class="pager wizard no-margin">
                                                                        <!--<li class="previous first ">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                                                        </li>-->
                                                                        <li class="next">
                                                                            <a href="javascript:void(0);" class="btn btn-lg txt-color-blue"> Seterusnya / <i> Next </i> </a>
                                                                        </li>
                                                                        <li class="previous ">
                                                                            <a href="javascript:void(0);" class="btn btn-lg btn-default"> Sebelum / <i> Previous </i> </a>
                                                                        </li>
                                                                        <!--<li class="next last">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                                                        </li>-->
                                                                        
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                
                                    </div>
                                    <!-- end widget content -->
                
                                </div>
                                <!-- end widget div -->
                
                            </div>
                            <!-- end widget -->
                
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET ENffD -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
            </div>
            <br>
                <div class="page-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                   
                </div>

                <div class="col-xs-6 col-sm-6 text-right hidden-xs">
                    <div class="txt-color-black inline-block">
                        <span class="txt-color-black">NetXpert Data Solution © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>

<!-- Modal -->

        <div class="modal fade" id="form_hantar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Pengesahan</h4>
                    </div>
                    <div class="modal-body">
        <?php echo Form::open(['url' => 'moform/term','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]); ?>

                      <fieldset>
                                      
                                          <input type="hidden" autocomplete="false" name="pernyataan" id="pernyataan">
                                        
                                          <input type="hidden" id="id_pra_submit" name="id_praapplication" value='<?php echo e($pra->id); ?>'>
                                          <input type="hidden" id="token" name="_token" value='<?php echo e(csrf_token()); ?>'>

                                  
                                        
                                       <!-- <span id="message"></span>
                                        <span id="message_0"></span>-->

<div id='loadingmessage' style='display:none'>
       <img src="<?php echo e(url('/img/loader.gif')); ?>">
</div>
                                        <section >
                                            <label class="label">Kata Laluan</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-key "></i>
                                                <input type="Password" autocomplete="off" id="password" name="password" placeholder="Password" >
                                                <b class="tooltip tooltip-bottom-right">Password</b>
                                            </label>

                                        </section>
                             
                                 
                               
                                     <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                      
                                     
                                </fieldset>
                       
                       
        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>
                        <a id="submit_application" class="btn btn-primary">
                                       Submit
                                    </a>
                                   
                           <?php echo Form::close(); ?>   
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script type="text/javascript">
        
        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        
        $(document).ready(function() {
            
            pageSetUp();
            
            
    
            //Bootstrap Wizard Validations

              var $validator = $("#wizard-1").validate({
                
                rules: {
                  email: {
                    required: false,
                    email: "Your email address must be in the format of name@domain.com"
                  },
                  fname: {
                    required: true
                  },
                  lname: {
                    required: true
                  },
                  country: {
                    required: true
                  },
                  city: {
                    required: false
                  },
                  postcode: {
                    required: false,
                    minlength: 4
                  },
                  wphone: {
                    required: true,
                    minlength: 10
                  },
                  hphone: {
                    required: true,
                    minlength: 10
                  }
                },
                
                messages: {
                  fname: "Please specify your First name",
                  lname: "Please specify your Last name",
                  email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                  }
                },
                
                highlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                  if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                  } else {
                    error.insertAfter(element);
                  }
                }
              });
              
              $('#bootstrap-wizard-1').bootstrapWizard({
                'tabClass': 'form-wizard',
                'onNext': function (tab, navigation, index) {
                  var $valid = $("#wizard-1").valid();
                  if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                  } else {

                   

                      $.ajax({

                            type: "PUT",
                           
                             url: '<?php echo e(url('/moform/')); ?>'+'/'+index,
                            data: $('#tab'+index+' :input').serialize(),

                            cache: false,
                            beforeSend: function () {
                               
                            },

                            success: function () {
                             
                            
                             

                            },
                            error: function () {
                               
                                 

                            }
                        });
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                      'complete');
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                    .html('<i class="fa fa-check"></i>');
                  }
                }
              });
              
        
            // fuelux wizard
              var wizard = $('.wizard').wizard();
              
              wizard.on('finished', function (e, data) {
                //$("#fuelux-wizard").submit();
                //console.log("submitted!");
                $.smallBox({
                  title: "Congratulations! Your form was submitted",
                  content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                  color: "#5F895F",
                  iconSmall: "fa fa-check bounce animated",
                  timeout: 4000
                });
                
              });

        
        })

        </script>

        <!-- Your GOOGLE ANALYTICS CODE Below -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
            
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();


                $('.startdate').datepicker({
                dateFormat : 'dd/mm/yy',
                 changeYear: true,
                 changeMonth: true,
                 yearRange: "-72:+0",
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });

                 $('.date').datepicker({
                dateFormat : 'dd/mm/yy',
                 changeYear: true,
                 changeMonth: true,
                 yearRange: "-72:+0",
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });


        </script>


<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

         <script type="text/javascript">
       
$( "#postcode2" ).change(function() {
    var postcode = $('#postcode2').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        //$("#city").val(data[k].post_office );
                        $("#state2").val(data[k].state.state_name );
                        $("#state_code2").val(data[k].state.state_code );
                    });

                }
            });

   
});

</script>

  <script type="text/javascript">

$( "#postcode" ).keyup(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

    
                        $("#state").val(data[k].state.state_name );
                         $("#state_code").val(data[k].state.state_code );
                  
                    });

                }
            });

   
});
</script>

      <script type="text/javascript">
       
$( "#postcode3" ).change(function() {
    var postcode = $('#postcode3').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        //$("#city").val(data[k].post_office );
                        $("#state3").val(data[k].state.state_name );
                         $("#state_code3").val(data[k].state.state_code );
                    });

                }
            });

   
});

</script>


<?php for ($x = 1; $x <= 12; $x++) {  ?>

<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : '<?php echo e(url('/')); ?>/form/upload/<?php echo e($x); ?>';
    $('#fileupload<?php echo e($x); ?>').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
             var text = $('#documentx<?php echo e($x); ?>').val();
            $("#document<?php echo e($x); ?>").html("<a target='_blank' href='"+"<?php echo e(url('/')); ?>/admin/downloaddocpdf/<?php echo e(str_replace('/', '', $pra->id)); ?>/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
             $("#document<?php echo e($x); ?>a").hide();
            

             document.getElementById("fileupload<?php echo e($x); ?>").required = false;
             
            
            

        }
       
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        
});
</script>

<?php } ?>

<script type="text/javascript">




$( "#agree" ).click(function() {
     var icnumber = $('#new_ic').val();
      var remark = $('#remark_verification').val();
    var id_praapplication = $('#id_praapplication_term').val();
      var _token = $('#token_term').val();
      var declaration = 2;
      var fileupload6 =$('#document6').text();
      var fileupload7 = $('#document7').text();
var branch = $('#branch').val();

       
                                                                         
      if ((remark=='') || (branch=='')) {
      //code
       bootbox.alert("Please Select Branch AND Fill a Remark!");
      }
      else{
         $('#form_hantar').modal('show');
         var r = confirm("Are You Sure to Route this Application to Branch?");
        // $('#loadingmessage').show();
      if (r == true) {
         $.ajax({

               type: "POST",
              
               url: '<?php echo e(url('/verifiedMoForm/term/')); ?>',
               data: { branch: branch, id_praapplication: id_praapplication, _token : _token, declaration : declaration, remark : remark},
     
               cache: false,
               beforeSend: function () {
                  
               },

               success: function () {
                   //$('#loadingmessage').hide();
             $("#result").html(data);
                   alert("Verification Success, Application Approved Route to Branch !");
                   location.href='<?php echo e(url('/')); ?>/admin';
                

               },
               error: function () {
                  
               }
           });
      }
                }             
                                       

   
});


$( "#password" ).keyup(function() {
    var password = $('#password').val();
     
     $("#password_copy").val(password);
   
});

$( "#email" ).keyup(function() {
    var email = $('#email').val();
     
     $("#email_copy").val(email);
   
});


$( "#submit_application" ).click(function() {
    //var icnumber = $('#new_ic').val();
    //var remark = $('#remark_verification').val();
    var id_praapplication = $('#id_praapplication_term').val();
    var _token = $('#token_term').val();
    var purchase_application = $('input[name="purchase_application"]:checked').val();
    var appointment_mbsb = $('input[name="appointment_mbsb"]:checked').val();
    var credit_yes= $('#credit_yes:checked').val();
    var credit_no = $('#credit_no:checked').val();
    var consent_for_yes = $('#consent_for_yes:checked').val();
    var consent_for_no = $('#consent_for_no:checked').val();
    var high_networth_yes = $('#high_networth_yes:checked').val();
    var high_networth_no = $('#high_networth_no:checked').val();
    var politically_yes = $('#politically_yes:checked').val();
    var politically_no = $('#politically_no:checked').val();
    var product_disclosure = $('input[name="product_disclosure"]:checked').val();   // 7.10

    var fullname = $('#fullname').val();
    var mykad = $('#mykad').val();
    var passport = $('#passport').val();
    var relationship = $('#relationship').val();

    var name1 = $('#pep_name1').val();
    var relationship1 = $('#pep_relationship1').val();
    var status1 = $('#pep_status1').val();
    var prominent1 = $('#pep_prominent1').val();

    var name2 = $('#pep_name2').val();
    var relationship2 = $('#pep_relationship2').val();
    var status2 = $('#pep_status2').val();
    var prominent2 = $('#pep_prominent2').val();

    var name3 = $('#pep_name3').val();
    var relationship3 = $('#pep_relationship3').val();
    var status3 = $('#pep_status3').val();
    var prominent3 = $('#pep_prominent3').val();



                                           
    $.ajax({
    type: "POST",                       
        url: '<?php echo e(url('/moform/term/')); ?>',
        data:   $('#tab9 :input').serialize(),
        cache: false,
        beforeSend: function () {  
            $("#message").html("<div class='alert alert-default alert-dismissable'>"+
            "<a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+
            "Please Wait..</div>");                            
        },
         success: function (data) {   

            if(data.status==0) {
                 // $("#message").hide();
                  $("#message").html("<div class='alert alert-danger alert-dismissable'>"+
                  "<a href='#'' cl//ass='close' data-dismiss='alert' aria-label='close'>&times;</a>"+
                  data.message+"</div>");
                                  
              }
              else {
                  $("#message").html("<div class='alert alert-success alert-dismissable'>"+
                  "<a href='#'' cl//ass='close' data-dismiss='alert' aria-label='close'>&times;</a>"+
                  data.message+"</div>");

                   alert("Success!");
                   window.location.href='<?php echo e(url('/')); ?>/admin';

              }     


        },
        error: function () {  
        alert("Something Wrong!");                                                             
        }
    });         

   
});

$( "#agreed" ).click(function() {

    
      var id_praapplication = $('#id_praapplication_term').val();
      var _token = $('#token_term').val();
      var branch = $('#branch').val();
      var declaration = 2;
      var remark = $('#remark_verification').val();
      
      if ((remark=='') || (branch=='')) {
      //code
       bootbox.alert("Please Select Branch AND Fill a Remark!");
      }

      else {
      var r = confirm("Are You Sure to Route this Application to Branch?");
      if (r == true) {
         $.ajax({

               type: "POST",
              
               url: '<?php echo e(url('/verifiedMoForm/term/')); ?>',
               data: { branch: branch, id_praapplication: id_praapplication, _token : _token, declaration : declaration, remark : remark},
     
               cache: false,
               beforeSend: function () {
                  
               },

               success: function () {
                
                   alert("Verification Success, Application Approved Route to Branch !");
                   location.href='<?php echo e(url('/')); ?>/admin';
                

               },
               error: function () {
                  
               }
           });
     }
   }
});
</script>
<script type="text/javascript">
    var $loading = $('#loadingmessage').hide();
$(document)
.ajaxStart(function () {
  $loading.show();
})
.ajaxStop(function () {
 $loading.hide();
});
</script>
<script type="text/javascript">

$( "#disagree" ).click(function() {
   var id_praapplication = $('#id_praapplication_term').val();
   var _token = $('#token_term').val();
   var declaration = 3;
   var branch = $('#branch').val();
   var remark = $('#remark_verification').val();
   if (remark=='') {
      //code
       alert("Please fill Remark !");
   }
   else {
      var r = confirm("Are You Sure to Rejected this Application?");
      if (r == true) {
     
         $.ajax({
               type: "POST",
               url: '<?php echo e(url('/verifiedMoForm/term/')); ?>',
               data: { branch: branch, id_praapplication: id_praapplication, _token : _token, declaration : declaration, remark : remark},
     
               cache: false,
               beforeSend: function () { 
               },
               success: function () {
                   alert("Application Rejected !");
                   location.href='<?php echo e(url('/')); ?>/admin';
               },
               error: function () {
               }
           });
     }
   }
});
</script>



<script type="text/javascript">

$( ".income" ).keyup(function() {

var monthly_income = $('#monthly_income').val();
var other_income = $('#other_income').val();


var total_income = parseFloat(monthly_income) + parseFloat(other_income);
    $("#total_income").val(parseFloat(total_income).toFixed(2));


   
});
</script>
<script type="text/javascript">
    
$(document).ready(function() {

var found = [];
    $("select[name='title'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='gender'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='ownership'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });


var found = [];
    $("select[name='country'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='race'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='bumiputera'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='religion'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='marital'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='education'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='emptype'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='empstatus'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='spouse_emptype'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='purpose_facility'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='type_customer'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });

var found = [];
    $("select[name='payment_mode'] option").each(function() {
        if($.inArray(this.value, found) != -1) $(this).remove();
        found.push(this.value);
        });
});













</script>

<script type="text/javascript">

$( "#old_ic" ).change(function() {

     var old_ic = $('#old_ic').val();
  
      
if(old_ic == 'A1340304' | old_ic =='A2124626'  | old_ic =='4999546' | old_ic== 'A1962144'  | old_ic== 'A3563969')
{

alert("sorry, your olc ic blocked by system");
$('#old_ic').val("");

} 





});
</script>



<script>
function isNumberKey(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          if (key.length == 0) return;
          var regex = /^[0-9.,\b]+$/;
          if (!regex.test(key)) {
              theEvent.returnValue = false;
              if (theEvent.preventDefault) theEvent.preventDefault();
          }
}
</script>

<script>
function toFloat(z) {
    var x = document.getElementById(z);
    x.value = parseFloat(x.value).toFixed(2);
}
</script>


<script>
function alertWithoutNotice(message){
    setTimeout(function(){
        alert(message);
    }, 1000);
}
</script>

<?php if($data->marital != 'M'): ?>
  <script type="text/javascript">

  $(document).ready(function() {
    

        $("#spouse-group :input").attr("disabled", true);
            $("#statuskawin").show();
            $("#spouse_emptype").hide();
             $("#spouse_emptype_others").hide();
            $("#spouse_emptype_kosong").show();
            
            
   
});
  </script>
  <?php else: ?> 
  <script type="text/javascript">

  $(document).ready(function() {
    

     $("#spouse-group :input").attr("disabled", false);
            $("#statuskawin").hide();
            $("#spouse_emptype").show();
            $("#spouse_emptype_kosong").hide();

            <?php if($spouse->emptype != 'Others (Specify)/ Lain-lain (Nyatakan)'): ?>

                $("#spouse_emptype_others").hide();
            <?php else: ?> 
        
                $("#spouse_emptype_others").show();       
            <?php endif; ?> 
   
});
  </script>

<?php endif; ?> 
<!--
  <script type="text/javascript">

$( "#marital" ).change(function() {
    var status = $('#marital').val();
    var spouse_emptype = $('#spouse_emptype').val();
     
    if(status  != 'M' ) {
       
        
        $("#spouse-group :input").attr("disabled", true);
            $("#statuskawin").show();
              $("#spouse_emptype_kosong").show();
            $("#spouse_emptype_others").hide();
             $("#spouse_emptype").hide();
    }
    else {
   
      
        $("#spouse-group :input").attr("disabled", false);
            $("#statuskawin").hide();
            $("#spouse_emptype").show();
            $("#spouse_emptype_kosong").hide();

            if(spouse_emptype  != 'Others (Specify)/ Lain-lain (Nyatakan)') {
       
                $("#spouse_emptype_others").hide();
            }
            else {

                $("#spouse_emptype_others").show();
            }

    }

   
});
</script>
-->

<script type="text/javascript">

function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.visibility = 'visible';
    } else {
        document.getElementById('ifYes').style.visibility = 'hidden';
    }
}

</script>

<script type="text/javascript">
    
    var FormStuff = {
  
  init: function() {
    this.applyConditionalRequired();
    this.bindUIActions();
  },
  
  bindUIActions: function() {
    $("input[type='radio'], input[type='checkbox']").on("change", this.applyConditionalRequired);
  },
  
  applyConditionalRequired: function() {
    
    $(".require-if-active").each(function() {
      var el = $(this);
      if ($(el.data("require-pair")).is(":checked")) {
        el.prop("required", true);
      } else {
        el.prop("required", false);
      }
    });
    
  }
  
};

FormStuff.init();
</script>

<script type="text/javascript">
function minmax(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 0; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}

function minmax_tempoh(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 24; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}

</script>


<?php if($data->title != 'Others'): ?>
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#salutation_others").hide();
       
      });
  </script>
  <?php else: ?> 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#salutation_others").show();
       
      });
  </script>

<?php endif; ?> 

  <script type="text/javascript">

$( "#salutation" ).change(function() {
    var salutation = $('#salutation').val();
     
    if(salutation  != 'Others' ) {
       
            $("#salutation_others").hide();
    }
    else {
   

            $("#salutation_others").show();
    }

   
});
</script>

<?php if($empinfo->emptype != 'Others (Specify) / Lain-lain (Nyatakan)'): ?>
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#emptype_others").hide();
       
      });
  </script>
  <?php else: ?> 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#emptype_others").show();
       
      });
  </script>

<?php endif; ?> 

  <script type="text/javascript">

$( "#emptype" ).change(function() {
    var emptype = $('#emptype').val();
     
    if(emptype  != 'Others (Specify) / Lain-lain (Nyatakan)' ) {
       
            $("#emptype_others").hide();
    }
    else {
   

            $("#emptype_others").show();
    }

   
});
</script>

<?php if($spouse->emptype != 'ZZZ'): ?>
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#spouse_emptype_others").hide();
       
      });
  </script>
  <?php else: ?> 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#spouse_emptype_others").show();
       
      });
  </script>

<?php endif; ?> 

  <script type="text/javascript">

$( "#spouse_emptype" ).change(function() {
    var emptype = $('#spouse_emptype').val();
     
    if(emptype  != 'ZZZ') {
       
            $("#spouse_emptype_others").hide();
    }
    else {
   

            $("#spouse_emptype_others").show();
    }

   
});
</script>


<?php if($financial->product_bundling != '1'): ?>
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#product_bundling_specify").hide();
       
      });
  </script>
  <?php else: ?> 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#product_bundling_specify").show();
       
      });
  </script>

<?php endif; ?> 

<script type="text/javascript">

$( "#pb_yes" ).change(function() {
    var pb_yes = $('#pb_yes').val();
      $("#product_bundling_specify").show();
});

$( "#pb_no" ).change(function() {
    var pb_no = $('#pb_no').val();  

     $("#product_bundling_specify").hide();

   
});
</script>

<?php if($financial->cross_selling != '1'): ?>
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#cross_selling_specify").hide();
       
      });
  </script>
  <?php else: ?> 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#cross_selling_specify").show();
       
      });
  </script>

<?php endif; ?> 

  <script type="text/javascript">

$( "#cs_yes" ).change(function() {
    var cs_yes = $('#cs_yes').val();
      $("#cross_selling_specify").show();
});


$( "#cs_no" ).change(function() {
    var cs_no = $('#cs_no').val();  

     $("#cross_selling_specify").hide();

   
});

</script>



<?php if($data->country == 'Non-Citizen (Specify)/ Bukan Warganegara (Nyatakan)'): ?>
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#country_others").show();
                $("#country_origin").hide();
       
      });
  </script>
  <?php elseif($data->country == 'Permanent Resident / Penduduk Tetap'): ?>
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#country_origin").show();
                $("#country_others").hide();
       
      });
  </script>
  <?php else: ?> 

      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#country_origin").hide();
                $("#country_others").hide();
       
      });
  </script>

<?php endif; ?> 
<?php if($view=='view'): ?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#wizard-1 :input").prop("disabled", true);
    });

</script>
<?php endif; ?>

  <script type="text/javascript">

$( "#country" ).change(function() {
    var country = $('#country').val();
     
    if(country  == 'Non-Citizen (Specify)/ Bukan Warganegara (Nyatakan)') {
       
            $("#country_others").show();
            $("#country_origin").hide();
    }
    else if(country  == 'Permanent Resident / Penduduk Tetap') {
       
            $("#country_origin").show();
            $("#country_others").hide();
       
    }
    else {
   

            $("#country_origin").hide();
            $("#country_others").hide();
    }

   
});
</script>

<?php if($data->race != 'Others (Specify)/ Lain-lain (Nyatakan)'): ?>
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#race_others").hide();
       
      });
  </script>
  <?php else: ?> 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#race_others").show();
       
      });
  </script>

<?php endif; ?> 

  <script type="text/javascript">

$( "#race" ).change(function() {
    var race = $('#race').val();
     
    if(race  != 'Others (Specify)/ Lain-lain (Nyatakan)') {
       
            $("#race_others").hide();
    }
    else {
   

            $("#race_others").show();
    }

   
});
</script>

<?php if($data->religion != 'Others/ Lain-lain'): ?>
  <script type="text/javascript">

      $(document).ready(function() {
        
                $("#religion_others").hide();
       
      });
  </script>
  <?php else: ?> 
      <script type="text/javascript">

      $(document).ready(function() {
        
                $("#religion_others").show();
       
      });
  </script>

<?php endif; ?> 

  <script type="text/javascript">

$( "#religion" ).change(function() {
    var religion = $('#religion').val();
     
    if(religion  != 'Others/ Lain-lain') {
       
            $("#religion_others").hide();
    }
    else {
   

            $("#religion_others").show();
    }

   
});
</script>














