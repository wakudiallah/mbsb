<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Application Status";


$page_css[] = "your_style.css";
include("asset/inc/header.php");





?>
<style>
.not-active {
   pointer-events: none;
   cursor: default;
}
</style>

    <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <div id="content">
     <?php if(Session::has('message')): ?>
    

    <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
   
      <strong><?php echo e(Session::get('message')); ?></strong> 
    </div>
            
            <?php endif; ?>             
            <?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
          <button class="close" data-dismiss="alert">
         ×
    </button>  
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>
<section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-5 col-md-5 col-lg-5">
                    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Update Password</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body ">
                              
                              
                            <?php echo Form::open(['url' => 'update_password','class' => '', 'id' =>'smart-form-register' ]); ?> 

                                    <div class="form-group">
                                        <label class="control-label">Current Password</label>
                                        <input type="password" name="old_password" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">New Password</label>
                                        <input type="password" name="password"  class="form-control">

                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Re-type New Password</label>
                                        <input type="password"  name="password_confirmation"  class="form-control">
Password must be between 8 - 36 characters long, and must contain a combination of numbers(0-9) and letters(A-Z,a-z), but no spaces.
                                    </div>



                                    <div class="margiv-top10">
                                          <button type="submit"  class="btn btn-primary"><i class="fa fa-check"></i> Change Password</button>
                                          <a href="<?php echo e(url('/')); ?>"   class="btn btn-default">Cancel </a>
                                    </div>

                              <?php echo Form::close(); ?>  

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
            </div>
            <br>
                <div class="page-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                   
                </div>

                <div class="col-xs-6 col-sm-6 text-right hidden-xs">
                    <div class="txt-color-white inline-block">
                        <span class="txt-color-white">NetXpert Data Solution © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>

      <?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

