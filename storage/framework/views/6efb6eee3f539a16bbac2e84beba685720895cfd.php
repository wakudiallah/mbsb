<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Marketing Officer Page";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
  <script type="text/javascript" src=""></script>
 <!-- jQuery 2.1.3 -->
<script src="<?php echo e(asset ("/bin/push.min.js")); ?>"></script>

<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   	<?php if(Session::has('error')): ?>
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('error')); ?></strong> 
        </div>
        <?php elseif(Session::has('success')): ?>
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('success')); ?></strong> 
        </div>
            
      <?php endif; ?>
	  
	  <section id="widget-grid" class="">
                 
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-8 col-lg-8">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Current Statistics Application Under <b><?php print $user->name ; ?></b></h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                              
						<?php 	
								function ringgit($nilai, $pecahan = 0) 
									{ 
										return number_format($nilai, $pecahan, '.', ','); 
									}
								
								$sumloan= 0;
								$sumapp= 0;						
						
								$sumapproved= 0;
								$sumloan_approved= 0; 
								
								$sumrejected= 0; 
								$sumloan_rejected= 0; 
								
								$sumprocess= 0;
								$sumloan_process= 0; 
								
								$sumpendingapproval= 0; 
								$sumloan_pendingapproval= 0;

								$sumpending2ndadmin= 0; 
								$sumloan_pending2ndadmin= 0; 

								$sumpendingadmin= 0; 
								$sumloan_pendingadmin= 0; 

                                $sumpendingtenure= 0; 
                                $sumloan_pendingtenure= 0; ?>
								
                        <?php $__currentLoopData = $terma; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $termb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php $sumloan = $sumloan + $termb->PraApp->loanamount;
									$sumapp = $sumapp + 1; ?>
							<?php if($termb->verification_result_by_bank ==1 AND $termb->status_waps ==1): ?>
									<?php 	$sumapproved = $sumapproved + 1; 
											$sumloan_approved = $sumloan_approved + $termb->PraApp->loanamount; 
									?>
							<?php endif; ?>
							<?php if(($termb->verification_result_by_bank ==2) OR ($termb->verification_result ==3) OR ($termb->status ==88)): ?>
									<?php 	$sumrejected = $sumrejected + 1; 
											$sumloan_rejected = $sumloan_rejected + $termb->PraApp->loanamount; 
									?>
							<?php endif; ?>
							<?php if($termb->status_waps ==0 AND $termb->verification_result ==2 AND $termb->verification_status ==1): ?>
									<?php 	$sumpendingapproval = $sumpendingapproval + 1;
											$sumloan_pendingapproval = $sumloan_pendingapproval + $termb->PraApp->loanamount; 
									?>
							<?php endif; ?>
							 <?php if($termb->verification_result_by_bank ==0 AND $termb->verification_result ==0 AND $termb->status ==77): ?>
									<?php 	$sumprocess = $sumprocess + 1; 
											$sumloan_process = $sumloan_process + $termb->PraApp->loanamount; 
									?>
							<?php endif; ?>
							<?php if($termb->verification_result ==1 ): ?>
									<?php 	$sumpending2ndadmin = $sumpending2ndadmin + 1; 
											$sumloan_pending2ndadmin = $sumloan_pending2ndadmin + $termb->PraApp->loanamount; 
									?>
							<?php endif; ?>
							<?php if($termb->verification_result ==0 AND $termb->status ==1): ?>
									<?php 	$sumpendingadmin = $sumpendingadmin + 1; 
											$sumloan_pendingadmin = $sumloan_pendingadmin + $termb->PraApp->loanamount; 
									?>
							<?php endif; ?>
                            <?php if($termb->status ==0): ?>
                                    <?php   $sumpendingtenure = $sumpendingtenure + 1; 
                                            $sumloan_pendingtenure = $sumloan_pendingtenure + $termb->PraApp->loanamount; 
                                    ?>
                            <?php endif; ?>
							
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
											<th>Status</th>
											<th>Total Application</th>
											<th>Total Loan Amount Request</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<tr>
											<td>Total Approved by Branch </td>
											<td><?php echo e($sumapproved); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_approved)); ?></td>
										</tr>
										<tr>
											<td>Total Rejected by Branch or Admin </td>
											<td><?php echo e($sumrejected); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_rejected)); ?></td>
										</tr>
										<tr>
											<td>Total Pending WAPS </td>
											<td><?php echo e($sumpendingapproval); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_pendingapproval)); ?></td>
										</tr>
										<tr>
											<td>Total Pending Verifiction </td>
											<td><?php echo e($sumprocess); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_process)); ?></td>
										</tr>
										<tr>
											<td>Total Pending DSR  </td>
											<td><?php echo e($sumpendingadmin); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_pendingadmin)); ?></td>
										</tr>
                                        <tr>
                                            <td>Total Pending Financing Eligibility  </td>
                                            <td><?php echo e($sumpendingtenure); ?></td>
                                            <td>RM <?php echo e(ringgit($sumloan_pendingtenure)); ?></td>
                                        </tr>
										<tr>
											<td><b>All</b></td>
											<td><b><?php echo e($sumapp); ?></b></td>
											<td><b>RM <?php echo e(ringgit($sumloan)); ?></b></td>
										</tr>                                     
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
						
						
		<section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Application List</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                              
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
											<thead>			                
												<tr>
													<th>No.</th>
                                                    <th class="hidden-xs">IC Number</th>
                                                    <th>Name</th>
													<th class="hidden-xs">Phone</th>
                                                 
                                                    <th>Submit Date</th>
                                                    <th>Status</th>
                                                 
                                                    <th>Branch Status</th>
                                                    <th>Last Activity</th>
                                                    <th>Action</th>
                                                     <th>Download</th>
                                                      

												</tr>
											</thead>
											<tbody>
												<?php $i=1; ?>
                                                 <?php $__currentLoopData = $terma; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $term): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                   
                                              <tr>
                                              
                                                <td>
                                                   <?php echo e($i); ?>                              
                                                </td>
                                                  
                                                  <td class="hidden-xs">
                                              <?php echo e($term->Basic->new_ic); ?>

                                              <input type='hidden' id='ic99<?php echo e($i); ?>' name='ic' value='<?php echo e($term->Basic->new_ic); ?>'/>
                                         
                                                  
                                                  
                                                  </td>
                                                <td><?php echo e($term->Basic->name); ?></td>
												<td class="hidden-xs"><?php echo e($term->PraApplication->phone); ?></td>
                                                <td><?php echo e($term->PraApp->created_at); ?></td>
                                                <td><div align='center'>
                                                <?php if(($term->status==1) AND ($term->mo_stage==1)): ?>
                                                       <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-default'>Check DSR</span>
                                                <?php elseif($term->status==88): ?>
                                                <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-danger'>Rejected</span>
                                                 <?php elseif($term->status==77): ?>
                                                <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-danger'>Complete  form</span>
                                                <?php else: ?> 

													<?php if(($term->status==0) AND ($term->verification_result ==0)): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-warning'>Select Tenure</span>
                                                    <?php elseif($term->verification_result ==1): ?>                                                
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-success'>Submitted to WAPS</span>
                                                    <?php elseif($term->verification_result ==2): ?>
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-primary'>Routed to Branch / Submitted to WAPS</span>
														<br>
														<?php if($term->id_branch=='0'): ?>
                                                         <div align='center'> - </div> 
														<?php else: ?>                                            
                                                        <i><?php echo e($term->Branch->branchname); ?></i>
														<?php endif; ?> 
                                                
                                                    <?php elseif($term->verification_result ==3): ?>
                                                      <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-danger'>Rejected</span>
                                                 
													<?php elseif($term->verification_result ==4): ?>
                                                      <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-default'>Route Back to User</span>
                                                 
                                                    <?php endif; ?>
                                                <?php endif; ?>
													
						
                                                       </div>
                                                </td>
                                         
                                                <td><div align='center'>
													
                                                  <?php if($term->verification_result_by_bank >=0 AND $term->verification_result ==2): ?>
                                                       <!-- <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-default'>In Process</span>-->
                                                                                                       <?php
                                                 $this->soapWrapper = $soap_Wrapper;

                                                $this->soapWrapper->add('cds', function ($service) {
                                                                  $service
                                                                    //->wsdl('http://219.92.49.250/MOMWS/MOMCallWS.asmx?WSDL');
                                                                  ->wsdl('http://moaqs.com/MOMWS/MOMCallWS.asmx?WSDL');
                                                                    });
                                                                  
                                                 $param=array(
                                                        'AppNo'   => $term->id_praapplication,
                                                        'txnCode' => 'ST003',
                                                        'ActCode' => 'R',
                                                        'usr' => 'momwapsuat',
                                                        'pwd' => 'J@ctL71N$#'
                                                    );
                                                $response = $this->soapWrapper->call('cds.MOMCallStat',array($param));
                                                $smsMessages = is_array( $response->MOMCallStatResult)
                                                ? $response->MOMCallStatResult
                                                : array( $response->MOMCallStatResult );

                                                foreach ( $smsMessages as $smsMessage )
                                                {
                                                  $result=$smsMessage->ApvStat;
                                                  //echo $result;
                                                }


                                                    ?>
  
                
                                                 <?php if($result=='GLAN'): ?>
                                                 <span data-toggle="tooltip" title=""  class='label label-success'>Pending WAPS</span>
                                                 
                                                 <?php elseif(($result=='DECL')): ?>
                                                 <span data-toggle="tooltip" title=""  class='label label-warning'>Decline</span>
                                                 <script type="text/javascript">
   setTimeout(function(){
       location.reload();
   },15000000);
</script>
                                                 <!--<script type="text/javascript">
                                                    Push.create("New Process Waps");
                                                  </script>-->
                                                 <?php elseif($result=='APVA'): ?>
                                                 <span data-toggle="tooltip" title=""  class='label label-info'>Approved with </span>
                                                  <?php elseif($result=='REJM'): ?>
                                                 <span data-toggle="tooltip" title=""  class='label label-danger'>Rejected</span>
                                                  <?php elseif($result=='ACCP'): ?>
                                                 <span data-toggle="tooltip" title=""  class='label label-primary'>Acepted</span>
                                                 <?php endif; ?>
                                            
                                                  <?php endif; ?>
                                                  
                                                   
                                                    </div>  
                                                </td>
                                               
                                                   <td>
                                                  <a href="JavaScript:newPopup('<?php echo e(url('/')); ?>/activities/<?php echo e($term->id_praapplication); ?>');"   class='btn btn-sm btn-default'><i class="fa fa-search"></i></a>
                                                   <!--  <form method="POST" class="form-horizontal" action="<?php echo e(url('/move')); ?>" >
                                                         <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                         <input type="hidden" name="id_praapplications" value="<?php echo e($term->id_praapplication); ?>">
                                                          <input type="hidden" name="ics" value="<?php echo e($term->Basic->new_ic); ?>">
                                                         <input type="submit" value="MOVE" class="btn btn-primary">
                                                    </form>-->
                                                  </td>
                                                 <td>
                                                  
                                               <?php if(($term->status==1) AND ($term->mo_stage==1)): ?>
                                                    <a href="<?php echo e(url('/admin/step1/'.$term->id_praapplication.'/verify')); ?>" class='btn btn-sm btn-success' ><i class='fa fa-file'></i> Check DSR</a>
                                                <?php elseif($term->status=='77'): ?>
                                                   <a href="<?php echo e(url('/moform/'.$term->id_praapplication.'/verify')); ?>" class='btn btn-sm btn-primary' ><i class='fa fa-file'></i> Complete  form</a><br>
                                                    <a href="<?php echo e(url('/admin/step1/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>
                                                <?php elseif(($term->status==0) AND ($term->verification_result==0)): ?>
                                                    <a href="<?php echo e(url('/moapplication/'.$term->id_praapplication)); ?>" class='btn btn-sm btn-success' ><i class='fa fa-file'></i> Select Tenure</a>


                                                 <?php elseif(($term->status==1) AND ($term->mo_stage==2)): ?>
                                                   <a href="<?php echo e(url('/moform/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default fa fa-book' >View Form</a>
                                                          <a href="<?php echo e(url('/admin/step1/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>

                                                     <!--<a href="<?php echo e(url('/add_document/'.$term->id_praapplication.'/verify')); ?>" class='btn btn-sm btn-default fa fa-book' >Remark Documents</a>-->
                                                <!--tambahan-->
                                                <?php elseif($term->status=='99'): ?>
                                                    <?php if(($term->mo_stage=='2') AND ($term->verification_result=='0')): ?>
                                                        <!--<a href="<?php echo e(url('/admin/user_detail/'.$term->id_praapplication.'/verify')); ?>" class='btn btn-sm btn-primary fa fa-book' >Verify  Form</a>-->
                                                             <a href="<?php echo e(url('/moform/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default fa fa-book' >View Form</a>
                                                          <a href="<?php echo e(url('/admin/step1/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>
                                                    <?php elseif(($term->mo_stage=='2') AND ($term->verification_result=='2')): ?>
                                                          <a href="<?php echo e(url('/moform/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default fa fa-book' >View Form</a>
                                                          <a href="<?php echo e(url('/admin/step1/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>
                                                    <?php elseif(($term->mo_stage=='2') AND ($term->verification_result=='3')): ?>
                                                          <a href="<?php echo e(url('/moform/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default fa fa-book' >View Form</a>
                                                          <a href="<?php echo e(url('/admin/step1/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>
                                                    <?php endif; ?> 

                                               
                                                <?php elseif($term->status==88): ?>
                                                    <a href="<?php echo e(url('/admin/step1/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>
                                                <?php endif; ?>

                                                <?php if(($term->status==1) AND ($result!='GLAN') AND ($term->verification_result==2) ): ?>
                                                     <a href="<?php echo e(url('/')); ?>/admin/detail_pra/<?php echo e($term->id_praapplication); ?>" class='btn btn-sm btn-default fa fa-book' >Detail</a>
                                                <?php endif; ?>

                                                        <?php if($term->edit=='1'): ?>
                                                            <?php if($term->verification_result !=3): ?>
                                                                <?php if($term->verification_result !=2): ?>
                                                                    <a href="<?php echo e(url('/')); ?>/form/approveedit/<?php echo e($term->id_praapplication); ?>" 
                                                                    onclick="return confirm('Are you sure ?')"  class='btn btn-sm btn-warning'>Approve RB</a>
                                                                <?php endif; ?> 
                                                            <?php endif; ?> 
                                                        <?php endif; ?> 



                                                </td>
                                               
                                                 <td>

                                                   <?php if(($term->status=='77') OR (($term->status=='1') AND ($term->verification_status=='1'))): ?>

                                                    <a href="<?php echo e(url('/')); ?>/admin/downloadzip/<?php echo e($term->id_praapplication); ?>"
                                                ><img width='24' height='24' src="<?php echo e(url('/')); ?>/asset/img/zip.png"></img></a>&nbsp;&nbsp; 
                                                 
                                                <a href="<?php echo e(url('/')); ?>/admin/downloadpdf/<?php echo e($term->id_praapplication); ?>" target="_blank"
                                                ><img width='24' height='24' src="<?php echo e(url('/')); ?>/asset/img/pdf.png"></img></a>

<!--                                                <a href="<?php echo e(url('/')); ?>/admin/form/view/<?php echo e($term->id_praapplication); ?>"
                                                ><img width='24' height='24' src="<?php echo e(url('/')); ?>/asset/img/pdf.png"></img>view</a>-->
                                                
                                                      <?php endif; ?>

                                                 


                                          
                                                 </td>
                                            </tr>
                                              <?php
                                              $i++; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</tbody>
										</table>
                         
													<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



		<script type="text/javascript">
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
		
			
			/* // DOM Position key index //
		
			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing 
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class
			
			Also see: http://legacy.datatables.net/usage/features
			*/	
	
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				
	
			/* END BASIC */
			
				/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */
		})
		</script>
          
							

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					  
						"scrollX": true,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 800);
 
});
</script>

<script type="text/javascript">
// Popup window code
function newPopup(url) {
    popupWindow = window.open(
        url,'popUpWindow','height=400,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes')
}
</script>





          
