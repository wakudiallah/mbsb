<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Package Master";


$page_css[] = "your_style.css";
include("asset/inc/header.php");


$page_nav["master"]["sub"]["package"]["active"] = true;
include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	    <?php if(Session::has('error')): ?>
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('error')); ?></strong> 
        </div>
        <?php elseif(Session::has('success')): ?>
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('success')); ?></strong> 
        </div>
            
      <?php endif; ?>

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            
                            <br>
                            <a href='' data-toggle='modal' data-target='#addBranch' class='btn btn-success'><i class='fa fa-plus'></i> Add Score Card</a>
                            	<!-- Modal -->
                                <div class="modal fade" id="addBranch" tabindex="-1" role="dialog" aria-labelledby="addBranch" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="addBranch">Add Score Card</h4>
                                            </div>
                                            <div class="modal-body">
                                             <?php echo Form::open(['url' => 'admin/master/save/scorecard','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]); ?>

                                             
                                              <fieldset>
                                                
                                                    <section >
                                                      <label class="label">Grade</label>
                                                         <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <input type="text" id="grade" name="grade" placeholder="Grade" required>
                                                            <b class="tooltip tooltip-bottom-right">Effective Rate</b>
                                                        </label>
                                                    </section>
                                                    <section >
                                                      <label class="label">Score Min</label>
                                                         <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <input type="text" id="score" name="score" placeholder="Score Min" required>
                                                            <b class="tooltip tooltip-bottom-right">Flat Rate</b>
                                                        </label>
                                                    </section>
                                                    <section >
                                                      <label class="label">Score Max</label>
                                                         <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <input type="text" id="score2" name="score2" placeholder="Score Max" required>
                                                            <b class="tooltip tooltip-bottom-right">Flat Rate</b>
                                                        </label>
                                                    </section>
                                                     <section >
                                                      <label class="label">Decision</label>
                                                         <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <input type="text" id="decision" name="decision" placeholder="Decision" required>
                                                            <b class="tooltip tooltip-bottom-right">Flat Rate</b>
                                                        </label>
                                                    </section>
                                                     
                                                     <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                 </fieldset>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Cancel
                                                </button>
                                                <button type="submit" name="submit" class="btn btn-primary">
                                                               Submit
                                                            </button>
                                                           
                                                   <?php echo Form::close(); ?>   
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            
                            <br><br>

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                                <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                    
                                    data-widget-colorbutton="false" 
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true" 
                                    data-widget-sortable="false"
                                    
                                -->
                                
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
										<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
                                                <tr>
                                                  <td>No</td>
                                                    <th>Grade</th>
                                                    <th>Score Min</th>
                                                    <th>Score Max</th>
                                                    <th>Decision</th>
                                                                                                        <th>Action</th> <th>Action</th>
                                               
                                                </tr>
											</thead>
                                            <tbody>
                                            <?php $i=1; ?>
                                            <?php $__currentLoopData = $score; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                  <td><?php echo e($i); ?></td>
                                                    <td><?php echo e($val->grade); ?></td>
                                                    <td><?php echo e($val->score); ?> </td>
                                                    <td><?php echo e($val->score2); ?> </td>
                                                     <td><?php echo e($val->decision); ?></td>
                                                  <td> <a href='#' data-toggle='modal' class="btn btn-default btn-sm" data-target='#myModal<?php echo e($i); ?>'><i class="fa fa-pencil"></i> Edit</a>
													 <!-- Modal -->
													<div class="modal fade" id="myModal<?php echo e($i); ?>" tabindex="-1" role="dialog" aria-labelledby="myModal<?php echo e($i); ?>" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		&times;
																	</button>
																	<h4 class="modal-title" id="addBranch">Edit Score Card <?php echo e($val->name); ?> </h4>
																</div>
																<div class="modal-body">
																 <?php echo Form::open(['url' => 'admin/master/update/scorecard/'.$val->id,'class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]); ?>

																   <fieldset>
                                                 
                                                    <section >
                                                      <label class="label">Grade</label>
                                                         <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <input type="text" id="grade" name="grade" placeholder="Rate" value="<?php echo e($val->grade); ?>" required>
                                                            <b class="tooltip tooltip-bottom-right">Effective Rate</b>
                                                        </label>
                                                    </section>
                                                    <section >
                                                      <label class="label">Score Min</label>
                                                         <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <input type="text" id="score2" name="score" placeholder="Flat Rate" required value="<?php echo e($val->score); ?>">
                                                            <b class="tooltip tooltip-bottom-right">Flat Rate</b>
                                                        </label>
                                                    </section>
                                                    <section >
                                                      <label class="label">Score Max</label>
                                                         <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <input type="text" id="score2" name="score2" placeholder="Flat Rate" required value="<?php echo e($val->score2); ?>">
                                                            <b class="tooltip tooltip-bottom-right">Flat Rate</b>
                                                        </label>
                                                    </section>
                                                     <section >
                                                      <label class="label">Decision</label>
                                                         <label class="input">
                                                            <i class="icon-append fa fa-user"></i>
                                                            <input type="text" id="decision" name="decision" placeholder="Flat Rate" required value="<?php echo e($val->decision); ?>">
                                                            <b class="tooltip tooltip-bottom-right">Flat Rate</b>
                                                        </label>
                                                    </section>
                                                     
                                                     <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                       <input type="hidden" name="idUser" value="<?php echo e($val->id); ?>">
                                                 </fieldset>

																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">
																		Cancel
																	</button>
																	<button type="submit" name="submit" class="btn btn-primary">
																				   Submit
																				</button>
																			   
																	   <?php echo Form::close(); ?>   
																</div>
															</div><!-- /.modal-content -->
														</div><!-- /.modal-dialog -->
													</div><!-- /.modal -->
													</td>
                                                    <td>
                                                        <a href='#' data-toggle='modal' data-target='#deleteModal<?php echo e($i); ?>' class="btn btn-sm btn-default" ><i class="fa fa-trash"></i> Delete</a>
													 <!-- Modal -->
													<div class="modal fade" id="deleteModal<?php echo e($i); ?>" tabindex="-1" role="dialog" aria-labelledby="deleteModal<?php echo e($i); ?>" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		&times;
																	</button>
																	<h4 class="modal-title" id="DeleteBranch"><b>Are You Sure to Delete  
                                                                      <?php echo e($val->name); ?> ? </b></h4>
																</div>
														
																<div class="modal-body">
                                                                     <?php echo Form::open(['url' => 'admin/deletepackage' ]); ?>

                                                                     [ Warning!. You can not restore this execution. The data will be permanently deleted and may involve the loss of related data ]
                                                                    <div align='right'>
                                                                    	  <input type="text" name="id" value="<?php echo e($val->id); ?>">
                                                                     <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
																	<button type="button" class="btn btn-default" data-dismiss="modal">
																		No
																	</button>
																	<button type="submit" name="submit" class="btn btn-primary">
																				   Yes
																				</button></div>
																			   
																	   <?php echo Form::close(); ?>   
																</div>
															</div><!-- /.modal-content -->
														</div><!-- /.modal-dialog -->
													</div><!-- /.modal -->
                                                    </td>
                                                  
                                                </tr>
                                                <?php $i++; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											</tbody>
                                        </table>
																					   
										
													<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



		<script type="text/javascript">
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
		
			
			/* // DOM Position key index //
		
			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing 
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class
			
			Also see: http://legacy.datatables.net/usage/features
			*/	
	
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				
	
			/* END BASIC */
			
				/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */
		})
		</script>
          
													
													

									</div>
									<br><br><br><br>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>
    
    <script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email: {
                    required : true,
                    email : true
                },
                Password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                 Branch : {
                    required : true
                },
                PasswordConfirmation : {
                    required : true,
                    minlength : 3,
                    maxlength : 20,
                    equalTo : '#Password'
                }
            },

            // Messages for form validation
             messages : {

                    Email: {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
                },
                    Branch: {
                    required : 'Please select a Branch'
                },
      
                Password : {
                    required : 'Please enter your password'
                },
                PasswordConfirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>



          
