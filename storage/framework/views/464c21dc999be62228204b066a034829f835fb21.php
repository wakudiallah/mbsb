<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Marketing Officier Page";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");



?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header.php");
?>
    <div id="main" role="main">
      <!-- MAIN CONTENT -->
      <div id="content" class="container">
                     <?php if(Session::has('message')): ?>
    

    <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
     <i class="fa-fw fa-lg fa fa-exclamation"></i>
      <strong><?php echo e(Session::get('message')); ?></strong> 
    </div>
            
            <?php endif; ?>             
            <?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
          <button class="close" data-dismiss="alert">
         ×
    </button>  
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>
           
          
                <div class="row">
     
           <div class=" col-md-3 "></div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="well no-padding">

                              <?php $__currentLoopData = $pra; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pra): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <?php 
        $icnumber = $pra->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
         $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur =  $oDateIntervall->y;
        $durasix = 60 - $oDateIntervall->y;
        if( $durasix  > 10){ $durasi = 10 ;} 
        else { $durasi = $durasix ;}
 
        ?>
                        
                                   <div class='smart-form client-form'id='smart-form-register2'>
                                <header> <p class="txt-color-white"><b>    Kelayakan Pembiayaan  </b> </p> </header>
                               
                              
<form action='<?php echo e(url('/')); ?>/mopraapplication/<?php echo e($id); ?>' method='post' enctype="multipart/form-data">
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label"> <b>  Pakej </b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <b><input type="text" id="Package2" value="<?php echo e($pra->package->name); ?>"  name="Package2" placeholder="Package" disabled="disabled"></b>
                                                <input name="id_praapplication" id="id_praapplication" type="hidden"  value="<?php echo e($pra->id); ?>" >
                                                 <input type="hidden" name="_token" id="token" value="<?php echo e(csrf_token()); ?>">
                                                
                                                <b class="tooltip tooltip-bottom-right">Pakej</b>
                                            </label>
                                        </section>
                                        
                                        <section class="col col-6">
                                            <label class="label"> <b> <font color="red" size="2.5" > KELAYAKAN  PEMBIAYAAN  MAKSIMA  </font>  </b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
 <?php $__currentLoopData = $loan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $loan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     
                        <?php
                            $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
                            $ndi = ($zbasicsalary - $zdeduction) -  1300;

                            $max  =  $salary_dsr * 12 * 10 ;
                                                   
                            function pembulatan($uang) {
                              $puluhan = substr($uang, -3);
                              if($puluhan<500) {
                                $akhir = $uang - $puluhan; 
                              } 
                              else {
                                $akhir = $uang - $puluhan;
                              }
                              return $akhir;
                            }

                        if(!empty($loan->max_byammount))  {
                      
                            $ansuran = intval($salary_dsr)-1;
                                if($pra->package->id=="1") {
                                //$a= $pra->package->effective_rate;
                                //$bunga = ((($a/12)+0.025) *10)/100;
                                    $bunga = 3.7/100;
                                   //$bunga = $pra->package->effective_rate / 100;
                                }
                                elseif($pra->package->id=="2") {
                                //$a= $pra->package->effective_rate;
                                //$bungas = ((($a/12)+0.025) *10)/100;
                                    $bunga = 4.45/100;
                                     //$bunga = $pra->package->effective_rate / 100;
                                }
                                else {
                                //$a= $pra->package->effective_rate;
                                //$bungas = ((($a/12)+0.025) *10)/100;
                                    $bunga = 7.45/100;
                                     //$bunga = $pra->package->effective_rate / 100;
                                }
                          
                            $pinjaman = 0;


                            for ($i = 0; $i <= $loan->max_byammount; $i++) {
                                $bungapinjaman = $i  * $bunga * $durasi ;
                                $totalpinjaman = $i + $bungapinjaman ;
                                $durasitahun = $durasi * 12;
                                $ansuran2 = intval($totalpinjaman / ($durasi * 12));
                                  //echo $ansuran2."<br>";
                                    if ($ansuran2 < $ndi)
                                        {
                                            $pinjaman = $i;
                                        }
                                }   

                            if($pinjaman > 1) {
                                $bulat = pembulatan($pinjaman);
                                $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                                $loanz = $bulat;
                            }
                            else {
                                $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
                                $loanz = $loan->max_byammount;
                            }
                        }
                        else { 
                            $bulat = pembulatan($loan->max_bysalary * $total_salary);
                            $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                            $loanz = $bulat;
                                if ($loanz > 199000) {
                                    $loanz  = 250000;
                                    $loanx =  number_format($loanz, 0 , ',' , ',' ) ; 
                                }
                        }

                     ?>
                    <!--<?php echo e($pra->package->effective_rate); ?>-->
                    
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                <b><input readonly type="text" id="MaxLoan"  
                                                value=" RM <?php echo e($loanx); ?>" name="MaxLoan" placeholder="Max Loan Eligibility (RM)"
                                                 class="merah" requierd> </b>
                                                    <input readonly type="hidden" id="maxloanz"  
                                                value="<?php echo e($loanz); ?>" name="maxloanz" placeholder="Max Loan Eligibility (RM)" requierd> 
                                                <b class="tooltip tooltip-bottom-right">Max Loan Eligibility (RM)</b>
                                            </label>


                                        </section>
                                        
                                       
                                    </div>
                                    <div class="row">
                                    <section class="col col-6">
                                            <label class="label"><b>  Jumlah Pembiayaan (RM) </b> </label>
                                            <label class="input state-<?php if( $pra->loanamount <= $loanz ) { print "success"; } else { print "error"; }?>">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="tel" name="LoanAmount2"  id="LoanAmount2" onkeypress="return isNumberKey(event)" value="<?php echo e($pra->loanamount); ?>"  placeholder="RM " onkeyup="this.value = minmax(this.value, 0, <?php echo e($loanz); ?>)">
                                                <b class="tooltip tooltip-bottom-right">Jumlah Pembiayaan</b>
                                            </label>


                                        </section>
                                        
                                         <section class="col col-6">
                                            <label class="label"> <b>  Jumlah Pendapatan  </b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" id="pendapatan" value="RM <?php echo e(number_format($total_salary, 0 , ',' , ',' )); ?>" name="pendapatan" placeholder="Loan Amount" disabled="disabled">
                                                <b class="tooltip tooltip-bottom-right">Loan Amount</b>
                                                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            </label>
                                        </section>
                                         <section class="col col-6">
                                            <label class="label"> <b>  Ansuran Maksima </b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" id="ansuran_maksima" value="RM <?php echo e(number_format($ndi, 0 , ',' , ',' )); ?>   / month" name="ansuran_maksima" placeholder="Ansuran Maksima" readonly>
                                                <b class="tooltip tooltip-bottom-right">Loan Amount</b>
                                            </label>
                                            
                                        </section>
                                         <section class="col col-6">
                                            <label class="label"> &nbsp; </label>
                                             <footer>
                                             <button class="btn btn-primary " type="submit">
                                    <b> Kira Semula </b>
                                </button>
                                            
                                        </footer>

                                        </section>


                                        
                                        </div>
                                       
                                </fieldset>
                                 
                              </form> 
                                <fieldset>
 <?php if($loanz >= $pra->loanamount): ?>
                                    <table class="table table-bordered table-hover" border='1'>
                                        <thead>
                                            <tr>
                                                <th valign="middle"> <b>  Tempoh </b> </th>
                                                <th class="hidden-xs"> <b>  Jumlah Pembiayaan  </b> </th>
                                                <th><b>  Ansuran Bulanan </b> </th>
                                                
                                                   <th> <b>  Pilih </b> </th>
                                                

                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                  <?php
    $total_loan = $pra->loanamount;
    $loans = $total_loan;
    $interest =$pra->package->effective_rate;
    $interest1 =$pra->package->effective_rate/12*10;
    $interest11 =  number_format($interest1, 2 ) ;
    $interest3 =(($pra->package->effective_rate/12)+0.025)*10;
     $interest31 =  number_format($interest3, 2 ) ; 
    $year2 = 2;
    $year3 = 3;
    $year4 = 4;
    $year5 = 5;
    $year6 = 6;
    $year7 = 7;
    $year8 = 8;
    $year9 = 9;
    $year10 =10;

   function pmt2($interest, $year2, $loans) {
       $months = 12*$year2;
       $interests = $interest / 1200;
       $amount2 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
       return number_format($amount2,0,",","");
    }

    function pmt3($interest, $year3, $loans) {
       $months = 12*$year3;
       $interests = $interest / 1200;
       $amount3 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
       return number_format($amount3,0,",","");
    }
    function pmt4($interest3, $year4, $loans) {
       $months = 12*$year4;
       $interests = $interest3 / 1000;
       $amount4 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
       return number_format($amount4,0,",","");
    }
    function pmt5($interest3, $year5, $loans) {
       $months = 12*$year5;
       $interests = $interest3 / 1000;
       $amount5 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
       return number_format($amount5,0,",","");
    }
     function pmt6($interest3, $year6, $loans) {
       $months = 12*$year6;
       $interests = $interest3 / 1000;
       $amount6 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
       return number_format($amount6,0,",","");
    }
     function pmt7($interest3, $year7, $loans) {
       $months = 12*$year7;
       $interests = $interest3 / 1000;
       $amount7 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
       return number_format($amount7,0,",","");
    }
     function pmt8($interest3, $year8, $loans) {
       $months = 12*$year8;
       $interests = $interest3 / 1000;
       $amount8 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
       return number_format($amount8,0,",","");
    }
     function pmt9($interest3, $year9, $loans) {
       $months = 12*$year9;
       $interests = $interest3 / 1000;
       $amount9 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
       return number_format($amount9,0,",","");
    }
     function pmt10($interest3, $year10, $loans) {
       $months = 12*$year10;
       $interests = $interest3 / 1000;
       $amount10 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));

       return number_format($amount10,0,",","");
    }

    $amount2 = pmt2($interest, $year2, $loans);
    $amount3 = pmt3($interest, $year3, $loans);
    $amount4 = pmt4($interest3, $year4, $loans);
    $amount5 = pmt5($interest3, $year5, $loans);
    $amount6 = pmt6($interest3, $year6, $loans);
    $amount7 = pmt7($interest3, $year7, $loans);
    $amount8 = pmt8($interest3, $year8, $loans);
    $amount9 = pmt9($interest3, $year9, $loans);
    $amount10 = pmt10($interest3, $year10, $loans);
    //echo "Your payment will be &pound;" . round($amount2,0) . " a month, for " . $months . " months";

?>
     
         <?php
        if(($amount2 <= $ndi)){
            $y = '2';
        }
        elseif(($amount3 <= $ndi)){
             $y = '3';
        }
        elseif(($amount4 <= $ndi)){
             $y = '4';
         }
        elseif(($amount5 <= $ndi)){
             $y = '5';
         }
        elseif(($amount6 <= $ndi)){
             $y = '6';
         }
        elseif(($amount7 <= $ndi)){
             $y = '7';
         }
        elseif(($amount8 <= $ndi)){
             $y = '8';
         }
        elseif(($amount9 <= $ndi)){
             $y = '9';
         }
        elseif(($amount10 <= $ndi)){
             $y = '10';
         }
      
?>
         <?php for ($x = $y; $x <= 10; $x++) {  ?>
            <tr>
                <td> <?php if(($x==2) AND ($amount2 <= $ndi)): ?>
                        2 Tahun
                    <?php elseif(($x==3) AND ($amount3 <= $ndi)): ?>
                        3 Tahun
                    <?php elseif(($x==4) AND ($amount4 <= $ndi)): ?>
                        4 Tahun
                    <?php elseif(($x==5) AND ($amount5 <= $ndi)): ?>
                        5 Tahun
                    <?php elseif(($x==6) AND ($amount6 <= $ndi)): ?>
                        6 Tahun
                    <?php elseif(($x==7) AND ($amount7 <= $ndi)): ?>
                        7 Tahun
                    <?php elseif(($x==8) AND ($amount8 <= $ndi)): ?>
                        8 Tahun
                    <?php elseif(($x==9) AND ($amount9 <= $ndi)): ?>
                        9 Tahun
                    <?php elseif(($x==10) AND ($amount10 <= $ndi)): ?>
                        10 Tahun
                    <?php endif; ?></td>
                    <td class="hidden-xs"> RM <?php echo e(number_format( $pra->loanamount, 0 , ',' , ',' )); ?>  </td>
                <td>
                    <?php if(($x==2) AND ($amount2 <= $ndi)): ?>
                        RM <?php echo e($amount2); ?>

                    <?php elseif(($x==3) AND ($amount3 <= $ndi)): ?>
                        RM <?php echo e($amount3); ?>

                    <?php elseif(($x==4) AND ($amount4 <= $ndi)): ?>
                        RM <?php echo e($amount4); ?>

                    <?php elseif(($x==5) AND ($amount5 <= $ndi)): ?>
                        RM <?php echo e($amount5); ?>

                    <?php elseif(($x==6) AND ($amount6 <= $ndi)): ?>
                        RM <?php echo e($amount6); ?>

                    <?php elseif(($x==7) AND ($amount7 <= $ndi)): ?>
                        RM <?php echo e($amount7); ?>

                    <?php elseif(($x==8) AND ($amount8 <= $ndi)): ?>
                        RM <?php echo e($amount8); ?>

                    <?php elseif(($x==9) AND ($amount9 <= $ndi)): ?>
                        RM <?php echo e($amount9); ?>

                    <?php elseif(($x==10) AND ($amount10 <= $ndi)): ?>
                        RM <?php echo e($amount10); ?>

                    <?php endif; ?>
                </td>
                <td align="center">  <input type="radio" name="tenure" id="tenure" class="tenure" value="<?php echo e($x); ?>" required>
                <input type="hidden" name="rates" id="rates" class="rates" value="<?php echo e($interest31); ?>">
                <input type="hidden" name="rate" id="rate" class="rate" value="<?php echo e($interest11); ?>">
                <input type="hidden" name="amount2" id="amount2" class="amount2" value="<?php echo e($amount2); ?>">
                <input type="hidden" name="amount3" id="amount3" class="amount3" value="<?php echo e($amount3); ?>">
                <input type="hidden" name="amount4" id="amount4" class="amount4" value="<?php echo e($amount4); ?>">
                <input type="hidden" name="amount5" id="amount5" class="amount5" value="<?php echo e($amount5); ?>">
                <input type="hidden" name="amount6" id="amount6" class="amount6" value="<?php echo e($amount6); ?>">
                <input type="hidden" name="amount7" id="amount7" class="amount7" value="<?php echo e($amount7); ?>">
                <input type="hidden" name="amount8" id="amount8" class="amount8" value="<?php echo e($amount8); ?>">
                <input type="hidden" name="amount9" id="amount9" class="amount9" value="<?php echo e($amount9); ?>">
                <input type="hidden" name="amount10" id="amount10" class="amount10" value="<?php echo e($amount10); ?>">
                 <input type="hidden" name="interests" id="interests" class="interests" value="<?php echo e($pra->package->effective_rate); ?>">

                </td>
            </tr>
        <?php } ?>
      
                                        </tbody>
                                    </table>


                                </fieldset>
                                <i style="font-size: 12px; margin-left: 12px">  <sup>*</sup><?php echo e($pra->package->effective_rate); ?> % setahun (Kadar Efektif) /  <?php echo e($pra->package->flat_rate); ?> % setahun (Kadar Tetap untuk 3 tahun)</i><br>
                                <i style="font-size: 12px; margin-left: 12px">  <sup>*</sup>Kadar Asas (KA) MBSB Bank semasa ialah 3.90% dan kadar keuntungan siling ialah 15%.</i>

                                <fieldset>
                                    

                                <footer>
                                     
                                  <button type="submit" class="btn btn-primary btn-lg" id="submit_tenure">
                                    <b>  Teruskan </b>
                                  </button>
                                   <?php else: ?>
Mohon maaf jumlah pembiayaan yang Tuan/Puan ajukan melebihi had Kelayakan pembiayaan maksima. Sila kira semula dengan jumlah pembiayaan yang baharu.
<?php endif; ?>
                                  
                                </footer>

                    <div>
                    </form>
                                      

                        </div>
                        
                    
                    </div>
 <div class=" col-md-3 "></div>
    </div>

        

  
<!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Pendaftaran</h4>
                    </div>
                    <div class="modal-body">
        <?php echo Form::open(['url' => 'user','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]); ?>

                      <fieldset>

                      
                                      <section >
                                          <label class="label"><br>Emel</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-envelope"></i>
                                                <input type="Email" id="Email" name="Email" value="<?php echo e($pra->email); ?>" placeholder="Email" readonly>
                                                <b class="tooltip tooltip-bottom-right">Emel</b>
                                            </label>
                                        </section>
                                          <input type="hidden" id="FullName2" name="FullName2"  value="<?php echo e($pra->fullname); ?>" placeholder="Full Name" readonly >
                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>     
                                        <section >
                                            <label class="label">Kata Laluan</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-key "></i>
                                                <input type="Password" id="password" name="password" placeholder="Password" >
                                                <b class="tooltip tooltip-bottom-right">
Must be between 8 – 36 characters long - combination of numbers (0 to 9) and letters  (a to z) with at least one capital letter (A to Z)</b>
                                            </label>

                                        </section>
                                        <section >
                                            <label class="label">Pengesahan Kata Laluan</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-key "></i>
                                                <input type="Password" id="password_confirmation" name="password_confirmation" placeholder="Password Confirmation" >
                                                <b class="tooltip tooltip-bottom-right">Pengesahan Kata Laluan</b>
                                            </label>
                                        </section>
                               
                                     <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                      
                                     
                                </fieldset>
                       
                       
        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Batal
                        </button>
                        <button type="submit" name="submit" class="btn btn-primary">
                                       Daftar
                                    </button>
                                   
                           <?php echo Form::close(); ?>   
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
    
        

<!-- ==========================CONTENT ENDS HERE ========================== -->


<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email2 : {
                    required : true,
                    email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                Password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                ICNumber : {
                    required : true,
                    minlength : 12,
                    maxlength : 13
                },
                PasswordConfirmation : {
                    required : true,
                    minlength : 3,
                    maxlength : 20,
                    equalTo : '#Password'
                }
            },

            // Messages for form validation
             messages : {

                    Email2 : {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
                },
                BasicSalary : {
                    required : 'Please enter your email address'
                },
                Password : {
                    required : 'Please enter your password'
                },
                PasswordConfirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>


<script type="text/javascript">

$( ".tenure" ).change(function() {
    var tenure = $('input[name=tenure]:checked').val();
    var id_praapplication = $('#id_praapplication').val();
     var _token = $('#token').val();
       var maxloanz = $('#maxloanz').val();
        var package = $('#Package2').val();
       var maxloanz = $('#maxloanz').val();
        var interests = $('#interests').val();
       var loanamount = $('#LoanAmount2').val();
    var package = $('#Package2').val();
    if(tenure==2){
         var installment = $('#amount2').val();
        var rate = $('#rate').val();
    } 
    else if(tenure==3){
        var installment = $('#amount3').val();
        var rate = $('#rate').val();
    }
    else if(tenure==4){
        var installment = $('#amount4').val();
        var rate = $('#rates').val();
    }
    else if(tenure==5){
        var installment = $('#amount5').val();
        var rate = $('#rates').val();
    }
    else if(tenure==6){
        var installment = $('#amount6').val();
        var rate = $('#rates').val();
    }
    else if(tenure==7){
        var installment = $('#amount7').val();
        var rate = $('#rates').val();
    }
    else if(tenure==8){
        var installment = $('#amount8').val();
        var rate = $('#rates').val();
    }
    else if(tenure==9){
        var installment = $('#amount9').val();
        var rate = $('#rates').val();
    }
    else if(tenure==10){
        var installment = $('#amount10').val();
        var rate = $('#rates').val();
    }

  $.ajax({
                type: "PUT",
                url: '<?php echo e(url('/form/')); ?>'+'/99',
                data: { id_praapplication: id_praapplication, tenure: tenure, _token : _token, maxloan : maxloanz, package : package, maxloan : maxloanz, package : package, rate : rate, installment : installment, loanamount:loanamount, interests:interests
                   
                },
                success: function (data, status) {

                

                }
            });

   
});
</script>


<script>
$(document).ready(function(){
    $("#submit_tenure").click(function(){
    var tenure = $('input[name=tenure]:checked').val();
    var id_praapplication = $('#id_praapplication').val();
    var _token = $('#token').val();
   
     
        if (tenure>0) { 
        // $('#myModal').modal('show');
           $.ajax({
                    type: "PUT",
                    url: '<?php echo e(url('/moform/')); ?>'+'/stage',
                    data: { id_praapplication: id_praapplication,  _token : _token},
                    success: function (data, status) {

                    

                    }
            });

       location.href = '<?php echo e(url('/')); ?>/admin/step1/'+id_praapplication+'/verify/';
       //location.href = '<?php echo e(url('/')); ?>/admin';
    }
    else{
      bootbox.alert('Sila Pilih Tempoh Pembiayaan !');
      
    }
    });
});

</script>
<script>
var apiGeolocationSuccess = function(position) {
  showLocation(position);
};
var tryAPIGeolocation = function() {
  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyATzsafy5WHjV4871j-qZmJYfZXXlrxSv0", function(success) {
    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
  })
  .fail(function(err) {
    // alert("API Geolocation error! \n\n"+err);
    window.location.href = "/geolocation/error/"+err;
  });
};

var browserGeolocationSuccess = function(position) {
  showLocation(position);
};

var browserGeolocationFail = function(error) {
  switch (error.code) {
    case error.TIMEOUT:
      alert("Browser geolocation error !\n\nTimeout.");
      break;
    case error.PERMISSION_DENIED:
      if(error.message.indexOf("Only secure origins are allowed") == 0) {
        tryAPIGeolocation();
      }
         window.location.href = "<?php echo e(url('/')); ?>/geolocation/error/"+error.code;
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Browser geolocation error !\n\nPosition unavailable.");
      break;
  }
};

var tryGeolocation = function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      browserGeolocationSuccess,
      browserGeolocationFail,
      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
  }
};

tryGeolocation();

function showLocation(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
   var location = $('#location').val();
  var _token = $('#_token').val();
  $.ajax({
    type:'POST',
    url: "<?php echo e(url('/')); ?>/getLocation",
    data: { latitude: latitude, _token : _token, longitude : longitude,location:location },
    success:function(data){
            if(data){
              $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");
              $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");
               $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");
                 

            }else{
                $("#location").html('Not Available');
            }
    }
  });
}
</script> 
<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
        


<script type="text/javascript">
function minmax(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 0; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}
</script>


<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>


<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
    


<!--Add the following script at the bottom of the web page (before </body></html>)-->
