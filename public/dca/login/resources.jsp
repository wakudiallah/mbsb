






	$.messages = {
		confirmCancel: 'Are you sure you want to cancel this record?',
		confirmDelete: 'Are you sure you want to delete this record?',
		confirmDeleteAll: 'Are you sure you want to delete all the selected records?',
		confirmApprove: 'Are you sure you want to approve this record?',
		confirmApproveAll: 'Are you sure you want to approve all the selected records?',
		confirmReject: 'Are you sure you want to reject this record?',
		confirmRejectAll: 'Are you sure you want to reject all the selected records?',
		confirmReturn: 'Are you sure you want to return this record?',
		confirmReturnAll: 'Are you sure you want to return all the selected records?',
		regexError: 'Invalid value.',
		ajaxError: 'Connection Error. Please try again later. If error persisted, please contact System Administrator.',
		minSelectedError: 'Please select at least one record.',
		password: 'Password length must be between {0} to {1} and with at least 1 upper case, 1 lower case and 1 numeric character.',
		notEqualTo: 'Please enter different value.',
		minRequired: 'At least one field must be filled in.',
		date2: 'Please enter a valid date.',
		time: 'Please enter a valid time.',
		logoutMessage: 'You have been logged out from MBSB Bank (Inquiry).',
		alphaonly: '"{0}" is invalid. Only letters[a-z][A-Z] are allowed.',
		alpha: '"{0}" is invalid. Only letters[a-z][A-Z] and numbers[0-9] are allowed.',
		numeric: '"{0}" must be a valid number.',
		alphaornumeric: 'Input is invalid. Only alphanumeric are allowed.',
		alphaornumericnospace: 'Input is invalid. Only alphanumeric without space are allowed.'
	}
	$.countdown = {
		
		
		timeoutURL: "/corporate/common/timeout/timeout.html",
		logoutURL: "/corporate/j_security_logout"
	}
