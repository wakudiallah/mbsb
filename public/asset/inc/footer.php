

 <div class="page-footer">
        <div class="row">
            <div class="col-xs-4 col-sm-4 text-left ">
                <div class="txt-color-black inline-block">
                    <span id="logo"> <a href="<?php print url('/') ?>"> <img src="<?php echo ASSETS_URL; ?>/img/islam.png" alt="SmartAdmin"  style="margin-top: -17px !important; width: 150px"> </a></span>
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 text-right " style="font-size: 11px !important">
                <div class="txt-color-black inline-block">
                    <span class="txt-color-black">Copyright © <?php echo date('Y'); ?> MBSB Bank Berhad</span><br>
                    <span class="txt-color-black">Registration No: 200501033981 (716122-P). All Rights Reserved.</span>
                </div>
            </div>
        </div>
    </div>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163298716-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-163298716-1');
</script>
