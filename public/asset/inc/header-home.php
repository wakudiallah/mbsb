	
 <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<script>
var apiGeolocationSuccess = function(position) {
  showLocation(position);
};

var tryAPIGeolocation = function() {
  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDCa1LUe1vOczX1hO_iGYgyo8p_jYuGOPU", function(success) {
    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
  })
  .fail(function(err) {
    
  });
};

var browserGeolocationSuccess = function(position) {
  showLocation(position);
};

var browserGeolocationFail = function(error) {
  switch (error.code) {
    case error.TIMEOUT:
      alert("Browser geolocation error !\n\nTimeout.");
      break;
    case error.PERMISSION_DENIED:
      if(error.message.indexOf("Only secure origins are allowed") == 0) {
        tryAPIGeolocation();
      }
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Browser geolocation error !\n\nPosition unavailable.");
      break;
  }
};

var tryGeolocation = function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      browserGeolocationSuccess,
      browserGeolocationFail,
      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
  }
};

tryGeolocation();

function showLocation(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
  var _token = $('#_token').val();
  $.ajax({
    type:'POST',
    url: "{{url('/')}}/getLocation",
    data: { latitude: latitude, _token : _token, longitude : longitude },
    success:function(data){
            if(data){
              $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");
              $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");
               $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");
                 

            }else{
                $("#location").html('Not Available');
            }
    }
  });
}
</script>

		<header id="header">
			<!--<span id="logo"></span>-->

			<div id="logo-group">

				<span id="logo"> <a href="<?php print url('/') ?>"> <img src="<?php echo ASSETS_URL; ?>/img/logo_bank.png" alt="SmartAdmin" width="79px" > </a></span>

				<!-- END AJAX-DROPDOWN -->
			</div>

	<?php if (!(Auth::user())) {?>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/contact" class="btn hidden-xs btn-primary">Contact</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/mbsb_login" class="btn hidden-xs btn-primary">MBSB</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/applynow" class="btn hidden-xs btn-primary">Apply Now</a> </span>
		<!-- <span id="extr-page-header-space"> <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;" class="btn hidden-xs btn-primary"><i class="icon-append fa fa-lock"></i> Login</button></span>-->
		<span id="extr-page-header-space"> <a class="btn hidden-xs btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="icon-append fa fa-lock"></i>  Login</a> </span>
		
		
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>" class="btn hidden-xs btn-primary">Home</a> </span>
 	<?php } else if ((Auth::user()->role=='4')) {?>
 		<span id="extr-page-header-space"> <a href="<?php print url('/'); ?>/mbsb_logout" class="btn hidden-xs btn-primary" onclick='return confirm("Are you sure to log out?")'>Sign Out</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/contact" class="btn hidden-xs btn-primary">Contact</a> </span>
		<!--<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/applynow" class="btn hidden-xs btn-primary">Apply Now</a> </span>-->
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/admin" class="btn hidden-xs btn-primary">Admin Area</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>" class="btn hidden-xs btn-primary">Home</a> </span>
	<?php } else if ((Auth::user()->role=='5')) {?>
		<span id="extr-page-header-space"> <a href="<?php print url('/'); ?>/mbsb_logout" class="btn hidden-xs btn-primary" onclick='return confirm("Are you sure to log out?")'>Sign Out</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/contact" class="btn hidden-xs btn-primary">Contact</a> </span>
		<!--<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/applynow" class="btn hidden-xs btn-primary">Apply Now</a> </span>-->
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/admin" class="btn hidden-xs btn-primary">Admin Area</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>" class="btn hidden-xs btn-primary">Home</a> </span>
	<?php } else if ((Auth::user()->role=='10')) {?>
		<span id="extr-page-header-space"> <a href="<?php print url('/'); ?>/mbsb_logout" class="btn hidden-xs btn-primary" onclick='return confirm("Are you sure to log out?")'>Sign Out</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/contact" class="btn hidden-xs btn-primary">Contact</a> </span>
		<!--<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/applynow" class="btn hidden-xs btn-primary">Apply Now</a> </span>-->
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/admin" class="btn hidden-xs btn-primary">Admin Area</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>" class="btn hidden-xs btn-primary">Home</a> </span>
	<?php } else if ((Auth::user()->role=='0')) {?>
		<span id="extr-page-header-space"> <a href="<?php print url('/'); ?>/mbsb_logout" class="btn hidden-xs btn-primary" onclick='return confirm("Are you sure to log out?")'>Sign Out</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/contact" class="btn hidden-xs btn-primary">Contact</a> </span>
		<!--<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/applynow" class="btn hidden-xs btn-primary">Apply Now</a> </span>-->
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/home" class="btn hidden-xs btn-primary">My Application</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>" class="btn hidden-xs btn-primary">Home</a> </span>
	<?php } else if ((Auth::user()->role=='7')) {?>
		<span id="extr-page-header-space"> <a href="<?php print url('/'); ?>/mbsb_logout" class="btn hidden-xs btn-primary" onclick='return confirm("Are you sure to log out?")'>Sign Out</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/contact" class="btn hidden-xs btn-primary">Contact</a> </span>
		<!--<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/applynow" class="btn hidden-xs btn-primary">Apply Now</a> </span>-->
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/home" class="btn hidden-xs btn-primary">My Application</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>" class="btn hidden-xs btn-primary">Home</a> </span>
	<?php } else if ((Auth::user()->role=='8')) {?>
		<span id="extr-page-header-space"> <a href="<?php print url('/'); ?>/mbsb_logout" class="btn hidden-xs btn-primary" onclick='return confirm("Are you sure to log out?")'>Sign Out</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/contact" class="btn hidden-xs btn-primary">Contact</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/admin" class="btn hidden-xs btn-primary">Admin</a> </span>
		<span id="extr-page-header-space"> <a href="<?php print url('/') ?>" class="btn hidden-xs btn-primary">Home</a> </span>
 	<?php } ?>
	

	
<?php 	if (empty($user))  { ?>
			
			<!--<span id="extr-page-header-space"> <a href="<?php print url('/') ?>/faq" class="btn hidden-xs btn-primary">FAQ</a> </span>-->
			
		
			<!-- Single button -->
			<span id="extr-page-header-space"> 
				<div class="dropdown visible-xs center" style="margin-right:67px!important;margin-top: 9px !important; ">
				  <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
					 &nbsp; &nbsp;Menu &nbsp; &nbsp; &nbsp;<span class="caret"></span>
				  </a>
				  <ul class="dropdown-menu">
				  	<?php if (!(Auth::user())) {?>
						<li><a href="<?php print url('/') ?>">Home</a></li>
						<li><a href="<?php print url('/applynow') ?>">Apply Now</a></li>
						<li><a data-toggle="modal" data-target=".bd-example-modal-lg">Login</a></li>
						<li><a href="<?php print url('/mbsb_login') ?>">MBSB </a></li>
						<!--<li><a href="<?php print url('/') ?>/faq">FAQ</a></li>-->
						<li><a href="<?php print url('/') ?>/contact">Contact</a></li>
 					<?php } else if ((Auth::user()->role=='4')) {?>
 						<li><a href="<?php print url('/') ?>">Home</a></li>
						<li><a href="<?php print url('/') ?>/admin">Admin Area</a></li>
						<!--<li><a href="<?php print url('/') ?>/faq">FAQ</a></li>-->
						<li><a href="<?php print url('/') ?>/contact">Contact</a></li>
					<?php } else if ((Auth::user()->role=='5')) {?>
 						<li><a href="<?php print url('/') ?>">Home</a></li>
						<li><a href="<?php print url('/') ?>/admin">Admin Area</a></li>
						<!--<li><a href="<?php print url('/') ?>/faq">FAQ</a></li>-->
						<li><a href="<?php print url('/') ?>/contact">Contact</a></li>
					<?php } else if ((Auth::user()->role=='10')) {?>
 						<li><a href="<?php print url('/') ?>">Home</a></li>
						<li><a href="<?php print url('/') ?>/admin">Admin Area</a></li>
						<!--<li><a href="<?php print url('/') ?>/faq">FAQ</a></li>-->
						<li><a href="<?php print url('/') ?>/contact">Contact</a></li>
					<?php } else if ((Auth::user()->role=='0')) {?>
 						<li><a href="<?php print url('/') ?>">Home</a></li>
						<li><a href="<?php print url('/') ?>/home">My Application</a></li>
						<!--<li><a href="<?php print url('/') ?>/faq">FAQ</a></li>-->
						<li><a href="<?php print url('/') ?>/contact">Contact</a></li>
					<?php } else if ((Auth::user()->role=='7')) {?>
 						<li><a href="<?php print url('/') ?>">Home</a></li>
						<li><a href="<?php print url('/applynow') ?>">Apply Now</a></li>
						<li><a href="<?php print url('/') ?>/home">My Application</a></li>
						<!--<li><a href="<?php print url('/') ?>/faq">FAQ</a></li>-->
						<li><a href="<?php print url('/') ?>/contact">Contact</a></li>
					<?php } else if ((Auth::user()->role=='8')) {?>
 						<li><a href="<?php print url('/') ?>">Home</a></li>
						<li><a href="<?php print url('/') ?>/home">Admin</a></li>
						<!--<li><a href="<?php print url('/') ?>/faq">FAQ</a></li>-->
						<li><a href="<?php print url('/') ?>/contact">Contact</a></li>
					<?php } ?>
				
				</ul>
				</div>
			</span>
			<span id="extr-page-header-space"> &nbsp;</span><span id="extr-page-header-space"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        <?php 
		} 
		 ?>


		</header>



        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163298716-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-163298716-1');
</script>

